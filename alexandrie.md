---
output:
  word_document: default
  html_document: default
---

Il faut organiser la littérature en fonction de plusieurs topics
IL faut un nom de code par article
il faut ... 

### Keywords

-   Aubert effect : tilt de la V quand on penche son corps d’un côté, notamment si tilt très fort > 60°. On perçoit la V dans le sens de l'inclinaison.
-   Muller-effect : si écart plus léger <60° on perçoit la V comme étant orientée à l’opposé de la déviation (compensation).
-   Mesure de la V dans roll (= frontal = roulis) et pitch (= sagittal = tangage) : PV, VV, HV
-   BLS = Burke Lateropulsion Scale
-   SCP = Scale for Contraversive Pushing
-   illusion wundt jastrow = forme
-   **Modèle interne de gravité/verticalité** : processus général qui intègre/synthétise signaux B-U (afferents + efférents) + T-D (cs du corps, IM) pour créer representation de la gravité afin d'interagir avec l'environnement sur le plan moteur (e.g., locomotion).
-   **Cortex vestibulaire**  = réseau cortical comprenant toutes les aires du cortex recevant des inputs vestibulaires. Ce réseau serait impliqué dans la cognition spatiale (navigation, conscience de soi, perception d’auto-mouvement). (Lopez, Blanke, & Mast, 2012)
-   **PIVC =** *core* vestibular cortex. Zone du cortex qui répond le plus aux inputs vestibulaires.
-   baking tray test => disposer cube le plus uniformément possible sur une grille
-   **galvanic stimulation** with anodal/cathodal currents delivered on the right and left mastoid. ANODAL = (-) ; CATHODAL = (+), effet inhibiteur vs excitateur.
-   **subjective sagittal axis** = représentation d’un plan virtuel superposé au plan sagittal du corps, qui sépare le corps et l’espace en 2 secteurs gauche/droite.
-   **subjective straight ahead** = Point donné dans la dimension horizontale qui définit la direction de l'axe subjectif sagittal. La SSA est maintenant moins utilisée que la SBA, car cette dernière peut être évaluée aussi dans la dimension verticale (roll plane).
-   méthode des limites = range of uncertainty = min – max où on l’on perçoit la verticale visuelle
-   diff threshold = moitié de la range
-   constant error = bias (pse – 0° ou - 90°)
-   **Reference frames/Cadre de référence** = définit le systême de coordonnées dans lequel on effectue des mesures.
- path integration : distance parcourue dans ev estimée grâce à l'intégration de signaux self motion.
  




# Lien RBD - MIV

Plusieurs études montrent que la représentation de la verticalité est déficitaire après un AVC hémisphérique droit. On retrouve une forte corrélation avec la nsu. 


### Kaski et al. (2016) = lésion TPJ liée à perte du heading et de la perception du temps

**Contexte théorique =** head velocity signal est envoyé par csc au TC. Ce signal donne angular motion de la tête. Ce signal serait peut être transformé au niveau cortical pour permettre *path integration*, intégration signal velocity au cours du temps. cela permettrait au systeme vb d'indiquer au sujet, dans condition darkness par ex, sa position. Ici on teste cette proposition de façon indirecte chez patient avec lésion droite pour observer si le *where am I ?* est perturbé. 

**Méthode =**
- 18 rbd,pas de lbd (dysphasie) + 14c + 2 patient avec perte vb periphérique bilatérale.
- j+3 à +12
- en tout 72 essais, mais des pauses.
- pas de trouble périphériques vb chez patient cortex.
- diag spatial neglect : albert, lb, drawing
- mri : 2 approches : lesion soustraction + vlsm. Pour lesion soustraction, on prenait patient inside vs outside 95CI des contrôles dans tâches de where am I (ici vd *position bias*)
- 3 tâches : 
- 1) sujet placé sur chaise tournante yaw plane. pas de bruit, dans le noir. dans 1ère tâche, on demande au sujet d'indiquer sa position par rapport à aiguille. position de départ à 12h. rotation entre 30-360° random. feedback visuel de la réponse, puis retour position départ. *On mesure symmetry de la position estimée quand rotation vers droite vs gauche, afin d'extraire un biais de position*. 
- 2) On teste le *Am I moving ?* = on demande au sujet d'appuyer sur bouton dès que sensation de mvt. on calcule ici un *velocity bias* de la même façon que le position bias
- 3) On teste duration of moving, avec duration asymétrique entre left et right (1s vs 4s chez patient). on calcule *temporal bias*. 
- EOG
- :-1:= pas de taille lésion reportée, handedness demandée à l'oral, pas de mri sur temporal bias, pas de report prévalence nsu. 
- ![](https://i.imgur.com/0PuYwva.png)

**Resultats**=
- *position bias*: 4/18 patient au dela des 95CI contrôles. 
- mri soustraction : lésion tpj chez les 4 patients pas présente chez les 14 autres patients. 
- vlsm = angular gyrus + stg. (tpj).
- *velocity bias* : 8 patients avec insula vs 10 no lesion insula = ns. pas d'effet du groupe ici. même si insula impliqué dans pcp mvt tête, ici ns.
- *temporal bias* : control sont à 0.5 (symetrical perf). le groupe rbd était à 0.76, asym qui révèle mauvause path integration pour rotation vers la gauche. 3/4 sujets avec position bias étaient les + altérés ici.
- *nsu* : dissociation claire. 2/4 patients avec position bias et donc lésion tpj avaient désorientation topographique mais de nsu, même R. 

**Discussion =** ici résultat entre lien position bias et temporal bias avec lésion tpj peut être expliqué par lien dissocié (tpj impliqué dans R temporelle) ou associé (sum du signal velocity dans le temps par tpj = *cortical integrator*). Si lésion cortical integrator, alors désorientation spatiale égocentrée.  Mais ce trouble de navigation spatiale est dissocié de la nsu, même R. Rés intéressant.   
nb = rés nul chez lésion insula pour tâche motion bias était inattendu. auteurs ad hoc : encodage bil, compensation HG.




### Baier et al. (2013) = lésion isolée insula postérieure etraîne perte de thermoception/nocipception contra corrélée ) biais VV contra = mss !

**Contexte théorique =** pivc/région péri-insulaire impliqué dans tt des inputs otolithiques. Ici on regarde SVV, skew deviation, ocular torsion. de façon secondaire, on regarde aussi si perte de thermoception/nociception chez ces patients.

**Méthode =**
- pop = 14rbd / 13 ldb / 
- j + 7 (très tôt) / mri a peu près j+5. 
- lésion isolée région insulaire/operculaire.
- 1 seul patient avec nsu quand rbd (bit ou cloche).
- 20 patients extra pour tester thermoception (non décrite...)
- test chaleur/froideur.
- mri = vlsm => svv en variable continue

**Résultats =**
- 4/13 lbd contra anormale (>2.5°)
- 5/14 rbd contra anormale
- pas de biais ipsi dans ces 2 groupes. pas d'OT ou de SD.
- lésion rbd associé à tilt svv = locus proche short insula gyrus, wm, putamen
- lésion lbd = locus Ig3 (insula granualaire) OP3 (fait partie de l'insula)
- ![](https://i.imgur.com/Tv2F5hq.png)
- Quand on analyse sous groupes rbd et lbd qui ont svv anormale dans la cohorte 20 patients supp, on a 1/8 lbd et 4/12 rbd svv anormale.
- cor svv et thermoception pour froid/chaud. => + j'ai un biais SVV sèvère contral, + j'ai un déficit thermoceptif du côté contralésionnel.

**Conclusion =** pleins de rés intéressants ici ! d'une part une lésion isolée de l'insula posterieure/operculum pariétal ne suffit pas biaiser systématiquement la svv, qui repose dur un réseau + large. d'autre part, pas d'association avec nsu. Aussi, on a association avec déficit thermoceptif. donc déficit vb est associé à un déficit tactile. C'est cohérent avec association entre troubles de la posture et somesthésie, et aussi avec hypothèse d'une interaction entre voie graviceptive soma et vb car on peut spéculer que infos tactiles peuvent contribuer à gravité soma. Enfin, perception de la verticalité n'est pas latéralisée quand phase acute. En revanche dans la litté on sait qu'on récupère bcq - bien de lésion droite. Tout ça est aussi cohérent avec hypothèse que insula postérieure est une aire visceroceptive primaire (Craig, 2002). 



### Rousseaux, Braem, Honoré, & Saj (2015) = corrélat VV/HV cortex occipito-temporal + WM


**Contexte théorique =** Corrélat de la VV estimé antérieurement avec technique IRMa de faible résolution. Ici on utilise vlsm. VV + HV + VH

**Méthode =**

-   pop = 46 RBD **without pushing**
-   **limite = selection RBD uniquement ! on a déjà un biais ... on a pas ce défaut chez Baier et al. 2012**
-   J+43
-   pint fort = ACA + MCA + PCA (pas que MCA, seulement dans les autres études je suis pas sûr qu’il y ait une sélection uniquement de MCA...).
-   diag clinique
-   HH
-   LB + copy + bells + motor + somatoloss + HH
-   NSU cut off = 2/3 patho
-   VV + HV + VH
-   apparatus **différent** ici puisque cible est positionnée au centre à gauche à droite.
-   18 essais * 3 modality
-   10 s pour réaliser l’ajustement
-   VD = precision + bias
-   position strappée pour torse, head rest
-   cut off verticality à 2 SD.
-   FLAIR T2 + VLSM
-   covarié = lesion size
-   non par permutation test (bien) + analyse min taille voxel = 10 % des patients avec lésion pour ce voxel (peu ?)
-   **limites = stat non par pour le comportemental (mais moins important ici).**

**Résultat =**

1.  IRMa

-   VV = pMTG + TOJ + POJ + IPG + SCWM + STG + LOC + ITG
-   HV = SCWM + STG + STS
-   VH = STG + TPJ + STS + MTG

nb = pas d’aires en communes entres VV et HV, mais overlap de HV et VV avec VH (logique car intégration des 2 modalités).

 

1.  comportemental

-   on est plus biasé et imprécis pour HV > VV > VH
-   coorélation VV * VH + HV * VH (logique) + corrélation entre NSU/HH avec VV et VH + corrélation entre lesion size et tilt
-   16/21 RBD patho sont NSU pour VV + 7/9 pour HV + 12/15 pour VH. Donc à noter que VV est + fréquemment lésée ! > HV.

 

**Discussion =** On retrouve grossomodo un gradient antérieur-postérieur au niveau temporo-parietal pour la localisation des lésions qui détermine si patient est patho ou non pour HV (anté) ou VV (posté). Ici on ne retrouve pas implication de l’Insula comme ailleurs. Peut être car méthode plus fine, ou parce que on a patient avec atteinte PCA (7/46) et pas que MCA. Cela dit on a seulement patient RBD ici.

-   **Limites = à nouveau on dichotomise avec cutoff si patient est lésé ou non pour telle tâche. mauvaise méthode ! eg pour HV on a comparaison patient patho = 8 vs 38 ! Mais c’est une limite inhérente à tous les papiers sur le sujet ...**

 


### Rousseaux, Honoré, Vuillemier, & Saj (2013) = pass lié à lésion insula, VV liée à lésion temporo-parietale, et sba lésion parietal antérieure, dorsale+ tpj. lien pass et VV qd lésion wm. 

Contexte théorique = On cherche corrélats anat’ dans la SBA vs VV misperception , et quels overlaps avec pb postural.

Méthode =

-   pop = 42 RBD (21 NSU) + 15C
-   J + 30-90
-   diag clinique = NSU cut off 2/3 dans copy + bell + CBS
-   HH + **no PS**
-   PASS scale pour évaluer la posture
-   Tâches = SBA dans plan horizontal (même si shift initial en rotation aussi)
-   SVV = 18 essais par tâches , déplacement initial -45/+45 en rotation et -15/+15 en translation
-   :-1: version visuo-haptique + rod pas au niveau de la tête...
-   apparatus = participant sur lit clinique = uniquement un strap pour la tête. jambes étendues.
-   VLSM = ici méthode un peu particulière, avec sélection des 10 patients avec le plus/moins de pb dans une tâche donnée, afin d’avoir des contrastes IRM + forts.

**Résultats =**

-   comportemental =
-   biais contra SVH et ipsi SSA (yaw) NSU > no NSU. 
-   idem quand **quand hemianopie > no hemianopie**. 
-   PASS NSU > no NSU
-   corrélation VH * SBA, SBA * PASS, mais VH * PASS ns ! (réplique Barra et al 07, 08). NB = attention à noter que ici on a aucun patient pusher ...
-   corrélation VH, PASS, SBA , avec sévérité de NSU dans les 4 tests, y compris le CBS.
-   **cor de la SSA avec lesion volume très forte**
- <font size="5"> ++*lesion overlap diff avec 10 vs 10 (best/worst) =*++</font> 
-   SVH = STG+MTG+IPL+Pu+Th
-   SBA = **SMG + MFG**+Pu+STG
-   PASS = Ip + Th
-   <font size="5">++*vlsm =*++</font> 
-   VH = **SPG**+STG+MTG
-   SBA = **SPG + SI** + IPG + STG
-   PASS = **SPG** + **Ip** + IPG
-   **VH + PASS = STG + TPJ + IPG + SLF**
-   **SBA + PASS = SI + STG**

![](https://i.imgur.com/CD5zQSM.png)
![](https://i.imgur.com/N98jVFD.png)



Discussions = implications SMG dans SBA pas étonnant, aire impliquées dans processus égocentrés. Mais aussi dans BN, même si pas lien direct ici.Aires MFG due à partie motrice.  VH ici due à lésion temporale postérieure qui overlap avec PIVC.

-   En gros, SBA repose + sur poids soma, et VH sur poids vb-visuel.
-   **lésion Ip dans PASS problème posture va dans le sens d’une interprétation vestibulaire du pushing syndrome. Mais on retrouve aussi lésion dans région intégrant infos somesthésique.Comme si pushing résultait la fois d’un déficit vestibulaire et somesthésique ! VI + VG ! conforme avec études montrant que VV touchée dans PS. et aussi que PS corrèle avec shift SBA, mais aussi avec problème PV ! Interprétation plausible car on retrouve des lésions liées à PASS qui overlap avec respectivement SBA et VH shift.**

**limites = que RBD, méthode VH qui ne neutralise pas totalement modalité vestibulaire, pas de strap (mais à relativiser car pas de gros problème de posture).**


### Brandt et al. (1994) = insula post + operculum + stg liée à biais VV contralesionnel patho

**Contexte théorique =** Ici on argumente que la VV est un moyen fiable de tester l’intégrité du cortex vestibulaire, récemment identifié chez le singe de façon précise, par traceur, par Guldin et Grusser.

**Méthode =**

-   pop = 71 RBD + LBD (répartition pas précisée)
-   lésion MCA PCA ACA
-   subacute + acute
-   contrôle = pas de problème vestibulaire périphérique
-   VV = ajustement manuel avec potentiomètre (pas VH à proprement parler). Stimuli présenté en mono/binoculaire pour contrôler HH. On regarde aussi cut-off
-   Autre contrôles = torsion oculaire + skew deviation
-   aMRI = CT scan sur 26 patients MCA uniquement. (limite = biais ici !)

**Résultat =**

-   VV = 23/52 MCA biais patho dont 21 contralésionnel, associée pour 18 à lésion Ip et PAR Operculum.
-   Sur ces 18, on regarde particulièrement 14/52 à cause des limites de l’époque sur la délimitation manuelle des lésions d’après un atlas ... => STG, long INS, transverse T gyrus
-   **Quand on compare groupe patho vs sain (14 vs 12) on a rôle du PIVC + STG**
-   On retrouve peu de VV patho chez ACA + PCA lésions, ce qui va dans le sens inverse de Rousseaux 2015...
-   pas d’OT ou de SDeviation

**Discussion =** cortex vestibulaire sous-tend stabilisation tête-yeux dans l’espace, ici on a montré que atteinte du cortex vestibualire au niveau cortical avait n impact sur la perception spatiale. Ce cortex n’est pas juste vb mais plutôt multisensoriel. Par nature, les stimuli qui stimulent le cortex vestibualire, cad la locomotion, la navigation spatiale, sont multisensoriels. Orientation spatiale et contrôle postural possible grâce à infos redondantes.

-   Reflexion intéressante = stim vb viendrait alors stimuler des neurones qui répondent à plusieurs modalités, et donc on aurait un effet transmodal d’une stimulation galvanique par ex. eg = aires sylvienne chez le singe répond à 30% à stim vb et le reste à stim visuels ...

**Limite ici =** technique AMRI de l’époque faible résolution, pas les moyens d’examiner finement les corrélats neuronaux ... donc on ne peut pas conclure à la seule implication du PIVC dans la misperception de la verticalité visuelle.

### Baier et al. (2012) =  lésion wm + cortex vestibulaire (ifg+stg+op2+insp).poc et prc associé à lésion droite. pas de lésion pariétale.

**Contexte théorique =** Quel lien entre lésion gauche et droite et SVV ? Quelles lésions spé peuvent prédire SVV pathologique ? au niveaux GM et WM ? On regarde ça avec patients **sub-aigu, J+5, pour limiter effet de plasticité.**

**Méthode =**

-   Pop = 22 LH / 32 RH. NB = RH + hémiparétique que LH.
-   **84% des patients RH** **diag NSU avec test cloches**. LH + NSU + contratilt SVV = 33%. Ipsitilt = 0% de NSU.
-   SVV visuo-motrice. On définit SVV patho sur la base d’un cut-off fixé à +/- 2.5degrés.
-   VD = SVV
-   VI = Lesion/pas lesion sur la base de t-test entre voxels.
-   2 analyses séparées pour les 2 groupes.

**Résultats =**

-   **Comportemental**
-   VV = RH ont 63% contra, LH ont 55% contra. **SVV patho associée surtout à RH, avec SVV patho contra > ipsi.** Diff LH-RH SVV patho pas significative.
-   Corrélation + entre NSU et SVV tilt.
-   **IRMa = SVV as continuous DV.**
-   **LH => IC > rolandic operculum > IFG lésions } prédisent * niveau de SVV tilt.** Régions sup = IOF, SLF WM
-   **RH => IFG > STG** Régions sup = Po/PrC.
-   **Pas de relation entre SVV tilt et région PAR !**
-   **Région en commun = IC.**
-   **A noter que lesion size ==.**

**Discussion =** Résultats congruents avec implication du PIVC dans VV. Interaction visuo-vb utile pour VV. Peut être que différence entre LH et RH due à asymétrie dans tt vb, plutôt latéralisé à droite, dans l’H non dominant. **Résultat va dans le sens d’un réseau peri-sylvian impliqué dans tt VV.** A noter que dans autres articles, stroke delay ++ peut favoriser compensation du réseau. Cette obs peut expliquer rés différent entre études IRMa, comme celle de **Barra 2010**, qui avait trouvé implication Th posterieur !

**Limite = 6 SVV patho avec LH, 13 avec RH. Puissance bof, surtout chez LH.**

** **


### Pérennou et al. (2008) = transmodal contra tilt chez stroke patients. Pv relié spécialement à trouble posture.

**Contexte **: Le lien entre misperception de la V et trouble de la posture pas établi de façon fine encore. On peut avoir latéropulsion/pushing par exemple dû à lésion TC, mais on aura pas forcément problème de verticale similaire à lésion hémisphérique (biais ipsi vs contra). Ici on essaie de démontrer que problème de latérop et pushing se trouvent sur un continuum de problème posturaux corrélé à problème de V.

**Méthode** :

-   Pop : 33 contrôles / **86** patients dont 6 ont lésion TC.
-   Evaluation SCP pour établir si latero + pushing « **pusher **» / latero seule (« **contralist**» vs « **ipislist** » si TC) / rien « **upright** »
-   IRMa
-   W + 12
-   lesion size = small/medium/large en fonction du nombres d’aires distinctes touchées. (pas de vlsm).
-   VV, HV, PV (strap total)
-   Limites stats : test non paramétrique … + PCA

**Résultats** :

1.  Descriptif = V * biais

-   PV : 34 contra, 1 ipsi (TC)
-   HV : 26 contra, 2 ipsi
-   VV : 44 contra, 13 ipsi (6/6 TC)
-   **18 transmodaux biais**, 41 ont ½ biais
-   PCA : VV et PV expliquent la majorité de la variance (87%)
-   Transmodal tilt en grosse majorité chez **RH stroke**

 

1.  IRMa chez H stroke

-   Extension lésion augmente avec nombre de modalités touchées (**confirme Barra 2010**)
-   No diff LH/RH * pour VV ou HV
-   **Biais PV RH>LH et augmente avec taille que pour RH**
-   Transmodal tilt implique lésions PAR **(SPL)**
-   **PV** implique spécialement **ROL et TH** (**confer Barra**)
-   De façon générale, implication capsule interne, cortex F-T …

 

1.  Lien entre pcp V et trouble de la posture

-   V plus atteinte chez **pushers > listing** (ipsilist = latéropulsion) > upright = contrôle
-   Upright = biais > contrôle, pas de diff entre modalité
-   **Listing = biais > contrôle**, idem
-   **Pusher > + PV bias > VV = HV**
-   **Corrélation +++ entre degré de latéropulsion et PV bias ! > HV > VV (toutes *)**

**Conclusion** : Résultats supportent modèle de la V davantage latéralisé à droite, avec problème de posture en lien avec une mauvaise construction de la V. IRMa supporte hyp d’un PIV cortex qui sous-tend la construction de la V. Les biais ipsi retrouvés chez H stroke sont rares, et restent un mystère. Latéropulsion chez TC pas toujours associée à pb de V (5/6 TC ont PV ok !, et **6/6 ont VV lésée**, **ce qui est compatible avec problème nystagmus quand lésion TC** voir Dieterich/Brandt).

NB = critique de Karnath : pp hémiplégique couché sur le côté : se sentent plus stable et donc plus droit. Aussi, tête et pieds non strapés, on peut donc avoir biais sensoriels dû au balancement de ces extrémités !

 


Expériences où une intervention ciblant une modalité sensorielle peut induire un effet sur une tâche impliquant une autre modalité sensorielle.

### Barra et al. (2008) = MIV dans VPL + VV liée à lésion VV. L'hémiplégie unilatérale module la VV et l'effet Aubert.


Contexte théorique = On sait que VV et SBA en rotation vers la gauche après stroke droit. ya t-il un seul mécanisme derrère ce biais patho ? on teste cette hypothèse en regardant si dissociation quand on manipule info somesthésique.

Méthode =

-   pop = 15 stroke = 9RBD + 6 LBD + 12 C
-   M + 1-4
-   diag clinique = HH + SCP + vb. seul un patient PS.
-   apparatus = drum = strap en position assise.
-   Tâches = SSA + VV dans le noir. Le sujet devait positionner un baton lumineux par rapport à son corps ou à la verticalité. 30 essais en tout = 0/-30/+30° * 10.
-   VD = biais
-   stat = bias ~ group (control, stroke) (inter sujet) * body position (-30, 0, 30) * tâche (VV, SSA) (intra-sujet)

Résultats =

-   group * body position + tache * body position = *
-   SBA = ipsi tilt = ipsi shift; contra tilt = contra shift
-   VV = ipsi tilt = ipsi shift; **contra tilt = 0°**
-   **on a corrélation entre SBA et VV quand position upright et ispi tilt, mais pas quand contratilt** SVV pas modulé par contratilt, contrairement à PV chez patients. Comme si pas d'intégration des infos du corps...pas d'effet aubert... normalement effet aubert car VI devient plus fiable que le VG, mais pas ici ?... pourtant, on a effet sur pv ! 

![](https://i.imgur.com/0utFezL.png)


Discussion = Pour VV, on peut d’effet Aubert, shift qui va dans la direction du corps, uniquement quand tilt ipsi. C’est plutot en accord avec travail de Barra (2010), avec effet Aubert quand patient hypoesthétique incliné vers la droite, mais pas quand biais vers la gauche du côté patho. Mais pourquoi alors la SBA est ok si elle repose sur infos somato ?? **hyp de Barra = SBA repose sur schéma corporel, une R « centrale », insensible au changement de position du corps, ALORS QUE VV repose sur infos somato + latéralisée.** En tout cas, on pourrait avoir un partage de ressources entre mécanismes qui sous-tend SBA et VV.=> hyp un peu bancale quand même.

-   **A noter que le patient PS avait tilt -8° dans SBA et VV. donc ne soutient pas idée d’un mismatch entre VV et SBA. de +, si PS dépendait juste d’un tilt dans le plan frontal de la SBA ou de la VV, tout le monde serait pusher !!**

** **

-   **Reflexion perso = interprétation de type schéma corporel ne colle pas forcément avec idée que NSU perso aurait + de problème de PV ? le soucis ici c’est qu’on a pas testé la PV chez ces patients, et que le lien entre PV et SBA est en fait très hypothétique... de + la SBA peut être shiftée dans des plans différents, avec parfois des dissociations ... est ce que PV corrélé à problème SBA ? pas sûr que cela ait été testé !**

** **

-   **pourquoi ici VI et VG dissocié quand shift contra ? aubert effect c’est VI qui module VG. quand ipsi tilt, modulation, mias pas quand contra tilt, peut etre car pas d’intégration mss hémicorps gauche. si PV = VI, alors la dissociation VI-VG apparente quand shift contra, comme si dans ce cas VG ne pouvait pas communiquer avec VI...**

 




### Pérennou (2006) = hyp de la nsu graviceptive, et discussion sur perte d'intégration signaux visuo-vetibulaires. Idée que perte d'intégration somato sous-tend pb postural et pb verticale comportementale. 

**Revue =** Lien entre postural disorder et NSU

**Résumé =

- **V comportementale** = R implicite de la V qui contrôle la posture contre la gravité. Celle ci est donné chez l'homme par le LBA.
- Comment mesurer LBA ? debout on a accès au centre de masse, mais c'est dur car déséquilibre. Assis c'est mieux, on accède à orientation du tronc. mais encore une fois influence des problèmes de stabilité. 
- **la V comportementale != de la V subjective/explicite évaluée par svv/shv/spv !** ces mesures ne mesurent pas la V implicite directement.
- [25,67,109,122, 2,56, 84,89, 90] 
- 60,89 => pas de cor trop forte entre problème de posture et svv. rejoint travaux qui associe mauvaise spv à trouble posture. 
- Si biais supramodal de la V implicite, on devrait avoir tilt dans même direction.
- Problèmes de récupération de la posture plus lourd après rbd et nsu. Voir =  9,65,85,97,102 16,85,88,119 85]

i) lien entre nsu et V 
- lien entre nsu et svv/shv et V comportementale (travaux de DP)
- interprétation de DP des hyp de Kerkhoff & Zelch est ici un peu fausse ! cad qu'on met avant idée que circuits code V et H en 3d. Or ces deux auteurs ont mentionné plutôt l'idée d'un réseau plutôt 

**  La NSU + RH ralentit récup de problèmes posturaux. Echelles rapides pas bonne car c’est subjectif de distinguer latéropulsion avec/sans pushing. Il vaut mieux échelles + longues qui comprennent évaluation de la V. **A noter = seuls les patients avec supramodal déficit auraient une mauvaise construction de la verticalité ! Si on a dissociation, c’est que l’atteinte de la V a une explication de moins haut niveau ?? ** Mesures posturographiques = statiques et dynamiques, pour noter body sway et centre de pression/masse.

Problèmes de posture + forts après RH stroke, dans position couchée/assise/debout. Lien entre PS et NSU postulé par le fait que NSU semblent lié à déviation contra de la VV, HV, et PV selon certaines études = gentaz 2002/kherkoff 1999/k et zoech/per01+02/saj/yenik.

-   **Hypothèse 1 = NSU = problème supramodal de la verticalité => problème de posture !**
-   **Hypothèse 2 = NSU = problème intégration somaesthetic = NSU graviceptive, shift du vecteur idiotropique => problème de posture**

L’hypothèse 1 peut être soutenue par le fait que NSU patients peuvent aussi négliger infos visuelles pour se maintenir stable (ref = Yelnik 2006). en effet optic flow en périph utile pour coder orientation. peut être que pb attentionnel empêche utilisation de ces indices. compatible avec pb attention exogène vers périph ! 
Important = selon DP, pb de schéma corporel dans nsu. les patients négligent les segments corporels contral et donc on a donc nsu + pb de posture. Il est important ici que d'un point de vue théorique on parle d'une R dynamique, inconsciente, qui sous-tend action = contrôle postural. Mais dans la litté, on a évidence pour perte de R topographique cs ! cad pb de R du corps non orientée vers action. 
**conclusion =** les patients atteints de nsu auraient pb postural car négligent infos corporelles et non-corporelles. selon DP, on a + de négligence somatosensorielle que visuelle/vb. le référentiel exocentrique est moins touché. Comme argument majeur, DP utilise i) étude de 2002 = déviation du bassin, pas de la tête; ii) forte association entre PB et nsu ; iii) lien entre WBA et R du corps > SVV ; iv) implication infos somato à la fois dans spv et verticale comportementale ; v) effet TENS sur posture, mais aussi sur nsu (voir Kerkhoff).
==> renvoie vraiment à idée de pert d'intégration mss qui sous-tend R inconsciente de l'espace orientée vers action. 





### Saj et al. (2005) = influence de la posture sur la VV chez nsu

**Contexte** : Lien entre NSU et perception de la V souvent évalué en position assis. Ici on module les inputs vestibulaires et soma en manipulant la position durant laquelle les sujets NSU effectuent une VV task.

**Méthode** :

-   Sélection patients NSU **without** pushing (SCP) (ltp du coup ?)
-   8 NSU (tous avec deficit sensorial soma) / 6 contrôles
-   VI Position : assis + pied / assis (**x input plantaire**) / assis + jambes allongés sur support (**x input ppc jambes**) / couché (**x input otolithe**)
-   Tâche : **VV visuo-haptique** (ajustement manuel avec présentation visuelle) = ajustement avec 6 essais par position.

**Résultats** :

-   On retrouve biais contra chez NSU (rés classique).
-   Position pas * chez contrôle
-   **Biais ispi diminue linéairement chez NSU** * avec r2 = 95% pour le facteur position.

 

**Conclusion **: En modifiant les afférences, on modifie une perception erronée de la V chez NSU. **Confirme résultat de Pizzamiglio 1995 qui a montré que dans une position couchée le biais ipsi dans l’axe horizontal (bissection de ligne) diminuait chez 8 sujets NSU.** Interprétation : intégration asymétrique des inputs vestibulaires  (**R de haut niveau biaisée)** dans la NSU et donc diminution de la NSU verticale quand on est couché.  MAIS cette interprétation vaut aussi pour réduction des inputs somesthésiques (quand je passe de assis à couché).

 


### Pizzamiglio et al. (1997) = influence de la posture sur la BL chez nsu

Contexte théorique = Modèle d’intégration mss pour réf égocentrée/R de haut niveau égocentrée suppose que cette mss est asymétrique chez NSU. On peut alors soit a) stimuler la mss avec stimulation sensorielle « artificelle », ou b) supprimer un signal sensoriel pour réduire l’asymétrie. On teste cette idée chez NSU vs pas NSU dans la modalité vestibulaire.

Méthode =

-   pop = 12 NSU RBD + 8 RBD
-   M + 3-36. majorité M+3.
-   HH + albert + letter + wundt + reading + LB upright
-   Apparatus = LB in supine position, **ligne au niveau des yeux (facteur à prendre en compte ??)**, réponse donnée verbalement (minimise influence NSU motrice du bras, pas des saccades). L’expérimentateur bougeait un point laser
-   VI = vision + group + position
-   VD = bias

Résultats =

-   dans groupe NSU on a effet position ++ avec réduction du biais ipsi dans position couchée.
-   dans group RDB tout ns
-   effet vision ns.
-   Encore une fois, statistique grossière quand mesures répétées ! pourquoi ne pas rapporter group * position effect ? ici on regarde effet simple directement ...

Discussion = dans position couchée, pas d’input graviceptif, seulement récepteurs somatiques dans l’arrière du corps. Les signaux somatiques seuls biaiseraient moins les traitements spatiaux égo et allocentrés (LB) vers la droite. Cela soutient hyp d’une mss asymétrique à la base d’un shift de la RE vers la droite dans le dimension horizontale. Saj démontre que cette mss sous tend aussi VV biaisée dans le dimension verticale car baisse des inputs somatiques, proprioceptifs et vestibulaires réduit le biais VV. **De plus, effet de l’input visuel négligeable ici, donc effet vraiment attribuable à baisse des inputs graviceptifs et non à absence d’indices attentionnels visuels.**

 



### Yelnik et al. (2002) = influence position tête sur VV chez nsu

**Contexte** : On a l’intuition que x de la V est lié à NSU, mais on pourrait aussi avoir un lien caché avec hémianopsie/quadranopsie. Ici on essaie de voir le lien entre x de la VV et NSU/visualx/site de la lésion.

**Méthode** :

-   Pop exclue si x vb.
-   Goldmann campimétrie
-   NSU avec test d’Albert et Bergego
-   SCP
-   Pop : 20 LH, 20 RH, tous hémiplégiques. + 20 contrôles
-   Pop hémianopsie = 9 sujets (**6 RH, tous NSU !).**
-   **Pop NSU = 16 sujets dont 9 RH**.
-   Tâche VV MAIS version visuo-haptique ! de plus inclinaison initiale de +/- 60°, c’est bcq par rapport à d’autres papiers (effet scanning ??).
-   VD = mean alg et U range.
-   VI = groupes * tête inclinée/debout * vision(one eye/two eyes).
-   Stats non par (distribution non-gaussienne).
-   On fait normes à partir de sujets sains.

**Résultats** :

-   Aucun effet de la vision chez contrôle et patients.
-   RH = VV contra quand debout ; U range > contrôle ; **Effet muller quand tête inclinée contra, mais pas ipsi. (NB = relativement au biais initial , déjà à gauche, en position upright).**
-   LH = idem mais dans l’autre sens (logique).
-   **U range RH > LH**.
-   **Degré NSU des 40 patients corrélé avec biais contra quand debout.**
-   Site de la lésion : pas de diff * de VV entre pp qui ont lésion PIVC ou non = peut être car tous les patients avaient larges lésions dans zones également importantes pour la VV (OC, noyaux lenticulaires (GB), capsule interne).

**Conclusion **: Rés va dans le sens de l’hypothèse de la négligence graviceptive somesthésique : pas d’input du cou contralésionnel et donc pas d’effet muller (compatible avec effet positif de la TENS, cf Pérennou, et étude de Barra 2010). De plus, corrélation entre biais contra quand tête droite et NSU, ce qui confirme corrélation forte entre déficit horizontal et V.
 = influence 

### Funk et al. (2010b) = influence position tête sur VV/HV chez nsu

**Contexte théorique =** Pourquoi verticale subjective mismatch avec verticale gravitaire chez NSU ? Hypothèse d’une négligence graviceptive = intégration asymétrique de signaux gravitaires/vestibulaires. Ici on a 2 modèles : howard propose que inclinaison de la tête devrait baisser nsu car moins de signaux otolithique. En revanche, mittelstaedt propose plutôt que vecteur idiotropique devrait déterminer direction de la VV si système gravitaire en panne.

**Méthode =**

-   pop = 8 nsu + 8 RBD + 8 ldb + 8c
-   nsu = M+9 vs M+5 dans autres groupes (chronique)
-   test nsu extra = LB + daisy + star + number + reading
-   test visuo-spatiaux = orienter à l’horizontal ou verticale baton dans modalités VV ou HV.
-   VD = diff threshold + constant error
-   méthode des limites = on tourne apparatus jusqu’à que le pp dise stop, en partant d’une position de départ donnée.
-   VI = modalité + head position (-25/0/+25).
-   trunk contrôlé + chin rest pour la tête.

**Résultat =**

-   de façon générale, tilt contralésionnel chez nsu ( !=0) mais pas chez autres groupes. Chez les 8 sujets.
-   constant error =
-   pour SVV et SHV, on a effet attendu, aggravation du tilt quand tête à gauche, baisse du tilt quand tête à droite. On retrouve aggravation chez RBD pour la SVV.
-   même résultat pour ajustement horizontal. a la différence que dans HV on a pas réduction du biais * quand tilt à droite.
-   diff threshold =
-   On a + imprécision chez nsu > autres groupes dans H et V. pas d’interaction avec la tête.

**Discussion =**  Résultat en faveur du modèle « idiotropique ». Soutient hypothèse que PPC intégre infos mss. Une mauvaise mss = mauvaise coord spatiales. « spatial orientation constancy » = **perte orientation spatiale multimodale** = **problème de posture, PS, déviation du tronc ...** **hypothèse aussi que ce déficit postural dans les plans frontaux et horizontaux --> perturbation du traitement des infos spatiales dans tous les plans ! pas clair = problème codage orientation 3d assuré par modèle interne, près PIVC/PPC/STG => X => X posture => X traitement égocentré ! ici le problème de codage de l’orientation spatiale se traduit par des problèmes posturaux, mais par quels mécanismes ? ici on parle de « pathological default position adjustement ».**

 




### Funke et al. (2010a) = influence posture sur VV/HV chez nsu = ns. 

**Contexte théorique =** troubles orientation spatiale dans le plan frontal constitue l'un des deficits coeur de la nsu visuelle. Comme on retrouve biais contra dans tâche visuelle et haptique, arg pour miv multimodal. jusqu'ici on a pu montré que gravitational et haptic cues ne sont pas intégré correctement par miv nsu (pour infos soma, ça reste à prouver, rés plutot négatif). Ici on part du postulat que integration mss asymétrique et que si on réduit inputs via position allongée, on réduit le biais contra. En effet quand on réduit infos graviceptives (soma et vb) on réduit nsu. quand on juge svv dans position couchée, on devrait s'appuyer ++ sur inputs soma quand même !   
*nb = quand saj a montré que biais svv se réduisait quand position allongée, on avait quand même tjrs un biais, ce qui montre quand même queintégration inputs soma n'est pas bonne non plus !* 

**Méthode=**
- pop = 20n+ rbd / 20c (:-1: pas de groupe rbd contrôle)
- m +1-8
- lesion size ? puching ? :-1:
- diag = lb/star/letter/bit/reading
- head and chin-rest + stabilisation scarf si tronc hémiplégique.
- SHV, 10 T par cond , +/- 40° shift initial
- vd = bias + interval of uncertainty (range).
- Cond = group + plane (pitch/roll) + posture (up, supine)
- NB = check stat montre que pas d'effet du shift de départ.

**Résultats =**
- de façon générale imprécision et bias nsu > c. 
- sujet + imprécis dans plan pitch > roll et supine > up. ce tout dernier effet est + fort chez nsu > c. 
- on retrouve même rés avec biais, + fort pour supine et pitch, étant + forts chez nsu>c. 
- lien entre sévérité nsu et shv = quand score composite des tests extraperso, pas de corrélation. quand corr spearman ind pour chaque test (non corrigée ... :-1: on obtient cor* avec test de barrage, ns avec lecture, et cor - forte mais * pour le biais pour lb. 

**Conclusion =** 
- On réplique bien effet nsu > c dans 2 plans différents pour shv. corrobore hyp kerkhoff sur nn multimodaux qui traitent les axes de l'espace dans le plan frontal et sagittal. miv pourrait être lié à cortex PAR, aussi impliqué dans attention spatiale dans axe horizontal. de+, arg sur sakata ne pas de rapport avec le biais spatial mais explique bien deficit d'orientation spatiale non latéralisé. 
- Cependant, shv se dégradait + dans position supine > upright. post hoc explanation : baisse de vigilance ici car essais réalisés trop lentement.
- enfin, cor** avec cancellation peut être due à meilleure reliabilité dans ce test > lb pour évaluer nsu. pas forcément convaincant cela dit. Selon moi *peut être que cancelattion task implique deconnexion fr-par, et donc + forte nsu, + sévère, alors que lb a lésion + locale sur le plan par-oc*.  

 
### Kerkhoff (1999) = influence position tête sur VV chez nsu

**Contexte :** Dans un premier temps NSU vu comme mispercep de l’axe horizontal uniquement : déviation vers la droite, compression et négligence de l’hémi-espace gauche. Voir ref intéressante, plus vieille citée ici : eg : stimulation optokinétique qui réduit déficit H ET V ! Cette étude va plus loin que l’étude de K et Zoech en examinant si la perturbation de la V chez les patients NSU est multimodale.

**Méthode **:

-   11 NSU (NET) / 12 RH / 11 LH / 22 normaux
-   ~150 j post-adm
-   VD : index de mobilité (marche, position debout) / visual acuity / NET
-   VD VV : position V,H, Oblique. Bias : erreur constante (signe) ; précision : moitié de la U range
-   VD HV : idem avec erreur constante ; erreur constante non signée. threshold impossible à calculer en fait
-   VI : groupe * rotation (H,V,O => déviation de 20° à la base).

**Résultat expé 1** :

-   NSU > autres groupes pour chaque modalité et chaque orientation pour erreur constante et précision
-   RH = LH = contrôle ici.
-   Le biais chez NSU est vers la gauche pour 11/11 dans VV et 10/11 dans HV
-   Quasi toutes les corrélations sont * entre perf HV et VV (**limite : pas de correction multiples** …)
-   Corrélation positive entre NSU (NET) et perf V, idem avec index de mobilité. **On a lien entre perte orientation spatiale et mispcp de la V**.
-   NB : le biais axe V > biais sur axe H.


**Résultat expé 2** :

Ici on regarde si corrélation transmodale, c’est-à-dire on demande à 2 patients NSU et un normal de réaliser jugement de **matching V** avec S présenté en V/H avec réponse H/V. orientation : 0/45-90° (pas de swith de 20°)

-   On retrouve biais gauche chez NSU> normal dans toutes les cross-modalités et orientations.
-   Perf NSU VH > HV dans toutes les orientations (logique …système haptique moins précis, cf Ernst & Banks)
-   **Montre bien que R multimodale de la V est touchée dans NSU ! va dans le sens de l’expé 1. Problème intégration mss ?**

**Résultat expé 3** :

Ici on regarde si tilt de la tête (+/- 25°) influence perf V chez 1 NSU vs 1 contrôle alors que tronc est stable.

-   Jugement V très affecté par tilt chez NSU > contrôle (3-5°)
-   VV = Chez NSU, tilt L augmente erreur > upright = D, idem chez sujet sain
-   VH = L > R > up en erreur. Inverse chez contrôle.
-   VHV = L > up > R chez NSU, on a amélioration de la V quand penche la tête à droite ! idem pour VHH.
-   On a bien un effet Aubert (biais vers la gauche augmente quand tilt gauche !) associé à une plus grande imprécision! Pas d’effet Muller mais diminution (du moins dans modalité H) quand tilt à droite (va dans le sens de Yelnik, pas d’influence input du coup sur la VV MAIS c’est le cas quand réponse haptique ! donc c’est ambigu … ). Donc le fait de modifier inputs vestibulaires et soma (cou) a un effet sur NSU V. Rés interprétable avec modèle interne, vecteurs vb qui supplée la NSU graviceptive quand vers R.

**Conclusions** : en somme, ces résultats suggèrent mécanisme avec ressources partagées, mss, pour construire V.

 

 







### Mazibrada et al. (2008) = Déafférentation complète du corps baisse précision VV (rejoint Barra 2008). 
**contexte =** on sait que pv et vv dissociées. mais on sait que patients lésion VV ont imprécision PV. Ici on étudie la relation inverse = effet deaff soma sur VV.

**méthode =**
- pop = 3 patients avec différents profils de deaff. s1 = guillain barré = bil sensory loss (*atteinte gaine de myéline périph, schwann cell*); s2 et s3 = sensory loss en dessous de vertèbre thoracique.
- 20 contrôle
- pop chronique
- echelle RST = Rivermead Somatosensory Test = épingle, pcp, toucher épicritique ou protopathique. score sur 300. 
- tache PV frontale
- vd = biais + "cone de verticalité" = range + SD
- test à T0 puis T+7 pour s1; T+2 pour s2/3
- on regarde effet tilt direction.

**Resultat =**
- contrôle ok. range normality -2/2.
- ![](https://i.imgur.com/76V2CiW.png)
- RST = 22 pour s1. puis 130 t+7.
- estimation biais très biaisé en fx de la direction initiale du tilt, en dehors de normal range. (bn = **fait penser à effet cadre chez nsu = utilisation de cue extérieurs !**).
- s2+3 ok.
- le biais influencé par direction se normalise à t+7 pour s1.
- concernant sd, sd++ pour s1+2 test 1 quand dire leftward. cet sd reste le même à t7 pour s1.
- pour range, res idem, mais s1 qql que soit direction initiale.

**Discussion =** le patient S1 récupère en terme de biais, mais pas en terme de variabilité, surtout pour tilt initial à droite. montre importance des cues soma des épaules, non endommagés chez s1/2. compatible avec théorie de mittelstaedt, 2 voies graviceptives, up et down.



### Saeys et al. (2018) = hemianesthésie unilatérale augmente le Muller-effect. Soutient modèle de Mittelstaedt. 

**contexte =** le E-effect occurs quend léger tilt <60°. **selon mitt., reflète tendance à aller vers VI, pour éviter surcompensation ? à creuser... mais je crois que ça reflète un aubert effect inversé, quand on a le VI tout droit. comme otol est moins fiable, on a + confiance dans VI.** du coup on regarde si on observe cela quand patients sont déafférentés sur le plan soma. de +, on regarde Eeffect sur PV et VV.

**méthode =**
- exclusion nsu visuelle et PB. 24 patients, dont moitié r/lbd. :-1: pas de groupe contrôle.
- RASP test = dichotomy en 5 groupes (au lieu de score continu...:-1:)
- SVV, réponse joystick ; 21T ; tilt max 20°. pas de chin rest (mais pas de PB). 
- :-1: = pour induire E-effect, on demande au patient de pencher le plus possible sa tête ! gros bruit dans les données !! on a range de 35 à 45°...
- PV dans frontal plane. même procédure (peu de description ici...)
- pour calcul Eeffect, on multipliait raw bias par -1 (pour inverser le signe). 

**Résultats =** 
- VV = gros E effect. augmentation linéaire avec RASP sécérité.
- PV = idem.

**discussion =** augmentation du Eeffect interprété comme "reweighting strategy". les patients ne pourraient pas utiliser infos soma pour bonne pcp de la verticalité. **néanmoins, l'interprétation théorique derrière est très floue... pas rigoureuse... la discussion est nébuleuse...pourquoi E effect augmente?, on pourrait avoir augmentation SD...**:-1:



### Alberts et al. (2016) = l'effet Aubert atteint pic d'imprecision quand head in space 60°, mais ensuite inputs somato deviennet + précis pour estimer VV, peut-être car les gravicepteurs sont + sollicités.

**Contexte théorique =** On utilise inputs vestibulaires, visuels, et ppc pour estimer direction de la gravité. Effet Aubert prédit que reliability des inputs otolithiques diminue quand angle augmente. Ici on regarde si c'est aussi le cas pour les cues soma !  
Ici on utilise modèle bayésien pour prédire bias et sd de SVV en modélisant inputs vb et ppc, en provenance des otolithes (head in space), cou (head on body), corps (body in space). En sachant que SVV est surtout estimée en position normale par head in space coordinates.

Nouveauté = ici on rajoute par dans le modèle pour que le poids des inputs ppc (qualité du signal) ne soit pas fixe mais diminue avec condition extrême.

**Méthode =**

*   Pp = 10 sujets, 110 essais de SVV
*   VD = SVV bias + error
*   VI = On manipule BS (7 positions ; -90+90) * HB (3 positions ;-30+30), en manipulant tilt de la tête + tilt du corps, dans les 2 sens, associés ou dissociés, +/- 30° jusqu’à 90°. Sujets déplacés dans roue. On a une tâche transmodale = jugement visuel avec manip inputs vb/ppc.
*   On compare performance cptale à perf du modèle bayésien.
*   Plusieurs modèles bayésiens = full modèle (poids décroissant pour ppc), old model (ppc W fixe), head in space seul.

**Parenthèse méthodo sur modèle bayésien !**

*   On utilise likelihood pour signaux modélisés, sur la base de précédentes données, avec estimation du bruit (inverse de la précision).
*   3 étapes qui mènent à estimation de gravité = estimer likelihood des sensors, transformer les coordonnées de réf différents (coup et soma) en sommant les gaussiennes afin de replacer les cues dans une réf spatial commun (**head in space**), combiner les signaux avec en + le prior et les coord visuelles head-centrées.
*   Prior = le cerveau pense que la gravité est alignée avec direction de la tête.
*   On compare les modèles bay avec LRT.

![](https://i.imgur.com/3tRMk8S.png)


**Résultat =**

1.  Cpt =

*   Quand HS augmente (degrés BS + HB), bias augmente. pas de diff de pattern pour HB positions.
*   Quand HS alignées avec le corps, variabilité faible quand aligné avec verticale, puis augmente (HS) jusqu’à 30° mais se level off entre 30 et 90\. Quand HS avec dissociation, var augmente augmente jusqu’à un pic à 60° puis diminue.
*   Overlap des courbes psychophysique entre HB et HS > BS, ce qui montre bien importance des inputs vb dans pcp **direction** la SVV. Par contre pour la variabilité il y a plus d’overlap entre HB et BS quand > 30 °. La var observée à plus de 30 degrés pourrait s’expliquer par baisse des poids soma.

1.  Comparaison Bayes et cpt =

*   Full model explique mieux les données que modèles réduits.
*   Analyse du full model montre que inputs vb ont W = 1 quand 0 degrés puis réduction W progressive. Augmentation du poids augmente pour Soma jusqu’à 60 degrés, et prior augmente linéairement. **Prior peut expliquer effet Aubert ! Quand petit tilt, prior a faible W, et quand grand tilt > 60degrés, prior pense que gravité aligné avec orientation du corps. Contestable, on pourrait avoir juste effet Aubert a cause d’inputs ++ du corps et du zéro W vb.**

![](https://i.imgur.com/h1OuvXB.png)

- ***Résultat à retenir :*** 
- larger SVV errors quand augmentation **head in space** = on code SVV dans cadre de ref head in space = **quand tête 0°, malgré orientation corps à +/-30°, on peu d'erreurs.  
- quand head on body (-30,0,30) on a aug de la var quand >60°. 
- on a overlap de la var pour *small tilt* entre head on body et head in space
- on a overlap de la var pour *large tilt* entre head on body et body in space
- Ca indique que je me repose + sur signaux corps et moins sur otol quand j'augmente tilt body.
- prior drive bias vers head in space coord. 
- rés nouveau = head in space var atteint pic à 60°, puis diminue ! effet jusqu'à 60° est bien connu, postulat que dist *non-uniforme des cellules ciliées dans otol* (fernandez et goldberg, 76, rosenhall, 72-74). **On a moins de cell ciliées qui code pour large tilts, donc + de bruit**. 
- congruent avec albert 2015 = large var à 90° dans SVV chez patient avec deaff bil vb ! 
- ? = pourquoi la var se stabilise à 60° pour head in space var ? spéculation = Stim ++ des capteurs tactiles quand large tilt body in space.
 
**Discussion =** vb reliability (W) de 0 quand tilt augmente, car PA rate des otolithe désynchronisés. Quand vestibular bi loss, on a var increase quand tilt, seulement attribuable à input soma. **Pourquoi plateau ?** Extrapolation = signaux multiples et divers du corps, input non linéaire, avec certains récepteurs étant beaucoup plus stimulés avec gros tilt (eg = pression surface peau, tension tendons, viscères du tronc).

### Funk et al. (2011) = RFE plus fort chez nsu

**Contexte =** Les patients avec nsu ne peuvent plus intégrer signaux gravitaire. On va donc faire hypothèse qu'ils vont s'aider +++ de signaux extérieurs comme des repères visuels allocentrés => **sensibilité exacerbée au RFE**. 

**Méthode =**
- pop 12 rbd+ / 12 rbd-/lbd/12C
- pas de pushing behavior
- m+6 
- pas de report de la taille de lésion ici :-1: 
- lésion T-P quand nsu. lésion MCA++
- tache diag clinique = dessin de M/lb/number canc/reading
- SVV avec RFE qui était placé à plusieurs shifts graduellement par rapport à 90°. 
- contrôle = masque oval. 
- départ de la ligne = +/- 20°
- chin rest. tâche ordi

**Résultats =**
- en baseline, contra tilt SVV > autres groupes ==.
- de façon générale, tilt SVV constamment dans même direction que tilt RFE.
- la SVV dans la nsu est radicalement différente des autres groupes surtout pour orientation extreme à +/-45°, car les autres groupes ici ne sont pas sensibles à l'illusion pour cette orientation (effet connu dans la litté).

**Conclusion =** Comme intégration cue gravitaire asymmétrique dans NSU, déviation RFE/intégration cue visuels est + forte pour compenser. Peut être que cette intégration se fait au niveau IPS, ce qui serait cohérent avec Résultats de Conrad 2016. Cohérent avec **perte du référentiel gravito-world-centré** = perte d'**orientation spatiale constante**




### Barra et al. (2012) = influence des connaissances top-down sur la VV
### Bisdorff et al. (1996a) = Atteinte vestibulaire périphérique baisse précision PV

**Contexte =** On cherche dissociation entre perception du droit-dessus = *uprightness* et SVV. Pour cela on regarde range de précision et biais SPV chez patients avec diverses atteintes vestibulaires, et aussi chez des contrôles avec S opto/galv/tilt body. pitch + roll

**Méthode** = 
- Plusieurs populations
- 42 contrôles 
- 6-15 = chronic bilateral loss (>2ans) / Acute unilateral peripheral (3-7j) / chronic stable unileral peripheral / BPPV (benign paroxysmal positional vertigo) => several weeks / Meniere / Vertical nystagmus (lésion TC) / Longitudinal peripheral (Meniere) => neurectomie du nerf vestibualire VIII.
- Méthode = strap intégral assis head + torse
- benson (1989) => 1.5°/s => pas de S des CSC.
- Test dans pitch + roll
- réponse joystick
- manip => body tilt => on fait shift progressif 10° vers droite/gauche sur plusieurs cycles (voir Barra 2010 aussi).
- optok fild (30cm) + tilt pendant 6min. => vection subj ressentie.
- galva = sham/cl/cr. => ressenti subj = sway toward the anide (effet classique).
- SVV = réponse joystick.

**Résultats**
- bil = precision baisse dans es 2 plans/ pas de biais
- vertical nystagmus = idem
- BPPV = idem
- acute uni periph = idem / biais tendance ipsi
- chronic uni peiph = controle
- après neurectomie du nerf vb, on a biais ipsi SVV. ras pour spv.
- sujets sains => effet du body tilt adaptation seulement, que sur biais. on a aussi influence **S galvanique * sur précision spv** ! 
-  sur SVV, la S galvanique a une influence trop petite pour être significative, mais bon de noter que SVV avait tendance à shifter vers anode (effet classique).

**Conclusion =** Effet déficit vestibulaire sur précision roll+pitch de la spv. On a dissociation au niveau du biais entre spv et S visuo-vb. cela dit on voit que ref de verticale allocentré permet au référentiel de verticalité égocentré d'être + précis. Ca permet au modèle interne d'être + robuste. Va dans le sens de **2 voies graviceptives**.  
A noter que biais svv ici est dû à une réponse motrice vestibulo-spinal (nystagmus).







**Contexte théorique =** Influence de la position du corps sur la verticale perçue (Aubert effect) est compatible avec MIV qui intègre graviception soma/vb/vis en donnant un poids à chaque modalité selon la tâche en cours. Jusqu'ici on parlait de processus bottom-up. Le but de cette étude est de montrer qu'il y a aussi des processus top-down qui influence le MIV. *Hyp = la conscience de l'orientation de son corps devrait influencer l, avec postulat que miv correlates overlap avec body awareness/first-person perspective correlates*. 

**Méthode =** 
- 9C
- simulateur de vol, tronc strapé, tête strappée. réponse joystick.
- design : 
- position debout obj + subj / position tilt obj + subj / position tilt obj mais pas subj ! On demandait à pp avant chaque essai si il se sentait droit.
- 12 svv * 3 cond. 
- nb = 3 cycles d'oscillation de 30s avant essai pour supprimer effet du tilt précédent,de +/- 22°.
- ordre constant = up o+s / tilt o+ns / tilt o+s
- Comment induire cond 2 ? => *5 min de tilt à 30°*. nb = on demande svv de faire svv avant pour voir si effet d'hab sensorielle au tilt a joué rôle sur svv (contrôle).
- vd = bias + sd / vi = body awareness

**Résultats =**
- tilt illusion (o + ns) fonctionne pour tilt gauche et droit. en moyenne, tilt de 9°.
- svv = o + s avec ou sans tilt  = très précis, on prend en compte position du corps assez bien pour la tâche, même si tilt 30°. 
- svv = tilt o + non s = incongruence = cette fois ci c'est comme pas prise en compte du tilt, on a performance + loin de 0 ! 
- NB = de +, la sd la plus forte intervient quand conscience du tilt ! comme si processus Bottom-up + imprécis comparé à quand on n'intègre pas ces infos là car on pense ne pas en avoir besoin d'une certaine façon... **les sujets ont plus confiance en eux quand ils se sentent droit !**
- NB2 = adaptation sensorielle constatée dans cond incongruente est très rapide mais se stabilise très vite.

**Discussion =** baisse de poids accordé aux inputs égocentrés, et + de confiance accordé aux inputs allocentré (vb). Interprétation pour la variabilité faible quand incongruence = modèle interne "robuste" car utilisation d'infos allo+égo. alors que normalement quand tilt Aubert on est moins précis car on se fie plus aux inputs somato ! 
![](https://i.imgur.com/jao3013.png)




### Anastasopoulos et al. (1997) = Atteinte vestibulaire périphérique baisse précision PV

**Contexte théorique** = tester contribution du système vestibulaire dans la PV

**Méthode =**

-   pop = 20C + 5(1 meniere + 4 vb neuritis) (4 right side)
-   acute stage, pas de compensation, J+3/4
-   VV = joystick + 6 essais (+/- 40)
-   PV = 12/16 essais. (5/15/25). on tourne doucement la roue à vitesse différente pour éviter cueing.
-   apparatus = roue

**Résultat =**

-   VV ipsi (classique)
-   SPV ok ! == control.

**Discussion =** renforce idée que poids soma ++ pour estimer PV. Ref à voir = dichgans young brandt 1972

# TMS - SVV

Question de recherche : la TPJ joue un rôle dans l'intégration de signaux vestibulaires. Quand on inhibe la tpj, la SVV est moins précise. 

### Otero-Millan et al. (2018)
**contexte =** on a montré en 2015 que TMS appliquée sur tpj (smg) créait un effet muller quand rotation tête de 20° chez sujet sain. ici on essaye de montrer que cet effet n'est pas due à torsion oculaire. 

**méthode =**
- 12 sujets sain, design intra-sujet
- tms sham + smg + regions voisine + baseline
- méthode psychophysique avec 800 essais en tout ! 300 pour pretest, 500 pour posttest. session sur diff jours
- procédure = tete penchée à gauche pour baseline / tete droite tms / tete à gauche pour posttest
- tâche staircase = range degrés de la barre lumineuse rétrécit avec les essais en fonction de perf. 
- seulement 1500 s pour répondre.
- oculographie
- irm + tms theta burst => on envoie plusieurs décharges d'affilé. 

**résultats =**
- réplication keradhman 2015. on a shift contralateral (vers la droite) en post-pre. on a pas ce shift quand tms sham ou sur torsion oculaire.
- à noter que dans le temps (mesuré tous les 10 essais) on a progressivement shift vers la gauche (car tete inclinée à gauche). 
- sur une deuxème session tms smg, même résultat.

**conclusion =**
- rés compatible avec role du tpj dans cognition spatiale, très divers = gravité, attention, rég égocentré, heading pcp, self-awareness... hub d'intégration mss+++. 
- hyp = tpj envoie copie d'efference au système oculomoteur qui prévoie torsion. ocular torsion = sub-cortical level. 
- effet sur miv ? 

### Kremmyda et al. (2019)

**Etude de cas**

**Méthode =**
- femme 29 ans epileptique chronique, 15 electrodes rh
- syst vb normal
- phenytoine tt pour éviter crise avant chirurgie, entraine down/up nsagmus, mais pas vertigo
- parietal et frontal electrode rh => vertigo vers la gauche + illusion visuell et soma.
- video => nystagmus en diagonale/downbeat vers la droite, correspondant à drift diagonale up left.
- bucking test svv, réponse verbale. 12 essais cw/ccw avec electrical S (ES) + 24 essais cw/ccw sham
- ES 10s sur electrode j1 (WM parietal, vers Post Cingulaire) /p4 (PoC).

**Résultats ES** =
- sensation de vertige -> gauche
- svv normale avant et après ES
- biais ipsi quand ES +5.7° quand rotation bucket ipsi (11/12 ipsi)
- quand bucket rotation contra, 2/12 valeur anormale (1 ipsi+1contra)
- rés similaire avec electrode D5 WM frontale 5s
- A noter que durant ES on a nystagmus du à medoc qui diminue, comme si drift yeux vers la droite.
- limite = influence medoc / 1 seul cas.

**Discussion =** effet ES quand bucket en rotation vers la gauche indique quelieu visé par électrode J1 près PoC WM , proche SLFII, est impliqué dans intégration d'infos visuelles sur le mouvement avec vb signal. Ici on fait rapprochement avec les travaux de Baier qui avaient trouvé implication de IFOF et de SLF associé à tilt svv quand stroke (2012). ici idem vace SLF II. Ici on a vertigo, ce qui est très rare, mais rare cas avce lésion PoC et SMG (*Anagnostou, 2010*).  
Selon auteurs, cohérent aussi avec étude sur TMS sur PAR ips/smg qui facilite ou biaise svv. 

### Willacker, Dowsett, Dieterich, & Taylor (2019)

**Contexte théorique =** cortex pariétal involved dans transformations de coord spatiales. quand tms, on a "facilitation paradoxale", levée d'inhibition entre les systèmes sensoriels, par ex entre vision et vb. 
Ici on fait hypothèse que inhibition de cIPS (vip) impliqué dans tt visuels (3d chez le singe ! voir sakata 97) devrait faciliter tt vb et améliorer perfs dans svv.

**méthode =**
- 32 normals
- rTMS
- svv psychometrique
- chin rest. 
- tâche contrôle : landmark = dire si barre blanche a portion + grande dans haut/bas. 
- nb = ligne flashée (beaucoup de supers contrôles dans cette étude)
- 6 blocs de 60 essais (5 min) (3 svv / 3 lmn dont a chaque fois sham/left/right tms sur ips)
- eeg mesures sur temps de repos en début/milieu/fin d'expé.
- stimuli = +/- 2° from true V. + variation ratio haut/bas. 60 combi différentes random par blocs.
- VD = slope + abs mean + pse
- stat ok. 

**Résultats =** 
- pas d'effet de tms sur pse. 
- effet * abs mean pour tms right et left = on a normalisation de la perf, avec tms right > left. 
- interessant =  on regarde aussi si effet tms est modulé par direction du biais de départ. on voit que pse est * different en baseline. mais + interessant, on voit que cette diff n'est plus du tout * quand right tms vs left tms ! c'est comme si le biais de base était effacé uniquement après tms droite ips. 
- steepness = ns. 
- tâche contrôle = Même interaction biais * tms, mais dans l'autre sens ! effet normalisateur pour tms gauche. 
- néanmoins, effet tms sur abs mean est ns. 

- dernier contrôle : pas d'effet entre tms et biais pour tâche visuo-spatiale en général.
- eeg = on regarde en baseline wave en fonction du biais. on a bien wave different. 

**Discussion =** Res suggère implication causale ips dans svv. on normalise perf quand tms, cad inhibition integration visuelle, ou plutôt *diminution du poids des inputs visuels vs vb* !   
A noter aussi que effet ips est spécifique a roll plane, pas à plan haut/bas.   
nb = regarder agosta, qui propose que ips est une interface spatio-attentionnelle.

**Ref ++ =**
- kaulmann 2017
- agosta 2014


### Kheradmand, Lasker, & Zee (2015)

**Contexte théorique =** Modèle interne de gravité nous donne une référence allocentrée qui permet *orientation constancy*, c'est à dire que l'environnement, malgré différentes positions corporelles, déplacement de la rétine, est toujours perçu de façon stable par rapport au référentiel gravitaire. Or, TPJ utile pour orientation spatiale, donc on regarde si TMS a effet délétère sur miv.

**Méthode =** 
- pop = 8 normals
- tête immobilisée
- tâche = svv avec procedure psychophysique (4 min). +/-16° max. avec pas de 2°. potentiomètre pour réponse avec gauche/0/droit. 136 essais
- 5s max. 
- tms = theta burst
- stat = multinomial logistic regression
- procedure : tms sur 4 sites du tpj. on a effet max +2° (meilleures perf, on se rapproche de 0 = -2°!)
- du coup, isl essaient aussi position tête penchée à 20°. cette fois-ci, pop = 8. 
- now TMS vs sham sur session différentes en intra. on regarde pre-post test avant et après TMS sham/smg. pour 6/8 sujets, on stimule aussi voisin du smg. 
- stat = one sample t-test dans chaque condition head tilt.
- vd = mean tilt.  

**Résultats =** 
- left tilt = smg > voisin et sham = 2.7 vs 0.2 vs -0.9
- right tilt = smg > voisin et sham = - 3.6 vs -0.6 vs -0.8
--> A chaque fois, on provoque biais contra-tilt. 
--> critique = moyenne des rés comportementaux relatifs à chaque aire adjacente ? pas clair ... 
--> **pourquoi dans condition up head j'ai amélioration des performances ? erratum ??** 

**Discussion =** Tout ce qu'on peut conclure de cette étude c'est que rtpj est impliqué dans l'intégration d'information head-on-body ou/et head-on-space et/ou eye-on-head. De façon générale, tpj utile pour alimenter modèle interne de la gravité allocentré à partir de plusieurs cadres de références (systeme de coord). Intéressant de noter que les zones voisines à smg sont des zones très impliquées +/- dans mss (ips, stg, ag, s1)
**Interprétation intéressante = pas d'intégration du message de torsion oculaire (eye in head signal) du à head tilt quand tms sur tpj ! Quand on a head tilt on a en effet mvt des yeux qui compense !**

### Fiori et al. (2015)

**Contexte théorique** = rTPJ est une aire très impliquée dans mss. On regarde si TMS sur rTPJ a un effet chez sujet sain dans une tâche de verticalité visuelle qui implique parfois intégration d'informations visuelles.

**Méthode =**
- pop = 20 sujets sains
- tms = theta burst = aftereffect de 20min. 
- nb = tpj coord selectionnées sur base litté.
- 3 conditions = sham / rTPJ / V1-V3
- 2 tâches en intra-sujet = svv + rft (contrebalancé) 
- tâche contrôle patch de gabor si direction verticale ou horizonrtale (normalement nécessite pure infos visuelles V1) => comme ça on est sûr que effet TMS rtPJ sur RFT task pas dû à effet sur processus de bas niveau V1-V3.
- ajustement verbal
- ilt frame +/-33° & tilt barre = +/-22°
- donc en intra = 2 x 3 conditions. 
- VD = absolute mean (pour patch gabor, RT)
- test non paramétrique

**Résultats =** 

a) aucun cadre = 
- erreur tms tpj > sham = v1-v3 (mediane = 2.134 degrés vs 1.250)
b) cadre tilted 
- ns
c) cadre droit
- ns

e) tâche contrôle
- effet tms v1-v3 sur detection time > rtpj.= sham 

**Conclusion =**
Ces résultats suggèrent que rTPJ est utile quand cue visuels non disponibles et qu'il faut utliser inputs visuo-vestibulaires et somato. Quand indices contextuels dispo, TMS sur rTPJ n'a plus d'effet vs TMS v1-v3. 
--> On interprète ici que tpj recoit copie modele interne de la gravité indépendant d'infos allocentrée visuelles.

**critique** => :-1: On a la tête non immobilisée + stat non par qui empêchent de regarder si interaction significative. néanmoins il y a beaucoup de tests contrôle. 

**Ref** =
- Zoccolotti 1997



# bottom up - MIV

### Bisdorff et al. (1996b) = dissociation PV/VV en terme de biais chez sujets sains quand stimulation optokinétique et GVS. 

**Contexte théorique =** On regarde si PV peut être modulée par stimulation visuelle, vestibulaire, ou somesthésique (simple déplacement de la roue)

**Méthode =**

-   pop = 10C
-   apparatus = roue, strap torse + head
-   traitement =
-   1) pendant déplacement de la roue, stimuli optokinétique qui tourne aussi dans le même sens ( ?) (cond = avec vs sans)
-   2) pendant déplacement de la roue, stimulation galvanique sur les mastoïdes (anode left vs lright vs sham) +

--> tilt de 5/10 +/- à chaque fois

-   PV = réponse joystick après déplacement (méthode des limites, on dit jusqu’à où on se sent vertical).
-   VD = range of uncertainty + bias (constant error)

**Résultats =**

-   optokinétique = effet fonctionne quand debout, mais quand pas quand rotation dans roue. intéressant de noter que les sujets rapportaient sensation de vection (je bouge alors que non) mais se sentait verticaux quand même !!
-   galvanique = effet debout, mais pas dans la roue.
-   en revanche, on retrouve un effet de la direction du tilt, avec biais congruent. mais pas d’effet sur range of uncertainty. pas d’effet du temps non plus, effet immédiat.

**Discussion =** tilt modifie inputs soma et donc PV. le sens vb pourrait permettre une plus grande précision, avec moins de poids = lire ref clark par ex. rés intéressant = perception plutôt contradictoire dans le cas de la stimulation optokinétique.


### Nakamura et al. (2019) = coton dans le dos réduit précision PV et permet à la GVS de biaiser la PV. jolie démonstration de la dynamique de repondération multi-sensorielle. 


**Contexte théorique =** la PV est déterminée à l’aide de signaux somatosensoriel. Quand on manipule ces signaux, on manipule la perception de la verticalité ou de la posture.Indices somato peuvent $etre d’origine viscérale, venir des récepteurs du dos, des jambes, des fesses ... Ici on manipule récepteurs du postérieur quand personnes assises afin de moduler la PV, avec ou sans perturbation vestibulaire.

**Méthode =**

-   pop = 15 C
-   traitement = GVS et/ou mousse sur le siège pour biaiser retours vestibulaires et / ou somato.
-   apparatus = strap total, sauf des jambes ne touchant pas le sol, et bras posés sur cuisses.
-   A noter que PV a été testée 1 mois avant avec les mêmes sujets, afin de tester la reliabilité test-retest, bonne en l’occurence.
-   contrôle de la vitesse de rotation de l’apparatus pour minimiser stimulation CSC.
-   36 essais = 3 position (11,12,13 degrés) * side * 6 trials. A noter que déviation de départ très minime ici en comparaison de ce qui se fait avec d’autres paradigmes comme la SVV.
-   GVS = pas contrebalancée, uniquement anode à droite. Donc pas de contrôle d’effet attentionnel ici, de type vigilance. Test préalable de la GVS pour s’assurer de l’effet habituel chez les sujets (illusion de mouvement).
-   VD = magnitude (abs) et direction (bias) de la PV
-   VI = group en intra sujet (control + vb + vb/somato + somato) bonferoni en posthoc.

 

**Résultats =**

-   bias =
-   somato = control = vb, avec déviation vers la gauche. résultat intéressant= pas de déviation vers anode dans condition vb ! par contre, effet anode quand associé à dégradation du signal somato !!
-   abs = accuracy
-   somato avec ou sans vb a magnitude plus éloignée de 0 vs vb ou control.

**Discussion =** modèle interne de la verticalité en intégrant multiples signaux. PV repose sur signal somato, car quand vb dégradation, le signal somato compense la perte de signal vb « fiable ».

**Limites =** pas de contrebalancement GVS. On a aussi, inexorablement, des différences de paradigmes (position de départ, apparatus, position assise ou debout...).

 

 






### Michel et al. (2003) = LPA biaise CoP sur axe horizontal chez sujet sain les yeux fermés. effet encourageant sur VV. 

**Contexte =** AP agit sur cadre de référence spatial corporel/RE/schéma corporel, comme l'a montré Rode (1998). Du coup, on veut répliquer cet effet chez sujets sains pour confirmer effet AP sur posture.

**Méthode =** 
- pop : 14 sujets controle = 7/7 rpa/lpa
- vd posture : centre de masse : variation sur x/y/surface totale
- vd "bonus" : la svv (plutot bien evaluée ici, même si réponse joystick et que la posture n'est pas contrôlée, ce qui est peut etre embetant ...?)
- vd controle sm : ssa + pointage open-loop (sans vision fb)
- tâche ap : 15° sur 20 minutes vers 10 cibles sur champ visuel. 
- design pre-post test + yeux ouvert/fermés + group rpa/lpa

**Results =**
- var controle sm : effet classique dans les 2 tâches pour les 2 groupes : montre bien que ap fonctionnelle.
- postural after-effects : 
- surface area : yeux ouverts : ns/ yeux fermés : augmentation globale
- mean x (lateral deviation) : yeux fermés : sessionXgroup interaction avec effet * pour groupe lpa avec shift vers la droite du centre de pression. yeux ouverts ns. 
- mean y : yeux fermés : shift vers l'avant global / yeux ouverts : idem
- svv : ns mais interessant de noter que chez groupe lpa le shift vers la droite est corrélé à shift de la svv vers la gauche ! 

**Discussion =** Comme Rode, on pense que effet ap agit pas seulement sur arcs reflexe/bas niveau mais aussi sur cognition spatiale en lien avec contrôle de la posture. Ici ap a démontré effet sur pcp espace extra-perso, et ici espace perso ! De + à noter que effet spé à lpa, effet assez classique qui démontre que c'est bien spécialisation hémisphérique de la cognition spatiale qui joue un rôle.  
D'un point de vue neural, confirme idée que ap agit sur aires par impliquées dans integration mss. tpj ? ppc + globalement ?

**Ref +++ à lire =** Richard 2001 = compression hc gauche ! / grusser 1990 : importance aire TPJ pour tt soma + vb. 

### Tilikete et al. (2001) = RPA améliore WBA chez 5 sujets RBD. 
(Article bref)
**Contexte =** Rode a montré effet CVS sur WBA chez lesion droite avec hemiparesie. Quid des prismes ?

**Méthode=** 
- pop = 15 RBD => 5/5/5 pour RPA/LPA/neutres = bonne méthodo qui élimine interprétation de type "arousal" = effet non spécifique.
- M+2-3
- important = pas de trouble de la posture = pas de PS. 
- pas de nsu ici.
- ap 10° + 50 pointages
- balance = tenir debout = CoP x/y
- contrôle sm OLP ok

**Résultats =**
- pre test Cop = effet classique CoP ipsi.(droite). 
- effet pre-post uniquement pour RPA pour cop x. 

**Conclusion =** Supporte travail de Rode 98 (+ tard supporté par michel 2003 et indirectement par nijboer 2014). Convaincant = effet spécifique direction des prismes RPA. interprétation= PA agit sur schéma corporel : représentation spatiale du corps qui sous-tend l'action.



### Nijboer et al. (2014) = RPA améliore station assise avec baisse biais contra (comme dans pv !) chez 10 patients nsu. 

**Contexte =** PA a montré efficacité sur wba chez hémiparétiques et sur sujets sains. Ici on veut étendre ces résultats aux patients hémi-négligents seulement (même si patients hémiparétique avec lésion droite de Michel 2003 avait sûrement pas mal de nsu dans le lot, mais pas reporté cela dit...)

**Méthode =**
- pop = 10 nsu (cancellation parmi distracteurs + lb / nb = version souris/ordi)
- j+37
- critere diag = tenir assis seul = dommage car exclu de facto les patients avec trouble postural sévère, comme pusher PS. 
- Tâche : rester assis yeux ouverts/fermé sur une balance. On enregistre CoP et postural sway. NB = méthode vd similaire a Pérennou 2000/2002 ! Tout pareil, sauf qu'on se restreint à patients nsu. 
- vd = cop/postural sway en x/y.
- intervention RPA = +10°/3 cibles g/m/d
    - contrôle sm = open loop pointing (OLP). Contrôle intéressant : minimum 3cm d'after-effect, sinon on continuait au delà des 100 essais de bases. 
    - a noter que couleur utlisiée pour faciliter pointages vers la gauche.
    - Critique = pas de groupe contrôle ici (lpa ou npa = neutral pa)
- design pre-post test

**Resultats =**
- Cop = effet pre-post uniquement pour CoP horizontal/X + yeux ouverts. Comme chez Michel 2003. On avait à la base CoP décalé vers la gauche(contrashift) qui se déplace vers le vrai 0 (vers la droite)
- Très important : **shift Cop initial contralesionnel**.
- Sur le plan pitch plane, on a aussi shift vers l'avant ! 
- Postural Sway = idem effet X avec/sans vision. => réplique Pér. 00 ! **Effet uniquement dans le plan roll,  pas d'effet dans le plan pitch** 

![](https://i.imgur.com/Ao6MKzk.png)


**Conclusion =** Rés en accord avec travaux de perennou, cad postural instability evalué assis independant de si vision ou non après les prismes. On a donc amélioré modèle interne de verticalité en agissant sur inputs soma avec RPA. Rés plutôt compatible avec hyp d'un shift contralesionnel rotation de la verticale posturale. Cette étude va dans le sens d'un effet des prismes sur verticale postural, ce qui est unique! Va dans le sens de l'effet de la TENS sur VP aussi ! Point commun => stimulation sensorielle qui cible aires mss améliore VP. 





### Pérennou et al. (1998) = Tens améliore la station assise chez stroke patients quand yeux fermés. Tens agit sur voie somatosensorielle et sur miv en retour ? 

**Contexte théorique =** on a trouvé dissociation entre verticale posturale et visuo-vestibulaire (cf Bisdorff). PV repose sur infos somatique. Dans ce cas, elle devrait être sensible à la TENS. Problème :-1: **rocking chair paradigm**, pas vraie spv ! mais mesure verticale comportementale

**Méthode =**

-   pop = 22 stroke = 13 RBD + 9 LDB
-   diag clinique = motor weakness/somatoloss/nsu (canc)/spasticité/HH
-   Paradigme = rocking chair. main croisée sur genou et jambes qui pendent.
-   Tâche = posture assise pendant 8s.
-   VD = orientation du support par rapport à 0. renseigne sur tilt de la PV contra ou ipsilesionnel. (on normalise en fonction de l’atteinte gauche ou droite).
-   VI = VI inter = group / VI intra = vision + TENS (baseline vs pendant). NB = pas de TENS chez sujet sain (bof ... ?)
-   TENS appliquée à partir de 10min avant la tâche et pendant la tâche sur le cou contralésionnel.
-   Limite = pas de stimulation sham ici (il manque un groupe contrôle). Donc recrutement attentionnel vers le côté contralésionnel possible. Cela dit explication attentionnelle dans tâche posture moins probable que dans tâche de jugement égocentré ? à voir

**Résultat =**

1.  baseline

-   AVEC VISION
-   PV contra tilt chez 19/22 patients vs contrôle.
-   corrélation entre PV bias et NSU
-   corrélation entre PV bias et hémianesthésie sévérité (problème = pas une variable continue...).
-   cutoff patho fixé à 2SD (3°) , dans ce cas on a 8RBD patho, dont 6 NSU.
-   SANS VISION
-   même résultat pour PV
-   ns, pas de relation entre PV et une variable clinique.

1.  TENS

-   AVEC VISION
-   On a réduction du PV tilt chez PV patho vs non patho (à noter = petits groupes, 8 vs 14 ...).
-   SANS VISION = ns (trend).

**Discussion =** Le biais contre de la PV peu affecté par le facteur vision, ce qui confirme que PV repose surtout sur signaux somatic et vestibulaire. Le fait que PV corrèle avec NSU et hémianesthésie va dans le sens d’une négligence des informations somatiques contralésionnelles, c’est à dire venant des « récepteurs graviceptifs du tronc » (Mittelstaedt).

-   Spéculation = intégration asymétrique des infos du tronc participerait à distorsion de la référence égocentrée qui sous-tend la verticale posturale et de façon plus globale l’orientation du corps dans toute les dimensions de l’espace.




### Rubens (1985) = effet CVS sur nsu visuelle
**contexte =** pour la 1ère fois, on essaie s calor froide chez nsu visuelle. S calorique active vor, vsr, qui devie gaze et head direction vers la gauche. on applique aussi Stim warm à gauche
**methode**=
- 18 rbd
- j +4/7
- pre/during/after5min S cal. 
- plusieurs éval = 
- gaze = suivre objet en mvt sur X. 
- pointer vers objets et gens
- lecture de mots
- line (albert)
- 14/18 non hlh. 
- jour 1 = a) Stim froide gauche - 30min - b) stim chaude gauche
- jour 2 = a) Stim chaude droite - 30min - b) stim froide droite
- nb = on empêchait les sujets de tourner la tête vers direction slow nystagmus.
**resultats =**
- globalement, déviation nystagmus, amélioration globale quand stim du bon côté. on remarque que amélioration + forte avec eau froide.
- au contraire, nsuempire quand stim chaude. 
- on note effet + de la stim sur alterness. rép + rapide. peut être car connexion avec la formation réticulée. 
- impression de vertige, faux mvts de l'ev. 
**conclusion =** étude princept sur effet + stim vb sur nsu visuelle. + tard, on montrera que effet sur hemianesthesie (vallar), mais aussi effet sur tache perceptive comme lb chez sujet sain, ou encore effet sur pcp de la posture (rode). ca va dans le sens de modulation de la représentation de l'espace, et pas seulement effet arousal. 


### Vallar et al. (1997a) = CVS améliore nsu personnelle. 

**Contexte =** Vallar 90 a montré amélioration nsu extrap + hemianesthesie tactile (équivaut à nsu tactile/perso) après S galvanique, ce qui l'amène à penser que processus commun (ref égocentré) qui empêche représentation consciente de l'espace extraperso + perso contralatéral. Mais des diss existent. **Donc ici on va S galvanique chez patients avec nsu perso pure.**

**Méthode =**
- 20 rbd + 11 lbd avec deficit somato +/- sevère
- j+14 (1-60)
- screening avec test de bisiach
- 3 eval = slight touch (tapping = on garde patient si deficit >30% des hits) + extinction tactile (dss) + pressure sensitivity (filament)
- nsu visuelle = line/letter/circle
- 14/17 rbd avait nsu visuelle + 2/11 lbd.
- cold water left ear
- eval tapping tactile pre-post-post30min

**resultats=**
- anova = side(lbd,rbd) * session montre effet positif S calorique sur detection touch en posttest immédiat chez rbd. pas chez lbd même si légère amélioration d'un point de vue descriptif. pas d'effet à +30. 
- 2/17 n'ont pas eu d'amélioration
- chez 2/3 rbd sans nsu visuelle, on a aussi amélioration. 
- enfin, on a regression de extinction chez 3 patients rbd, avec effet parfois +30 (2/3)

**discussion=** on a pattern d'amélioration qui est asymétrique, effet + cantonné à lésion droite. On retrouve large association de nsu perso et non perso aussi. cela montre que perception consciente des S somatosensorielle est asymmétrique, contrairement à la sensation. chez lbd, pas d'amélioration car le deficit somato reflère un deficit sensoriel. en revanche chez rbd on a à la fois deficit sensoriel + deficit perceptif. S calor améliore deficit perceptif (on a pas de perf plafond après S quand meme).    
Bisiach & Vallar 1988 reprennent idée de mesulam/heilman concernant rôle hemis droit dans traitement des 2 hemicorps !  
nb = les auteurs reportent aussi 2 cas avec nsu visuelle pure, qui ont bénéficié de la S.   
plusieurs théories abordées : 
- 1) activation (heilman) = problème arousal. heilman 78 = perte de rép electrodermale chez patient rbd vs lbd. comme latéralisation arousal dans HD, on a perte arousal dans les 2 hemiespaces, mais surtout dans le contra. vb S pourrait améliorer arousal. néanmoins interprétation critiquée, car ce qui importe ça n'est pas de S l'hemis droit, c'est la nature du signal vb ! par ex, warm water dans left ear aggrave le deficit !
- 2) VOR = n'explique pas recede of body neglect.
- 3) rôle vb dans orientation spatiale dans ref égocentré. ref égocentré construit à partir de signaux som/vis/vb. Voir howard 1982 = "push-pull" = balance, au niveau TC pour sys vb.  Hyp de vallar qui s'inspire de mesulam, c'est que RH percoit les 2 hemicorps, donc quand lésion rbd, j'ai imbalance dans integration mss. lien avec kinsbourne aussi. nb = association avec ataxie optique colle bien avec mismatch entre referentiel alloc (retino) et égo, qui explique erreur de pointage. idem avec la carte somatotopique qui mismatch avec carte spatiale ! "perceptual egocentric r", on a donc perte de cs de la partie gauche du corps. **S vb va créer un matching temporaire, en deplacant les coord spatiale du corps**.
**nb = hyp qui va dans le sens d'un déficit qui affecte image corps, mais pas schéma corporel...**


### Bottini et al. (1995) = CVS améliore hemi-anesthésie avec effet BOLD sur TPJ (s2-smg-insula) + putamen

**Etude de cas** (4 sujets sains et 1 sujet RBD)  
**Contexte théorique** = Sur le plan comportemental, effet des stimulations
caloriques sur la nsu tactile, "hemianesthésie" selon Vallar. Ici on examine effet cold calorique stimulation sur activité corticale BOLD chez sujets normaux, et on regarde aussi si effet chez sujet RBD.  

**Méthode 1=**  
- expé 1 : 4 sujets sains
- 12 scans, 6/12 avec stimulation tactile de la main gauche toutes les 2 sec pendant le scan. 6 scans rest closed eyes. 
- 2/4 sujets avaient cold caloric S avant scan quand toucher. 
- les 2 sujets avaient bel et bien diziness/vertigo et déviation tonique du regard vers la gauche (slow nystagmus).  
- expé 2 = chez patient RF, lésion droite qui touche SI, M1, et SMG, hemianesthesia et hemiparésie.   
- Patient dont on sait que S calorique permet perception correcte tactile. On regarde activité BOLD rest + S + washout. On fait contraste pour observer act spécifique quand perception consciente du toucher. 12 scans aussi avec alternance de toucher ou rien du tout. 
 
**Résultats** = 
- expé 1 = Quand toucher + vb S = activations putamen/PM/SII/IPL(smg)/insula.
- expé 2 = expérience cs du toucher quand S associée à act insula et putamen.
- rés présenté de façon plus anecdotique : déviation du référentiel égocentré (droit-devant yeux fermés) chez sujet sains. Mais les rés ne sont pas décrits...

**Conclusion** = tactile perception en intéraction avec le système vestibulaire. Cohérent avec activité de l'insula, qui le siège du PIVC, et dont l'intégrité est importante pour la perception de la verticalité. Vallar extrapole sur théorie égocentrée, avec théorie où touch imperception témoigne d'une déviation des coord du corps vers la droite, dû à un système neural distribué altéré, qui serait au coeur de la nsu. 




### Conrad et al. (2016) = effet de CVS sur connectivité fonctionnelle chez nsu aigue et chronique. plus de connexion operculum-hipp-cingulaire bil + operculum-ppc-occ + PPC-occ. + de poids des infos visuelles dans mss ! 

**Etude de cas**

**Contexte théorique =** 3 arg qui relient NSU et dys vb = latéralité nsu/ vb à droite chez droitiers + overlap lésionnel + effet + S vb. Question ici, est ce que récupération spontanée due à réorganisation fonctionnelle du réseau mss vb ? on regarde si cette réorg est similaire à activité observée après S vb dans phase aigue.

**Méthode =**

-   1 patient 62 ans, AVC MCA. diag = bell + albert test (line)
-   étude du sujet en phase aigue dans un premier temps à J+4, puis M+6.
-   saccades + lentes vers la gauche / optokin nystagmus (poursuite objet en mvt) réduit vers la gauche
-   IRM = **AG + IPS + V5 + TOJ**. nb = **pas de lésion insula ou operculum ou PPC**.
-   Stimulation = calorique, 30 s d’eau froide **oreille gauche.**
-   **Functional connectivity , seeds = hipp / parhipp + aires visuelles + operc par + ppc**
-   protocole =
-   On enregistre fc dans phase aigue (j+4) après stimulation calorique + après 6 mois. (même protocole). après 6 mois on a de + une mesure du resting state.
-   Après S calorique, on fait test Albert (barrage ligne) en phase aigue.
-   Seeds = OP2 bil, PPC, V1-2-5
-   On extrait time course de ces régions.
-   on analyse cluster > 26 voxels

**Résultat =**  
A) Acute stage
1) OP2 R seed = 
-   fc après stimulation calorique augmente dans **réseau bil t-p-ins.
-   A gauche = **stg, op2, pic**
-   A droite = **pic surtout (activations + petites pour op2).**
-   Bilaterally = **dACC, paraHIpp, Hipp**

2) OP2 L = idem à peu près
3) V1-2
- baisse de la fc interhémisphérique entre les aires visuelles D et G.
4) V5
- pas de connectivité avec op2 avant et après S. 
- Mais on a augm fc avec après stg L
- Avant et après on a + de fc avec PPC aussi (7a)

B) Chronic stage
1) circuit vb-vis
- Sans S, circuit op2-hipp-phipp-dacc connectivité ressemblait à connectivité stade aigu après S !
- Sans S, aug fc op2 avec PPC bil + v2-3 bil.
- Après S, on a au contraire baisse de fc dans circuits vb-vis.  
2) circuit vis
- Sans S, on aug fc v1/2 R avec hipp R, PPC 7a R, V4 R et IPL bil. 
- Sans S, aug fc V5 L avec OP2 L et PPC L, et hipp bil.
- Après S, il reste que fc avec hipp bil. 

**Discussion =** De façon globale, S calorique vb a augmenté com bil entre aires vb et a réduit com entre aires vis. S calorique a effet positif sur barrage de ligne. Cette fc a un effet pendant 20 min.  
Le fait d'avoir aug fc phase aigue entre op2 et hipp-dacc et non avec ppc suggère que S calorique a augmenté poids des afférents vb dans mss au détriment des afférents vis.  
De +, recrutement aires p-hipp fit bien avec recrutement ++ de circuits impliqués dans navigation spatiale-allocentric maps-world centred coord, capacité à se positionner et s'orienter dans l'ev. Mort 2003 avait trouvé lésion nsu dans parahipp regions (mais cas plutot isolé).  
En phase chronique, on a aug op2-hipp interH en resting state, mais cette fois ci on observe aussi augm de la connectivité avec op2-PPC, mais aussi entre aires visuelles et PPC.  Ce résultat va dansd le sens **d'une augm de la mss, avec + d'intégration cues visuels**.  
Le fait que fc op2-hipp diminue après S calorique suggère que platicité des nn davantage tunés sur cue visuels que vb. **Substitution sensorielle**. Hyp que S galavanique a induit sensory mismatch.

En phase acute, amélioration dans test albert pas dû à + fc entre op2 et PPC, mais plus probablement dû à augmentation du poids vb dans mss sous tendant orientation spatiale, **car augmentation com vb bil**. DE +, la baisse de fc entre les cortex visuels pourrait avoir entraîné moins d’inhibition du vc droit. **On a un peu deux mécanismes complémentaires ici. +6 mois, on a des ressemblances avec phase aigues = augmentation com bil vb (hipp, op, acc) au resting state. Mais à l’inverse, on a cette fois aug fc entre PO et PPC et VC bil ! mais diminue avec stimulation.

-   Hypothèse = avec récup, augmentation de la mss avec aires visuelles. Quand S vb, on a mismatch car les inputs vb sont mss par nature.


### Volkening, Kerkhoff, & Keller (2018) = effet nul de la GVS sur nsu à long-terme. 

**Contexte théorique =** Selon « transformation theory », mss vb-v-ppc intègre et transforme chaque signal en coordonnées centrées sur le corps afin de coder les différentes dimensions de l’espace (Vallar 1997). Effet reconnu de la GVS/CS sur symptômes NSU. Pas d’expérience cependant avec effet sur le long terme avec condition sham.

**Méthode =**

-   pop = 24 RBD droitiers avec **NSU ou suspiscion NSU selon BIT.** 17/24 dans groupe GVS = 17 GVS + 7 SHAM
-   subacute = M+~1/2
-   double blind.
-   Mesures VD (intra) à **posttest + 2 semaines + 4 semaines** 
-   VD NSU = BIT = cancellation + reading + copy + picture scanning = 17 subtests. => 3 ctg de NSU selon score.
-   VD tt egocentré = lines + letter + stars
-   VD tt objet centré = copy + writing
-   VD tt egocentré + objet centré = LB
-   VD multimodal tt = visuo-tactile research task
-   VD V = VV + HV = 50 trials per task ! = **moy abs + moy alg + range**

**Intervention =**

-   daily training session 20 min 5/7 * 10/11/12 sessions.
-   smooth pursuit training
-   en simultané = GVS/SHAM

**Analyse =**

-   handle missing data BIT = last observation carried forward (LOCF) = dernière observation remplace les manquantes. **Très criticable car sous-estime SD !**
-   missing value SVV + HV = baseline remplacée par moy baseline du groupe.
-   Approche ANCOVA avec VD ~ group + intra-measure.

**Resultats =**

-   BIT = ns total = pas d’effet principal traitement ni du temps ni interaction
-   visuo-tactile research = idem
-   Dans les 2 cas on a amélioration des scores dans le temps.
-   SVV = accuracy = effet principal du temps avec baseline != FU1 et FU2.
-   **A noter le biais contralatéral dans HV et SVV**

Discussion = résultat intéressant, on a thérapie smooth + GVS < smooth ou GVS tout seul d’ordinaire ! Manque de puissance, données manquantes +++ ... Cela dit, peut être ns attribuable à inefficacité GVS elle même. Dans d’autres études, pas de condition sham ou contrôle, seulement mesure online ... Pas de discussion sur effet smooth + GVS ... 


### Oppenländer et al. (2015) = GVS améliore nsu visuelle

**Contexte =** On veut voir si S galvanique subliminale peut améliorer composante égocentrée et allocentrée de la NSU.

**Méthode =** 
- pop = 24 rbd + 28 C (pas de gvs, juste pour data normantive pour tâches cliniques)
- M+2 (gros écrt type)
- 4 tâches : number cancellation / reading (que composante ego car pas de déficit sur les mots : pas de nsu objet centrée) / copy dessin / LB
- gvs = sham vs cl / cr. sur des sessions différentes pour éviter aftereffect. espace de 48 h entre cl/cr. 
- limite = session d'une heure. 

**Résultats =**
- effet + de la gvs subliminale sur toutes les tâches. cad que à chaque fois on avait soit cl soit cr qui était * différent de sham avec un effet +. 
- al => copy de figure + cancellation
- ar => reading + lb 

**Conclusion =** Résultat en accord avec la litté, cad que les 2 types de S gvs peuvent être bénéfiques pour réduire composante ego et allo. néanmoins, aucune tâche ici n'était purement allocentrée. la plus alloc c'était la copie de dessin. En accord aussi avec **Fink 2003** qui retrouve activation bilatérale après S vestibulaire. 





### Saj et al. (2006) = GVS améliore biais VV chez nsu

**Contexte =** effet de GVS sur SVV dans nsu.
**méthode =**
- pop = m+1-4 / 12RBD (dont 7/12 N+) + 8C / no PB
- diag = bell + gainotti + LB
- SVH
- bonne stabilisation. patients assis
- L et R GVS en intra-sujets
- 6 essais +/-45° +0°. X 2 sessions pour R/L gvs.
- **ici L GVS = anode right et vice-versa**. 

**Résultats =**
- ![](https://i.imgur.com/FZA2FzR.png)
- On voit bien que R LVS > RGVS en terme d'efficacité chez N+ ! la RGV au contraire aggravait le biais ! 
- On a effet similaire chez N- et chez sujet sains. 

**discussion = ** rappel = la cathode est inhibitrice = donc ça veut dire que inhibition du nerf vestibulaire gauche permet d'induire biais SVV vers la droite. Si on part du principe qu'il y a eu effet inhibiteur sur cortex gauche, alors + activité relative à droite et donc on réequilibre la balance ! Compatible avec effet excitateur ipsi de l'anode sur HD.


### Rode et al. (1998) = CVS améliore wba chez stroke. 

**Contexte =** Ici on argumente que wba est relié à shift de la RE vers le côté lésé. Du coup, comme stimulation sensorielle réduit shift RE (Karnath...), une stimulation CVS devrait réduire wba. 

**Méthode =** 
- pop = 3 * 15 sujets : RBD/LBD/control. **pop hemiplégique**
- pas de matching pour age avec groupe control, ni de contrôle stat sur matching taille lesion :-1: 
- de +, exclusion sujet pas debout pour 30s ! on exclut les patients PS ! :-1: 
- a noter que 9/15 rbd ont nsu, mais on connait pas diag ... 
- recrutement <6mois
- tâche : rester debout sur plateforme qui enregistre variation de posture avec poids dans axe devant/lateral. yeux ouverts. detail important car selon lafosse 14 ou perennou 00, stim sensorielle moins efficace yeux fermes. 
- design pre-post test. t-test. 
- CVS froide cote contralesion pour provoquer nystagmus lent 3s vers côté contralesionnel.

**Resultat des variations de poids/posture latérales=**
- baseline : rbd > autre groupes
- diff pre-post * rbd que lbd (mais baseline + pourrie)
- posttest : rbd = lbd = c ! 
- delta score : interaction * quand on compare rbd vs autre / pas avec lbd. 

**Conclusion =** Effet CVS sur posture. selon auteurs, petit effet periph avec stimulation TC avec inputs vb qui régissent muscles antigravité avec reflexe vestibulo spinal. mais aussi effet central car on sait que sur le plan cortical connexion avec noyaux vb, pivx etc. de façon floue, on peut penser que vb inputs impliqué dans cadre de ref spatiale qui determine contrôle postural. Souligne aussi interaction vestibulo-somesthesique. 



### Saj et al. (2013) = Déficit vestibulaire périphérique entraîne déviation/rotation du droit-devant = syndrome de Bonnier = hyperschématie

**Contexte théorique =** Atteinte vestibulaire unilatérale périphérique peut avoir conséquences sur le plan postural, oculomoteur, neurovégétatif, perceptif, et cognitif. Même si compensation centrale, on peut avoir quelques symptômes qui restent, pour mouvement de haute fréquence de la tête.  Qd stimulation vb, chgt perception du corps. Est ce dû à simple shift oculo ? ou bien effet cognitif ? Ici on investit cette hypothèse avec paradigme mesure de la SBA avec composante rotation et translation chez des patients avec atteinte vb périphérique, dans les axes H et V (frontal).

**Méthode =**

-   pop = 20 ménière = 9L + 11R + 10C
-   Contrôle = pas de nystagmus résiduel. **Y + 2**.
-   Controle2 = mouvement de la tête durant tâcheen 3D en position yeux fermé vs ouverts.
-   Contrôle 3 = questionnaire sub sur sensation posture et vertige.
-   apparatus = stabilisation tronc et tête
-   tâche d’ajustement visuo-haptique, avec barre visuelle à aligner
-   shift initial = rotation * translation = 18 essais dans les **2 plans**
-   Dans le plan frontal on rajoute une variable = **aligner le baton avec tête/tête-tronc/tronc**
-   **VI = group * initial direction of shift *ref).**
-   **VD = bias**

**Résultat =**

-   H plane =
-   translation * vers la droite pour L seulement vs R+C.
-   rotation ns.
-   effet principal shift initial (attirance) pour translation et rotation
-   V plane =
-   translation * vers la droite pour L encore, avec **effet tête > tête-tronc > tronc**. indep du ref, on **+15°, mais dans R on peut avoir shift à peu près de 5-10°donc la diff n'est pas si impressionante, SAUF pour le réf tête, où le biais est à 18° vs 3° !**
-   rotation * vers la gauche pour L seulement, pas en fonction de ref. mais bon, **à peine de 1°... taille d'effet très petite à mon avis.**
-   contrôle questionnaire + mvt angulaire de tête ns entre les groupes, bonne récupération.

**discussion ** = importance du sys vb dans self orientation dans l'espace. rs compatible avec pb de trajectoire de marche dans lésion vb pérph.   
nb = spéculation avec 'hyperschématie' => surR de l'hemicorps gauche, et donc shift du milieu (pas clair...). Rés majeur = inputs des labyrinthes gauche suffisants pour maintenir R fonctionnelle de la SSA. Suggère dominance left vestibules pour body R orientation. c en contradiction avec rés de Hufner, qui trouve dominance right vestibuale pour tâche de navigation et de mémoire spatiale...  
A noter que vb projette ++ vers H ipsi. Saj et al se contredisent aux mêmes... car si lésion vb gauche, alors diaschisis vers HG ... à l'inverse, si lésiion vb droit, alors impact sur HD... les auteurs essaient de se rattraper en disant que chez le singe c'est plutôt des inputs contra vb vers cortex qui dominent... tout ça n'est pas cohérent du tout...

 

 




### Ferrè et al. (2013) = GVS biaise BL

**Contexte théorique =** Le système vestibulaire, une fois stimulé, peut réduire la NSU. Mais le mécanisme derrière cet effet n’est pas clair. 2 hypothèses = boost attentionnel aspécifique ou effet spécifique sur la perception spatiale. On confronte ces 2 hypothèses chez le sujet sain avec une stimulation galvanique dans une tâche de LB en manipulant la distance (profondeur). La pseudo NSU s’inverse quand on augmente la distance.

**Méthode =**

*   Pop = 14 C droitiers
*   Apparatus = galvanic stimulation with anodal/cathodal currents. ANODAL = (-) ; CATHODAL = (+), effet inhibiteur vs excitateur.
*   LB = pointer avec un laser vers le milieu.
*   VI = **intra-sujet** = Rgvs + Lgvs + Shamgvs. Sham = sensation de picotement dans le cou.
*   VI = **inter-sujet** = Distance = 5 levels (30,60,90,120,150 cm)
*   VD = Méthode Judd avec intercept et slope individuelle dans chaque condition intra. Intercept = effet intra : slope = interaction avec distance.
*   Hypothèse = **on teste 2 contrastes = R+L vs Sham / R vs L.** Le 1<sup>er</sup> contraste teste hypothèse attentionnelle, le 2<sup>ème</sup> l’effet spécifique.

**Résultat =**

*   Pas d’effet de la distance en général
*   Pas d’effet * ns entre R+L et sham.
*   Effet * entre R vs L = quand anode à gauche, biais LB à gauche, and vice-versa.

**Discussion =** Ce résultat ne supporte pas effet aspécifique de la stimulation vb. Au contraire, effet spécifique sur la perception de l’espace, avec shift pcp spatiale dans l’espace péri-personnel/extra-personnel dépendant de la direction de la stimulation. Effet vb intra ou inter hémisphérique reste cependant à explorer ! Hypothèse = activation aires péri-sylvienne dont PIVC ipsi-latérale par rapport à la cathode.

### Oppenländer et al. (2015) = GVS biaise VV et HV mais pas PV
Contexte théorique = La perception de la verticalité est vu comme une ppté émergente d’une intégration multimodale, preuve = problème chez NSU à la fois dans modalité visuelle et haptique. On a réseau autour du PIVC avec STG, SII ... peut être que PIVC contrôle ces aires (voir ref là dessus, cf Lopez).

But ici = appliquer GVS subliminale pour biaiser perception de la verticalité dans VV et HV chez patients RBD. Comme ça, pas de biais attentionnel (bien que Farnè ait apporté des preuves contre cette hyp alternative). On regarde si réduction du tilt et précision dans VV et HV.

**Méthode =**

-   pop = 24 RBD ; M+2
-   patients divisé en 2 groupes selon perf comparée à groupe contrôle normatif (cut-off VV = 2 ; HV = 2.5) = Anormal/Normal
-   session 1 = sham ; session 2/3 = CR ou CL contrebalancé
-   procedure = 20min de stimulation puis tâche VV + HV avec main droite
-   diag clinique = hémiparésie + HH.
-   diag nsu = number cancellation + copie dessin + copie texte + LB
-   11 NSU + 13 RBD
-   8/24 + 9/24 pour VV et HV
-   stabilisation uniquement de la tête ...
-   VD = bias + range (min – max de tous les essais) + diff thereshold (moitié de la range pour chaque essai) uniquement pour VV.
-   10 essais par tâche
-   VI = group * session

 

**Résultat =**

-   *VV*
-   globalement, GVS augmente precision et réduit biais
-   group A a + de reduction du **biais ipsi** que groupe N, **de +, cet effet est observé avec CL/AR, cad quand mastoide gauche est excité vs sham (pas vs CR**).
-   **globalement, CL augmente precision quelque soit le groupe, pour diff thres et range.**
-   *HV*
-   tendance = CL a tendance a baissé biais ipsi (A) et contra (N) quelque soit le groupe. Mais on est moins précis par nature dans cette tâche.

**Discussion =** CL a davantage tendance a baisé biais et augmenter precision de la VV et HV. **+ important, la CL réduit davantage le biais ipsi dans le groupe qui a une VV anomale comparé au groupe qui a une VV normale.**

-   **A retenir = résultat va dans le sens d’une influence du système vestibulaire (via S GVS) sur des zones somatiques (SII) impliquées dans la perception de la verticalité (HV).**

**Limite =** effet plafond chez groupe normal ? Peut être pas justement, peut être même qu’on a une précision pas terrible, mais ça c’est dû au choix de la mesure de précision !!!  variable de type « precision » avec range et diff threshold pas terrible , car extrêmement sensible aux valeurs extrêmes ! Ici les valeurs « outliers » font justement partie des scores et ne peuvent pas être détectées. Comme faible nombre d’essais, pas de calcul SD possible ... Méthodologie pas super. Même remarque pour Utz, Kherkoff ... mais les résultats concernant les biais sont intéressants.



### Volkening et al. (2014) = GVS biaise VV et HV. effet opposé vers cathode à long terme.

**Contexte théorique =** Ici on s’intéresse à effet de la stimulation galvanique pendant et après sur la verticalité. Pourquoi ? car on a observé dans litté réponse nystagmus vers l’anode, mais réponse + tard vers la cathode.

**Méthode =**

-   pop = 10 sujets sains
-   VV + HV + PV. Position assise, sauf pour PV avec position debout.
-   18 essais par tâche.
-   GR ou GL balancée entre sujet (point faible de l’étude selon moi, pas de cb en intra ...) en effet on regardait le biais !
-   VI = time line =
-   expé 1 = **avant, pendant** (a noter que stimulation durait le temps de faire la tâche, en continu), après (**+3min**).
-   expé 2 = 11 mesures en tout espacée dans le temps, avant (1) pendant (5) et après (5) GVS pendant HV jusqu’à **20 min, + jusqu’a 20 min** en post (40 min en tout).
-   VD = accuracy + bias
-   expé 1 = on vi modalité * timeline
-   expé 2 = on a vi time (on groupe les modalités ensemble) **uniquement avec HV**.

**Résultat =**

-   expé 1
-   **bias** = interaction time * modality =
-   quand on compare baseline à pendant + pendant à après, effet GVS sur PV < VV + HV.
-   **accuracy** = effet seulement sur HV avec effet online + after-effects > VV + PV.
-   expé 2
-   principal résultat = 5 premières mesures != 5 dernières *.

**Discussion =** expé 1 = Comme attendu, on a swith vers anode dans 1 er temps en online, puis shift inverse vers cathode après 3min. Quand on regarde chaque modalité, c’était le cas pour pour VV et HV, **mais pas pour PV**. Spéculation = 1) voies verticales tt différentes, avec GVS qui n’en influe que une. Ou 2) suffisament d’infos soma/ppc pour que PV soit précise même avec stimulation vb. Malgré mismatch vb-pv, plus de poids accordé à PV. En tout cas, hyp supportée par rés avec HV quand on explore l’aftereffect sur une période de temps + longue.

**Limites = pendant PV, pas de contrôle de la vitesse de la roue pour éviter de stimuler le sys vb otolithique !! En tout cas ça n’est pas décrit ... DE +, pas possible de faire ce protocole avec PV et VV avec seulement 10 pp. dommage.  **

** **












### Utz et al. (2011) = déficit VV/HV dans plans sagittal et frontal

**Contexte** : En 2008 Pérennou met en exergue la nature multimodale du biais de la V chez des patients cérébrolésés RH en particulier. Le lien entre sévérité pushing et NSU découvert (Pérennou 2002), de même que lien entre VV bias et NSU (Yelnik, 2002, Kherkoff…). Ici on évalue ce lien dans plan **roulis ET tangage**.

**Méthode** :

-   16 NSU RH (6 tests classiques, **NET** + tests visuo-spatiaux Benton Line Orientation), 18 patients contrôles, 16 contrôles sains
-   NET = letter + number cancellation + LB + reading + letter + star + copie dessin
-   -   Tâche : HV et VV (-/+ 20°), méthode ajustement
-   VD : mean alg + absolute

**Résultats** :

1.  Erreur absolue

-   **NSU > contrôle**
-   Pas d’interaction entre groupe x plan/modality (HV,VV). NB = Les auteurs interprètent les effets simples quand même …

1.  Erreur algébrique

-   **Plane * group interaction**
-   Roll plane : ns NSU vs patient contrôle > contrôle = **biais contra** (problème puissance ?)
-   Pitch plane : * NSU vs autres groupes = **biais vers l’arrière**
-   MAIS effet global NSU > groupe patient
-   + corrélation entre les VDs quand on associe plan et modalité, mais un peu moins quand crossmodal.

**Conclusion** : On a bien biais contra (roll) ou arrière (pitch) chez NSU > contrôle dans modalité haptique et visuelle. Va dans le sens d’une atteinte transmodale de la V dans la NSU, ce qui va dans le sens des travaux de Pérennou 2008. Ref intéressante : Sakata 1997

**Limite** : **jugement dans pitch plane plus difficile > roll, on rajoute de la variabilité en +** …

### Kerkhoff & Zoelch (1998) = déficit VV

**Contexte théorique** = Cramon & Kerkhoff ont montré que déficit comportemental d'orientation d elignes dans le plan frontal était associé à patients RBD avce lésion PIC/PoC/SMG. On étudie possibilité lien systématique avec NSU cette fois ci car il s'agit de zones PPC lésées dans NUS selon Vallar & Perani (1986, critiqué depuis).

**Méthode =**
- pop = 13 nsu (10rbd+) / 14 rbd / 11 lbd / 12c.
- m + 6 
- pas de contrôle lesion size ici :-1:
- pushing ? :-1:
- Spatial orientation test = dans modalité visuelle = méthode des limites. le sujet a une range =(secteur où la ligne est considérée bien orientée) et une erreur constante (biais). ici pas de toucher. réponse verbale.
- tache H/V/Oblique, 10 T chacune. 
- chin rest. 
- total darkness

**Résultats =** 
- Contralesional shift dans plan frontal pour les 3 tâches d'orientation spatiale. idem pour nsu lbd. 
- de +, difference threshold (range/2) aussi nsu>. Plus de variabilité.
- les autres groupes sont équivalents, ce qui montre que ce déficit est propre à nsu.
- l'erreur non signée est corrélée à nsu quand score composite des 4 tests.
- A noter que aucune impression subjective de tilt n'est reportée. il y a anosognsie en quelque sorte. Compatible avec pushing. 

**Discussion=** 
- hyp 1 = overlap circuit PIVC/IPL, zones trè sproches , avec lésion qui touchent les deux. tant au niveau grey que white matter. 
- hyp 2 = circuit cortico-vb code orientation des axes (visuels ? surement) dans le plan frontal. il y a un réseau partiellement contingent à celui ci qui code orientation spatiale vers le côté contralesionnel. IPS par exemple pourrait être une interface entre attention et orientation spatiale. 
- hyp 3 = lesion size + forte dans nsu explique ce lien, qui en fait ne résulterait de lésion d'interface comme ips. mais ici on pense que association systématique et la fréquence entre svv et nsu n'est pas accidentelle. de +, Yelnik et al. ont montré que SVV est liée à nsu mais pas à taille de la lésion. 
- nb = lésion dans la nsu => témoigne de **anisotropy of visual space** ? anisotropie = propriété qui signifie = *être dépendant de la direction*.
**remarque =** étiologie SVV gauche == droite quand first week, mais quand chronique lésion droite donne svv > alors que lésion gauche récupère. ca peut expliquer pq les papiers qui étudient svv très tôt ont des profils lésionnels symétriques, alors que d'un point de vue cptal svv et rbd + associé à long terme, comme pour la nsu, ce qui donne argument de + pour une asociation systématique.

**ref** = teuber 1954 






### Pérennou et al. (2013) = quelques repères sur les déficits de verticalité

Article de revue : **pourquoi et comment mesurer la verticalité chez des sujets cérébro-lésés ?**

**Résumé **: risque de chute ++ si problème de V. Notion de pushing. Si problème **TC (Wallenberg syndrôme**) on peut avoir problème de V visuelle mais qui en fait **ne résulte pas d’une modification de R de haut niveau** (**car PV et HV ok** ! tâche contrôle). Pour modifier modèle interne, il faut H stroke, avec tilt généralement **contra-lésionnel qui corrèle avec sévérité de la latéropulsion** (Pérennou, 2008) et fait davantage suite à lésion RH. Il faut contrôler avant des problèmes de tonus moteur, faiblesse, spasticité d’un hémicorps … etc pour identifier l’origine de la latéropulsion. **Le patient cérébrolésé est utilisé comme un modèle pathologique de la construction de la verticalité**. Le modèle interne de la V a des propriétés que l’on étudier par inférence chez le sujet lésé, eg chez le **NSU** (Pérénnou 2008) ou l’**hypoesthésie** (Barra 2010). Ca nous renseigne sur contributions des modalités VS,VB, et soma dans la V. Hypothèse ajd que **NSU comporte une négligence graviceptive, c’est-à-dire une négligence des gravicepteurs somesthésiques provenant d’un hémicorps** ! ici stimulation galvanique pas forcément efficace pour réduire pushing, puisque pushing serait surtout lié à problème PV, et donc un problème soma (Pérennou 2002 (pelvis mal aligné), 2008 (corrélation +++ entre pushing et PV), Barra 2010 (importance intégration soma dans VV et HV bias et variability)). D’ailleurs on peut réactiver circuits soma endommagé par TENS avec résultat intéressant sur ltp.





### Arshad (2017)

Opinion paper : ***Dynamic interhemispheric competition and vestibulo-cortical control in humans; A theoretical proprosition***

a) Introduction
- hypothèse que vb mss nn traité par aires T-P, avec convergence vers TPJ.
- overlap vb reseau avec spatial attt.
- dominance H droite.
- head et eye mvts sont les substrats moteurs de l'attt spatiale. implique le colliculus supérieur.
- pas de vb primary area, car mvt de tête sont toujours multisensoriels.
- Argument clef de cette revue : vb cortical processing facilite processus top down, pas que bottom up ! De +, on a compet' interH qui serait commun à attt spatiale et vb cortical processing.

b) InterH competition
- heilman vs kinsbourne
- ici l'auteur fait analogie avec lésion unilatéral vb TC : on a nystagmus et trouble de la posture, vers côté ipsilésionnel, à cause d'une asymétrie du *tonus vb*. ocular tilt/skew deviation.

![](https://i.imgur.com/xHBmZSJ.png)

- de façon parallèle, on peut imaginer un *cortical tone*
- or on sait que attt spatiale, pseudoneglect, reflète biais.
- circuit fronto-parietal va médier vb functions telles que : *orientation spatiale, postural control, verticality perception, top-down vb control sur TC nystagmus.*
- overlap avec attt spatiale logique selon auteurs car mvt tête est substrat moteur de l'attt et doit être tt pour adapter gaze.
- nb = l'auteur préfère *inter-H account* de kinsbourne plutôt que *hemi-spatial theory* de heilman. en effet, biais interH va avoir influence TD sur velocity storage mechanism dans TC.  

c) Influence on dynamic interH competition upon higher order vestibular function  
c1) Spatial orientation and self-motion perception  
- Afin de percevoir chgt angulaire de notre position dans l'espace, on a besoin que vb head velocity signal soit transformé en heading signal, via path integration.
- **TMS appliquée sur PPC droite venait empêcher encodage head angular signal vers la gauche dans tâche de navigation ! et sans que ça soit du à nystagmus VOR. NB = effet le + fort quand TMS sur HD > HG. A noter *velocity signal* pas touché, que path integration (synonyme de heading ici ? heading updaté ?)**
- **Quand lésion TPJ, on a aussi sous-estimation temps et distance quand rotations vers la gauche, mais pas vers la droite !**
- *à lire : Seemungal 2008 ; Kaski 2016*  
*--> ici on peut donc considérer trouble de l'orientation spatiale quand lésion virtuelle ou réelle (tpj) de l'HD*

c2) Verticality perception  
c3) Postural control  
- lateropulsion : interprétation motrice (tonus) ou spatiale (réalignement).
- plus liée à HD lésion. là aussi, on peut avoir interprétation en terme de balance.
c4) Perception du self
- lésion TPJ = déréalisation, out of body experience
- lire Blanke ici.  
c5) Anosognosie, cognition numérique, pain perception, social cognition ...
- Effet vb S sur number bisection task peut être en lien avec LB task ?
  
d) Relationship between spatial neglect and vestibular mechanisms
- arguments : biais position head + eye spontané vers la droite dans la dimension H de l'espace
- overlapping neuro-anat avec attt spatiale
- hyp cependant critiquée, car Corbetta et Shulman 2011 mais en avant importance d'un hypofonctionnement d'aires dorsales dans la nsu, alors que les lésions sont plutôt ventrales. Or, ce sont davantage les régions dorsales qui sont impliquées dans attt dirigée, gaze exploration, et codage égocentré ! 
- reflexion personnelle : néanmoins, il faut donner sa chance à cette théorie vb de la nsu, car la nsu est de + en + vue comme un pb de déconnexion. De +, on sait que carte topographique de l'espace tt par zones hipp. Quid decnx avec hipp ?
- La déviation du droit-devant vers la droite, même en translation, peut-être liée par pb vb ! en effet, Saj 2013 montre que droit devant est shifté vers la droite dans plan horizontal et frontal chez patient avec antécédent de Ménière disease gauche ! 
- On a aussi effet des S vestibulaires sur nsu, à la fois sur deficit H et V. 
- Enfin, litté a montré que lésion fr-par venait modulé le **low level brainstem-mediated VOR**. sur ce dernier point, voir *Dorrichi, 2002 ; Ventre-Domniney, 2003*. Dorrichi par ex suggère que biais d'orientation vers la droite pourrait être causé par VOR déficitaire.

### Dakin & Rosenberg (2018)

*Revue de littérature : **Gravity estimation and verticality perception***
- gravity : force qui contraint notre perception et nos interaction sphysiques avec l'environnement. Il est central d'avoir une bonne représentation de la gravité puisque nous sommes bipèdes. 
- gravity estimation : estimation de la direction du vecteur gravitationnel. 
- verticality : orientation parallèle à la gravité.   

1) Modeling verticality perception using cue integration theory

- nous devons en permanence estimer l'orientation de notre corps par rapport à G. 
- il faut qu'on ait une R de notre deviation vs G. estimer le tilt est un problème d'inférence statistiqu qui peut être décrit par une distribution de probabilité. 
- **lk : proba d'avoir un tilt x sachant mes capteurs sensoriels.**
- Or nos capteurs sensoriels à eux seuls donnent des signaux ambigus, il faut les combiner avec d'autres types de capteurs pour désambiguiser le signal (eg : vection, train...). 
- regle générale : le produit de deux capteurs est + précis > un seul. 
- **cue integration** : description de comment j'intègre capteurs.

2) Mesure de la perception de la verticalité : **the subjective vertical tasks**  
Important de noter que ces tâches diffèrent et donc vont nécessiter une intégration de signaux sensoriels différente à chaque fois. Certaines tâches vont favoriser tel cue vs autre cue etc  

i) SVV
- rely +++ on **gravitational cues** + soma cues, mais suppression des soma cues ne fait que baisser la précision de la svv. 
- intéressant de noter que la SVV repose aussi sur ce qu'on appelle le **vecteur idiotropique=axe Z**. Cette contribution du corps permet de diminuer la variabilité du systeme vestibulaire quand les tilts de la tête sont petits. 
- cue integration du **gravitational vector = GV** et du **idiotropic vector = IV** peut être modéliser et donner lieu à l'effet Aubert. + j'ai un tilt fort, plus la précision du GV est faible et donc plus je fais confiance au VI.   
ii) SPV  
- ici aussi on se repose sur cues gravitaires + somato (mais on ne parle pas du tout de VI ici, ça fait débat, mais le VI pourrait être utilisé par le systeme de verticalité pour se réorienter. Si dans nsu on a shift de la VP, c'est que signal VI fonctionne de façon biaisée).
- selon perennou/barra 2013 on a peut etre une graviceptive somesthésique et ET gravitaire.   
iii) multsensory integration  
- si lesion, eg du systeme vestibulaire, j'induis un biais dans le traitement des infos vb, cad dans le calcul du lk. d'ou biais à gauche. le shift contra peut être comme une integration mss biaisée. 

3) Achieving gravity-centered visual perception
- Malgré le fait qu'on encode des signaux codés dans des cadres de références avec des coord égocentrés nous percevons le monde exterieur par rapport à une référence allocentrée, qui est la gravité. 
- malgré le fait que eye on world signal puisse ne pas être à la verticale (eg head tilt) on percoit tjrs le monde comme vertical, car transformation de ref eye on world en gravity in world. **gravity-centered reference frame**. 
- v1 code inputs retiniens en coord egoc. mais des que integration dans cortex parietal, le signal visuel va être codé par rapport à la gravité. 
- G FRAME retrouvé dans nn CIP = **caudal intra-parietal neurons**. interessant de noter que quand corps/head tilt, ces nn font un compromis entre VG et VI, cad entre cadre de ref egocentré et allocentré.
  
--> Orientation spatiale est donc traitée à niveau intégration plutot par que visuel.  

3) Désambiguiser l'orientation vis-à-vis de la gravité relativement aux acc linéaires.
- les otoliths donnent un signal ambigu, car rép à rotation et acc de la tête.
- otoliths donnent **GIF** => sum de force gravité + inertie, *gravito-inertial-force*, afin de nous donner le **GIA** => *gravito-inertial acceleration*.  
- sag et lag => sag = rotation / lag = acc lineaire

 ![Uploading file..._o46n5sv84]()

- or on peut desambiguiser ce signal avec signal csc qui donne rotation de la tête, avec cue *angular velocity*. on a aussi signal ppc neck + copy efference.
- On a donc estimation Ghat (gravité par rapport à head) 
- Ahat (accélération de la tête) = Ghat - GIF.  
--> ***gravito-inertial force hypothesis***
- repose sur nn TC et cv, et adn.
- ce modèle explique pourquoi vection (roll rotation de la scene visuelle) agit sur nystagmus up. On a conflit entre infos otolith et csc, avec au bout une illusion d'accélération (Ghat - GIA)
- explique aussi l'effet des acc linéaires constantes sur la perception de l'orientation.
- effet bien connu => tilt prolongé de la tête implique sensation tjrs + forte du tilt de la tête => ***somatogravic effect*** = **Tendance à percevoir des forces gia constantes comme étant une inclinaison de la tête plutot que acc linéaire***. peut affecter orientation de la tête pendant vol.
- effet somatogravic expliqué par updating du modèle interne vb. on a gia qui est pullé vers Ghat. car Ahat est reis à 0 à chaque fois (prior).

**Ref** : Gibson 

### Brandt & Dieterich (2018)

Revue de littérature : ***Thalamo-cortical network: a core structure for integrative multimodal vestibular functions***

a) Introduction
- Le Th n'est pas un simple relais sensoriel, mais aussi un centre intégrateur = **Thalamic integrative hub**.
- **connector (global) vs provincial (local) hubs**

b) Structure of the vestibular thalamocortical system 
b1) Th hubs
- cognitive functions assurées par multiples réseaux modulaires avec différents types de hub. 
- Modules organisés en hubs provinciaux qui organise les *within-network* connections. 
- Interactions entres différents modules assurés par connector hubs (*between-network*).
- Ici on défend idée que th subnuclei peuvent être provinciaux + connector !
b2)Str connectivity of the vb nuclei, th, and cortex

![](https://i.imgur.com/gZHNefP.png)

- 5 grandes voies vb bil : 
- 3 ipsi / 2 con###tra (cross dans TC)
- ipsi : 2/3 passe par p-l th/ 1/3 va direct vers pivc.
- contra : 2/2 passe par p-l th
- on a nn TC important pour acheminer angular velocity information toward hd cells dans formation hipp. 
- nb = hyp de brandt je crois : lésion angular velocity nn = sustained vertigo ; lésion head cells = sway/dizziness
- hd = hyp compas 3d + importance mss pour expliquer hd quand mvt actif + ring attractor hyp (velocity rotation --> hd).  A noter que th vpl nn préfère mvt passifs > actifs chez macaques.

c) Functions and disorders of the vestibular thalamocortical system
c1) Developmental and functional advantages of two thc networks operating separately in the two H.
- si 2 circuits, alors spécialisation fonctionnelle possible.
- chez droitiers, dominance rh vb. **Hyp que durant le dév on a dév de 2 "compas 3d", un égocentré, pour exploration motrice (liée à handedness), et un allocentré, pour orientation spatiale (liée à loc du self dans l'espace).
- hyp congruente avec distinction H local/global.
- A lire : **Kirsch V, Boegle R, Keeser D, et al. Handedness-dependent functional organizational patterns within the bilateral vestibular cortical network revealed by fMRI connectivity based parcellation. Neuroimage 2018**

c2) Clinical vestibular thalamocortical syndromes
- lésion infratentoriale (cv et TC = *fosse postérieure*) : labyrinthe / nerf 8/ vbn / lower TC-pedoncule cv => nystagmus spontané + vertige rotationnel prolongé
- ces symptômes sont très rares quand lésion supratentoriale (cortex et sous cortex, eg ventricules). seulement 10 cas de vertiges après lésion mca. on pense que compensation centrale, grâce à H sain qui a intégration vb-visuelle optimale. la pcp des mvts et de la position du corps reste bonne.

c3) Disorders of higher vestibular function
- pushing syndrome : tilt du corps dans l'espace. origine/mécanisme assez controversé. lésion th-pic
- room tilt illusion et nsu : hyp ici : **disturbance de la connectivité entre les sytèmes visuels et vb de haut niveau**. Mais bon cette hypothèse ne s'appuie pas sur grand chose d'un point de vue causal...
- evidence globale pour lien entre vestibualr hight order function et nsu : système vb impliqué dans **body représentation, spatial orientation, spatial memory**. **Or on sait que on a distorsion schéma corporel dans nsu, que vb stimulation peut moduler tâche de verticale, line bisection, et même posture. En revanche, faire le lien entre un biais du compas 3d et le biais nsu reste hautement spéculatif !! En effet, ça revient à voir nsu comme un biais de navigation spatiale, cad biais constant dans estimation de la position et de l'orientation du corps dans espace par rapport à coord world-centrées**.
- enfin, on a aussi arg anatomiques, mais là encore c pas forcément très clair vu hétérogénéité des lésions** 

### Cullen & Taube (2017)

Revue de littérature : ***Our sense of direction: progress, controversies, and challenges***

a) Sense of direction versus a selef motion-detection system
- sens of direction : capacité à enregistrer endroit où nous nous déplaçon relativement à l'endroit où nous étions, grâce à K de notre position et orientation dans l'espace.
- selef motion : vb/vis/pcp.
- vb encode notre self-motion dans 6 dimensions (voir ddl) = rotational head velocity + translation head acceleration
- ce signal en 6d permet stabilité position gaze et body grâce à plusieurs reflexe : ocular / cervical / spinal
- au delà de ces reflexes de "bas niveau", on a message envoyé par fibres asc au cortex.= **thalamo-cortical pathways**
- *anterior pathway* = a-d th --> retrosplenial cortex --> erc = **encoder sens de direction quand navigation ou memoire spatiale**
- *posterior pathway* = vb nuclei --> v-p th --> s1/pivc (voir Guldin & Grusser 1998) = **detection de self motion et perception (verticalité)

![](https://i.imgur.com/f0QXToE.png)


b) Classic head direction system
- HD cells = presubiculum => compasrepond à heading dans plan H.
- chaque HD est tunée vers une direction.
- HD dans systeme limbique et TC
- quand vb lesion bil = perte de hd signal dans adn (a-d-th).
- hd cells use more autre signaux que vb quand translation/rotation passive.

c) ring attractor network 
- HD signal a 2 composantes : 1) signal de direction (from self mvt cues) ; 2) ancrage de ce signal dans un cadre de ref. 
- circuit ici = vbn --> TC nuclei (voir schema) --> adn qui contient "classic" hd nn.--> striatum/P/erc/subic
- théorie dominante = pour updater head dir, il faut angular head velocity signal (csc) envoyés à nn TC
- modèle computationnel rép hd nn = *ring attractor* used by nn qui recoivent angular velocity signal. hd cell avec dir opposée s'-. 
- ring ici = rotatio plan h yaw. on a pic d'activ a un endroit part du ring.
- si lésion nn TC => perte de signal. 
- si lésion des afférents, on a ring intact, mais perte de updating, **perte d'ancrage**, perte de "reset" du pic d'act (**activity hill**). En effet, c'est ce qu'on constate quand lésion TC en amont des nn tegmental et mamillaire.

![](https://i.imgur.com/DBUj7HP.png)

d) The vb systeme integrates multimodal information and motor signals
- vb sys utile pour updater head signal. mais il fait davantage ! en effet on a **intégration d'infos commandes motrices (eye/head/neck), ppc, visual inputs dans les noyaux vb.**
- **VO** = *vb only* nn. ou *non eye mvt*. = **posture + self motion pcp**. reflexe vb-spinal. connexion corticale et cerebelleuse.
- **VOR** = *vb-ocular reflex* nn. nystagmus.
- VOR sont en majorité **PVP** = *position-vb pause* nn. = rep quand mvt passif des yeux et de la tête.
- VOR sont en minorité **FTN** = *floccular target nn*., car afference du cervelet. Complémente rôle de PVP.
- Au niveau du nerf VIII, on ne fait pas de diff entre acc et rot passive/active. mais ça sera le cas au niveau thal.

e) Preferential encoding of passive versus active self motion in the vestibular nuclei
- VO act (posture vb signal) est supprimé quand mvt volontaire.eg =active head mvts. de + cet annulation est là uniquement quand matching entre neck ppc signal et efferent motor copy.
**--> Très utile de supprimer ce reflexe quand mvt volontaire, car reflexe utile quand chgt de posture inattendu !**
- nb = vo nn vont vers vp th aussi. ça veut dire que mvt actif atténue la perception de self motion. mais pb du coup : *car si signal hd est très diminué quand volontary head mvt, comment heading navigation fonctionne t-il ?*

d) Conundrum : building HD signal during active head mvt
- possible que signal extravb de self motion soient envoyés aux ring nn ! circuit hd. 
- eg = neck ppc signal dans VO passif ou active.
- autre possibilité : on a un *reweigting* du signal vb hd partiellement annulé ! quand on a active mvt, on pourrait combiner vb avec autres signaux.
- Mais du coup, quels circuits assurent ce reweighting ? ou mss ?

f) Eye-mvt information in the HD network ?
- VOR nn : head et eye motion qui projettent vers centres oculomoteurs pour contrôler nystagmus.
- VOR encode ces infos en fonction de gaze strategy. eg : si saccade, suppression act PVP. FTN carry vb et eye saccade. Donc VOR transmettent inputs dépendants du gaze à hd ?
- oui ! on a VOR qui projettent sur nucleus prepositus qui appartient à circuit hd. le np est un centre d'intégration oculomoteur qui shape eye mvts. C'est donc possible que ADN recoivent inputs visuo(gaze)-vb(angular).
- de +, np projette aussi sur presubiculum. *du coup, hd activity est-elle témoignage de head direction ou gaze direction ?*
- spatial view nn : rep quand une certaine partie de l'ev rentre dans champ visuel singe. --> dans hipp. dans erc on a cell qui réponde à S vb visuel mais eye centré.
- Chez le singe on a donc des nn qui code hd signal dans coord allocentré et eye centrée dans région hipp.

g) Rapid updating of a forward model underlies the suppression of vb signals during active mvts.
- source de l'annulation act VO ? ***Modèle forward = sensory expectation du motor outputs comparé à actual fb.*** 
- si il y a *matching* il y a suppression.
- ce méc dépend intégrité du cervelet.
- quand on contraint/bloque nck mvt singe, on a d'abord rép des nn vbn comme si mvt passifs, puis on a réadaptation rapide au bout de 40/50 mvts de tête, ce qui montre adapatation du forward model.
- cette flexibilité du modèle interne est importante pour switcher entre plusieurs ev (eg, VR : conflit visuo-vb).

h) Implications for navigation in immersive environments
- pb = cadre de réf du jeu vidéo (de l'avatar) et du vrai monde autour. hd signal codé dans quel cadre ? loca ou global ?
- dans erc on retrouve les 2 types de codages.

i) HD cells in 3D and the vb system
- HD cells in presubiculum bats 
- hd cells maintient même act même la tête à l'envers.
- hyp 1 : vertical plane est une extension du plan h. 
- hyp 2 : *mosaic hyp* : utilisation de 2 règles pour adapter heading : une rule pour *yaw rotations* et une rule pour *roll/pitch rotations* autour de la gravité.
- **shinder & taube** : rat heading cells act enregistrée quand plusieurs 3d config. l'axe graviatire modulait l'act hd. 
- idée que heading codé au moins partiellement dans axe gravitaire fit bien avec effet délétère lésion otolithes sur hd act. fit bien aussi avec act ADN nn pour roll/pitch planes orientation. Voir =   
**--> Laurens, J., Kim, B., Dickman, J.D. & Angelaki, D.E. Gravity orientation tuning in macaque anterior thalamus. Nat. Neurosci. 19, 1566–1568 (2016).**
- enfin, act vbn atténué aussi quand otolith signal volontaire.  
**--> travaux prometteurs sur signal d'orientation en 3d durant navigation**

j) Outllok and conclusions
- La navigation requiert sens of direction, hd, mais aussi place et grid cells. 
- retenir que le syst d'orientation s'adapte facilement (forward model).
- conundrum sur utilisation mss par circuit hd. 
- **enfin, mystère sur interaction entre voie ADN et VPN au niveau cortical ?**

### Chen, DeAngelis, & Angelaki (2018)

**Contexte théorique =** Dans quel cadre de référence codons le heading direction signal provided by the vestibular afferents ? Navigation spatiale liée à coord egoc, mais aussi alloc, avec activation de la formation hipp. Ici on va + loin que Chen 2013 en dissociant pas seulement head et eye signal et head et body signal, mais aussi *dissociation body from world*

**Méthode =**
- 2 macaques, idem que Chen 2013. electrodes intracranienne + stimulation vb optic flow. act nn vip enregistrée.
- calcul displacement index et gain field. 
- 2 conditions diff ici = 
- body fixed gaze = congruence entre cible laser et eye/head/body 
- world fixed gaze = incongruence eye vs head/body.
- si tuning curve shift des nn vip absent quand wfg condition, alors on a nn centré sur w. 

![](https://i.imgur.com/6OlksGD.png)

**Résultats =**
- vip = di 0.9 et 0.4 pour body centred et w-centred conditions. On a donc R intermédiaire entre world et body centrées maps.
- 30/60 vip nn = head , 2/60 = intermédiaire, 0/60 world pur. 
- --> **montre que map est flexible dans vip**.
- fitting von mises pour modéliser gain filed (modulation rep nn par head/eye position= gaze). on observe corr des gain field eye centred dans les 2 conditions, ce qui suggère que body in world signal module aussi rép nn (body gain field ?)

**Discussion =** On confirme bien que vestibular heading signal traité par vip est ancré dans coord bodycentrée, car rép de ces nn est invariante avec chgt position gaze/head (voir chen 2013). Ici on démontre aussi que codage worldcentré existe dans nn vip.  
**hyp = témoigne d'une flexibilité avantageuse dans la navigation spatiale, être capable de switcher entre repère alloc et égoc. Peut être que gaze/attt est le facteur clef ici**.

### Chen, DeAngelis, & Angelaki (2013)

**Contexte théorique=** de façon générale, systeme vb utile pour transformer infos de la tête et du corps dans spatial frame body-world centred. Aires MSTd/ PIVC/VIP recoivent ++ inputs vb. Challege pour le PPC = combiner info des diff afférents qui ont leur propres spatial frame, head (vb) ou rétina (vision) par ex.   
A noter que *Cohen & Andersen (2002)* font hyp qu'il faut un cadre de référence commun. Mais la litté va plutôt dans le sens de != spatial frames flexibles (voir Chen 18).  
A lire : **Bolognini & Maravita (2007) / Renzi (2013)**  
nb = les auteurs extrapolent un peu avec nsu , en mentionnat qu'il pourrait s'agir d'un déficit de cadre spatial egocentré basé par sur les inputs vb ...   
Ici on fait varier head par rapport à eye coord et head par rapport à body coord en enregistrant réponse de nn chez le singe dans aires mstd/pivc/vip quand on simule vestibular accelerations avec visual optic flow.  On étudie les différent spatial frame = eye/head/body associés à inputs vb (car mst/pivc/vip recoivent ces inputs vb).

**Méthode =**
- 2 singes, intracraniens.
- apparatus avec tête et gaze dissocié (plusieurs combi avec shift -20/0/20°).
- **tâche du singe =** 
- 1) tourner sa tête dans plan horizontal pour aligner le laser de sa tête avec laser extérieur. Puis dans un second temps on a 2ème target laser, qui doit être atteinte avec les yeux (oculomètre ici).
- 2) **On soumet singe à fake optic flow pendant 1s (optic flow en translation dans plan latéral et sagittal) = simulation de optic flow avec des random dots.** 
- nb = on testait en amont si les nn des 3 aires ciblées répondaient à ces stimulations.
- On analyse tuning curve de chaque nn, voir sa variation en fonction des combi head/eye. Indice "displacement index" permettait d'avoir cette info en regardant si overlap avec 0 ou 1 en fonction des cond.
- cond = eye vs head / head vs body

**Résultats =**
- vip = DI proche 0 dans les 2 conditions = nn plutôt body centré
- pivc = DI 0.10 et 0.60 pour e/h et h/b. représentation intermédiaire entre head et body. 
- mst = DI 0.4 / 0.8 = entre head et eye-centré.
- Quand classification des nn, on a 40/33/0 % body centré respectivement / 9/6/13 % head centré / 0/0/2 % eye centré.
- analyse gain field = modulation de l'act des nn par la position du regard. ici on regarde gain field pour eye and head selon condition. = ns à chaque fois. 
- par contre, on a variance des gain field !=. mst > pivc=vip.

**Discussion =** vb heading tuning codé dans spatial frame body centré dans vip, head-body dans pivc / head-eye dans mstd. le heading vb signla, qui donne azimuth direction, est codé différemmentdans corte par, peut être en fonction de la tâche en cours.  
gain field =  mst modulé par gaze. idem pour vip mais moins.  
**--> important de pouvoir transformer vb head coord en vb body coord, pour coder les chgts de position de la tête par rapport au corps.**  
**--> hyp = efferent copy mvt tête + pcp neck pourrait permettre cette transformation !**.  
**--> Evidence pour transformation de head à body centrée vb coord via circuit cérébral pivc-vip**
**--> débat sur multiples cadres de réf = serait utile pour avoir représentation multimodale unifiée du heading direction qui serait indépendant de la position du regard ou du tronc! Si pas de cadre de réf unifié, alors les traitements de l'espace par modalité sensorielle seraient très discordants ! car chaque modalité a son cadre de ref privilégie ! Pourtant la litté indique des spatial frame intermédiaire ! par ex ici on a mst qui code vb signal dans coord eye-centrées ! Ici 2 interprétations :**  
1) R hiérarchiques 
2) permet d'avoir connexion bid entre airs mss et aires unimodales, avec mss utile quand il y a du bruit. rés de Chen 13 plutôt dans ce sens, eg avec mst qui répond + à S vis > vb.

### Duhamel, Bremmer, BenHamed, & Graf (1997)

**Contexte =** VIP reçoit infos visuelles de MT, infos vis de mouvements. Réponse de vIP est multimodale, et sensible à direction et vitesse du S visuel. VIP aussi connecté avec région premotrices, pour préhension, oral préhension, coord main/bouche (logique car nn soma visage dans vip).   
Au niveau occipital, on a nn avec RF ancré sur rétine. Néanmoins on a d'autres nn au niveau ips par ex qui ne sont pas ancré sur rétine, et vont répondre par rapport à variation mvt bras par ex.   
Ici on souhaite dissocier eye centré de head centré nn dans VIP.

**Méthode =**
- 2 singes entraînés à regarder diff cibles, avec tête **fixe**. Ainsi, on peut voir si certains nn vont décharger indépendemment de chgt de regard. On dira qu'ils sont head-centré.
- ici on regarde spike de nn et on en fait une moyenne. on regarde si ce nn répndait le plus dans un certain champ visuel, dans dim h et v.
- 9 loc visuelles. 
- nn intracraniennes.
- on contrôlait pour activité gaze en baseline.

**Résultats =**
- On voit que certains nn sont relativement centré sur la tête, mais avec préférence pour une certaine position h ou v. 
- En effet, on peut avoir un nn qui est head-centré quand exploration cibles sur même continuum vertical, mais qui devient rétino-centré quand cibles visuelle varie dans axe verticale ! On peut dire qu'on a nn head centré qui code l'élevation de la target visuelle.
- cross correlation with each nn sur les 9 maps. 
- head centred nn : les cartes sont toutes superimposed.
- eye centred nn : on a shift entre 0 et 1 pour shift v et h.
- clustering montre qu'on a 2 pop : head centrée et head-eye-centrée. 

**Conclusion =** Continuum de neurones head or eye-centré dans vip. Intéressant de noter que futures études de Chen 14/18 montreront que VIP contient en fait des nn head, body, et world-centré. Je me demande si ici on ne mesure pas en fait des nn allo-world centré.
### Sakata, Taira, Kusunoki, Murata, & Tanaka (1997)

**The parietal association cortex in depth perception and visual control of hand action**

**a) Introduction**
- historiquement Balint découvre que PAR impliqué dans stéréopsie (perception de la profondeur) et guidage visuel de la main. patient avec difficulté perception de la profondeur et des orientations visuelles.
- patient lesion oc-t ou p ont X stereo
- apraxie 3d apres lesion bil par. 

--> **Proposition de l'article : le cortex Par rassemble infos visuelles et non-visuelles pour construire R de la profondeur utilisée par le systeme moteur des mouvements de préhension** (*"goal directed hand movements"*)  

**b) Visual fixation neurons and the discrimination of distance**

- IPL a neurone sensible a fixation (VF) ou mouvement (VTracking, avec preference pour mvt dans plan frontal et sagittal (profondeur) et orientation
- res classique : VF 7a et VT MST
- **VF** ont preference dans les 2 axes. et prefere near space en majorité. 
- hyp que ces neurones recoivent signal musc/pcp de convergence-accomodation/visu. 
- point important : efference copie vergence joue enorme role dans sensibilité aux profondeur (voir gnadt & mays, 95). 

**c) depth-movement sensitive neurons**

- **VT** + impliqué dans encodage cible visuelle en mouvements en profondeur. 
- sensible a taille var du S. 
- **disparité binoculaire = décalage horizontal d'un objet sur les 2 rétines**. 
- Ici on manipule disparité et taille. neurones **DMS** (*depth mov sensitive*) etaient sensibles aux 2 facteurs. (sakata 93). rep + forte qd stimuli se rapprochait (facteur disparité)
- **majorité DMS sensible à combi disp + taille.**
- loc  = mst + sts + vip 
- Colby et vip = DMS vis ou vis/soma (bimodaux) prefere orientation oblique.

![](https://i.imgur.com/KlzP9NP.png)


**d) depth-rotation-sensitive neurons**

- avant on parlait de S en movement en translation. Ici on étudie movement en rotation.
- **RS** (*rotation sensitive neurons*) dans STS ont parfois preference pour mouvement de rotation dans la profondeur, avec RF assez large, donc moins sensible a chgt de position. 
- RS depth locus MST. 
- illusion **Ames window**. certains RS prefere rotation dans un sens ou un autre (en fonction de la profondeur en réalité, car rotation dans un sens faiait appraitre fenetre +/- pres/loin. Le fait que neurons sensible a illusion montre lien entre physiologie et ressenti subjectif.

![](https://i.imgur.com/4xtyGdq.png)

**e) hand-manipulation task-related neurons**

- spl + 7a = *reaching*
- aip = grasping = *"manipulation-related neurons"*

*e1) Separation of visual and motor components*
- tache : singe dans le noir/light pour enregistrer neurons moteur-visuel, avec plusieurs types d'objet (var de shape). 
- neurons moteurs et visuels preferraient de façon conjointe les même objet. **Congruent avec role PAR pour traiter *"pre-shaping"* sur base orientation visuelle/profondeur utile + tard pour le systeme moteur**

*e2) Selectivity to the 3d shape of objects*
- 6 objets shape diff : sphere/cone/cube/cylindre/anneau/plat carré (Biederman)
- tache singe = grasping et tourner un "microrupteur". 
- **1/4 des neurons sensible à 1 shape particulière**

![](https://i.imgur.com/kV1xT1G.png)

- les plus selectives : cell qui adore carré plat ! voir schéma. 
- en deuxieme, viennent les anneaux
- **de +, selectivité à l'orientation de ces formes !**
- mystere : le signal 3d qui va vers aip, d'ou vient-il ? selon sakata, pas de IT, car pas de diff++ entre réponse objet 2d vs 3d.

**d) Selectivity to 3d orientation**

- Pour manipuler objet, important de connaitre orientation de l'objet relatif à un cadre de référence égocentré. 

*d1) Axis-orientation-selective neurons* 

- cIPS selection orientation barre lumineuse = **AOS**, qui étaient sensible à signal visuel binoc. 
- preference aussi pour S fin > epais.
- **suggere donc encodage de l'orientation longitud de l'objet en 3d par aip nn!** 
- corrobore patho clinique (von cramer, 93) = probleme pcp dans plusieurs axes dans plans frontal.

*d2) Surface-orientation-selective neurons*

- y a t-il nn sensibles à courbure/surface/ pptés 3d des objets ?  
- nn qui adore carré plat sont sensible à orientation de ce carré plat, ce qui suggère codage 3d. 
- Quand on manipule disparité, nn cIPS repondent diff. (shikata, 96)
- AOS répondent diff en fx de S flat ou allongé, avec preference derriere pour telle orientation.= **SOS** = *"surface-orientation-selective"
- eg = SOS qui aime carré plat penché à 45° augmente réponse quand taille augmente.
- important : **SOS insensible ou sensible à changement shape**  
**en somme AIP nn dans cIPS impliqué dans codage orientation et shape via notamment l'encodage de la profondeur**

**f) Subsystems of the dorsal visual pathway**
- mst + ips = mouvement dans profondeur + codage 3d position/orientation
- contribution voie ventrale ? 

**Conclusion = cortex PAR impliqué dans encodage mouvement et orientation objet visuels utile pour action motrice dans un cadre de référence égocentré.**

**Ref =** 
- cramer & kerkhoff 1993 (à resumer)
- Gnadt, J.W. and Mays, L. (1995) J. Neurophysiol. 73, 280–297 
- Sakata, H., Kusunoki, M. and Tanaka, Y. (1993) 
- Colby, C.L., Duhamel, J.R. and Goldberg, M.E. (1993)
- shikata, 96



### Frank & Greenlee (2018)

Revue de littérature : **The parieto-insular vestibular cortex in humans : more than a single area ?**

Résumé :

1.  Introduction

*   balance system : vestibualr système, qui contrôle les mouvements reflexes des yeux, de la tête et des membres posturaux grâce à la détection des mouvements de la tête.
*   Impliqué aussi dans navigation spatiale, mémoire spatiale, apg, conscience du corps.
*   Systeme en interaction avec autre systeme, utile pour résoudre ambiguité sensorielle (ex du train). de +, Réponse des noyaux vestibulaires modulés par matching entre feedback et copie d’efference (straka, 2016), permet distinction mvt passif/actif.
*   Noyaux vb projettent sur thalamus : partie ventrale et antérieure.
*   antérieur : navigation spatiale (voir Cullen), RSC, ERC.
*   (ventral) posterior : perception de la verticalité, vestibular cortex. signal transmis en 6ms ! (de Waele, 2001).
*   Le cortex vb overlap avec rgions sensorielles et cognitives.eg chez le singe, IPS, PoC. Chez homme, aires très diverses (fr, cing, par, ins). Identification du core avec S vb. Activation PIVC diverses : TPJ, retroinsula, OP, InsP.
*   Proposition pour expliquer ces loci différents : PIVC est égal à deux core régions ! 2 aires qui sont fonctionnt et anatomict distinctes : PIVC + PIC : posterior insular cortex mais qui est en fait localisé dans le cortex retroinsulaire ! (Sunaert, 1999). En gros, le PIC répond diff à S visual que PIVC. Parallèle chez le singe avec PIC = VPS : visual posterior sylvian area.

1.  Vestibular areas in the sylvian fissure of nonhuman primates.

*   pdv cytoarchitechtonique, PIVC = insP à OP/retroins. != PIC = partie posterior retroins.
*   PIVC connecté avec aires SI, SII, cing, LPI, VPS... overlap ++ de ces connexions avec celles du PIC. Sauf que PIC a connexion avec aires STS TOJ.- POJ, de + même si connexion PIC avc VP th, PULV> !!
*   PIVC et PIC répondent tous les deux à mvt de la t^te. PIC répond aussi à optic flow. VPS préfère S visuel > vb. Le PIVC ne répond pas tjrs à S visuels (Chen, DeAngelis, Angelaki, 2010).Mais certains travaux trouvent des réponses. Peut être que discrepancy est dû en fait à var des S utilisés, qui peuvent être très larges ou petits., comporter une composante de poursuite motrice, ou bien mvt oculaires supprimés ... MAIS CHEZ HOMME c’est moins controversé !
*   Quand S visuels en mouvements, on a suppression d’act du PIVC, surtout si attt dirigée.
*   PIVC = mss area, répond à S cou, épaules. CHEZ l’HOMME, mecanorecepteurs cervicaux répondent aussi dans PIVC. Soutient hyp que PIVC vb signal sont représentés dans des coord à cheval entre body centered et head centered ! (Chen 2013). Le simple touché tactile active aussi PIVC (voir Bottini!).
*   Pour résumer, le PIC répond à S visuels en mvt. Diff de connectivité car PIC plus cablé sur PULV et STS.

1.  Identification of PIVC and PIC using functional brain imaging in humans

*   en IRMf, on peut faireCVS = illusion self motion dans raw et roll planes, idem mais en + complexe avec GVS, car le labyrinthe entier est stimulé.
*   IRMf a identifié act PIVC dans OP2-3 (Glasser al 2016) avec CVS (Frank 2016b). S optokinetique a activé surtout retroIns P = PIC. Le PIC répond aussi fortement a S vb.

1.  Are PIVC and PIC separate regions ?

_Anatomical differences_

*   PIVC et PIC tous les deux dans la fissure sylvienne, mais à des endroits diff.
*   tractogra = PIC connexion ++ avec SMG et STG comparé à PIVC (Wirth, 2018). NB = SMG est compris dans le TPJ. connexion aussi avec cingulate sulcus visual area (CSv) et VIP, precuneus. (Uesaki 2018). Au contraire, PIVC a + de cnx avec aINs, heshcle, precuneus, IPS, post callosum (Wirth 2018). Les deux aires ont conexions communes avec Ins, IFG, STG, lateral nuclei th- post aussi, PUL, BG.
*   Alors que PIC a connexion avec aires impliquées dans tt motion visue, les 2 ont connexiona avec sys vb.

_Functional differences_

*   Pattern act différent quand rép visuelle. « suppression » PIVC quand act motion vis = act plus basse que baselione. eg = quand vection (sensation mvt iduit par S vis). Théoerie de Brandt (1998) = supprssion PIVC pour éviter conflit visuo-vb ! S qui provoque vection ont pouvoir suppresseur le + fort sur PIVC. Hyp alter = effet attt. Quand vision passive à cause de charge attt ++, la suppression est moins forte (Fra,k 2016). Quand attt augmente, PIVC dim et PIC aug !
*   A noter que PIC rsponse est la pus forte avce vection, donc o a bien rpole ++ dans tt S self motion !
*   NB2 = dans la litté, si pas prise en compte de la diff PIVC/PIC on fait fausse route.
*   Spéculation des auteurs = quand S vb = act retroINs = region overlap avec PIVC et PIC. peut être **que PIC intègre visuo-vb cues**!

1.  A concept for the organization of the core of the vestibular cortex

*   « PIVC+ » car deux aires distinctes dans midposterior sylvian fissure.
*   Proposition d’org :
*   cnx PIC + PIVC avec vp th et pu.
*   PIC = pu + sts + mst, vip, CSv => selef motion network
*   PIVC inhibé par IPS durant attt dirigée
*   cnx PIVC + PIC avec autres str (Poc, PAR)
*   **PIVC encode head et full body mvt = estimation heading direction (je rajouterai aussi la verticalité !)**
*   **PIC servirait 2 fonctions : heading direction sur la base d’integration viuso-vb + distinction vb/visual motion du corps.**
*   **Enfin, le tt du PIVC+ est envoyé à zone TPJ, cad que R selef-motion + heading sont envoyé à TPJ pour construire réf égocentré du selef dans l’espace. (DWI confirme connexion dans ce sens).**
*   Petite note de fin : lesion PIVC et PIC entraîne déficit verticalité visuelle.

![](https://i.imgur.com/OkO6sWg.png)


### Bottini, Gandola, Sedda, & Ferrè (2013)

**Article de revue : Stimulation calorique vestibulaire : interaction entre le systeme somatosensoriel et l'apparatus vestibualaire**  

Résumé :  
- vestibular cortex overlap avec systeme somato
- cvs réduit pb somatoparaphrenie/negligence tactile/hemianesthesie/pain
- cvs a diminué hemianesthesie en agissant sur SII de l'H sain (bottini 2005)
- cvs augmente signal eeg associé à traitement somato

- L'operculum parietal (SII) pourrait être une aire d'intégration vestibulo-somesthésique. A noter que lésion OP associé à déficit VV. Cette région aussi impliquée dans orientation tactile. 

**Note personnelle : C'est cohérent avec déficit VV et HV dans NSU. Ca va dans le sens de lésion récurrente/hypométabolisme de l'aire OP dans PIVC dans nsu. Cohérent avec hypothèse que l'OP contient neurones mss qui code modèle interne de verticalité. Pourquoi lien avec la NSU ? la question reste ouverte. épiphénomène ? lien causal ?**

### Finkelstein, Las, & Ulanovsky (2016)

*Revue de littérature : 3-D maps and compasses in the brain*


Question principale : alors que les animaux ont besoin de naviguer dans un espcae à 3 dimensions, comment le cerveau code t-il ces différentes dimensions ? Ici on analyse résultats chez la chauve-souris et le rat.  
*Définition*
- Place cell : cellule qui décharge quand lieu particulier dans l'espace
- Head cell : quand tête pointe vers une direction donnée absolue (allo)
- Grid cell : quand animal passe à travers un point précis de l'environnement, découpé en sous-section d'un point de vue neural (fait penser à cellule de lieu locale ?)

*Mode de navigation*
- Planaire : quand on marche, contact avec le sol, eg = escalade, pas la peine de connaître une 3 ème dimension.
- Multicouche : mouvement dans plusieurs plans à la fois, nécessite codage 3-D : discrimination entre les plans.
- Volumétrique : idem, mais aucune contrainte spatiale, si ce n'est celle de l'appareil moteur. eg : vol de la chauve-souris, nage.   
**Idée clef ici : on peut basculer d'un mode de navigation à un autre.**  
![](https://i.imgur.com/9SY0yUF.png)


*Corrélats neuraux de la navigation 3d*
- la formation hipp contient cell de lieu/tete/grille/de bords. 
- place cell : vu comme substrat neural de la carte cognitive. On retrouve réponse 3d chez chauve-souris avec champs récepteur sphérique.
- head cell : propriété qui ressemble à un compas = code "azimuth". Présentes dans presubiculum dorsal. la bat a aussi head cell 3d.  
**Les auteurs pensent qu'on peut avoir cellule qui code 2d et 3d, avec codage qui bascule en fonction du mode de navigation**.

*Une mosaïque de cartes 2d et 3d assemblées par un compas global 3d.*  
**a) Des cartes multiples pour l'espace 3d**
- 3 modes de navigation = 3 R possibles. rapide 2d-3d switch ? attentional swith ? une seule R 3d tout le temps ? 
- nb : Ici on a difficulté méthodo due à la possibilité d'utiliser indice allo local ou global. dur à contrôler en situation écologique.
- nature codage 3d des grid cell ? à voir dans l'avenir.
- Hypothèse (Carpenter, 2015) : codage dans diff dimensions/plans d'abord local, puis intégration globale avec l'expérience. Intégration possible des R 2d et 3d grâce à "compas 3d global" pour avoir R 3d stable. Explique l'utilisation de "raccourcis 3d".

**b) Un compas 3d global**
- sur le plan neural, compas global peut être assuré par 3d head cells. Comme chez bat. Possible que R 3d globale soit active dans les 3 modes de navigations.
- question : codage global tout le temps ? hypothèse de Taube 2013 : seulement quand navigation active. quand translation passive, head cell s'ancre sur mode de navigation en cours.
- head azimuth cell peut donc avoir codage local dans modalité visuelle (étoile, montagne...), tandis que head "pitch" cells aurait toujours codage global, ancré sur gravité.  
**C'est donc intégration visuo-vestibulaire qui sous-tend compas global pour navigation. La continuité des mouvements/locomotion à travers les divers modes de navigation peut être expliquée par fusion de R 2d (planaire) et 3d (volumétrique) grâce à compas gobal. Permet d'avoir une métrique en 3d et d'use 3d shortcuts grâce à cadre de référence 3d global. Presubiculum impliqué.**  

*Les propriétés de la carte 3d et du compas dépendrait des caractéristiques du comportement*  
**a) Topologie de la R spatiale 3d**
- Est ce que codage cell tete/lieu dépend de l'ontogénie ? hypothèse que oui car réponse de ces neurones chez le rat apparaissent à diff moments du dév.
- Par exemple, peut être que réponse head cell précoce car mvt tete in utero.
- Cct du mode de navigation très important : proposition que chez bat on a un systeme de coord toroidal, cad avec dimension pitch x azimuth(yaw = rotation) avec 360° span. On aurait des cell head pitch/azim tunées sur l'une ou l'autre des dimensions avec réponses continue quand navigation dans ces dimensions. 
- NB = pas de prise en compte roll dimension, car pas importante pour la navigation a priori. 
- prédiction forte : representation 3d toroidale aussi chez rat et primate car dépendante de l'expérience de navigation.

![](https://i.imgur.com/lDCYyN6.png)



**b) Commutativité du compas neural 3d**
- peu de neurones presubiculum chez bat répondent à roll tilt
- si on a les 3 dim ça devient trop difficile de naviguer car trop de combinaison.

**c) Isotropie et échelle de la représentation spatiale 3d**
- Il y a non iso quand navigation, cad traitement des dim peut être diff. eg : rat prefère traiter espace hor quand nav. 
- Cpendant on retrouve réponse tuning head cell identique pour az et pitch chez bat. idem pour 3d cell place. 
- **proposition : mvt non iso : codage non iso, et idem pour mvt iso.** 
- taille de l'ev joue rôle aussi : large place field dans large ev. peut être tuning réponse s'adapte donc à taille ev. possibile que navigation volumétrique ait donc un tuning isotropique, car ev sphérique. 

*Interaction fonctionnelle et anatomique entre les cartes 3d et les compas.*  
**a) Anatomie fonctionnelle des circuits spatiaux 3d dans la formation hippocampique** 
- voir schéma article : en gros on a un gradient 2d-3d dans circuit presub-hipp-cortex entorhinal. 
- hipp : place cell = position
- psub : recoit inputs anterodorsal thalamus (###adn) ; head cell => **Combi d'inputs head cell az + pitch dans partie psub distale donne 3d head cell**. dans partie prox on a uniquement input head az. 
- **medial erc** : grid cell ; **proximal** : az inputs ; distal : pitch inputs. le fait que head cell envoient leur inputs à medial erc suggeste que head signal utilisé par grid cell cad traiter métrique de l'ev/dimension.

**b) La formation hippocampique : la réalisation neurale d'un filtre de Kalman ?**
- filtre de kalman : algo récursif qui utilise feedback pour réduire erreur de prédiction. très used par wolpert (95) dans domaine sm. Idée clef de **modèle interne** ici.
-  1) génération forward  : prédiction orientation à partie de signal sensoriel (vb) et moteur (position actuelle, self motion, path integration) 
-  2) feedback : presub + hipp donne info compas + position
-  3) comparaison fb et forward par filtre de kalman dans merc. Nouvelle estimate plus précise ensuite envoyée à presub + hipp, puis à systeme moteur.

![](https://i.imgur.com/YVZm2WH.png)


**Ref à voir : brandt et al. 2015 / McNaughton 2006 / deneve 2007 / yoon 2007 / jacobs ?**


### Lopez, Blanke, & Mast (2012)

 

**Contexte théorique =** le cortex  vestibulaire est défini comme un réseau comprenant toutes les aires du cortex recevant des inputs vestibulaires. Ce réseau serait impliqué dans la cognition spatiale (navigation, conscience de soi, perception d’auto-mouvement). Le PIVC serait le *core* vestibular cortex. VC activé quand stimulation vestibulaire, mais ces stim sont différentes, donc on regarde ici overlap grâce à méta-analyse.

**Méthode =**

-   méta-analyse des pics d’activation (350) de 16 études utilisant GVS (4), SC (calorique)(9) , tone(3).
-   Analyse aussi de la latéralisation de la stimulation.
-   192 sujets sains
-   fMRI, PET, MEG
-   déf de l’INS = aINS = short gyri antérieurs au sulculs insulaire central (1,2,3) et pINS = long gyri (4,5 = retroinsula, operculum P-T)

**Résultats =**

-   CVS = bil clusters = Ia + Ip + STG+ BG + PU
-   GVS = right sylvian fissure + LPI + OPf et OPp. / left = idem. Ip. + ventr- psterior noyaux du thalamus. + left cerebellum
-   sound = bil sylvian fis + STG
-   OVERLAP des 3 stim = jonction LPI et STG = retroinsular cortex
-   OVERLAP gvs + SC = left Ip + Ia / bilat OP2 (parietal) + retroI.
-   vestib projections RH et LH => on a plus de clusters ipsi > contra. quand activation oreille droite, + d’activation dans cortex ipsi, ET VICE VERSA avec stimulation oreille gauche.

**Discussion =** En gros, on résume **I, OP2, RI. + LPI, STG** + sous-c (**PU** par ex = relais boucle th-cortex-th + **VPN** th qui recoivent inputs noyaux thalamaiques) + **precuneus** (activé pour tâche de self ; motor imagery du corps) + **OPfr** (precentral gyrus) (IFG) peut être que act fr représente contrôle regard modulé par stim vb. + **cingulate** (attt dirigée) + **BG/CB**

-   insula = centre convergence info tactile, vb, optok, selfmotion.
-   limites = variabilité ++ dans les paradigmes (eg température, côté, sample size)
-   stim vb = SM side à prendre en compte = sensation déplaisante qui peut activer certaines aires ! + sensation tactile + nociceptive Stim ! On a donc une contamination constante SM.mais arg à modérer car on retrouve des act dans régions non SM. de + personnes déaff sur le plan vb n’ont pas act de ces régions malgré stim SM.

 
    ### Guldin & Grüsser (1998)

 

**Contexte théorique =** beaucoup de travaux chez le singe ont montré implications de certaines aires dites « vestibulaires ». Ici on stimule chacune de ces aires chez le singe pour montrer lesquelles répondent le mieux à des stim vb. De +, on utilise technique par tracage sur les aires majeures identifiées afin de déterminer nature des connexiosn entre ces aires.

**Méthode =**

-   pop = squirrel monkey
-   single unit recording
-   traceurs = technique complexe. Retenir qu’on arrive à calculer une carte de densité de cellules qui répondent au traceur que l’on a injecté dans une zone particulière. On peut donc en déduire où est allé le traceur en stimulant quelles cellules dans aires voisines.

**Résultats =**

1.  single unit recording =

-   area = 3a + PIVC + 7 + VPS
-   3a = central sulcus
-   7 = LPI
-   VPS = visual posterior sylvian
-   à retenir = 50% de rép dans PIVC, qui répondait le +.
-   3aNv = 3a neck vestibular region = 30-50% de réponses
-   VPS = 30%
-   nb = on a latence + courtes 3aNv > PICC => feedforward signal.

1.  vestibular cortex connectivity = tracer experiment

-   injection PIVC = vestib cingulate region + 3a + PMav + VPS + Ip + 7
-   3aNv = même pattern à peu près avec connexion PIVC, sauf VPS
-   VPS = connexion avec 3a mais pas 3aNv. On appelle cette région 3aHv pour « hand ».
-   A partir des résultats, reconstitution d’un **réseau cortical vestibulaire, avec le PIVC au centre !**
-   De plus, les aires identifiées ici concordent avec les aires stimulées par injection dans noyaux vestibulaires du pont !

**Discussion = on parlera donc non pas d’une aire vb corticale, mais bien d’un système. Avec PIVC au centre de ce système, que l’on peut qualifier de systeme multimodal qui régit le contrôle postural. Les auteurs suggèrent aussi que ce réseau sous tend informations concernant les mouvements de la tête relativement au tronc (body centred) et à l’espace (wordl-centred coordinates)(référence tronc centrée et monde- centrée). Ces informations seraient cruciales pour la perception spatiale en donnant représentation du corps et des mouvements dans l’espace et donc impacterait la mémoire spatiale.**


### Karnath & Dieterich (2006)


**Revue de littérature : NSU = a vestibular disorder ?**

**Résumé** = Parallèle possible entre trouble vestibulaire et NSU, car dans les deux cas problèmes d’exploration visuelle vers la gauche (si lésion gauche ou si S galvanique droite), problème pcp V, overlap neuro-anat.

**Rappel anatomique** = voie vestibulaire = labyrinthe > nerf 8 > noyaux vb > fibres grimpantes > noyaux oculomoteurs + centres d’intégration supranucléaires (eg = noyaux interstitiel de Cajal) dans pont-mésencéphale. Ce circuit est consacré au VOR, lui-même dans circuiterie SM plus générale pour orienter tête et tronc (eg réflexe vestibulospinal et vestibulocollique). La fonction corticale du sys vb consacré à navigation et pcp de la gravité.

**Csq lésion unilatérale vb (voir Brandt & Dieterich)** = déséquilibre des inputs vb -> vb neuritis = instabilité posture, démarche, nystagmus (phase lente vers lésion side), torsion oculaire, X verticalité, vertige … Problème périphérique peut être compensé au niveau central. Quand lésion centrale corticale, pas de nystagmus/tb oculaire, mais pb pcp V et posture.

**Csq stimulation vestibulaire** => prouver overlap avec lésion vb centrale

-   S calorique = CSC != S galvanique = le nerf 8.
-   On reproduit même csq qu’avec lésion ! shift tonique du regard avec nystagmus vers côté S. **Sujets sains explorent espace asymétriquement.** Mais aussi shift du tronc autour de l’axe lacet.
-   Enfin, parallèle neurofonctionnel, avec act cérébrale chez sujet sain pendant S équivalente à act cérébrale chez sujet lésé au repos.

**Parallèle entre NSU et trouble vb** => problème orientation tête et yeux chez NSU

-   Tâches avec composantes sensorimotrices = Barrage de cible, BL manuelle … Pointage droit devant … mais aussi shift spontané dans le noir.
-   Débat = hyp shift égocentré contesté car cas de double dissociation trouvé par Chokron.
-   Parallèle neuro-anat = le **PIVC**. Zone qui reçoit inputs vb (T-PAR-INSULA – retro insula…) région qui répond également aux autres modalités ! Infos redondantes dans cette zone d’intégration multisensorielle. Les S caloriques/galvaniques modulent l’activité de cette région ! mais aussi zones sillon central precuneus et hipp aussi.
-   Dominance vb HD, tout comme NSU stroke ! Activation cérébrale quand S dépend de latéralité et de côté stimulé.
-   **Région périsylvienne au cœur du traitement cortical vb, codant pour sens de la V et posture balance. Pour Karnath, région clef pour tt spatial égocentré, damaged in NSU. Ajd, implication lésion WM SLF va dans ce sens. Réseau TPJ-LPI-GFI.**

**Conclusion = NSU = vb disorder ? strictly speaking, no ! Car pas de cortex vb primaire. Lésion du cortex multisensoriel (STG, LPI, INSULA). Pas de nn qui code spécifiquement pour input vb seulement.**


### Pérennou (2006)

**Revue =** Lien entre postural disorder et NSU

**Résumé =

- **V comportementale** = R implicite de la V qui contrôle la posture contre la gravité. Celle ci est donné chez l'homme par le LBA.
- Comment mesurer LBA ? debout on a accès au centre de masse, mais c'est dur car déséquilibre. Assis c'est mieux, on accède à orientation du tronc. mais encore une fois influence des problèmes de stabilité. 
- **la V comportementale != de la V subjective/explicite évaluée par svv/shv/spv !** ces mesures ne mesurent pas la V implicite directement.
- [25,67,109,122, 2,56, 84,89, 90] 
- 60,89 => pas de cor trop forte entre problème de posture et svv. rejoint travaux qui associe mauvaise spv à trouble posture. 
- Si biais supramodal de la V implicite, on devrait avoir tilt dans même direction.
- Problèmes de récupération de la posture plus lourd après rbd et nsu. Voir =  9,65,85,97,102 16,85,88,119 85]

i) lien entre nsu et V 
- lien entre nsu et svv/shv et V comportementale (travaux de DP)
- interprétation de DP des hyp de Kerkhoff & Zelch est ici un peu fausse ! cad qu'on met avant idée que circuits code V et H en 3d. Or ces deux auteurs ont mentionné plutôt l'idée d'un réseau plutôt 

**  La NSU + RH ralentit récup de problèmes posturaux. Echelles rapides pas bonne car c’est subjectif de distinguer latéropulsion avec/sans pushing. Il vaut mieux échelles + longues qui comprennent évaluation de la V. **A noter = seuls les patients avec supramodal déficit auraient une mauvaise construction de la verticalité ! Si on a dissociation, c’est que l’atteinte de la V a une explication de moins haut niveau ?? ** Mesures posturographiques = statiques et dynamiques, pour noter body sway et centre de pression/masse.

Problèmes de posture + forts après RH stroke, dans position couchée/assise/debout. Lien entre PS et NSU postulé par le fait que NSU semblent lié à déviation contra de la VV, HV, et PV selon certaines études = gentaz 2002/kherkoff 1999/k et zoech/per01+02/saj/yenik.

-   **Hypothèse 1 = NSU = problème supramodal de la verticalité => problème de posture !**
-   **Hypothèse 2 = NSU = problème intégration somaesthetic = NSU graviceptive, shift du vecteur idiotropique => problème de posture**

L’hypothèse 1 peut être soutenue par le fait que NSU patients peuvent aussi négliger infos visuelles pour se maintenir stable (ref = Yelnik 2006). en effet optic flow en périph utile pour coder orientation. peut être que pb attentionnel empêche utilisation de ces indices. compatible avec pb attention exogène vers périph ! 
Important = selon DP, pb de schéma corporel dans nsu. les patients négligent les segments corporels contral et donc on a donc nsu + pb de posture. Il est important ici que d'un point de vue théorique on parle d'une R dynamique, inconsciente, qui sous-tend action = contrôle postural. Mais dans la litté, on a évidence pour perte de R topographique cs ! cad pb de R du corps non orientée vers action. 
**conclusion =** les patients atteints de nsu auraient pb postural car négligent infos corporelles et non-corporelles. selon DP, on a + de négligence somatosensorielle que visuelle/vb. le référentiel exocentrique est moins touché. Comme argument majeur, DP utilise i) étude de 2002 = déviation du bassin, pas de la tête; ii) forte association entre PB et nsu ; iii) lien entre WBA et R du corps > SVV ; iv) implication infos somato à la fois dans spv et verticale comportementale ; v) effet TENS sur posture, mais aussi sur nsu (voir Kerkhoff).
==> renvoie vraiment à idée de pert d'intégration mss qui sous-tend R inconsciente de l'espace orientée vers action. 



### Egocentric - Somesthesic Body and Verticality Representation


### Egocentric Spatial Task

### Galati et al. (2000)

Hypothèse = traitement allocentré ou égocentré activent différentes régions PPC chez le singe, quid chez l’homme ?

Méthode =

-   On manipule le traitement égo/allo dans une tâche de jugement spatial, trunk-centré ou objet-centré
-   Estimer si barre verticale à g/d de ces 2 réf = SA ou une ligne horizontale.

Résultats =

-   Tâche égocentrée = act HD PAR-FR large.
-   Tâche allocentrée = act également FR-PAR, mais beaucoup moins large.
-   Ego vs Allo (soustraction) = Ego est plus bilateral (FR-PAR) alors que Allo activent aires plus ventrales (Hipp, GLingual).

Discussion = confirment implication de zones motrices dans le traitement de l’espace, avec latéralisation plus forte à droite de ces processus (**surtout allocentré !** **à piste discussion ! si plus de latéralisation, plus de chances d’obtenir un biais ?)**. Lien avec Rizzolatti = le développement des traitements allocentrés arriveraient plus tard. Lien avec Karnath = on a beaucoup plus de NSU égocentrées que allocentrées !

**Spéculation = le traitement allocentré se ferait sur la base du traitement égocentré, avec ajout d’un input rétinien ! (Andersen, 1997). **


### Chechlacz et al. (2010)
 
 **Contexte théorique =** 
 La NSU est hétérogène, dissociation entre NSU ego et allo. Plusieurs propositions pour expliquer cela. Par ex, Verdon (2010) propose que allo liée à lésion IT, vs ego liée à lésion DLPF. D'autres travaux ont proposé implications SMG/AG dans ego. 
 A chaque fois on a qqles limites : preselection sample, batterie de tests hétérogènes, groupe dichotomique, pas d'analyse de la matière blanche avec DTI, lésion définie manuellement qui empêche de prendre en compte atrophie due au vieillissement, contrairement à approche wholebrain ...
 
 **Méthode =** 
 - Pop : 41(38 imagerie) patients lésion corticale vs 73 contrôles. Pas de critères d'exclusion, on a RH + LH, pas de biais. AVC ou maladie neurodeg. Patients chronique (>9 mois).
 - Imagerie : deux approches : lesion mapping + voxel. On rentre degré nsu allo vs ego gauche dans le modèle, avec covarié démog dont nsu droite quand même. VBM + DTI.
 - Dans l'analyse stat on regarde si telle variable corrèle +++ avec telle lésion. NB = analyse de conjonction aussi : quelle lésion est corrélée ++ à la fois à déficit ego et allo ? 
 - On prend des clusters à minimum $100mm^3$. (cad 50 voxels).Point négatif : FWE correction.
 - Tâche : apple cancellation : permet d'extraire degré de nsu allo vs ego : on demande au patient de barrer les pommes complètes parmi pommes imcomplètes qui ont côté droit ou gauche incomplet.

**Résultats =**
- D'un pdv comportemental, on retrouve dissociation nsu allo vs ego, vs les deux : 13/41 ; 10 ; 6. 

**Analyse grey matter RH**

VBM approche = 
- allo : ag + mtg + stg/p 
- ego : mfg + poc + smg + stg/a/s + insula + pu + bg
- asso : ips + tpj
VLSM approche = 
- allo = ag + mtg + stg+ mog
- ago = smg + stg/a/s + insula + bg
- asso = tpj + ips/p + stg/m/s

**Analyse white matter RH**

Globalement, tous les grands tractus - slf + ifof + ilf + sfo - sont touchés, avec lésion allo + postérieures que lésion ego plus antérieures.

**Conclusion =** On retrouve dissociation entre les deux déficits avec un gradient lésionnel antérieur/postérieur, tant sur le plan de la grey que white matter.
 
 **Ref**  : Médina (2009) ; Gottesman (2008) ; Verdon (2010)




### Egocentric - Body Midline in Roll and Horizontal Plane


### Rousseaux, Honoré, & Saj (2014)

*Revue de littérature : **Body representations and brain damage***

1) Introduction
- brain élabore plusieurs R de l'espace, dont l'espace du corps.
- après rbd, on peut avoir nsu des membres contral : "**hemi-asomatognosie**"

2) The median body reference
- syn de egocentric reference : plan sagittal != **plan longitudinal** = **Z axis**
- dans la litté étude du plan médian ou Z avec SSA ou LBA. 

3) Studies in healthy subjects
- ssa shift naturellement vers la gauche sujet sain
- comme l'a montré jeannerod 87, on a influence de eye-on-head signal on SSA. 
- SSA influencé par trunk-on-head signla aussi. 
- Ceyte 06 à lire : trunk rotation affecte aussi Z axis (mais dans quel plan ?)


4) Studies in patients with brain damage
- On a rotation de la Z + deviation latérale de la SSA
- au contraire, on ne retrouv epas tjrs rotation (eg : Chokron). 
- Selon Saj 05, hemianopsie peut agggraver ssa shift ipsi  en plus de la nsu. interessant de noter que hemianopsie seule ne shift pas Z axis ! à voir. 


5) Relationship with impairments in perceptual-motor tasks and posture
- nsu et ssa shift souvent associée, avec r * avec lb aussi (richard 2004).
- Barra 07 montre que corrélation entre Z et nsu/hemianesthésie
- Barra 08 : corrélation avec problème de wba. Confirmé par rousseaux 13 qui montre cor Z avec postural imbalance (autre échelle =PASS)
- dans ces deux dernière études, la relation était moins forte, ou ns, entre postural pb et SVV shift.   
**--> donc problème posturaux semblent plus dus à un deficit qui affecte le schéma corporel que la représentation de la verticalité**
    - A noter aussi que modulations of afferent systems **(somatic = vibration; vb = vestibular neuritis, S caloric ; visual = PA, S optokinétique)**
    - voir Alessandrini sur lien entre ssa et vb ? 
    
6) Representation of the lateral parts of the body
- quand on étudie ssa sur épaule droite, on étudie ssa sur nouveau hemicorps décalé. quand on demande pointage vers left épaule, on a gros shift ispil. 
- sur le plan tactile, on retrouve gradient chez rbd quand fluff test (voir rousseaux, 2013)
**--> NB = selon Rousseaux 2013 chez nsu on a pas un déficit perceptif/R de l'hémicorps gauche dans la nsu perso, en fait les mauvaises perfs dans tâche fluff ou bisiach peuvent être dues à un déficit du schéma corporel cad un décalage de la référence égocentrée dans les deux plans.**
- quand je stimule le schéma corporel avec inputs sensoriels, je réduis justement ces biais.
- Critique qu'on peut faire ici : **ici on fait le raisonnement suivant : si S sensorielle rétablit SSA, et si S sensorielle rétablit aussi nsu behavior dans espace corpo-péricorporels, alors il est possible que shift SSA soit la cause de la diminution de nsu. Néanmoins, la shift de la SSA peut aussi être un épiphénomène de la nsu/lésion droite et ne pas être impliquée causalement dans nsu.**

7) The cerebral substrates of body representations
- sujet sains : fronto-T network : PPC (ips, spg, prec, tpj) + zone premotrices
- sujets patho = aPAR (smg, PoC, spg)
- PASS disorders, très r à Z (barra) et LBA (rousseaux) = insula post (pic) + stgp + s1 + ipl
**--> sous tend idée que inputs somato sont cruciaux pour schéma corporel. autres aires comme tpj vont intégré infos mss en +. smg pourrait être impliqué ++ dans la RE. mais region autour de ips aussi.**  
**--> En revanche, pas de lésion autour de OP/PIC/STG impliqué dans cortex vb +++. ces régions + impliquées dans postural disorders que shift de la RE/distorson schéma corporel/body R.**  
**---> reflexion perso : la posture est donc un comportement déterminé par plusieurs facteurs en interaction : la posture est influencée par des troubles SM (bas niveau) + troubles cognitifs spatiaux qui impliquent des traitements spatiaux égocentrés (schéma corporel/shift RE) et allocentrés (modèle interne de verticalité).**   

**Ref :**
- Conti F, Fabri M, Manzoni T. Bilateral receptive fields and
callosal connectivity of the body midline representation in
the first somatosensory area of primates. Somatosens Res
1986;3:273—89.
- Ceyte 2006
- Alessandrini M, Napolitano B, Bruno E, Belcastro L, Ottaviani F,
Schillaci O. Cerebral plasticity in acute vestibular deficit. Eur
Arch Otorhinolaryngol 2009;266:1547—51.


### Rousseaux, Honoré, Vuillemier, & Saj (2013)

Contexte théorique = On cherche corrélats anat’ dans la SBA vs VV misperception , et quels overlaps avec pb postural.

Méthode =

-   pop = 42 RBD (21 NSU) + 15C
-   J + 30-90
-   diag clinique = NSU cut off 2/3 dans copy + bell + CBS
-   HH + **no PS**
-   PASS scale pour évaluer la posture
-   Tâches = SBA dans plan horizontal (même si shift initial en rotation aussi)
-   SVV = 18 essais par tâches , déplacement initial -45/+45 en rotation et -15/+15 en translation
-   :-1: version visuo-haptique + rod pas au niveau de la tête...
-   apparatus = participant sur lit clinique = uniquement un strap pour la tête. jambes étendues.
-   VLSM = ici méthode un peu particulière, avec sélection des 10 patients avec le plus/moins de pb dans une tâche donnée, afin d’avoir des contrastes IRM + forts.

**Résultats =**

-   comportemental =
-   biais contra SVH et ipsi SSA (yaw) NSU > no NSU. 
-   idem quand **quand hemianopie > no hemianopie**. 
-   PASS NSU > no NSU
-   corrélation VH * SBA, SBA * PASS, mais VH * PASS ns ! (réplique Barra et al 07, 08). NB = attention à noter que ici on a aucun patient pusher ...
-   corrélation VH, PASS, SBA , avec sévérité de NSU dans les 4 tests, y compris le CBS.
-   **cor de la SSA avec lesion volume très forte**
- <font size="5"> ++*lesion overlap diff avec 10 vs 10 (best/worst) =*++</font> 
-   SVH = STG+MTG+IPL+Pu+Th
-   SBA = **SMG + MFG**+Pu+STG
-   PASS = Ip + Th
-   <font size="5">++*vlsm =*++</font> 
-   VH = **SPG**+STG+MTG
-   SBA = **SPG + SI** + IPG + STG
-   PASS = **SPG** + **Ip** + IPG
-   **VH + PASS = STG + TPJ + IPG + SLF**
-   **SBA + PASS = SI + STG**

![](https://i.imgur.com/CD5zQSM.png)
![](https://i.imgur.com/N98jVFD.png)



Discussions = implications SMG dans SBA pas étonnant, aire impliquées dans processus égocentrés. Mais aussi dans BN, même si pas lien direct ici.Aires MFG due à partie motrice.  VH ici due à lésion temporale postérieure qui overlap avec PIVC.

-   En gros, SBA repose + sur poids soma, et VH sur poids vb-visuel.
-   **lésion Ip dans PASS problème posture va dans le sens d’une interprétation vestibulaire du pushing syndrome. Mais on retrouve aussi lésion dans région intégrant infos somesthésique.Comme si pushing résultait la fois d’un déficit vestibulaire et somesthésique ! VI + VG ! conforme avec études montrant que VV touchée dans PS. et aussi que PS corrèle avec shift SBA, mais aussi avec problème PV ! Interprétation plausible car on retrouve des lésions liées à PASS qui overlap avec respectivement SBA et VH shift.**

**limites = que RBD, méthode VH qui ne neutralise pas totalement modalité vestibulaire, pas de strap (mais à relativiser car pas de gros problème de posture).**


### Richard, Rousseaux, Saj, & Honoré (2004)

**Contexte =** Débat sur rotation ou translation de la SSA dans nsu.

**Méthode=**
- 12 rbd = 6 nsu (j+50) + 6 rbd + 6control
- cutoff 2/3 => copie de scene (ogden version)+lb+cloche
- aucune info sur lésion
- tâche = ajustement visuo-haptique (led + rod) de la SSA en rotation ou translation dans le plan horizontal. shift initial -15/0/15 ou -45/0/45 en translation ou rotation.
- stat non par.
- vi intra = shift initial rot ou tra / VI inter = group

**Résultats =**
- translation = shift 5cm nsu+ > rbd=c. shift initial ns
- rotation = ns. quand on regarde patient ssa rotation quand condition initiale 0/0, on voit que 2/6 ont ssa contra. quand hemianopsie, ssa ipsi 2/6.

![](https://i.imgur.com/LtOxFMj.png)


**Discussion =** res dans le sens de translation, ici mesurée dans le plan horizontal, que l'on retrouvera plus tard dans le plan frontal dans autres études. hyp kin pas decisive ici car quand cond 0/0 on retrouve encore la translation ipsi.  
**reflexion = rotation vers la gauche 3/6 patients dans plan horizontal = mirroir de la rotation dans le plan frontal ??**

### Saj et al. (2013) => intéressant, mais :-1: d'un point de vue théorique (cf litté sur GVS).


**Contexte théorique =** Atteinte vestibulaire unilatérale périphérique peut avoir conséquences sur le plan postural, oculomoteur, neurovégétatif, perceptif, et cognitif. Même si compensation centrale, on peut avoir quelques symptômes qui restent, pour mouvement de haute fréquence de la tête.  Qd stimulation vb, chgt perception du corps. Est ce dû à simple shift oculo ? ou bien effet cognitif ? Ici on investit cette hypothèse avec paradigme mesure de la SBA avec composante rotation et translation chez des patients avec atteinte vb périphérique, dans les axes H et V (frontal).

**Méthode =**

-   pop = 20 ménière = 9L + 11R + 10C
-   Contrôle = pas de nystagmus résiduel. **Y + 2**.
-   Controle2 = mouvement de la tête durant tâcheen 3D en position yeux fermé vs ouverts.
-   Contrôle 3 = questionnaire sub sur sensation posture et vertige.
-   apparatus = stabilisation tronc et tête
-   tâche d’ajustement visuo-haptique, avec barre visuelle à aligner
-   shift initial = rotation * translation = 18 essais dans les **2 plans**
-   Dans le plan frontal on rajoute une variable = **aligner le baton avec tête/tête-tronc/tronc**
-   **VI = group * initial direction of shift *ref).**
-   **VD = bias**

**Résultat =**

-   H plane =
-   translation * vers la droite pour L seulement vs R+C.
-   rotation ns.
-   effet principal shift initial (attirance) pour translation et rotation
-   V plane =
-   translation * vers la droite pour L encore, avec **effet tête > tête-tronc > tronc**. indep du ref, on **+15°, mais dans R on peut avoir shift à peu près de 5-10°donc la diff n'est pas si impressionante, SAUF pour le réf tête, où le biais est à 18° vs 3° !**
-   rotation * vers la gauche pour L seulement, pas en fonction de ref. mais bon, **à peine de 1°... taille d'effet très petite à mon avis.**
-   contrôle questionnaire + mvt angulaire de tête ns entre les groupes, bonne récupération.

**discussion ** = importance du sys vb dans self orientation dans l'espace. rs compatible avec pb de trajectoire de marche dans lésion vb pérph.   
nb = spéculation avec 'hyperschématie' => surR de l'hemicorps gauche, et donc shift du milieu (pas clair...). Rés majeur = inputs des labyrinthes gauche suffisants pour maintenir R fonctionnelle de la SSA. Suggère dominance left vestibules pour body R orientation. c en contradiction avec rés de Hufner, qui trouve dominance right vestibuale pour tâche de navigation et de mémoire spatiale...  
A noter que vb projette ++ vers H ipsi. Saj et al se contredisent aux mêmes... car si lésion vb gauche, alors diaschisis vers HG ... à l'inverse, si lésiion vb droit, alors impact sur HD... les auteurs essaient de se rattraper en disant que chez le singe c'est plutôt des inputs contra vb vers cortex qui dominent... tout ça n'est pas cohérent du tout...
 

### Chokron & Bartolomeo (1997)

**Contexte** : Théorie de Karnath, ventre...etc. **rotation SSA vers la droite empêche exploration vers la gauche de l'espace. A la base, heilman interprèrte ça comme une hypokinésie directionnelle ! car pas de composante perceptive**. ici on essaie de démontrer que la déviation de l’ER n’est pas une condition obligatoire de la NSU, pas la cause, mais plutôt la conséquence.

**Méthode** : on a testé des sujets avec lésion PAR ++> avec ou sans NSU dans des tâches pour détecter NSU qu’on va corréler avec des perfs de SA (manip avec 4 positions).
- pop = 8C + 6 RBD
- hemiparésie
- diag nsu = figure 2d/lb/bell/line/gainotti
- j+30-1500
- lesion fr-par 4/6 + bg/capsule interne + left occ lesion/wm subcortical
- 3/6rbd were N+.
- SSA = pointing SA -30-30°.
- calcul d'un score de latéralité du biais spatial, à partir de figure 2d + lb + crossed lines.

**Résultats** :

-   On retrouve effets très classique (gradation selon position avec main droite) chez sujet sain, légère déviation vers la droite.
-   On retrouve tous les cas de figure chez P+/- et NSU+/- en termes de déviation patho du droit devant. On a même des sujets NSU qui sont déviés à gauche.

![](https://i.imgur.com/gcqDwxF.png)
![](https://i.imgur.com/O1l7gvX.png)


**Conclusion** : Résultats dans le sens de déviation ER = conséquence de la NSU ou d’une lésion PAR, et non à la racine de la NSU.

### Chokron & Imbert (1995)

Contexte théorique : La NSU est vue à ce moment-là comme la conséquence d’un shift de la référence égocentrée, une sorte d’ancre spatiale (représentation multi-modale) ancrée dans le tronc. La NSU ne serait donc pas rétinotopique, mais égocentrique. De plus on sait que ré-orienter le tronc vers la gauche diminue la négligence

Hypothèse : Ici on teste un sujet NSU pour voir si il est sensible à déviation du tronc, et des sujets sains pour voir si cette même déviation influence la SA : autrement dit, est ce que un shift de la RE résulte dans un shift du SA ?

**Méthode** :

-   la tête reste droite mais le tronc : +/- 15 degrés
-   4 positions (15,30 +/-)
-   Essais avec les 2 mains

**Résultats** :

-   la SA shift en même temps que la ER chez sujets sains
-   idem avec l’eccentricité
-   Chez sujet NSU même tendance. ex : Quand main droite + 15° : grosse déviation à droite, moins grande quand main gauche.
-   Le sujet NSU déviait spontanément vers la droite.

**Discussion** :

-   L’endroit d’où on commence le pointage joue un rôle, plus que la main.
-   Diminution de la NSU quand pointage gauche attribuable peut être à ré-activation de l’hémisphère droit ?       
-   Mais peut être aussi à indiçage spatio-moteur !
-   Mais c’est peut être aussi un effet de scanning ! quand scanning left to right = pseudo-NSU !
-   Le scanning (via position départ) + main used seraient 2 facteurs additifs !

**Conclusion** : résultat en faveur de ER = ancre spatiale dans tâche égocentrée

### Jeannerod &  Biguier (1989)

Contexte théorique = subjective sagittal axis = SSA R interne d’un plan virtuel superposé au plan sagittal, qui sépare le corps et l’espace en 2 secteurs. On étudie à quel point cette ssa est biaisée dans tâche de droit devant avec/sans indices visuels.

Méthode =

*   pop = 10 droitiers
*   apparatus = pointage avec position départ main sur le côté du corps. tête immobilisée et alignée avec le tronc.
*   Expé 1 =
*   vi = main gauche/main droite * cible visuelle/pas de cible (obscurité)
*   Expé 2 =
*   vi = position cible = 0/5/10/15 degrés vers la gauche ou la droite. Uniquement des pointages main droite.
*   vd = bias

Résultat =

*   **Expé 1** **=**
*   main droite = pseudo NSU
*   main gauche = léger biais à droite, plus de variabilité.
*   cible = biais à droite / pas cible = biais à gauche
*   **Expé 2 =**
*   Plus la cible s’éloigne du plan sagittal objectif, moins on a un biais à gauche.

**Discussion =** Dans expé 1, résultat va dans le sens d’une dominance de l’HD pour la perception de l’espace personnel. **Quand on a un repère extra-personnel, ce biais est minimisé**. Expé 2 = **Influence des signaux eye on head sur la direction de la ssa : quand le regard est dévié ++, déviation également de la ssa.**

*   Résultat expé va dans le sens d’une ssa bâtie sur la base d’intégration de plusieurs ref égocentrés = eye on head, head on trunk, trunk in space ... problème de mss dû à X PPC chez NSU ?

### Jeannerod & Biguier (1987)

**Résumé = “ The directional coding of reaching movements. A visuo-motor conception of spatial neglect.”**

Idée générale = la direction des mouvements moteurs seraient codés par un système de coordonnées égocentré, cad en référence à l’axe du corps. Ce référentiel fourni une carte spatiale utilisée par le système moteur pour diriger son action. Pb = quelle est la nature de cette référence ?

En effet, coord égocentrées oeil/tête/tronc pas toujours alugnées ! Hyp = peut être qu’un problème d’intégration d’une de ces coord entraîne une désorientation du système sur le plan visuo-moteur.

1) Eye position signal
-   Travaux chez le singe ont montré évidence pour “**motor inflow” signal, efference copy signal**”, grâce à **DSS** Montre déjà que localiser une cible visuelle on a recours à des signaux rétiniens et extra-rétiniens. EC utile pour différencier mvt du cadre du mvt des yeux.
-   Eye position signal en interaction avec modalité auditive, CR colliculus codant stimulus auditif change en fonction de mismatch avec cible Suggère **transformation des infos auditivo-visuelles** dans un même système de référence eye on trunk. revoir Anderson 1997  
-   Importance des signaux Bup aussi, signal ppc des muscles de l’oeil.. expé avec chaton kittens avec eye immobilisé.
-   **Quand immobilisation oeil et qu’on demande saccade, sensation de bouger l’oeil ! et quand inversement duction forcée sans saccade commande, pas de sensation de mvt** ! Ici ça n’est donc la ppc qui a jouer un rôle dans sensation position signal de l’eye, mais plutôt EC. On mêmes observations dans tâches de pointage, pointage dévié car sensation d’avoir fait une saccade.
-   De +, quand on demande au sujet de faire saccade vers cible visuelle apprise, une fois dans le noir, grande imprécision. le signal ppc ne suffit pas. Perte de la “direction constancy”, cible n’est plus perçue au même endroit. **Montre que signal ppc doit être intégré avec un signal visuel pour coder cible eye on trunk**.

 

2) head position signal
-   On manipule depuis les années 70 les sensations de head on trunk avec vibrations du cou. Quand cou gauche vibre, sensation d’étirement, et donc de mvt de la tête vers la droite. Quand le sujet fixe une cible, il a l’impression que la cible bouge !
-   Head on trunk signal important pour pointing accuracy = quand on empêche mvt de tête vers cible, pointage + imprécis. Cela s’explique par **séquence motrice eye > tête > arm.** On a besoin du signal ppc du cou pour coder la position du bras ! C’est d’autant + important que la cible est excentrée.

 

3) body axis signal
-   R interne du corps créee à partir d’afférence somatic/vb/v qui sépare le corps et l’espace en deux secteurs, et de ce fait devient un référentiel égocentré pour **orienter nos actions dans l’espace personnel et extrapersonnel** .
-   La SBA pourrait être une référence égocentrée utilisée à la fois dans la dimension horizontale et verticale. Jeannerod assimile d’ailleurs l’idée de vecteur idiotropique à celle de SSA. Mittelstaedt est bezaucoup + flou là dessus en parlant seulement de R centrale issus de multiples transformations sensorielles. Mais l’idée semble similaire.
-   SBA évaluée avec tâche de droit-devant. + dépendante de la déviation du tronc que de la tête, voir Karnath, Chokron...
-   SBA en compétition avec eye position signal dans DD task. On fait distinction ici entre **SSA et SBA**

4) ***asymmetrical behaviour following unilateral lesion of the nervous system***
-   **en temps normal, SBA, bien que légèrement asymétrique, oriente correctement nos actions vers la gauche ou la droite.**
-   **Chez invertébré, on observait cpt asym quand lésion unil. Idem chez vertébré, avec sprague effect par ex = regard dévié du côté lésé, même au repos en absence de cible. Rôle du colliculus ici.**
-   **Chez l’homme ref pionnière montre pointage biaisé chez NSU.**
-   **Ici jeannerod amorce deux hypothèses de la NSU = hyp de Kinsbourne, inhibition transhémisphérique, et hyp d’un biais d’orientation dans l’espace dû à perturbation de la mss qui sous tend la SBA.**
-   **Spéculation ici = si X SBA, on devrait observer “réactions compensatoires” de la part de processus de bas niveau. Ici, il fait référence aux symptômes vestibulaires qui arrivent après lésions dans les mêmes zones que celles qui produisent shift SBA ! Ici l’auteur décrit nystagmus spontané avec phase lente vers côté contralatéral + fort quand on induit rotation vers la côté ipsilésionnel ! On a donc biais oculo-moteur dans le sens inverse la lésion !! Peut etre dû à illusion de rotation vers la droite ! Le système vestibulaire organiserait une compensation au shift de la SBA.**


### Ventre, Flandrin, & Jeannerod (1984)
**Contexte =** quand yeux bandés, certains patients ont bras qui pointe vers côté de la lésion ou vers vers l'opposé. quand S calorique froide des 2 côtés, le VOR slow était + fort du côté de la déviation du pointage. interprétation = **si tête initialement shiftée en rotation vers la droite, alors illusion de tourner la tête + forte, et donc slow VOR + large**. ici on défend cette hyp chez le chat, avec lésion pariétale (expé1) et colliculuc supérieur (expé2).

**Méthode =**
- on enregistre VOR chez chat avant et après destruction aires supra-sylvienne postérieure, équivalent aires 5,7,19,21 chez homme = SPG surtout, et des fois aussi MTG, T-O
- 9 chats ou 4 chats (expé 1 ou 2)
- *gain* = rotation yeux/rotation tête (entre 0 et 1)
- *gain symmetry* = gain contra/ipsi = si > 1 = gain contra > ipsi.
- on manipule aussi *fréquence*, cad dire vitesse de rotation de la tête.
- dans expé 2, on fait ablation colliculus supérieur.

**Résultat 1 =** 
- dans le noir, nystagmus slow spontané du côté contralésionnel. comme si sensation head rotation côté ipsi.
- après destruction, gain VOR slow diminué de 50% quand rotation tête contra. au contraire, augmentation du gain quand rotation tête ipsi.
- effet + fort pour faible fréquence.
- lésion SPG donnait effet les + forts pour baisse de VOR slow.

**Résultat 2 =** 
- on observe, comme dans le *sprague effect*, mouvement de *circling* et d'orientation de la tête vers côté ipsi, avec négligence contra, pour sounds et gaze. à peine 5 % d'orientation côté contra quand detection S surprenant (cassette vidéo).
- nystagmus spontané slow idem vers coté contra sain.
- quand présentation S visuels + rotation, on retrouve encore + fort gain quand rotation ipsi.
- + fort pour fréquence faible.

**discussion =** on a effet similaire lésion pariétale supérieure et CS sur asymmétrie du slow VOR compensatoire quand rotation de la tête. important de noter que malgré ça on observait asymmétrie du comportement visuo-moteur (circling, head orientation ipsi) que quand lésion CS, et non PAR. les auteurs disent cependant qu'on aurait pu observer effet patho dans tâche de pointage, comme chez singe après lésion aires PPC.  
on ne peut pas expliquer VOR slow contraspontané et déviation head ipsi avec juste lésion bas niveau, car on aurait congruence. ici **Transformational theory** énoncée pour la 1ère fois avec 4 assumptions :  
- 1) action orientée dans l'espace par rapport à une référence égocentrée qui permet de situer son corps par rapport aux objets dans l'espace. 
- 2) Cette référence est une R interne de l'axe longitudinal du corps, construite à partir de signaux sensoriels extraperso-personnels traités de façon bilatérale et symmétrique. Une lésion unilatérale produit déséquilibre dans traitement de ces inputs et par conséquent déplacement de la SBA.
- 3) Les déviations oculaires spontanées VOR slow du côté contra illustrerait tentative de compensation du système vestibulaire du shift de la référence égocentrée.
- 4) réf égocentrée produite à partir d'un large réseau sous-cortico-cortical, ce qui explique récupération rapide quand lésion isolée.

Cette théorie prédit erreur de pointage directionnelle constante dans ataxie optique, ce qui est effectivement observé. en effet, on **aurait nouvelle coordonnées égocentrée en désaccord avec coordonnées allocentrées rétinotopiques ! On aurait alors mauvais codage des objets visuels / corps, mais bon codage des objets / autre objet !** nb = c'est ce qu'on induit avec les lunettes prismatiques.   
A noter enfin que selon Ventre et al., ataxie optique et nsu sont 2 syndrômes très proches, d'un point de vue anat (lésion par) et cognitif (déficit égocentré). 

### Alberts et al. (2016)

**Contexte théorique =** On utilise inputs vestibulaires, visuels, et ppc pour estimer direction de la gravité. Effet Aubert prédit que reliability des inputs otolithiques diminue quand angle augmente. Ici on regarde si c'est aussi le cas pour les cues soma !  
Ici on utilise modèle bayésien pour prédire bias et sd de SVV en modélisant inputs vb et ppc, en provenance des otolithes (head in space), cou (head on body), corps (body in space). En sachant que SVV est surtout estimée en position normale par head in space coordinates.

Nouveauté = ici on rajoute par dans le modèle pour que le poids des inputs ppc (qualité du signal) ne soit pas fixe mais diminue avec condition extrême.

**Méthode =**

*   Pp = 10 sujets, 110 essais de SVV
*   VD = SVV bias + error
*   VI = On manipule BS (7 positions ; -90+90) * HB (3 positions ;-30+30), en manipulant tilt de la tête + tilt du corps, dans les 2 sens, associés ou dissociés, +/- 30° jusqu’à 90°. Sujets déplacés dans roue. On a une tâche transmodale = jugement visuel avec manip inputs vb/ppc.
*   On compare performance cptale à perf du modèle bayésien.
*   Plusieurs modèles bayésiens = full modèle (poids décroissant pour ppc), old model (ppc W fixe), head in space seul.

**Parenthèse méthodo sur modèle bayésien !**

*   On utilise likelihood pour signaux modélisés, sur la base de précédentes données, avec estimation du bruit (inverse de la précision).
*   3 étapes qui mènent à estimation de gravité = estimer likelihood des sensors, transformer les coordonnées de réf différents (coup et soma) en sommant les gaussiennes afin de replacer les cues dans une réf spatial commun (**head in space**), combiner les signaux avec en + le prior et les coord visuelles head-centrées.
*   Prior = le cerveau pense que la gravité est alignée avec direction de la tête.
*   On compare les modèles bay avec LRT.

![](https://i.imgur.com/3tRMk8S.png)


**Résultat =**

1.  Cpt =

*   Quand HS augmente (degrés BS + HB), bias augmente. pas de diff de pattern pour HB positions.
*   Quand HS alignées avec le corps, variabilité faible quand aligné avec verticale, puis augmente (HS) jusqu’à 30° mais se level off entre 30 et 90\. Quand HS avec dissociation, var augmente augmente jusqu’à un pic à 60° puis diminue.
*   Overlap des courbes psychophysique entre HB et HS > BS, ce qui montre bien importance des inputs vb dans pcp **direction** la SVV. Par contre pour la variabilité il y a plus d’overlap entre HB et BS quand > 30 °. La var observée à plus de 30 degrés pourrait s’expliquer par baisse des poids soma.

1.  Comparaison Bayes et cpt =

*   Full model explique mieux les données que modèles réduits.
*   Analyse du full model montre que inputs vb ont W = 1 quand 0 degrés puis réduction W progressive. Augmentation du poids augmente pour Soma jusqu’à 60 degrés, et prior augmente linéairement. **Prior peut expliquer effet Aubert ! Quand petit tilt, prior a faible W, et quand grand tilt > 60degrés, prior pense que gravité aligné avec orientation du corps. Contestable, on pourrait avoir juste effet Aubert a cause d’inputs ++ du corps et du zéro W vb.**

![](https://i.imgur.com/h1OuvXB.png)

- ***Résultat à retenir :*** 
- larger SVV errors quand augmentation **head in space** = on code SVV dans cadre de ref head in space = **quand tête 0°, malgré orientation corps à +/-30°, on peu d'erreurs.  
- quand head on body (-30,0,30) on a aug de la var quand >60°. 
- on a overlap de la var pour *small tilt* entre head on body et head in space
- on a overlap de la var pour *large tilt* entre head on body et body in space
- Ca indique que je me repose + sur signaux corps et moins sur otol quand j'augmente tilt body.
- prior drive bias vers head in space coord. 
- rés nouveau = head in space var atteint pic à 60°, puis diminue ! effet jusqu'à 60° est bien connu, postulat que dist *non-uniforme des cellules ciliées dans otol* (fernandez et goldberg, 76, rosenhall, 72-74). **On a moins de cell ciliées qui code pour large tilts, donc + de bruit**. 
- congruent avec albert 2015 = large var à 90° dans SVV chez patient avec deaff bil vb ! 
- ? = pourquoi la var se stabilise à 60° pour head in space var ? spéculation = Stim ++ des capteurs tactiles quand large tilt body in space.
 
**Discussion =** vb reliability (W) de 0 quand tilt augmente, car PA rate des otolithe désynchronisés. Quand vestibular bi loss, on a var increase quand tilt, seulement attribuable à input soma. **Pourquoi plateau ?** Extrapolation = signaux multiples et divers du corps, input non linéaire, avec certains récepteurs étant beaucoup plus stimulés avec gros tilt (eg = pression surface peau, tension tendons, viscères du tronc).

### Barbieri et al. (2008)
**contexte =** on étudie influence de la proprioception sur MIV. Chez des sujets sains, on applique vibration sur les tendons du tibia.

**Méthode =** 
- 12C
- PV dans le plan frontal
- illusion tendon a déjà démontré efficacité pour induire sensation mouvement illusoire (mvt d'extension du muscle). quand vibration tendon achille, sensation d'aller vers arrière.
- vibration sur tendon d'achille
- expé 1 = test baseline --> 3 min de vibration 
- expé 2 = test baseline --> 8s vibration 
- on regarde after effect aussi

**Resultat 1 =**
- baseline = proche de la V
- quand vibration, -1.95° = backward bias
- effet tilt initial (effet classique)
- :-1: = pas de S contrôle

**Resultat 2 =**
- idem, backward tilt > avant ou après
- effet tilt initial (effet classique)
- :-1: = pas de S contrôle

**conclusion =** effet cue pcp sur MIV

### Postural vertical and weight-body asymetry 

### Subjective Postural Vertical (SPV)

### Saeys et al. (2018)

**contexte =** le E-effect occurs quend léger tilt <60°. **selon mitt., reflète tendance à aller vers VI, pour éviter surcompensation ? à creuser... mais je crois que ça reflète un aubert effect inversé, quand on a le VI tout droit. comme otol est moins fiable, on a + confiance dans VI.** du coup on regarde si on observe cela quand patients sont déafférentés sur le plan soma. de +, on regarde Eeffect sur PV et VV.

**méthode =**
- exclusion nsu visuelle et PB. 24 patients, dont moitié r/lbd. :-1: pas de groupe contrôle.
- RASP test = dichotomy en 5 groupes (au lieu de score continu...:-1:)
- SVV, réponse joystick ; 21T ; tilt max 20°. pas de chin rest (mais pas de PB). 
- :-1: = pour induire E-effect, on demande au patient de pencher le plus possible sa tête ! gros bruit dans les données !! on a range de 35 à 45°...
- PV dans frontal plane. même procédure (peu de description ici...)
- pour calcul Eeffect, on multipliait raw bias par -1 (pour inverser le signe). 

**Résultats =** 
- VV = gros E effect. augmentation linéaire avec RASP sécérité.
- PV = idem.

**discussion =** augmentation du Eeffect interprété comme "reweighting strategy". les patients ne pourraient pas utiliser infos soma pour bonne pcp de la verticalité. **néanmoins, l'interprétation théorique derrière est très floue... pas rigoureuse... la discussion est nébuleuse...pourquoi E effect augmente?, on pourrait avoir augmentation SD...**:-1:


### Mazibrada et al. (2008)
**contexte =** on sait que pv et vv dissociées. mais on sait que patients lésion VV ont imprécision PV. Ici on étudie la relation inverse = effet deaff soma sur VV.

**méthode =**
- pop = 3 patients avec différents profils de deaff. s1 = guillain barré = bil sensory loss (*atteinte gaine de myéline périph, schwann cell*); s2 et s3 = sensory loss en dessous de vertèbre thoracique.
- 20 contrôle
- pop chronique
- echelle RST = Rivermead Somatosensory Test = épingle, pcp, toucher épicritique ou protopathique. score sur 300. 
- tache PV frontale
- vd = biais + "cone de verticalité" = range + SD
- test à T0 puis T+7 pour s1; T+2 pour s2/3
- on regarde effet tilt direction.

**Resultat =**
- contrôle ok. range normality -2/2.
- ![](https://i.imgur.com/76V2CiW.png)
- RST = 22 pour s1. puis 130 t+7.
- estimation biais très biaisé en fx de la direction initiale du tilt, en dehors de normal range. (bn = **fait penser à effet cadre chez nsu = utilisation de cue extérieurs !**).
- s2+3 ok.
- le biais influencé par direction se normalise à t+7 pour s1.
- concernant sd, sd++ pour s1+2 test 1 quand dire leftward. cet sd reste le même à t7 pour s1.
- pour range, res idem, mais s1 qql que soit direction initiale.

**Discussion =** le patient S1 récupère en terme de biais, mais pas en terme de variabilité, surtout pour tilt initial à droite. montre importance des cues soma des épaules, non endommagés chez s1/2. compatible avec théorie de mittelstaedt, 2 voies graviceptives, up et down.

### Barra et al. (2008)


Contexte théorique = On sait que VV et SBA en rotation vers la gauche après stroke droit. ya t-il un seul mécanisme derrère ce biais patho ? on teste cette hypothèse en regardant si dissociation quand on manipule info somesthésique.

Méthode =

-   pop = 15 stroke = 9RBD + 6 LBD + 12 C
-   M + 1-4
-   diag clinique = HH + SCP + vb. seul un patient PS.
-   apparatus = drum = strap en position assise.
-   Tâches = SSA + VV dans le noir. Le sujet devait positionner un baton lumineux par rapport à son corps ou à la verticalité. 30 essais en tout = 0/-30/+30° * 10.
-   VD = biais
-   stat = bias ~ group (control, stroke) (inter sujet) * body position (-30, 0, 30) * tâche (VV, SSA) (intra-sujet)

Résultats =

-   group * body position + tache * body position = *
-   SBA = ipsi tilt = ipsi shift; contra tilt = contra shift
-   VV = ipsi tilt = ipsi shift; **contra tilt = 0°**
-   **on a corrélation entre SBA et VV quand position upright et ispi tilt, mais pas quand contratilt** SVV pas modulé par contratilt, contrairement à PV chez patients. Comme si pas d'intégration des infos du corps...pas d'effet aubert... normalement effet aubert car VI devient plus fiable que le VG, mais pas ici ?... pourtant, on a effet sur pv ! 

![](https://i.imgur.com/0utFezL.png)


Discussion = Pour VV, on peut d’effet Aubert, shift qui va dans la direction du corps, uniquement quand tilt ipsi. C’est plutot en accord avec travail de Barra (2010), avec effet Aubert quand patient hypoesthétique incliné vers la droite, mais pas quand biais vers la gauche du côté patho. Mais pourquoi alors la SBA est ok si elle repose sur infos somato ?? **hyp de Barra = SBA repose sur schéma corporel, une R « centrale », insensible au changement de position du corps, ALORS QUE VV repose sur infos somato + latéralisée.** En tout cas, on pourrait avoir un partage de ressources entre mécanismes qui sous-tend SBA et VV.=> hyp un peu bancale quand même.

-   **A noter que le patient PS avait tilt -8° dans SBA et VV. donc ne soutient pas idée d’un mismatch entre VV et SBA. de +, si PS dépendait juste d’un tilt dans le plan frontal de la SBA ou de la VV, tout le monde serait pusher !!**

** **

-   **Reflexion perso = interprétation de type schéma corporel ne colle pas forcément avec idée que NSU perso aurait + de problème de PV ? le soucis ici c’est qu’on a pas testé la PV chez ces patients, et que le lien entre PV et SBA est en fait très hypothétique... de + la SBA peut être shiftée dans des plans différents, avec parfois des dissociations ... est ce que PV corrélé à problème SBA ? pas sûr que cela ait été testé !**

** **

-   **pourquoi ici VI et VG dissocié quand shift contra ? aubert effect c’est VI qui module VG. quand ipsi tilt, modulation, mias pas quand contra tilt, peut etre car pas d’intégration mss hémicorps gauche. si PV = VI, alors la dissociation VI-VG apparente quand shift contra, comme si dans ce cas VG ne pouvait pas communiquer avec VI...**

 


### Bisdorff, Wosley, Anastasopoulos, Bronstein, & Gresty (1996)

**Contexte =** On cherche dissociation entre perception du droit-dessus = *uprightness* et SVV. Pour cela on regarde range de précision et biais SPV chez patients avec diverses atteintes vestibulaires, et aussi chez des contrôles avec S opto/galv/tilt body. pitch + roll

**Méthode** = 
- Plusieurs populations
- 42 contrôles 
- 6-15 = chronic bilateral loss (>2ans) / Acute unilateral peripheral (3-7j) / chronic stable unileral peripheral / BPPV (benign paroxysmal positional vertigo) => several weeks / Meniere / Vertical nystagmus (lésion TC) / Longitudinal peripheral (Meniere) => neurectomie du nerf vestibualire VIII.
- Méthode = strap intégral assis head + torse
- benson (1989) => 1.5°/s => pas de S des CSC.
- Test dans pitch + roll
- réponse joystick
- manip => body tilt => on fait shift progressif 10° vers droite/gauche sur plusieurs cycles (voir Barra 2010 aussi).
- optok fild (30cm) + tilt pendant 6min. => vection subj ressentie.
- galva = sham/cl/cr. => ressenti subj = sway toward the anide (effet classique).
- SVV = réponse joystick.

**Résultats**
- bil = precision baisse dans es 2 plans/ pas de biais
- vertical nystagmus = idem
- BPPV = idem
- acute uni periph = idem / biais tendance ipsi
- chronic uni peiph = controle
- après neurectomie du nerf vb, on a biais ipsi SVV. ras pour spv.
- sujets sains => effet du body tilt adaptation seulement, que sur biais. on a aussi influence **S galvanique * sur précision spv** ! 
-  sur SVV, la S galvanique a une influence trop petite pour être significative, mais bon de noter que SVV avait tendance à shifter vers anode (effet classique).

**Conclusion =** Effet déficit vestibulaire sur précision roll+pitch de la spv. On a dissociation au niveau du biais entre spv et S visuo-vb. cela dit on voit que ref de verticale allocentré permet au référentiel de verticalité égocentré d'être + précis. Ca permet au modèle interne d'être + robuste. Va dans le sens de **2 voies graviceptives**.  
A noter que biais svv ici est dû à une réponse motrice vestibulo-spinal (nystagmus).

### Santos et al. (2018)
Méta-analyse pour connaître normes PV chez sujet sains.
- 16 articles=434 sujets
- critère = position assise, sain, bonne description de la méthode.
- **res1 = -2.87° à 3.11° biais +/- 2SD pour biais PV frontal**
- **res2 = -3.61° à 3.66° biais +/- 2SD pour biais PV sagittal**

### Santos et al. (2019) :-1: 

**Contexte =** noyaux post du th (vpt) lié à trouble de la verticalité. Ici on investit si matière blanche th et extra-th impliqué aussi. 

**Méthode =** 
- pop = 11 stroke + 25 control
- j+32 /mri +20
- exclusion patient avec tb cogn ! 
- plusieurs batteries cliniques
- pv/hv dans roll/pitch
- DTI + amri (critique = pour mri = on regarde que aires théoriquement impliquées trouble miv :-1: delineation manuelle...)
- On analyse FA (ansio) dans HD pour control group 
- On regarde aussi index de lat. 
- critique : pas de description des manips hv et pv...

**Resultats =**
- On retrouve chez stroke dim FA noyaux th. 
- stat :-1: : on reporte les médianes, pas de graphiques, corrélations pas corrigées, tests stat à gogo, pas de taille d'effet ... 
- Au final on retrouve corr entre certaines valeurs hv/pv/roll/pitch avec FA dim dans noyaux. 
- eg = lesion vpl + a + va + d predit shift vp roll. 

**Conclusion** =  mauvaise pratique imagerie, stats pas claire, multiples tests dans tous les sens, pas de bakground théoriques solides, on ne sait pas si nsu, tb posture, jamais dit clairement ... Evidences préalables cela dit peut être pour lien entre graviception somesthésique et matière blanche thalamique, mais sans lésion thalamique. Compatible avec PB observé en absence de lésion du th. matière blanche reliée à pulvinar, et noyaux ventraux et dorsaux. :-1: res pas généralisables à lésion corticale avec nsu.  

### Pérennou et al. (2008)

**Contexte **: Le lien entre misperception de la V et trouble de la posture pas établi de façon fine encore. On peut avoir latéropulsion/pushing par exemple dû à lésion TC, mais on aura pas forcément problème de verticale similaire à lésion hémisphérique (biais ipsi vs contra). Ici on essaie de démontrer que problème de latérop et pushing se trouvent sur un continuum de problème posturaux corrélé à problème de V.

**Méthode** :

-   Pop : 33 contrôles / **86** patients dont 6 ont lésion TC.
-   Evaluation SCP pour établir si latero + pushing « **pusher **» / latero seule (« **contralist**» vs « **ipislist** » si TC) / rien « **upright** »
-   IRMa
-   W + 12
-   lesion size = small/medium/large en fonction du nombres d’aires distinctes touchées. (pas de vlsm).
-   VV, HV, PV (strap total)
-   Limites stats : test non paramétrique … + PCA

**Résultats** :

1.  Descriptif = V * biais

-   PV : 34 contra, 1 ipsi (TC)
-   HV : 26 contra, 2 ipsi
-   VV : 44 contra, 13 ipsi (6/6 TC)
-   **18 transmodaux biais**, 41 ont ½ biais
-   PCA : VV et PV expliquent la majorité de la variance (87%)
-   Transmodal tilt en grosse majorité chez **RH stroke**

 

1.  IRMa chez H stroke

-   Extension lésion augmente avec nombre de modalités touchées (**confirme Barra 2010**)
-   No diff LH/RH * pour VV ou HV
-   **Biais PV RH>LH et augmente avec taille que pour RH**
-   Transmodal tilt implique lésions PAR **(SPL)**
-   **PV** implique spécialement **ROL et TH** (**confer Barra**)
-   De façon générale, implication capsule interne, cortex F-T …

 

1.  Lien entre pcp V et trouble de la posture

-   V plus atteinte chez **pushers > listing** (ipsilist = latéropulsion) > upright = contrôle
-   Upright = biais > contrôle, pas de diff entre modalité
-   **Listing = biais > contrôle**, idem
-   **Pusher > + PV bias > VV = HV**
-   **Corrélation +++ entre degré de latéropulsion et PV bias ! > HV > VV (toutes *)**

**Conclusion** : Résultats supportent modèle de la V davantage latéralisé à droite, avec problème de posture en lien avec une mauvaise construction de la V. IRMa supporte hyp d’un PIV cortex qui sous-tend la construction de la V. Les biais ipsi retrouvés chez H stroke sont rares, et restent un mystère. Latéropulsion chez TC pas toujours associée à pb de V (5/6 TC ont PV ok !, et **6/6 ont VV lésée**, **ce qui est compatible avec problème nystagmus quand lésion TC** voir Dieterich/Brandt).

NB = critique de Karnath : pp hémiplégique couché sur le côté : se sentent plus stable et donc plus droit. Aussi, tête et pieds non strapés, on peut donc avoir biais sensoriels dû au balancement de ces extrémités !

 

### Lafosse et al. (2004)

 

**Contexte théorique =** ER déviée ipsi chez NSU. Evidence d’une translation plutôt que d’une rotation. COG déplacée du côté de la ER. ER hypothèse favorisée par effet des S sensorielles sur reversion NSU. Ici on étudie ce phénomène chez RBD X à divers degrés.

**Méthode =**

-   Pop = 43 patients RBD +/- NSU / 10 sains. **M+18 +/- (varié).**
-   M +18 (plasticité ++)
-   Mesures pour catégoriser les sujets RH = **BIT (6 tests). BIT = barrage de ligne/lettre/etoile/LB/dessin de tête/copie. Cu off 129/227.**
-   **NO PUSHING !! position debout stable !**
-   On a donc 5 groupes = 10 en moy. Pas d’impact du stroke delay sur le BIT.
-   **Pour PV, patients ASSIS, stabilisé sur le côté avec jambes pendantes. On a aucune infos sur strap de la tête, du tronc ... ni de la vitesse de rotation ...**
-   Mesures **PV et COG**. Pour COG la tâche est de rester immobile 10s * 3. TENS appliquée en intra, session de 15min avant les mesures.
-   VI = yeux ouverts/fermés + TENS/sham + PV onset (variable modératrice en quelque sorte).
-   VD = biais

**Résultats =**

-   **PV**
-   Les patients NSU n’améliorent pas leur perf avec la vision quand no TENS.
-   NSU + et ++ ont shift ipsi par rapport à non NSU / **NSU +++ shift contra par rapport à NSU ++. en vrai = on est proche de 0° ici.**
-   Effet TENS = de façon générale, réduit shift ipis vers la vraie verticale.
-   **TENS fonctionne avec toutes les NSU sauf NSU +++ ! A noter qu’ici on a TENS + yeux ouverts pendant PV ! Pas d’effet de la TENS quand yeux fermés !**
-   Effet PV onset = même effet qu’avec le droit-devant ! quand début à gauche, on perçoit la verticale + à gauche, et vice-versa. Les hift onset a une influence quelque soit le groupe.
-   Dans condition L->R, peu de diff, dans R->L gros shift RBD vers la droite. (Effet pas intéressant ici).

** **

-   **COG**
-   NSU ++ shift ipsi comparé à NSU+/RBD/sain. 3 degré vs 6 degré.
-   **NSU +++ COG contra ! à noter = avec grosse variance du COG. = qui des fois pouvait être à la fois à droite ou à gauche !!**
-   Effet TENS sur COG = la TENS shift le COG vers la vraie verticale chez NSU+/++.
-   **Aucun effet sur NSU+++ **

**Discussion =** les NSU +++ ont variance+ forte, sensibilité très réduite pour estimer la V. Quand pas de TENS, pas d’influence de la vision quand **position assise** pour estimer la PV. Dans position debout, Perennou avait trouvé effet de la vision …. **La vision cela dit module l’effet de la TENS !** effet TENS * vision pour NSU sauf NSU+++. Interprétation = comme Karnath le suggère, 2 systèmes pour construire référentiel de la V, 1 visual /vb et l’autre postural.

Rés en accord avec Honoré il me semble, cad que chez PS qui ont oui/non NSU, on a shift de la SBA vers le côté ipsi quand on regarde corrélation avec SCP scale ! **MAIS ici les patients n’avaient absolument de PS** !! cela montre que l’on peut avoir des cas de dissociation entre PS et NSU, et qu’un problème de verticale posturale peut être liée à NSU (NB et BN ? on ne le sait pas ici...) sévère sans forcément avoir PS. Bien que biais chez NSU sévère proche de 0°, on a variabilité +++. l’absence de biais contra pas forcément expliquée par absence de PS, car Karnath a trouvé NSU + PS avec shift PV vers côté ipsi ...

### Nakamura et al. (2019)


**Contexte théorique =** la PV est déterminée à l’aide de signaux somatosensoriel. Quand on manipule ces signaux, on manipule la perception de la verticalité ou de la posture.Indices somato peuvent $etre d’origine viscérale, venir des récepteurs du dos, des jambes, des fesses ... Ici on manipule récepteurs du postérieur quand personnes assises afin de moduler la PV, avec ou sans perturbation vestibulaire.

**Méthode =**

-   pop = 15 C
-   traitement = GVS et/ou mousse sur le siège pour biaiser retours vestibulaires et / ou somato.
-   apparatus = strap total, sauf des jambes ne touchant pas le sol, et bras posés sur cuisses.
-   A noter que PV a été testée 1 mois avant avec les mêmes sujets, afin de tester la reliabilité test-retest, bonne en l’occurence.
-   contrôle de la vitesse de rotation de l’apparatus pour minimiser stimulation CSC.
-   36 essais = 3 position (11,12,13 degrés) * side * 6 trials. A noter que déviation de départ très minime ici en comparaison de ce qui se fait avec d’autres paradigmes comme la SVV.
-   GVS = pas contrebalancée, uniquement anode à droite. Donc pas de contrôle d’effet attentionnel ici, de type vigilance. Test préalable de la GVS pour s’assurer de l’effet habituel chez les sujets (illusion de mouvement).
-   VD = magnitude (abs) et direction (bias) de la PV
-   VI = group en intra sujet (control + vb + vb/somato + somato) bonferoni en posthoc.

 

**Résultats =**

-   bias =
-   somato = control = vb, avec déviation vers la gauche. résultat intéressant= pas de déviation vers anode dans condition vb ! par contre, effet anode quand associé à dégradation du signal somato !!
-   abs = accuracy
-   somato avec ou sans vb a magnitude plus éloignée de 0 vs vb ou control.

**Discussion =** modèle interne de la verticalité en intégrant multiples signaux. PV repose sur signal somato, car quand vb dégradation, le signal somato compense la perte de signal vb « fiable ».

**Limites =** pas de contrebalancement GVS. On a aussi, inexorablement, des différences de paradigmes (position de départ, apparatus, position assise ou debout...).

 

 



### Karnath, Ferber, & Dichgans (2000)

 

**Contexte théorique =** Pusher syndrome patients poussent leur corps vers la côté hémiparétique, par peur de tomber du côté sain. La latéropulsion c’est le fait de s’accrocher à qql chose pour ne pas tomber. Le PS ici pas associé dans litté spécifiquement à NSU. Ici on étudie si PS patients ont PS lié à une mauvaise estimation de la gravité.

**Méthode =**

-   Pop = 5 PS NSU + 5 HD sans PS dont 4 avec NSU + (5 HD sans PS/sans NSU + 5 sains) 10 contrôles.
-   **Diagnostique NSU = cloches/lettres/copie**
-   Mesures = PV + VV
-   Patients assis avec jambes pendantes. Réponses verbales. Pour VV = position droite. Pour PV = rotation.
-   Yeux ouverts/fermés avec PV.

**Résultats =**

-   **SVV normale chez tout le monde.**
-   Yeux fermés = PS + NSU PV biais ipsi à pas expliquée par NSU, car patients no PS + NSU n’ont pas ce biais !
-   Yeux ouverts = PS + NSU normalisent leur PV ! (**Résultat non retrouvé par Lafosse 04).**
-   Chez 2 pushers avec NSU, PV avec verre floutant les repères, biais ipsi à nouveau.
-   2^ème^ expé avec ces deux patients, on les incline directement dans leur propre PV pour faire VV. On retrouve VV normale, comme dans la condition VV yeux ouverts.

**Discussion =** biais ipsi++ chez PS, pas en lien direct ici avec NSU. La vision joue rôle sur PV, avec PV ipis dans noir/lunette floutantes. La SVV est intacte que le patient soit assis ou en PV ipsi ! Bizarrement, pas de compromis entre VV et PV dans la vie de tous les jours !! **Hyp 1 = tentative de compensation ou d’alignement du corps avec V gravitaire. Comme si shift de la PV vers la VV ! Hyp 2 = instabilité posturale à cause du shift PV.** On a bien double dissociation ici, donc peut être 2 systèmes gravitaires indépendants, lien avec modèle interne VI+VG, mss ; **Peut être  aussi double réseau ind, dont un serait associé à VV +PIVC (+ NSU ?) avec/sans PS ET l’autre associé à PV +PS X mais quel substrat ?**

Enfin, à noter que dans optique modèle interne mss, c’est input viscéral qui jouerait rôle, avec input ppc (jambes) qui joue rôle indirect en modulant les inputs viscéraux.  Quand je change de position, je change ppc mais aussi j’ai signaux viscéraux différents (pression vaisseaux sanguins, viscères abso, reins…, **voir Pérennou 02 aussi**).

Aussi à noter que patients hémiplégique sur le plan somato avaient PV+VV intactes. Résultat différent de Barra qui avait trouvé biais contra MAIS en position debout ! ici assis… **Variable posture -> variable modératrice ?**



### Anastasopoulos, Haslwanter, Bronstein, Fetter, & Dichgans (1997)

**Contexte théorique** = tester contribution du système vestibulaire dans la PV

**Méthode =**

-   pop = 20C + 5(1 meniere + 4 vb neuritis) (4 right side)
-   acute stage, pas de compensation, J+3/4
-   VV = joystick + 6 essais (+/- 40)
-   PV = 12/16 essais. (5/15/25). on tourne doucement la roue à vitesse différente pour éviter cueing.
-   apparatus = roue

**Résultat =**

-   VV ipsi (classique)
-   SPV ok ! == control.

**Discussion =** renforce idée que poids soma ++ pour estimer PV. Ref à voir = dichgans young brandt 1972

### Bisdorff, Bronstein, Gresty, & Wolsley (1996)

 

**Contexte théorique =** On regarde si PV peut être modulée par stimulation visuelle, vestibulaire, ou somesthésique (simple déplacement de la roue)

**Méthode =**

-   pop = 10C
-   apparatus = roue, strap torse + head
-   traitement =
-   1) pendant déplacement de la roue, stimuli optokinétique qui tourne aussi dans le même sens ( ?) (cond = avec vs sans)
-   2) pendant déplacement de la roue, stimulation galvanique sur les mastoïdes (anode left vs lright vs sham) +

--> tilt de 5/10 +/- à chaque fois

-   PV = réponse joystick après déplacement (méthode des limites, on dit jusqu’à où on se sent vertical).
-   VD = range of uncertainty + bias (constant error)

**Résultats =**

-   optokinétique = effet fonctionne quand debout, mais quand pas quand rotation dans roue. intéressant de noter que les sujets rapportaient sensation de vection (je bouge alors que non) mais se sentait verticaux quand même !!
-   galvanique = effet debout, mais pas dans la roue.
-   en revanche, on retrouve un effet de la direction du tilt, avec biais congruent. mais pas d’effet sur range of uncertainty. pas d’effet du temps non plus, effet immédiat.

**Discussion =** tilt modifie inputs soma et donc PV. le sens vb pourrait permettre une plus grande précision, avec moins de poids = lire ref clark par ex. rés intéressant = perception plutôt contradictoire dans le cas de la stimulation optokinétique.

### Baggio et al. (2016)

 

**Contexte :** Faire le lien entre problèmes de postures chez patients AVC et perception biaisée de la **PV ou HV** à l’aide d’une analyse de type **posturographie**.

**Méthode :**

-   Sélection de patients stroke (AVC = isch ou hemo), avec MMSE + mRs (échelle de sévérité de l’AVC)
-   On regarde si NSU aussi avec test vestib et sensoriel
-   Outcome : HV, PV (**algebraic**), posturiographie, échelle de fonctionnalité

**Résultats :**

-   PV corrèle avec posture dans toutes les conditions : si problème de PV, problème de posture dans toutes les positions (assis/debout/yeux ouverts/fermés, dans roll + stable/instable).
-   Corrélation * avec HV dans peu de condition
-   Deviation SH/PV corrèle avec perturbation sur échelle clinique

**Discussion** : le résultat majeur est le lien entre déviation de la PV et problème de posture quand patients assis. Pas de résultat dans le pitch plane par contre. (NB = pourtant, pb de déséquilibre avant/arrière des fois). Pas de lien clair entre problème posture et HV ici car rés ns quand patients assis (criticable = déficit peut être moins sévère assis).

**Limites** : beaucoup de tests : risque de faux positifs +++ ! Les auteurs reconnaissent **l’influence de facteurs cognitifs et musculaires** qui influencent aussi le contrôle postural (en plus d’un problème de modèle interne).

 

 

### Bergmann, Krewer, Selge, Müller, & Jahn (2016)

 

**Contexte** : Résultats contradictoires concernant le biais du modèle interne de la V chez les pushers. Karnath observe un tilt ipsilesionnel (push = compensation) et Perennou observe tilt contra (push = tentative d’alignement). Ici on réplique ces hypothèses mais en position debout (la plus sujette au mild/recovered pusher behavior) avec en plus plane pitch.

**Méthode :**

-   Pop = 8 pushers **stroke**, 10 AVC **sans push**, 10 contrôles
-   3 tilt (12, 15,18)
-   On regarde la **PV** * pitch/roll * 3 groupes
-   On regarde corrélation de la sévérité de la PV avec échelle **BLS et SCP** (< **BLS pour détecter mild pushing**).
-   Outcome : SPV range (max-min) et error (alg)

**Résultats :**

-   **Range** PV pusher > non pusher et control (pitch et roll) = plus d’instabilité
-   **Error** PV pusher > (**roll seulement**) = biais **ipsi**
-   **R* entre SPV et BLS**, pas avec SCP

**Discussion :** Rés en faveur de théorie de Karnath, biais R ipsi, donc le pushing behavior vu comme tentative de compensation. On a bien perte de sensibilité (moins bonne intégration mss ? moins bon signal to noise ratio ?) du modèle interne de la V.

**Limites **: diff avec expé de Perennou : **uniquement fixation des genoux ici** (fixation de partout avec Pérenou). Débat = est ce que c’est bien d’avoir une position écologique ? Mvt plus libre ? donc plus d’input somato ? (eg : pied au sol ici, mais pas dans Karnath). **Critique forte = patient pas strappé du tout en haut !!!**


 

 

 







### Weight Body Asymmetry - Rocking Chair - Behavioral Vertical - NSU

### Prismatic Adaptation

### Michel, Rossetti, Rode, & Tilikete (2003)

**Contexte =** AP agit sur cadre de référence spatial corporel/RE/schéma corporel, comme l'a montré Rode (1998). Du coup, on veut répliquer cet effet chez sujets sains pour confirmer effet AP sur posture.

**Méthode =** 
- pop : 14 sujets controle = 7/7 rpa/lpa
- vd posture : centre de masse : variation sur x/y/surface totale
- vd "bonus" : la svv (plutot bien evaluée ici, même si réponse joystick et que la posture n'est pas contrôlée, ce qui est peut etre embetant ...?)
- vd controle sm : ssa + pointage open-loop (sans vision fb)
- tâche ap : 15° sur 20 minutes vers 10 cibles sur champ visuel. 
- design pre-post test + yeux ouvert/fermés + group rpa/lpa

**Results =**
- var controle sm : effet classique dans les 2 tâches pour les 2 groupes : montre bien que ap fonctionnelle.
- postural after-effects : 
- surface area : yeux ouverts : ns/ yeux fermés : augmentation globale
- mean x (lateral deviation) : yeux fermés : sessionXgroup interaction avec effet * pour groupe lpa avec shift vers la droite du centre de pression. yeux ouverts ns. 
- mean y : yeux fermés : shift vers l'avant global / yeux ouverts : idem
- svv : ns mais interessant de noter que chez groupe lpa le shift vers la droite est corrélé à shift de la svv vers la gauche ! 

**Discussion =** Comme Rode, on pense que effet ap agit pas seulement sur arcs reflexe/bas niveau mais aussi sur cognition spatiale en lien avec contrôle de la posture. Ici ap a démontré effet sur pcp espace extra-perso, et ici espace perso ! De + à noter que effet spé à lpa, effet assez classique qui démontre que c'est bien spécialisation hémisphérique de la cognition spatiale qui joue un rôle.  
D'un point de vue neural, confirme idée que ap agit sur aires par impliquées dans integration mss. tpj ? ppc + globalement ?

**Ref +++ à lire =** Richard 2001 = compression hc gauche ! / grusser 1990 : importance aire TPJ pour tt soma + vb. 

### Tilikete et al. (2001)
(Article bref)
**Contexte =** Rode a montré effet CVS sur WBA chez lesion droite avec hemiparesie. Quid des prismes ?

**Méthode=** 
- pop = 15 RBD => 5/5/5 pour RPA/LPA/neutres = bonne méthodo qui élimine interprétation de type "arousal" = effet non spécifique.
- M+2-3
- important = pas de trouble de la posture = pas de PS. 
- pas de nsu ici.
- ap 10° + 50 pointages
- balance = tenir debout = CoP x/y
- contrôle sm OLP ok

**Résultats =**
- pre test Cop = effet classique CoP ipsi.(droite). 
- effet pre-post uniquement pour RPA pour cop x. 

**Conclusion =** Supporte travail de Rode 98 (+ tard supporté par michel 2003 et indirectement par nijboer 2014). Convaincant = effet spécifique direction des prismes RPA. interprétation= PA agit sur schéma corporel : représentation spatiale du corps qui sous-tend l'action.



### Nijboer, Olthoff, Van der Stigchel, & Visser-Meily (2014) :+1: 

**Contexte =** PA a montré efficacité sur wba chez hémiparétiques et sur sujets sains. Ici on veut étendre ces résultats aux patients hémi-négligents seulement (même si patients hémiparétique avec lésion droite de Michel 2003 avait sûrement pas mal de nsu dans le lot, mais pas reporté cela dit...)

**Méthode =**
- pop = 10 nsu (cancellation parmi distracteurs + lb / nb = version souris/ordi)
- j+37
- critere diag = tenir assis seul = dommage car exclu de facto les patients avec trouble postural sévère, comme pusher PS. 
- Tâche : rester assis yeux ouverts/fermé sur une balance. On enregistre CoP et postural sway. NB = méthode vd similaire a Pérennou 2000/2002 ! Tout pareil, sauf qu'on se restreint à patients nsu. 
- vd = cop/postural sway en x/y.
- intervention RPA = +10°/3 cibles g/m/d
    - contrôle sm = open loop pointing (OLP). Contrôle intéressant : minimum 3cm d'after-effect, sinon on continuait au delà des 100 essais de bases. 
    - a noter que couleur utlisiée pour faciliter pointages vers la gauche.
    - Critique = pas de groupe contrôle ici (lpa ou npa = neutral pa)
- design pre-post test

**Resultats =**
- Cop = effet pre-post uniquement pour CoP horizontal/X + yeux ouverts. Comme chez Michel 2003. On avait à la base CoP décalé vers la gauche(contrashift) qui se déplace vers le vrai 0 (vers la droite)
- Très important : **shift Cop initial contralesionnel**.
- Sur le plan pitch plane, on a aussi shift vers l'avant ! 
- Postural Sway = idem effet X avec/sans vision. => réplique Pér. 00 ! **Effet uniquement dans le plan roll,  pas d'effet dans le plan pitch** 

![](https://i.imgur.com/Ao6MKzk.png)


**Conclusion =** Rés en accord avec travaux de perennou, cad postural instability evalué assis independant de si vision ou non après les prismes. On a donc amélioré modèle interne de verticalité en agissant sur inputs soma avec RPA. Rés plutôt compatible avec hyp d'un shift contralesionnel rotation de la verticale posturale. Cette étude va dans le sens d'un effet des prismes sur verticale postural, ce qui est unique! Va dans le sens de l'effet de la TENS sur VP aussi ! Point commun => stimulation sensorielle qui cible aires mss améliore VP. 



### Rossetti et al. (1998)

 

**Hypothèsèses** = les patients NSU s'adapteraient aux lunettes prismatiques (expé 1). Cette adaptation sensorimotrice pourrait influencer des tâches visuo-spatiales utilisées pour diagnostiquer la NSU (expé 2).

**Méthode** = On comparait des sujets sains à des sujets NSU dans une tâche de pointage droit devant avant et après RPA (expé 1). Dans la 2^ème^ expérience on comparait deux groupes de patients NSU avant et après PA dans plusieurs batteries de tests (barrage decible,  bissection de ligne motrice, copie de dessin, dessin de mémoire, lecture). Le groupe contrôle était soumis à une PA factice (prismes neutres). Le groupe NSU expérimental était soumis à la RPA.

**Résultat** = Après adaptation prismatique, les patients NSU et les sujets sains pointaient davantage vers la gauche (expé 1). Par la suite, les signes de négligence diminuaient  chez le groupe NSU avec RPA comparé au groupe NSU contrôle, jusqu'à plus de deux heures après la PA.  
**--> Amélioration de la SSA**

**Conclusion** = La PA induit une adaptation sensorimotrice qui améliore les signes de négligences chez les patients. Un nouvel apprentissage sensorimoteur pourrait donc réorganiser la cognition spatiale et affecter en retour des représentations spatiales de plus haut niveau, impliquées par exemple dans la bissection de ligne et le barrage de cible.



### TENS - Posturography 

### Pérennou et al. (2002) 

**Contexte **: Analyse de la verticale comportementale (axe longitudinal du corps), subjective et implicite, que l’on aligne sur la verticale gravitaire, objective. Ici on analyse la posture de patients NSU, segment par segment corporel, pour estimer quel input sensoriel, négligé ou déficitaire, contribue à la ltp. Si soma = le pelvis (le petit bassin), si Vb = la tête.

**Méthode **:

-   14 sujets cérébro-lésés, dont 3 pushers atteints de NSU. IRMa. + 8 sujets sains.
-   M+ ??
-   Tâche = s’asseoir sur une plateforme instable pour forcer le sujet à utiliser cette verticale comportementale pour être stable = **se maintenir dessus durant 8 secondes.**
-   **On analysait alors la position de 4 segments corporels : tête/épaules/lombaires/pelvis grâce au système VICON.**
-   VD = nombre d’essais nécessaires pour réussir la tâche 2 fois deux suite.
-   VI : groupe * vision/no vision
-   Corrélations non par.

**Résultats** :

-   Non pusher = control
-   Pusher pelvis dévié à gauche > non pusher et contrôles
-   Pusher bias augmente quand yeux fermé > autres groupes
-   Chez les 11 non pushers, plus on a un biais du pelvis à gauche, plus on met d’essais (chutes) pour réussir la tâche, dans les conditions vision/non vision
-   PCA : NSU explique 60% de la variance de la **posture du pelvis et des chutes** dans les 2 conditions > degré de faiblesse motrice qui explique 10% des problèmes de posture du pelvis.

**Conclusion** : On a un continuum du problème de posture associé à NSU pour les déviations les plus extrêmes. On a peu d’influence de la vision chez non pusher et contrôle, ce qui montre importance des inputs soma dans la V cptale. Quand on  a problème soma +++ comme chez pusher (hyp) on est encore plus handicapé sans vision. Les informations graviceptives vestibulaires, liées à la tête, semblent correctement intégrées chez les pushers, bonne orientation de la tête. Soutient hypothèse d’un vecteur idiotropique biaisé dû à négligence somesthésique = **gravicepteurs somesthésiques se trouvent dans reins, pression hydrostatique dans les vaisseaux sanguins, tendons de Golgi, signaux moteurs afférents (lutte contre la gravité).**

 

### Pérennou et al. (2000)

**Contexte théorique =** On décrit ici la notion de modèle interne, un processus cognitif général s’assurant de la synthèse d’information sensorielles et de signaux top-down. On défend l’hypothèse qu’une atteinte TPJ entrâine une mauvaise intégration d’information corporelle, et donc un trouble de la posture. Ce trouble postural devrait donc être de nature multimodale si le modèle interne nécessite corrélats TPJ.

**Méthode =**

-   pop = 13 RBD + 9 LDB + 14C
-   lésion MCA
-   variable démo = spasticity + weakness + somato loss + NB (bell)
-   Tâche décrite dans Pérennou 2002. Tenir assis sur « rocking chair » pendant 8s , yeux ouverts ou fermés.
-   Intégration visuo-vestibulaire-somato est donc possible ici ! tâche qui requiert un tt « multimodale »
-   VD = nombre d’essais pour réussir la tâche 4 fois avec et sans vison (8 succès).
-   VD2 = dispersion angulaire = mesure de l’instabilité posturale.

**Résutat =**

-   *essais manqués =*
-   tâche + dure pour patients > contrôle.
-   On regarde ensuite cor avec cct patients : **instabilité augmente surtout avec NB NSU ! avec et sans vision.**
-   Lien avec le site lésionnel = 9 sites choisis, on performe comparaison entre 1 site vs les autres. **Seul lésion TPJ augmentait * nombre d’échec comparé aux autres sites, avec et sans vision.**
-   **Perf RBD < LDB avec ou sans vision.**

** **

-   *dispersion angulaire =*
-   avec vision, + de var chez patients, mais pas sans vision (dans cette cond, 5 exclusion chez patient, faible échantillon).
-   On regarde ensuite cor avec cct patients : **instabilité augmente surtout avec somato loss + NB NSU ! avec et sans vision.**
-   Quand on regarde chaque site lésionnel, seul le TPJ ressort une fois que lesion size est contrôlé. TPJ ressort seul quand pas de vision.
-   **TPJ lésion a même effet avec vision et sans vision, seulement l’auteur ça témoigne d’un manque d’intégration mss, et pas d’un seul canal sensoriel** pour contrôler la posture.

**Discussion = On retrouve dominance hémisphère droite pour problème de posture,associé surtout à NSU, avec lésion TPJ sous jacente. On pense que NSU graviceptive ici avec mauvaise intégration partie gauche du tronc. Néanmoins, on pense à un overlap entre zone NSU et zone qui assure la posture de façon multisensorielle. De +, si c’était seulement une atteinte vestibulaire, la vision aiderait à compenser le trouble postural, ce qui n’est pas le cas. On fait l’hypothèse que le TPJ n’intègre pas que info du tronc, mais aussi coord égocentrées des yeux, de la tête ...**

 

### Pérennou, Amblard, Leblond, & Pélissier (1998)

**Contexte théorique =** on a trouvé dissociation entre verticale posturale et visuo-vestibulaire (cf Bisdorff). PV repose sur infos somatique. Dans ce cas, elle devrait être sensible à la TENS. Problème :-1: **rocking chair paradigm**, pas vraie spv ! 

**Méthode =**

-   pop = 22 stroke = 13 RBD + 9 LDB
-   diag clinique = motor weakness/somatoloss/nsu (canc)/spasticité/HH
-   Paradigme = rocking chair. main croisée sur genou et jambes qui pendent.
-   Tâche = posture assise pendant 8s.
-   VD = orientation du support par rapport à 0. renseigne sur tilt de la PV contra ou ipsilesionnel. (on normalise en fonction de l’atteinte gauche ou droite).
-   VI = VI inter = group / VI intra = vision + TENS (baseline vs pendant). NB = pas de TENS chez sujet sain (bof ... ?)
-   TENS appliquée à partir de 10min avant la tâche et pendant la tâche sur le cou contralésionnel.
-   Limite = pas de stimulation sham ici (il manque un groupe contrôle). Donc recrutement attentionnel vers le côté contralésionnel possible. Cela dit explication attentionnelle dans tâche posture moins probable que dans tâche de jugement égocentré ? à voir

**Résultat =**

1.  baseline

-   AVEC VISION
-   PV contra tilt chez 19/22 patients vs contrôle.
-   corrélation entre PV bias et NSU
-   corrélation entre PV bias et hémianesthésie sévérité (problème = pas une variable continue...).
-   cutoff patho fixé à 2SD (3°) , dans ce cas on a 8RBD patho, dont 6 NSU.
-   SANS VISION
-   même résultat pour PV
-   ns, pas de relation entre PV et une variable clinique.

1.  TENS

-   AVEC VISION
-   On a réduction du PV tilt chez PV patho vs non patho (à noter = petits groupes, 8 vs 14 ...).
-   SANS VISION = ns (trend).

**Discussion =** Le biais contre de la PV peu affecté par le facteur vision, ce qui confirme que PV repose surtout sur signaux somatic et vestibulaire. Le fait que PV corrèle avec NSU et hémianesthésie va dans le sens d’une négligence des informations somatiques contralésionnelles, c’est à dire venant des « récepteurs graviceptifs du tronc » (Mittelstaedt).

-   Spéculation = intégration asymétrique des infos du tronc participerait à distorsion de la référence égocentrée qui sous-tend la verticale posturale et de façon plus globale l’orientation du corps dans toute les dimensions de l’espace.


### Longitudinal Body Axis

### Barra, Oujamaa, Chauvineau, Rougier, & Pérennou (2009)

 

**Contexte théorique =** Phénomène robuste en clinique = asymétrie du point d’appui du côté ipsilésionnel. N’apparaît pas très logique par rapport au pushing syndrome ! ici on regarde si lien en WBA (weight-bearing asymtry) et VV, SBA, ou encore autres variables comme NSU ou gait...

**Méthode =**

-   pop = 22 stroke = 13 RBD + 9 LBD
-   W + 4-30
-   diag clinique = inclusion uniquement des patients quand position debout at least 37 s. Limite ici = biais de sélection puisqu’on exclut d’emblée le spatients PS avec déficit +++.
-   NSU = bell + bisection + CBS (contient items sur BN du visage).
-   PASS (posture) + SCP + GAIT + MOTOR WEAKNESS + HYPOESTHESIA
-   apparatus = VV position assise, SBA position couchée, pour dissocier rôle des signaux somesthésiques et visuo-vb.
-   10 essais VV + SBA.
-   VD VV + SBA = bias
-   VD posture = pourcentage de poids du corps du côté ipsilésionnel, moyenné sur 4 essais.
-   Stat = on a ANCOVA avec hypoes et motor weak comme covarié quand on analyse lien entre les 3 VIs. On regarde aussi corrélation entre WBA et variable cliniques.

**Résultat =**

-   WBA * clinic variables =
-   63% poids ++ ipsi WBA.
-   corrélation * WBA * CBS / hypo / motor MAIS PAS avec visual neglect ! (LB ou cancellation)
-   WBA * posture/gait = * avec SCP et PASS.
-   WBA * VV * SBA =
-   SBA * VV corrélé +++
-   **SBA * WBA > VV * WBA (ns) => + on a shift contra de la SBA, + on WBA fort côté ipsi ! Suggère que problème WBA davantage lié à déficit égocentrique que allocentrique.**
-   Cette relation est + forte chez RBD.

**Discussion =** Bien que problème hypoest, motor weakness, jour dans WBA, on a aussi un effet d’un déficit de cognition spatiale ici ! biais R égocentrée + liée à problème de posture. Intéressant de noter que les auteurs postulent un modèle interne de la posture très lié au schéma corporel. la SBA serait utilisée comme référence posturale. En revanche, mécanisme derrière WBA reste un mystère ! car plusieurs explications possibles = impossible que PS soit alignement avec VV ou SBA. peut être que WBA est une solution pour contrer le PS ! ou selon d’autres auteurs, WBA reflète **shift de la SBA vers la droite dans le plan horizontal**.

-   mais si cette dernière hyp est vraie, l’expé de Lafosse (2004) est fort troublante, car ils trouvent un biais dans la PV qui va du même côté que WBA !! cela dit plusieurs limites = apparatus peut être différent, pas d’évaluation de la NSU perso, et de + on a des patients avec NSU sévère sur échelle visuelle extra-perso qui ont COG partout (variabilité +++) avec shift PV à gauche.

### Barra, Chauvineau, Ohlmann, Gresty, & Pérennou (2007)

 

**Contexte théorique =** pushing pourrait être due à une rotation de la RE.

**Méthode =**

-   pop = 26 C + 18 S = 10RBD/8 LBD
-   J + 80
-   aIRM M+2 = lésion classifiée petite ou grande (pas de vlsm ici).
-   diag clinic = bell + line + CBS + hypoest + spasticity + motor weakness+ HH + SCP + PASS+ gait
-   procedure = sujet couché (strap latéral), dans le noir, avec SBA judgement rotation dans modalité visuelle ou haptique (contrôle pour shift gaze).NB = milieu ligne centré sur nombril.
-   T = 10
-   shift initial 10à 30 degré
-   consigne imaginer ligne imaginaire de la tête au milieu des pieds.
-   VD = SD, bias
-   test retest coeff très haut chez 10 patient à un 1 jour d’intervalle.
-   norme calculée sur base des sujets sains.

**Résultats =**

-   BIAS = SBA shift contralésionnel stroke > C.
-   8/18 S shift patho => tâche haptique corrélée ++ à visuelle. => atteinte multimodale , avec atteinte striatum, corona radiata, rolandic cortex => corrobore rés de rousseaux.
-   SD = S >C. dont 5/18 patho.
-   SD et bias pas corrélé.
-   bias RH> LH + large lesion > small.
-   corrélation bias avec NSU et sensory loss
-   corrélation avec shift posture , mais pas avec spasticity et motor weakness.

**Discussion =** interprétaion cognitive de ce shift. Interprétation = infos latérale donée par strap est traité de façon asymétrique par NSU et sensory loss. A noter, comme ne 2008, que patient pusher sont suelement 2. donc shift de la SBA seule n’explique pas pushing. Confirme bien que pushing a une origine graviatire.








### Pushing Behavior

### Neuro-Anatomy

### Baier et al. (2011)

 

Contexte théorique = Recherche sur corrélats neuronaux du PS, avec lésion thalamique. Contrairement à Johannsen, on utilise VLSM, une technique plus puissante car on a une VI continue et non pas dichotomique comme avec la méthode soustractive. On regarde en plus corrélation cptale entre PS et SVV.

**Méthode =**

-   Pop = 16 RBD PS + 7 LDB PS + 22 RBD + 21 LDB
-   Subacute (5-6-7)
-   SCP
-   NSU = star seulement
-   VD IRM = SCP scale (pas dichotomique).
-   VD VV = accuracy + bias
-   VV trial = 7 (**méthode décrite en 1994**) tâche visuo-motrice avec joystick je crois
-   IRM = DTW + FLAIR

**Résultat =**

1.  SVV

-   Accuracy = Diff PS noPS ns entre LDB et RBD.
-   RBD = PS ont biais + sévère que no PS.
-   Bias = effet principal LDB/RBD avec biais contra à chaque fois > à biais ipsi.

 

1.  IRMa

-   Modèle = voxel ~ SCP + NSU + size.
-   Aucun voxel * quand correction.
-   Quand pas de correction =
-   RBD = Ip + STG + op + WM
-   LDB = Ia + op + internal capsule + thalamus latéral

 

**Discussion =** Zones impliquées ici dans PS confirme partiellement la litté. Pas d’activation th post ici. Peut être qu’on a un manque de puissance, de + voxels pas * avec correction. Seulement 7 patients LDB PS. On retrouve aussi dissociation entre NSU et PS. **Les auteurs avancent que lésion th p peut être pas absolument nécessaire pour PS mais que th p pourrait être impliqué plus largement dans codage de la gravité avec inputs vb ! Selon eux, lien entre contrôle postural et centre d’ intégration mss lésé. Congruent avec imprécision dans la SVV + forte chez pusher.**

Dieterich M, Brandt T (1993) Thalamic infarctions: differential effects on vestibular function in the roll plane (35 patients). Neurology 43:1732–1740

Ticini LF, Klose U, Naegele T, Karnath HO (2009) Perfusion imaging in pusher syndrome to investigate the neural substrates involved in controlling upright body position. PLoS ONE 4:e5737. doi:10.1371/journal.pone.0005737

### Johannsen, Broetz, Naegele & Karnath (2006)

**Contexte théorique =** En 2006 on postule hypothèse de deux système gravitaires, visuo-vestibulaire et somatique, car cas de double dissociation. Hypothèse négligence graviceptive/somatique chez PS, qui implique lésion des noyaux thalamiques postérieurs. On étudie ici lésion chez patients PS sans lésion thalamique.

**Méthode =**

-   Pop = 10 LDB PS + 11 RBD PS + 12 LDB + 12 RBD
-   1^er^ AVC unilatéral
-   IRMa en phase hyperacute + acute = 3.3
-   IRM DTW hyperacute + FLAIR acute (>48h) = soustraction + identifier center of overlap between PS RBD and RBD
-   SCP
-   NSU = letter + bells + baking tray test + copy (à noter que 100% des RBD sont NSU, et 10% des PS LDB sont NSU).
-   Aphasia

**Résultat =**

-   Center of overlap entre PS pour chaque hemisphère = comparable = Ip + PoC + WM voisine
-   **PS – no PS** **= left Ip + left STG + left LPI + right PoC**

**Discussion =** résultat encourageant qui va dans le sens de la littérature, avec voie graviceptive qui convergerait des récepteurs du tronc vers le thalamus postérieur, puis vers aires somatiques dans le gyrus PoC et l’insula, **tel que décrit chez le singe**. (et l’homme ? voir Lopez 2011)

Limite = manque de puissance dans cette étude, car patients PS sans lésion thalamique rare. Recrutement sur 6 ans et à peine 21 patients.

**Réf intéressante à approfondir =**

Bohning DE, Lomarev MP, Denslow S, et al. Feasibility of vagus nerve stimulation–synchronized blood oxygenation level– dependent functional MRI. Invest Radiol 2001;36:470–479

Chae JH,Nahas Z,Lomarev M, Denslow S,Lorberbaum JP,Bohning DE,etal.(2003) A review of functional neuroimaging studies of vagus nerve stimulation (VNS).J Psychiatr Res 37: 443–455

 

### Paci, Baccini, Lucio, & Rinaldi (2009)

 

**Revue =** Pusher behaviour : a critical review of controversial issues

**Résumé =** 
- Selon la littérature, pas de lien systématique entre NSU et PS. On devrait plutôt parler de PB que de PS. Beaucoup de biais, grosse variabilité selon les études, car biais d’hospitalisation et biais d’échelle. Beaucoup d’échelles dans la littérature, dont la SCP, révisée à 2 reprises, et sur le point de se transformer en SCALA ! (NB = consistance interne = autocorrélation, alpha Cronbach). Débat sur PB ou PS = cpt indépendant, ou fait partie d’un syndrome ? Proximité avec NSU peut être dû à un overlap lésionnel, car double dissociation existe.

- Quels mécanismes ? Karnath pense à un mismatch entre des infos du tronc et la verticale visuelle. Néanmoins le PS ne s’aligne pas avec la VV et va au-delà. Pérennou pense plutôt à mauvaise perception VV, et tentative d’alignement avec la VV. Mais on peut avoir PS et une bonne VV !? Perennou 2002 induit le terme NSU graviceptive, pas d’intégration de l’hémicorps gauche, et donc biais vers la gauche. Résultat contradictoire peut être à cause de paradigme différent. On a aussi résultat contradictoire pour la VV chez NSU. Aussi, certains auteurs ont étudié LBA chez NSU.

- Localisation des lésions ? PS semble impliquer systématiquement noyaux latero-ventro-posterieur du thalamus.        
- **Retrouvé impliqués dans PV et patients hypoesthétiques. Hypothèse d’un circuit avec boucle cortico-thalamo corticale !** Hyp = on pourrait avoir là un circuit distinct du circuit visuo-vestibulaire. Cela dit, on a trouvé lésions **left post insula**, STG, LPI, PoC. => **régions distinctes du PIVC ?
- **On a zone qui pourraient être liées à NSU perso ? tt égocentrés ? approfondir la litté !!**

**Ref à lire =**

Pedersen, 1996

Premoselli, 2001

Reding et al ????


### Straight Ahead 

### Honoré, Saj, Bernati, & Rousseaux (2009)

 

**Contexte théorique =** Lien entre déplacement ER/SSA et NSU. Translation plutôt que rotation. Ici on examine si rotation ou translation de ER chez NSU avec problème de posture PS. Afin d’apporter + de réponses sur 2 hyp, **Karnath vs Pérennou = lésion d’un système nourri par input ppc  qui sous-tend ER ? ou bien tentative de compensation d’un mismatch des verticales ppc/vb-visuel ?**

**Méthode =**

-   Pop = 18 patients en tout RH = 3 NSU + PS / 10 NSU + noPS / 5 noNSU + no PS / 10 sains (**NB = pas de groupe PS + noNSU ici).**
-   Diag NSU = cloche/LB/copie scène.
-   Diag PS = SCP
-   NB = 3 patients avec ps ont lésions surtout th-striatum. 
-   Patient assis durant SA, **jambes en équerre (pas pendante ici !).** Mentonnière.
-   Tâche visuo-haptique = **orienter un baton sur la SSA** = Baton onset était déplacé en rotation et translation, enregistrement en degré de ces déplacements.
-   VD = rotation et translation séparées.
-   VI = 4 groupes.

**Résultat =**

-   Translation = **NSU + noPS biais ipsi / NSU + PS = biais contra** } comparé aux 2 autres groupes proche de la vraie SSA.
-   Rotation = ns * group.
![](https://i.imgur.com/hBdJzh5.png)


**Discussion =** Résultat original = on a biais contra chez NSU pusher dans SSA, ce qui va dans le même sens que Chokron qui retrouvait cas de double dissociation. Possible donc que absence ou présence de trouble posturaux viennent moduler la SSA chez NSU ce qui expliquerait résultats contradictoires. **On a évidence ici que ER est prédite par le sens du pushing !! A noter que ER ici est shiftée contra tout comme les mesures de PV retrouvée chez Karnath, Lafosse, Pérennou chez NSU pushers ou NSU+++ ou pushers +++.

Hyp ici = **PS résulte d’un déficit moteur** = association entre pushing et déficit moteur contra, problème muscles erecteurs. Donc perception des axes H et V, lésés dans NSU, seraient modulés par biais posturaux. Critique = pushing = CAUSE OU CONSEQUENCE ? proposition perso = peut être que mauvaise intégration d’info ppc/tronc, à cause d’un déficit hemicorps shift ER à gauche OU selon Saj on a un problème efférent/moteur = problème de commandes motrices liées à la station debout. mélange de facteurs SM et spatiaux ?   
--> A noter que ce rés fit bien avec dissociation pushing/nsu. cependant on a seulement 3 patients. Ce rés devra être répliquer avant d'en tirer des conclusions.

**Limite =** Ici SSA représente partie haute du corps. Jambes pas impliquées. Réf à lire ici pour mieux comprendre la complexité de la SSA = Saj et al. 2006, Barra et al., 2007.

-   autre point pb = on a shift de la PV et du COG vers côté contra chez NSU +++ vs NSU ++ ou +, alors qu’il n’y avait pas du tout de pushing. Ce résultat est peut être une coincidence. mais ca montre peut etre aussi que un shift de la rég égocentrée, via PV, vers côté contra, pas forcément lié à PS. néanmoins ici le parallèle n’est pas clairement faisable.


### Vaes et al. (2015)


**Contexte théorique =** PS = on connaît certains corrélats neuro-anatomiques, avec hypothèse sur shift de l’ER. Inversion de l’ER retrouvée dans tâche de droit devant, dans ce cas on s’attend à une négligence « contraversive »/inversée dans des tâches visuo-spatiales telles que la bissection de ligne et la tâche maze.

**Méthode =**

-   Pop1 = NSU PS + NSU no PS (RBD). Matching incomplet = pas de contrôle équivalence taille des lésions, important ? MCA/PCA/TH.
-   Pop2 = 1 patient avec backward pushing/posterior pushing. Etude de cas ici.
-   J+ 32/25 = subacute ?
-   Diagnostique NSU = star/LB/bells
-   SCP avec cut-off > 0 + diagnostique confirmé par physiothérapeute.
-   Tâche = LB + maze (find the shortest way between two points). LB with 6 different sizes.
-   VD = LB = cross-over number ; maze = navigational terminus + center of navigation (mean lateral deviation according to the middle of the maze).
-   VI = group + LB locus (intra)
-   Maze = Stat non-par =U witney. (t-test)
-   LB = logit regression = Wald Chi Square test.

**Résultats =**

-   Les 2 groupes ont performances comparables dans les tâches visuo-spatiales qui diagnostiquent la NSU. Contrôle.
-   Maze = On retrouve déviation vers la droite dans les 2 groupes, mais le groupe avec PS a commencé son exploration davantage à gauche dans l’hémi-espace droit. Chez patient case study , tendance à aller en arrière ! résultat intriguant.
-   LB cross over = quelque soit position LB, NSU PS > no PS. Aucun cross-over pour ligne à gauche ; différance la + *** pour LB à droite.

**Limite =** pas de groupe RBD contrôle. Pas d’équivalence pour taille lésion. Pas de description stat sérieuse de la variable « length » dans l’analyse statistique pour la LB !

**Discussion =** On a bien NSU inversée dans tâche de perception de l’espace péri-personnelle quand présence PS. Ces tâches là font recours à processus égocentrés, possiblement dépendant de l’ER, shiftée différemment chez NSU PS.



### Spatial neglect : Models and Evidences


### USN : A Multi-Component Disorder
### Review
### Jacobs et al. (2012)
**Revue =** Neglect = a multisensory deficit ? => déficit representation supramodale de l'espace 

**Résumé =**  
1) Introduction :   
- nsu est un lack of awareness de l'hémiespace gauche. beaucoup de théorie. ici on ne cherche pas à créer un nouveau modèle théorique, mais à souligner la nature multisensorielle du déficit. Mais de quel déficit parle t-on ? Les auteurs pensent à un déficit conjoint de la représentation multisensorielle de l'espace et de l'attention spatiale. Pourquoi l'attention spatiale ? Car on aurait besoin de construire une carte multisensorielle de l'espace pour que celle ci puisse être capturée par le système attentionnel. Du coup, on a primauté du déficit représentationnel sur le déficit attentionnel ici.    
--> critique ici : les termes "représentation" et "perception" sont très souvent utilisé de façon interchangeable... en effet, les évidences présentées concernent la perception spatiale, et ils s'en servent comme argument pour plaider un déficit représentationnel. Ca n'est pas incompatible, car la perception sous-tend la représentation, mais ça induit un manque de clarté, car on peut avoir pb de representation et pcp ok !   

2) manifestation de la négligence dans plusieurs modalités  
- hemi-anopia/anesthesia/plegia souvent confondue avec nsu. dur à différencier, d'autant que le déficit sensoriel primaire peut aggraver le déficit perceptif !
- travail original de De renzi 1970 = nsu dans plusieurs modalités donc R supramodale de l'espace. 
- dans ce sens, des travaux montrent que c'est le référentiel égocentré qui marque la nsu, car quand bras gauche placé à droite, pas de nsu tactile ! Voir Valenza et Bartolomeo là dessus. Ca montre que nsu n'est pas ancrée dans référentiel bras centré. 
- ces travaux plaident pour un déficit multisensoriel selon les auteurs.

3) manifestation de la nsu dans de **multiples** modalités
- **R de l'espace est achevée en combinant des infos perceptives des différents sens**. 
- si on lèse R de haut niveau, alors on devrait obsever nsu dans plusieurs domaines sensoriels à la fois. 
- schindler 2006 va dans ce sens, avec corrélation entre biais haptique et visuel dans LB. 
- travaux sur la verticalité supporte aussi ce point de vue : **si on a R supramodale de la verticalité lésée, alors on devrait observer le déficit dans toutes les modalités!**. 
- travaux sur les stimulations sensorielles supportent aussi cette prédiction, car S dans une modalité améliore autre modalités, eg S galvanique. 

4) nsu et interactions multisensorielles  
- travaux sur les extinction crossmodales ici. certaines modalités sont encodées dans espace proche ou loin. quand extinction DSS tactilo-auditive, on a diminution ext si S auditif s'éloigne. idem avec vision. 
- confirme que R de l'espace n'est pas unitaire. 
- extinction mss compatible avec théorie de compétition inter-H. ici ce serait R de l'espace droit plus faible que R de l'espace gauche (Mésulam?). Les auteurs prennent un peu de liberté ici, il faut vérifier point de vue de Kinsbourne ! je ne suis pas sûr qu'il parlait de R spatiale, à voir. 
- csq d'un biais dans R spatiale : certains inputs sensoriels deviennent moins fiables, notamment inputs gravitaires. 

5) multisensory role of areas involved in neglect  
- aires mss très lésées dans nsu (insula, tpj, stg, ipl...) + WM + diaschisis de l'IPS et PF qui sont aussi mss. 
- neurones ipl et ips très étudiés (andersen 1997 ar ex). neurones avec RF tactile accorché au bras, avec réponse visuelle ou tactile ou le deux en +. 
- eg = nn VIP code espace tactile proche près du visage/bouche. 
- act qui peut être sub ou super-additive. eg = super-additivité pour smg quand S tactilo-auditive espace proche. 
- voir revue de sérino 2019 pour + de détail ici. 
- il en résulte que nsu après lésion ipl semble être plutôt reliée à altération du secteur spatial péri-personnel. Sérino et d'autres (Blanke) vont défendre idée que espace personnel construit en commun avec espace PPS. 
- enfin, ici on considère que construction R spatiale est utilisée par le système attentionnel. donc théorie compatiable avec Corbetta et al, mais avec des prédictions en +. 

6) remarque pour finir
- quid des dissociation ? plusieurs explications :
- difficulté et sensibilité des tests diffèrent. 
- dissoc peu fréquente, et non compatible avec le modèle (déficit modal). 
- effet des déficits sensoriels primaires. 

### Molenberghs, Sale, & Mattingley (2012)

 

**Contexte théorique =** Beaucoup d’aires différentes identifiées en rapport avec la NSU. Ici on fait une méta-analyse avec une méthode basée sur le likelihood pour examiner les corrélats anat en lien avec des sous-types de négligence, car NSU vue comme atteinte hétérogène par nature.

**Méthode =**

-   activation ll method ALE metanalysis qui quantifie relation entres aires lésées et sous types de négligence.
-   recherche d’articles = lesion localization, lesion overlap sur 20 études.
-   analyses de 69 “pics de coordonnées”.
-   subdivision = 6 ctg = Lb + CT + combi de tasks différntes + allo + perso + extinction

**Résultat =**

-   LB vs CT = gradient anté-posté grosso modo, peut être car LB a une composante + perceptive. NB = selon auteur, CT task impropre à bien classifier mécanismes derrière NSU, car on peut avoir déficit visuo-moteur ou visuo-spatial qui donne même résultat ...
-   allo vs ego = ventral vs dorsal
-   perso vs extraperso = dorsale-post vs ventrale-a
-   nsu vs extinction = extinction associée à lésion postérieure peut être dans aires avec compétition multisensorielle.
-   Toute catégorie confondue, on a SLF + MTGp + IPL + caudate + IPSah + precuneus + STG/STS + Ip + MOG (peut être avec inclusion PCA)

**Discussion = limite générale pour les études en neuroimagerie, c’est que si stroke MCA, on a forcément plus d eproba d’avoir telle aires lésées > à une autre. Autre limite générale, les tâches used pour diag USN sont toutes différentes ! de + les cut-offs sont aussi différents ! On encourage donc à utiliser échelle continue. De + , time from stroke assez variable aussi ...** **De +, technique aMRI , template used ... vlsm, variabilité en + ...  Mot de la fin assez intéressant = construire des tests plsu fins, plus pur, pour mieux déconstruire méc patho derrière NSU.**


### Rode (2017) +++

**Complexity vs. Unity in USN**

**Résumé =**  

Cet article passe en revue les différentes formes de NSU. De façon générale la NSU est un déficit de cognition spatiale, qui peut être divisé en deux composantes.   
La 1^ere^ est un biais ipsilésionnel pathologique associé à une perte de conscience des évènements sensoriels contralésionnels = nsu perceptive. Ici les théories attentionnelles sont privilégiées, elles expliquent biais d'orientation vers la droite.
  
La 2^ème^ est l'incapacité à créer et explorer des représentations spatiales/topographiques de l'espace (ipsi et contralésionnel). Dans ce sens, on retrouve des dissociations avec nsu pcp, et aussi des phénomènes de **symptômes productifs** (allochiria par ex.), ou de distorsion de la représentation de l'espace imaginaire (voir Fourtassi, 16).

==> Fourtassi 2016 => interprétation d'une expé où les déficits attentionnels et représentationnels se chevauchent. ils démontrent que phénomène d'orientation attt vers la droite dans espace imaginaire, mais que qd exploration gaze corrèle avce exploration imagianire à gauche, on a déficit de rappel ! ce qui va dans le sens d'une compression/distorsion de l'espace gauche ! 
==> NSU = mélange de déficit attt + représentationnel. avec des patho "bonus" = spatial remapping, mémoire de travail visuo-spatiale, revisite, nsu motrice (intentionnelle), et (l'auteur n'en parle pas) déficit pcp verticalité. Du coup, je pense que pb verticalité s'insère bien avec déficit représentationnel ! cad que la représentation de l'espace incorpore non seulement les formes géométriques des objets, positions, etc, mais aussi la direction des axes spatiaux (dimensions). Si c'est le cas, un déficit de verticalité reflète bien la nature mss de l'espace représenté.  

**La NSU affecte aussi la représentation** mentale de l'espace. Quand le patient ferme les yeux il n'est toujours pas capable de rappeler des caractéristiques d'un stimulus présentes à gauche (eg, villes de France) malgré le fait que son attention ne soit pas capturée à droite (par le biais ispilésionnel).

**Le traitement de l'espace ipsilésionnel est aussi touché**, dans plusieurs domaines = spatial remapping, comportement de revisite dans la production de dessin ou d'exploration visuelle, mémoire de travail visuo-spatiale, taille du focus attentionnel, représentation du temps … On a aussi un déficit d'attention exogène, c’est-à-dire un déficit de vigilance (arousal), de détection, et de réorientation. L'attention endogène est relativement préservée. Ces déficits peuvent aggraver les **core deficits** (attention + representation). 


Aujourd'hui les méthodes de réhabilitation bottom-up s'avèrent les plus efficaces, notamment la PA, qui démontre un effet positif assez large (pas limité à la NSU) sur la cognition spatiale (amélioration dans l'apraxie visuo-constructive ou dans le CRPS complex regional pain syndrome). Arriver à expliquer effet PA sur ces syndromes no spécifiques à la nsu demeure un challenge.

NB = pseuo hemianesthesia != de body neglect ! nsu perso fait référence à un déficit d'exploration et de conscience du corps (d'un point de vue théorique, déficit représentatio corps). Alors que pseudo hemianesthesie fait référence à déficit attentionnel dans la modalité tactile, qui n'implique pas forcément anosognosie de l'hémicorps gauche. néanmoins, même dans ce 2ème cas une explication en terme de représentation spatiale a été avancé (vallar). donc à creuser...

### Body Neglect 
### Committeri, Piervincenzi, & Pizzamiglio (2018)

 

**Revue de littérature sur la négligence personnelle**

**Résumé =**

-   sur le plan historique, NSU vue comme problème soit attt, soit représentationnel, soit égocentré. Ajd, on a de + en + de travaux qui plaident pour un trouble de la représentation du schéma corporel dans la BN. Problème : le schéma corporel n’est pas une représentation unitaire. On a différents modèles ici. **Polémique = BN=> problème du schéma corporel entier ou latéralisé ?**
-   Problème dans la litté = la façon de classifier la BN ! Pourquoi ? car la **représentation du corps n’est pas codée de façon unitaire** !! certains neurones spécialisés dans le codage de la tête, et d’autres du tronc. Or, on a des tests qui codent uniquement la tête, comme le fait de test du Comb et Razor ... alors que le fluff ou le vest test consistent à explorer l’hémicorps (tronc) contralésionnel. Donc l’idéal serait d’avoir des tests complémentaires ... à noter que les tests fluff et comb ne corrèlent pas bien ...
-   **NB = le test de la SSA comme diagnostique de la BN est impropre, il teste par déf la référence égocentrée, et non pas un manque d’exploration ou de conscience de l’hémicorps gauche ! donc attt à ne pas tirer des conclusions à partir de ces papiers ...**
-   NB2 = la BN peut être dissociée sur le plan clinique de la somatoparaphrénie, de l’hémiesthésie, et de l’apraxie.
-   L’auteur propose une piste de recherche sur la BN = regarder illusion de Weber ! permet d’estimer taille de différentes parties du corps à partir d’une représentation implicite du corps.
-   **Sur le plan neuro-anat, on a a peine 3 études vlsm ! dont celle de Rousseaux qui a cela dit utilisé la SSA comme test pour classifier BN ! On retrouve dans les 2 cas (baas + comm) problème matière blanche vers région par, notamment SMG, et gyrus PoC. Baas = TPJ, Comm = SMG + SI.**
-   **déconnexion pariétale selon Committeri, avec miscommunication entre info somesthésique et prioprioceptive ET SMG qui traite R abstraite du schéma corporel. De +, SMG activé dans tâche égocentrée, donc impliquée dans SBA aussi.**
-   **A noter que Comb et Razor aurait pu sélectionner type de BN centrée sur négligence de la tête.**
-   **Hyp de Committeri : On a overlap lésionnel au niveau SMG, impliqué dans à la fois schéma corporel et attt corporelle. On aurait lésion du VAN segment dédié au corps.**
-   **Autre proposition par rapport à la nature globale ou latéralisée du trouble du schéma corporel = diaschisis gauche provoqoquée par lésion droite qui affecte région traitant hémicorps droit.**

### Caggiano & Jehkonen (2018)

 

**Revue de littérature systématique** = the neglected personal neglect

Contexte théorique = Très peu d’étude qui évalue la negligence personnelle compare à l’extra-personnelle. Etude sur la nsu mortice et prémotrice. Mécanismes qui sous-tendent la bn ? On étudie ici 3 aspects des études sur la bn = les évaluations, les corrélats neuro-anatomiques de la bn, les différents mécanismes en jeu entre la bn et la nbn.

Méthode = Recherche biblio avec mots clefs.

Résultat = 81 papiers = 51 études de groupe, 30 études de cas. 

2.  evaluation = De façon générale, tests psychométriques peu utilisés, généralement seulement 1 ou 2. Quelques tests célèbres =

-   One item test = toucher sa main contra avec main ipsi.
-   Comb and Razor
-   Fluff = post-it à enlever de l’hémicorps contra.
-   dessin de corps.
-   CBS = 10 questions écolo sur nsu.
-   Tests ont tous des +/-. Il faut combiner des tests comme comb/razor + CBS + Fluff + drawing/body identification test + SA pour avoir composante perceptive (schéma corporel) et motrice. Prévalence NB peut varier avec la version que l’on utilise.

1.  Corrélats anatomiques de la BN.

-   72% prévalence BN après RBD vs 20% LDB. Cependant on a biais de sélection = sous estimation des NSU gauches.
-   Commietteri = gradient dorso-ventral + rostro caudal. PoC + SMG + WM
-   SMG impliqué dans R du corps, connexion avec PoC, hypothèse intégration mss SMG des info ppc du corps.
-   Lire Baas et Rousseaux

 

1.  NB vs BN

-   souvent associées
-   On a technique de rehab qui marche sur symptômes extra mais pas perso et vice-versa = suggère des mécanismes différents.
-   **Hypothèse que NSU perso pourrait être pas seulement due à déficit attt mais aussi problème schéma corporel.**

 





### Di Vita, Palermo, Boccia, & Guariglia (2019)

 

**Contexte théorique =** Modèle de Coslett (1998), ou de Longo (2016), avec représentation du corps = représentation pas unitaire. Ici  on s’intéresse spécifiquement à la représentation topographique du corps (parfois appelée “visuo-spatiale”). = représentation des segments du corps les uns par rapports aux autres, que l’on étudie chez BN.

**Méthode =**

-   pop = 7 LBD + 16 RBD / 23 dont 7 / 16 BN + 9/ 16 noBN + 9C.
-   subacute surtout.
-   diag clinique = batterie neuropsycho pour s’assurer de bonne cph de la tâche + MMSE.
-   NSU = letter + line + wundt + reading + comb
-   Task = “body representation test” = positionner ET nommer body part (eg = main) par rapport à la tête sur une grille où le corps est invisible. **limite = pas de tâche contrôle ! eg = avec voiture à la place du corps**.
-   VD = réponse correcte.
-   Stat = variables group + side, ainsi perf sur side devrait nous renseigné sur lien avec NB ou pas. Si segment droit autant patho que gauche, c’est que trouble schéma corporel global, et pas juste csq sde la NB (5/7).
-   Neuroimagerie = vlsm GM + analyse WM.
-   Stat = corrélation entre déconnexion d’un tract et perf cptale dans BRT.

** **

**Résultats =**

*cptal =*

-   NSU > pas NSU.
-   pas d’interaction de l’effet de groupe avec côté de l’erreur. **On montre donc que déficit du schéma corporel est GLOBAL, même si on a NSU NB, c’est assez habile en fait.**

*neuro-anat =*

-   GM = lien entre mauvaise perf dans tâche BRT et lésion RH = putamen, aI, MTG, STG, TPJ, **PoC**, AG, **SMG**, PrC.
-   WM = **fronto-marginal tract dans RH**.
-   limites = pas de contrôle de la NSU extrapersonnelle dans ce type d’analyse. On ne peut pas démêler clairement lésion due à NB et BN en lien avec problème tâche BRT. Si NSU extra-perso affecte le score de BRT, notamment des items GAUCHES, on a pas un contrôle très fin entre profil comportemental et neuro-anat’ !

**Discussion =** Ces rés supportent hypothèse de Committeri (2007). A noter implication de l’aInsula, qui traite infos interoceptives. TPJ = mss. A noter que l fronto-marginal tract relie SMG à cortex orbito-frontal. COF important pour mss sur le self, comme interoception.

-   Tout ça est cohérent avec idée que R du corps est nourrie de messages intéro- et extéro-ceptifs.

 

### Di Vita et al. (2016)

 

**Contexte théorique =** On regarde chez LBD et RBD si trouble du schéma corporel plutôt représentation visuo-spatiale topos. A gauche = autopagnosie. Même tâche que en 2019, avec en + la version voiture.

**Méthode=**

-   pop = 9C + 5/23 LBD + 18/23 + RBD
-   subacute (ici défini comme allant de 10 à 90 jours).
-   diag clinique = exclusion pb cph consignes.
-   NSU = line + letter+ reading + wundt. Si aphasie, on remplace letter + reading par star. cut off =2/4
-   BN = comb
-   apraxie de construction.
-   Task = BRT + car test
-   Stat1 = VD ~ group (C, RBD, LBD) avec NB + apraxie as covariate
-   Stat2 = VD ~ group (C vs BN+ vs NB-) avec NB + apraxie as covariate

**Résultat =**

-   Stat1 = VD ~ group (C, RBD, LBD) avec NB + apraxie as covariate = group ns
-   Stat2 = VD ~ group (C vs BN+ vs NB-) avec NB + apraxie as covariate = **task * group = BN > C + NB in BRT mais != in car**
-   Bonus = la seule patiente avec BN + LBD avait déficit + fort > LBD + NB dans tâche BRT.

**Discussion =** difficile de conclure à body R assurée que par RH. car on manque de patient ici avec BN + LBD. Ce résultat va dans le sens d’une attiente générale du corps. Même si on ne contrôlait pas ici le côté de l’erreur cptale, on avait NB en covarié. Ces rés complètent ceux de Baas, cad qu’on a atteinte rotation mentale de l’image du corps + atteinte visuo-spatiale du corps.

### Baas et al. (2011)

 

**Contexte théorique =** Derrière NB et BN on a peut être des mécanismes différents. Jusuqu’ici, théories attt, R, et ER. Ici on explore théorie R appliquée au schéma corporel dans le cadre de la BN. On utilise tâche d’imagerie mentale du schéma corporel.

**Méthode =**

-   pop = 22 RBD = 7 BN + 15 no BN, + 13C
-   subacute = J+37
-   diag BN = Fluff modifié pour cutoff, cutoff + 3 oublis (oublis max chez contrôles).
-   covariants = NB (bell + LB) + somato loss + weakness
-   scan = DW + FLAIR = technique soustractive + overlap
-   Tâche = dire oralement si main présentée centralement, ou rétroviseur, est droit ou gauche. Présentation prototypique ou non. De +, stimuli type rétroviseur permet de séparer trouble R de l’espace perso et extraperso. 15*4cond essais random.
-   VD = RT + %error
-   VI = group = BN + noBN + C / côté / View (proto+no) / ref

**Résultat =**

-   Error =
-   side x group * = BN font plus d’erreur quand stimuli gauche que droit. donc pas de diff stimuli main ou voiture. Quand on contrôle pour NSU NB, même résultat.
-   RT = ns
-   prediction of BN = quand fluff perf en VD, on a VI liée à perf dans tâche de jugement de latéralité de la main qui est * comparé aux autres prédicteurs (eg , NB, weakness).
-   lesion scan = quand groupe BN vs noBN on a lésion TPJ + WM. Ce locus était lésé avec une fréquence au moins 50% fois plus importante que chez noBN (seuil + conservateur).

Dicussion = Ici on a patient avec NSU personnelle qui ont mauvaise représentation de l’espace personnel et extrapersonnel. Le résultat fort concerne le fait que seul les performances dans la tâche de jugement de latéralité de la main, qui implique la représentation du corps, est significative. Ce résultat est conforté par la lésion TPJ, une zone impliquée dans NSU BN et NB comme retrouvé par Committeri. En conclusion, on pourrait avoir overlap lésionnel entre BN et NB qyui explique le double déficit, car ici on avait patient avec les 2 types de NSU.

**Limite =** quand analyse soustractive, pas de technique VLSM, pas de prise en compte de la NSU NB en covarié, donc résultat nonb pertinent.

 

### Rousseaux, Allart, Bernati, & Saj (2015)
 

**Contexte théorique =** La façon dont teste la négligence impacte les aires lésées associées à la dite méthode diagnostiques. Ici on recherche les corrélats anatatomiques associés aux scores d’ADLs = activity of daily living, 10 questions de la CBS (Azouvi, 2003). On cherche plus précisément si association entre sévérité CBS et NSU personnelle, péri-personnelle, et anosognosie.

**Méthode =**

-   Pop = 45 RBD droitiers **without PS.**
-   J+60 = chronique ?
-   lésion MCA/PCA/ACA
-   ADLS => 10 questions, dont seulement 3/10 centrées sur nsu du corps ! (habit, laver, manger)
-   NB = LB + bells
-   BN = SSA frontal plane (**contestable, est-ce qu'on teste nsu du corps ici ? on est su run pré-supposé théorique très contestable, car ataxie optique peuvent aussi avoir nsu du corps dans ce cas là !!**) + bisiach test (:-1:). 
-   anosognosie = différence entre questionnaire hom et hétéro.
-   Contrôle aussi déviation oculaire + hémianopie + motor deficit + somato deficit
-   **Score de négligence = Z score puis moyenne. Cut-off pour NB et BN = ½ X.**

** **

-   IRMa = VLSM + **non parametric permutation test pour correction. Covarié = lésion size, mais pas la NSU opposée quand patient avait les 2 !!!**
-   28 NB + 29 BN + 21 BN + NB !

**Résultat =**

-   ADL associé à lésion STG + TOJ + LOC + TPJ + SLF
-   NB = **STG** + IPG + MTG + ITG + SPG + aPoC + PrC + SLF + FOF
-   BN = pITG + TOJ + **pSTG** + TPJ + IPG + **IPS + PoC**
-   Les auteurs n’ont pas mis la NB/BN en covarié, comme Committeri ! Il sont covar
-   ADL davantage associé à NSU NB = logique car dans la vie de tous les jours les problèmes liés à la négligence par la vision. + Rares sont les cas où on n’a pas recours à la vision dans une situation donnée. A noter aussi que ADL corrélait ecore davantage avec déficit somato et moteur.

![](https://i.imgur.com/7QN879r.png)


Limites = scan effectué à J + 60 = plasticité cérebrale intervient ici. **Pas de covarié lié à NSU opposée** = grosse limite, car voxel pouvait être * pour NSU BN ou NB ! explique le grand nombre de locus lésionnel trouvé ... de + éval de la nsu perso très contestable.

### Committeri et al. (2007)

**Contexte théorique =** Espace codé en aires distinctes, dissociation entre espace perso et extra perso. Difficile de faire lien entre NSU forme extraperso et région distincte. Limite expé = classification cptale variable, sous-estimation NSU perso. Pourtant, asso + fréquente que double diss !

Note méthodo ici = limite liée à IRMa. Méthode soustractive victime de l’hétérogénéité des patients, avec zones lésées pas forcément pertinente. Solution = avoir patients avec lésion homogène, en ciblant eg territoire MCA.

**Méthode =**

-   Pop = 52 stroke HD
-   IRMa avec VLSM : carte voxel, concentration de proba H0 avec aucun a priori. On examine GM et WM.
-   Test neuropsy :
-   extra = 4 tests = barrage ligne/lettre/lecture/illusion de jastrow(bord perçu + long).
-   Perso = 3 objets sur le corps = peigne, rasoir, lunette.
-   Eval aussi def somato/moteur de l’hémicorps gauche.

Sur le plan IRMa = flair + t1 + t2 + proton density. Avec lesion density plot à voxel by voxel comparison. Le point ici est de comparer valeur de voxel (à un point anat standardisé), une fois le **tissu classifié** (GM,WM) entre les pp (**spatial normalization).** On aplique filtre gaussien afin d’avoir distribution des résidus paramétrique et faire des stats (**spatial smoothing**).

**Avec cette technique, les régions lésées chez tous les sujets ne ressortiront pas dans l’analyse, car pas de diff stat !**

-   VBM = VD = score t de chaque voxel (each group comparé avant à groupe HD non sélectionné) ~ NSUperso (oui/non) + NSU extraperso (oui/non) + ancova(size, J).

**Résultat =**

-   42% both
-   12% extra
-   15% perso
-   31% no NSU
-   Méthode soustractive =
-   Contrast extra NSU = **perisylvian** + **precentral_inf** + **INS_p** + **STG** + **PAR_operc** + **WM_supralenticulaire_sublenticular**
-   Contrast perso NSU = **perisylvian** + **LPI + SMG + PAR_WM**
-   Region-based analysis =
-   **analyse volume lesion** = Effet principal des deux NSU, mais pas interactions = effets additif des deficits. Patients both ont volume 166cm² a peu près egal à somme des deux sous NSU ! Patients sans NSU ont petit lésion 41cm².
-   **Analyse pourcentage lésion** = même effet afdditif.
-   Contrast extra = **PreC_inf + MFG + STG_middle + WM_sublenticular (lobe T)**
-   Contrast perso = **T et PAR post : STG post + AG + PostC + WM_supralenticulaire**
-   VB lesion-symptom mapping = ici variable continue, pas classification. Standardized t-score => severity de l’atteinte. Lien entre cette var continue et score cptal ! VI = score continu score extra ou perso.
-   Contrast extra = FR : PreC_inf + MFG ; T = STGm + MTGm + STGa.
-   Contrast perso = WM_PAR + PostC + SMG + MFGd + p (dans extra, c’est – le cas).
-   Quand extra-perso (**perso en covarié**) = on a tjrs *** MFG + mSTG + PreCi
-   Quand perso-extra (**extra en covarié**) = on a tjrs *** WM_central_sulcus_PAR + PostC + SMG (mais moins), MFG ns.
-   Résultat ici independant du temps J+. mais en revanche aucune *** quand lesion size pris en compte. Limite du VBSLM = voxel pas independant, car cerveau vascularisé !
-   NB **Parmi les régions qui n’ont pas survécu correction de Bonferoni, on retrouve WM CS + IFG + INS/OPERC regions du complexe T-peri-Sylvian, proche du PIVC . Ces régions sont celles qui sont hautement corrélées entre les 2 cartes stat de la négligence NB et BN !!! Les autres régions sont celles qui expliquent le dissociation. On a overlap de la variance ~70 % !**

**Discussion =** On a globalement une distinction entre NSU perso/extra sur le plan neuroanat avec deux profils = rostral et caudal (haut/bas). On retrouve ce gradient au niveau dorsal, avec distinction IFG/MFG vs PostC/PAR, et au niveau + ventral avec distinction  STG a-m vs STGp, et aussi avec WM avec gradient ventral-dorsal, contraste sublenticulaire et supralenticulaire.  Res additif suggère réseau différents derrière ces deux types de NSU.

**Hypothèse =** NSU perso soustendu par réseau + dorsal qui intègre info somato et ppc (PostC) dans R + abstraite du corps  (SMG), avec WM sous-jacente. A contrario, NSU extra pourrait être dûe à lésion FEF, + frontale , mais aussi à composante STG, aire mss. Peut être ici que NSU extraperso pourrait être liée à rupture réseau ventral dédié à attt exogène.

A noter que ici pas rôle essentiel PAR, mais plutôt T, contrairement à autre étude. Peut être car sousestimation de NSU perso, davantage dorsale par rapport au STG (eg = SMG). Mystère = composante PAR ou non dans NSU extraperso ?!

Enfin, quid de la LB ? pas utilisée ici, mais TDShotten a montré que S du mSTG induisait shift de la LB (2005). Or on ne retrouve pas toujours LB shift droite dans NSU extraperso (à éclaircir).

**Ref = Bottini 2001, Kahane 2003.**

 

 




### Transformational Theories
### Karnath (2015)


Revue = Spatial attention in spatial neglect

Résumé = ici Karnath reprend la théorie de Kinsbourne, revue par C et S (2011) qui émettent hypothèse d’un déficit attentionnel exogène et endogène. Ici on apporte des évidences démontrant simple dissociation entre ces 2 déficits, avec des fois le système attt endogène « intact ». Pas vraiment intact, car la distribution spatiale du focus attt est shiftée vers la droite, mais diminue pour les position à droite très excentriques. Cette observation va à l’encontre du modèle de Kinsbourne car on devrait au contraire observer des comportements d’exploration « cycliques ». Ca ne serait pas dû par problème d’hémiparésie  (argument contestable ... ?). L’auteur argumente pour un shift de la représentation centrale égocentrée. Il découle de ce shift un shift positionnel dû à nouvelle RE. Arg = on a chez NSU lésion qui touche aires intégrant info afférentes et efférentes assurant codage du corps dans l’espace.


### Schindler et al. (2006)
**contexte =** suivant l'expérience de derenzi 70 qui avait montré déficit d'exploration spatiale dans 2 modalités, ici on réplique avec paradigme un peu différent qui permet d'apprécier la distribution de l'exploration dans espace + large.
**méthode=**
- patient nsu / rbd / 10 et 10. nsu = lb/reading/canc/drawing
- min 4weeks post-stroke
- exploration objets avec forme/taille différentes, à nommer. pas de limites de temps, avec exploration complètement libre de la tête et du tronc (point critique ?? :-1:)
- tâche dans modalité tactile et visuelle.
- on regarde effet hemianopie, mais ns (peu de patients cela dit).
**resultats =**
- on a bien shift exploration vers la droite, + large pour vision que tactile
- beaucoup de revisite.
** discussion = ** selon les auteurs, confirme modèle égocentré, cad shift d'un référentiel égocentré supramodal et donc on observe exploration shifté dans référentiel body-part centré et rétino-centré. alternativement, lésion de plusieurs référentiels en même temps car grosse lésion. pas de gradient exponentiel donc critique l'hyp attentionnelle (argument contestable). comportement de revisite, pb de mémoire  de travail ? 

### Karnath (1997)

***Spatial orientation and the representation of space with parietal lobe lesions***
 
a) Introduction
- lesion IPL = nsu / lésion SPL = ataxie optique. *Cette dichotomie est intéressante compte tenu des analyses vlsm récentes ! en effet Rousseaux 13 retrouve chez nsu rbd lésion s1, spg... associée à pb posture et shift ssa yaw. Or, lésion spg ne sont pas clef dans la nsu, mais plutôt associé à ataxie optique. cela renforce le côté non spéc à la nsu du shift de la ssa...*
- **pérénin 1997** = spg utile de vision pour action, avec codage spatial des effecteurs, alors que **ipl plus impliqué dans R conscientes des infos spatiales** (point de vue défendu par rizzolatti aussi).
- selon goodale et milner, ipl recevrait inputs du ventral stream, ce qui permettrait d'accéder aux caractéristiques immanentes/persistantes des objets et leurs importance/signifiance, de façon cs.

b) deviation of egocentric space or lateral gradient of attention ?
- proposition de karnath (reprise à sa "sauce" de l'hyp de ventre 1984) = nsu causée par **R altérée de l'espace centré sur le corps**. hyp qui repose sur travaux sur mss, argued for a bodycentred or world centred space coding, and not retinocentred coding.
- cela permet d'obtenir une **R unitaire de l'espace égocentré**, utile pour exploration spatiale, orientation, codage de la position du corps.
- dans nsu, coordinate transformation déplacée côté ipsi.
- donc ça n'est pas la posiion du corps qui est déplacée, c'est la R interne du corps. 
- Théorie qui postule un gradient = kinsbourne, rizzolatti... karnath pense plutot à une déviation.
- critique des paradigmes attt = capture de l'attt vers côté ipsi quand présentation objets. il y a attt bottom up qui biaise les rés.
- paradigme d'attt dans le noir permet d'évaluer attt top down. avec ce type de paradigme, on a exploration saccadique bell shaped. 
- *critique selon moi = paradigme peut être pas très écologique ? attt spatiale dans vie de tous les jours se fait dans ev riche... quid d'essayer de trouver une cible qui n'existe pas ?*

c) translation or rotation ?
- vallar = deviation. karnath = rotation. *ajd, on penche pour translation. mais du coup cela signifie t-il que le postulat numéro 3 de ventre et al. 84 (slow VOR compensatoire pour rotation de la SBA) ne fonctionne plus ? pas sûr, car ventre mentionne une "tentative de compensation", ce qui est assez général.*

d) goal directed arm movements
- karnath argumente pour absence d'ataxie optique dans nsu.

e) conclusion
- une déviation ipsi de la ref égoc semble signifier que la perception des objets dans l'espace, par rapport au corps, est altérée à cause d'un déplacement du référentiel. Comme le référentiel est déplacé, un objet situé un peu à gauche du droit-devant objectif sera codé comme étant positionné dans la "périphérie" par rapport aux nouvelles coord du corps.

### Vallar (1997)

***Spatial frames of reference and somatosensory processing: a neuropsychological perspective***

**Résumé =**

-   hemispheric difference = Sterzi (1993) après **lésion RH** déficit soma, moteur, visuel (**hemianesthésie, hémiplégie, hemianopie**) + fréquent. Ici on a point commun avec **NSU,** qui **imite** cette triade, mais cette fois ci due à un **déficit spatial de haut niveau** !
-   en effet, tt sensoriel sont préservés chez NSU. eg = sur le plan tactile, on a réponse dermale, et réponse EEG somato quand stimulation tactile, mais pas de conscience du stimuli. On a donc traitement sans awareness. sur plan ant, aire primaire généralement épargnée, même si récente évidence ont montré lésion S1/PoC, surtout en lien avec déficit BN.
-   **Attt, on peut avoir patient avec lésion S1 qui pourtant montre tt without awareness! eg = chez patient hémianesthésique, Bottini a montré que stimulation vestibulaire diminuait la patho, avec activation d’aires insula + ifg + putamen !** Bottini 1995
-   **Intéressant de voir que des stimulations sensorielles améliorent BN mais aussi somato non awareness ! Cela dit, on a clairement des cas de dissociation entre NSU extrapersonnelle et somato non awareness ! avec patient RBD sensible à S galvanique pour réduire soma no awareness sans avoir de NB. Mais ces 2 patho partagent des mécanismes patho en commun/lésion anat, car les 2 sont amélioréres par des S sensorielles, notamment vestibulaires. Ce mécanisme doit être localisé davantage dans le RH.**
-   Interprétation de Vallar = **translation hyp** = on a déficit d’une représentation spatiale du corps de haut niveau, mais non-somatotopique. Peut être que cette R est liée/est = à la RE, que l’on pense déplacée chez NSU. Dans ce cas, shift de la RE expliquerait BN tactile ! RE vue comme une interface entre un système de coordonnées spatiales, la perception, et l’action. Si c’est le cas, shift de la RE vers 0° devrait aussi améliorer exploration motrice vers côté contralésionnel (Rode, 1992).

### Jeannerod &  Biguier (1989)
Contexte théorique = subjective sagittal axis = SSA R interne d’un plan virtuel superposé au plan sagittal, qui sépare le corps et l’espace en 2 secteurs. On étudie à quel point cette ssa est biaisée dans tâche de droit devant avec/sans indices visuels.

Méthode =

-   pop = 10 droitiers
-   apparatus = pointage avec position départ main sur le côté du corps. tête immobilisée et alignée avec le tronc.
-   Expé 1 =
-   vi = main gauche/main droite * cible visuelle/pas de cible (obscurité)
-   Expé 2 =
-   vi = position cible = 0/5/10/15 degrés vers la gauche ou la droite. Uniquement des pointages main droite.
-   vd = bias

Résultat =

-   **Expé 1** **=**
-   main droite = pseudo NSU
-   main gauche = léger biais à droite, plus de variabilité.
-   cible = biais à droite / pas cible = biais à gauche
-   **Expé 2 =**
-   Plus la cible s’éloigne du plan sagittal objectif, moins on a un biais à gauche.

**Discussion =** Dans expé 1, résultat va dans le sens d’une dominance de l’HD pour la perception de l’espace personnel. **Quand on a un repère extra-personnel, ce biais est minimisé**. Expé 2 = Influence des signaux eye on head sur la direction de la ssa : quand le regard est dévié ++, déviation également de la ssa.

-   Résultat expé va dans le sens d’une ssa bâtie sur la base d’intégration de plusieurs ref égocentrés = eye on head, head on trunk, trunk in space ... problème de mss dû à X PPC chez NSU ?

### Jeannerod & Biguier (1987)

 

**Résumé = “ The directional coding of reaching movements. A visuo-motor conception of spatial neglect.”**

Idée générale = la direction des mouvements moteurs seraient codés par un système de coordonnées égocentré, cad en référence à l’axe du corps. Ce référentiel fourni une carte spatiale utilisée par le système moteur pour diriger son action. Pb = quelle est la nature de cette référence ?

En effet, coord égocentrées oeil/tête/tronc pas toujours alugnées ! Hyp = peut être qu’un problème d’intégration d’une de ces coord entraîne une désorientation du système sur le plan visuo-moteur.

-   *Eye position signal*
-   Travaux chez le singe ont montré évidence pour “**motor inflow” signal, efference copy signal**”, grâce à **DSS** Montre déjà que localiser une cible visuelle on a recours à des signaux rétiniens et extra-rétiniens. EC utile pour différencier mvt du cadre du mvt des yeux.
-   Eye position signal en interaction avec modalité auditive, CR colliculus codant stimulus auditif change en fonction de mismatch avec cible Suggère **transformation des infos auditivo-visuelles** dans un même système de référence eye on trunk. revoir Anderson 1997

 

-   Importance des signaux Bup aussi, signal ppc des muscles de l’oeil.. expé avec chaton kittens avec eye immobilisé.
-   **Quand immobilisation oeil et qu’on demande saccade, sensation de bouger l’oeil ! et quand inversement duction forcée sans saccade commande, pas de sensation de mvt** ! Ici ça n’est donc la ppc qui a jouer un rôle dans sensation position signal de l’eye, mais plutôt EC. On mêmes observations dans tâches de pointage, pointage dévié car sensation d’avoir fait une saccade.
-   De +, quand on demande au sujet de faire saccade vers cible visuelle apprise, une fois dans le noir, grande imprécision. le signal ppc ne suffit pas. Perte de la “direction constancy”, cible n’est plus perçue au même endroit. **Montre que signal ppc doit être intégré avec un signal visuel pour coder cible eye on trunk**.

 

-   *head position signal*
-   On manipule depuis les années 70 les sensations de head on trunk avec vibrations du cou. Quand cou gauche vibre, sensation d’étirement, et donc de mvt de la tête vers la droite. Quand le sujet fixe une cible, il a l’impression que la cible bouge !
-   Head on trunk signal important pour pointing accuracy = quand on empêche mvt de tête vers cible, pointage + imprécis. Cela s’explique par **séquence motrice eye > tête > arm.** On a besoin du signal ppc du cou pour coder la position du bras ! C’est d’autant + important que la cible est excentrée.

 

-   *body axis signal*
-   R interne du corps créee à partir d’afférence somatic/vb/v qui sépare le corps et l’espace en deux secteurs, et de ce fait devient un référentiel égocentré pour orienter nos actions dans l’espace personnel et extrapersonnel.
-   La SBA pourrait être une référence égocentrée utilisée à la fois dans la dimension horizontale et verticale. Jeannerod assimile d’ailleurs l’idée de vecteur idiotropique à celle de SSA. Mittelstaedt est bezaucoup + flou là dessus en parlant seulement de R centrale issus de multiples transformations sensorielles. Mais l’idée semble similaire.
-   SBA évaluée avec tâche de droit-devant. + dépendante de la déviation du tronc que de la tête, voir Karnath, Chokron...
-   SBA en compétition avec eye positio signal dans DD task. On fait donc distinction ici entre **SSA et SBA !**

 

-   ***asymmetrical behaviour following unilateral lesion of the nervous system***
-   **en temps normal, SBA, bien que légèrement asymétrique, oriente correctement nos actions vers la gauche ou la droite.**
-   **Chez invertébré, on observait cpt asym quand lésion unil. Idem chez vertébré, avec sprague effect par ex = regard dévié du côté lésé, même au repos en absence de cible. Rôle du colliculus ici.**
-   **Chez l’homme ref pionnière montre pointage biaisé chez NSU.**
-   **Ici jeannerod amorce deux hypothèses de la NSU = hyp de Kinsbourne, inhibition transhémisphérique, et hyp d’un biais d’orientation dans l’espace dû à perturbation de la mss qui sous tend la SBA.**
-   **Spéculation ici = si X SBA, on devrait observer “réactions compensatoires” de la part de processus de bas niveau. Ici, il fait référence aux symptômes vestibulaires qui arrivent après lésions dans les mêmes zones que celles qui produisent shift SBA ! Ici l’auteur décrit nystagmus spontané avec phase lente vers côté contralatéral + fort quand on induit rotation vers la côté ipsilésionnel ! On a donc biais oculo-moteur dans le sens inverse la lésion !! Peut petre dû à illusion de rotation vers la droite ! Le système vestibulaire organiserait une compensation au shift de la SBA.**

### Ventre, Flandrin, & Jeannerod (1984)
**Contexte =** quand yeux bandés, certains patients ont bras qui pointe vers côté de la lésion ou vers vers l'opposé. quand S calorique froide des 2 côtés, le VOR slow était + fort du côté de la déviation du pointage. interprétation = **si tête initialement shiftée en rotation vers la droite, alors illusion de tourner la tête + forte, et donc slow VOR + large**. ici on défend cette hyp chez le chat, avec lésion pariétale (expé1) et colliculuc supérieur (expé2).

**Méthode =**
- on enregistre VOR chez chat avant et après destruction aires supra-sylvienne postérieure, équivalent aires 5,7,19,21 chez homme = SPG surtout, et des fois aussi MTG, T-O
- 9 chats ou 4 chats (expé 1 ou 2)
- *gain* = rotation yeux/rotation tête (entre 0 et 1)
- *gain symmetry* = gain contra/ipsi = si > 1 = gain contra > ipsi.
- on manipule aussi *fréquence*, cad dire vitesse de rotation de la tête.
- dans expé 2, on fait ablation colliculus supérieur.

**Résultat 1 =** 
- dans le noir, nystagmus slow spontané du côté contralésionnel. comme si sensation head rotation côté ipsi.
- après destruction, gain VOR slow diminué de 50% quand rotation tête contra. au contraire, augmentation du gain quand rotation tête ipsi.
- effet + fort pour faible fréquence.
- lésion SPG donnait effet les + forts pour baisse de VOR slow.

**Résultat 2 =** 
- on observe, comme dans le *sprague effect*, mouvement de *circling* et d'orientation de la tête vers côté ipsi, avec négligence contra, pour sounds et gaze. à peine 5 % d'orientation côté contra quand detection S surprenant (cassette vidéo).
- nystagmus spontané slow idem vers coté contra sain.
- quand présentation S visuels + rotation, on retrouve encore + fort gain quand rotation ipsi.
- + fort pour fréquence faible.

**discussion =** on a effet similaire lésion pariétale supérieure et CS sur asymmétrie du slow VOR compensatoire quand rotation de la tête. important de noter que malgré ça on observait asymmétrie du comportement visuo-moteur (circling, head orientation ipsi) que quand lésion CS, et non PAR. les auteurs disent cependant qu'on aurait pu observer effet patho dans tâche de pointage, comme chez singe après lésion aires PPC.  
on ne peut pas expliquer VOR slow contraspontané et déviation head ipsi avec juste lésion bas niveau, car on aurait congruence. ici **Transformational theory** énoncée pour la 1ère fois avec 4 assumptions :  
- 1) action orientée dans l'espace par rapport à une référence égocentrée qui permet de situer son corps par rapport aux objets dans l'espace. 
- 2) Cette référence est une R interne de l'axe longitudinal du corps, construite à partir de signaux sensoriels extraperso-personnels traités de façon bilatérale et symmétrique. Une lésion unilatérale produit déséquilibre dans traitement de ces inputs et par conséquent déplacement de la SBA.
- 3) Les déviations oculaires spontanées VOR slow du côté contra illustrerait tentative de compensation du système vestibulaire du shift de la référence égocentrée.
- 4) réf égocentrée produite à partir d'un large réseau sous-cortico-cortical, ce qui explique récupération rapide quand lésion isolée.

Cette théorie prédit erreur de pointage directionnelle constante dans ataxie optique, ce qui est effectivement observé. en effet, on **aurait nouvelle coordonnées égocentrée en désaccord avec coordonnées allocentrées rétinotopiques ! On aurait alors mauvais codage des objets visuels / corps, mais bon codage des objets / autre objet !** nb = c'est ce qu'on induit avec les lunettes prismatiques.   
A noter enfin que selon Ventre et al., ataxie optique et nsu sont 2 syndrômes très proches, d'un point de vue anat (lésion par) et cognitif (déficit égocentré).  


### Attentional Theories
### Koch & Tsuchiya (2006)
Opinion paper : ***Attention and consciousness: two distinct brain processes***  
Ici on défend idée que events or objects can be attented to sans être consciemment perçus. et de plus un objet peut être perçu consciemment sans qu'il n'y ait d'attt top-d.

a) Functional roles of attention and consciousness
- attt = filtre
- attt : bottom up saliency => exogenous cues = **image-immanent feature qui attire temporairement l'attention, indépendamment d'une tâche en cours**. Cette bottom up sallaince nécessite une carte spatiale de saillance. (*topographic saliency map*).
- attt : top down selection, cette fois ci l'attention **est focale, task-dependent, basé sur objets ou sur cct**.
- **La conscience est définie comme une fonction permettant de résumer toutes les informations accessibles, en temps réel, sur l'organisme et son environnement. Ces infos permettent ensuite la prise de décision, monitoring, inférence, pensée rationnelle...**
- comme attt et conscience ont fonction différente (filtre vs perception?intégration?), on fait hyp de substrats différents.

b) The four ways of processing visual events and behaviors

![](https://i.imgur.com/jYToKfV.png)

*Attention with consciousness*  
- cas le plus simple : je dirige mon attt sur une scène visuelle, et je perçois la scène, cad j'accède consciemment à ses attributs, manipulables en mdt, etc...

*No attention, no consciousness*  
- cas simple aussi, infos rétinienne n'est pas traitée en profondeur et ne déclenche pas accès au percept.

*Attention without consciousness*  
- quand on dirige notre attt sur un point mais qu'on accède pas à l'infos recherchée (où est charlie?). 
- eg = **visual corwding** = encombrement visuel = difficulté à identifier cible en périphérie quand entourée d'autres formes. L'attt est dirigée sur ce point, mais pourtant pas accès cs aux infos.
- eg2 = **priming** = efficace quand attt dirigée vers la cible invisible (mais pas si pas attt).
- eg3 = **blindsight** = RT detection cible quand attt dirigée vers elle est pourtant bon malgré manque de cs ! 
- eg4 = **visual search** (où est charlie?).

*Consciousness in the near absence of attention*  
- quand concentration sur un point, les infos traitées en périphérie ne sont pas jetées à la poubelle ! elle peuvent donner accès à cs.
- eg = **dual task paradigms** = malgré attt top-down vers centre pour une tâche donnée, on est tjrs capable d'identifier animal ou visage en périph. mais quand tâche disc de lettre en périph (tâche plus *bas niveau*) on a échec ! ça montre que succès dans tâche + complexe pas due à top down vers périph.

c) Processing without top-down attention and consciousness
- *Kirchner & Thorpe 2006* montre que ctg animal possible en 120 ms, sans feedback, que du feedforward !
- idée que tt feedforward qui ne nécessite pas attt top-d peut permettre cs.

d) Attention and consciousness can oppose each other
- attt blink = bizarrement, meilleure pcp des 2 cibles en simultané quand distractor ! 

e) Relationship to other conceptual distinctions
- *Dehaene et al. 2006* = tripartite model = subliminal / preconscious / conscious processing.
- modèle basé sur **Global workspace hypothesis** de *Baars 2005*. 
- ici c'est l'attt top-down qui décide de la force d'act du percept ! ici on défend idée que non, que **attt top down est un moyen parmi d'autres d'arriver à la cs**, mais pas obligatoire.

f) Do these conclusions hold for real life ?
- Au quotidien, action n'est pas tjrs dirigée par *introspection*, de façon *cs*. 
- eg = voie dorsale = vision pour action => action SM très sophistiquée ! et pourtant, incs. *Zombie behaviours*. 
- nb  = athlète + efficace quand pas concentrés sur leur tâche ! 

### Corbetta & Shulman (2011)

**Résumé** = La NSU affecte l'attention visuo-spatiale (spatial remapping, attention endogène) et l'attention exogène (vigilance, réorientation de l'attention, détection des stimuli saillants). L'attention endogène reposerait sur un réseau dorsal qui sous-tendrait la référence égocentrée, déviée vers la droite dans la NSU. Cependant les lésions NSU (matière grise et blanche) sont plutôt ventrales, et affecteraient donc davantage l'attention exogène. Les auteurs proposent qu'un déficit (eg, hypométabolisme) du réseau ventral pourrait à son tour impacter le réseau dorsal (les deux réseaux sont interconnectés et se modulent réciproquement, voir (Corbetta et Shulman 2002). Cette baisse d'activation indirecte du réseau dorsal entraînerait un déséquilibre au niveau de la balance inter-hémisphérique, d'où la déviation de la référence égocentrée vers la droite. Ce mécanisme expliquerait pourquoi les NSU après lésion dans l'hémisphère gauche sont rares puisque l'attention exogène est latéralisée dans l'hémisphère droit.

a) Proposition globale
- core deficit = un biais d'attt spatiale et de saillance des S centré sur coord egocentré. Quand lésion par-fr => perte d'orientation top down covert et overt (saccade visuelle) + perte de saillance des S.
- En revanche le deficit non latéralisé (non-core) origine + ventrale, perte arousal/reorientation/detection

b) core deficit
- **saillance perceptive** = distinctivité sensorielle + importance cptale d'un objet relativement à un autre objet
- saillance mesurée par magnitude de l'activité neurale des cartes topos de l'espace (fr-par-cs)
- saillance trop forte des S ipsi (voir Verdon, SAj...)
- NB = d'autres account existe. ici on se fixe sur account attt-spat. mais on a aussi account spt working memory, qui fait référence à nsu R => manipuler représentation mentale de l'espace en Mdt !
- assoc ++ entre nsu attt et R. surement gros overlap. 
- **Important, dissociation avec *spatial cognition*** = plus impliqué dans LB par ex, qui refléterait *distorsion* de l'axe horizontal (Bisiach et al 2001). critique = plutot pb d'exploration saccadique retrouvé, avec saccade qui s'arrêtent trop tôt vers la gauche. Ishiai et al 2006

c) neuro-anat = dommage structurel n'explique pas biais attt.
- nb = souvent on a lésion tjrs dans zone t-p, alors que composantes cptales très différentes ! 
- les dommages de matière grise n'expliquent pas les deficit d'attt spatiale, qui concernent aspect plus "dorsal"= precuneus, spl, fef, dlpf ...
- ce sont des régions recrutées pendant exploration visuelle et mdt spatiale.
- de +, spl et ips impliquées dans codage égocentré (galati 2010). 
- Hagler & Sereno 2006, Sereno et al 2001, Swisher et al 2007 = dlpf, carte topos (à voir) de l'hemiespace contralesionnel (uniquement, pas ipsi)

d) physiologie 
- 3w post stroke, on a hypoact dorsale righrt > left dans tâche attt spatiale.
- +9m = equilibre inter-h. => **Corbetta et al 2005**
- act spontanée deficitaire aussi.

e) non core attt deficit
- lesion plus ventrale, comme insula/stg/ipl pourrait être plus impliqué dans attt exogène.
- quand lesion ventrale, hypoact dorsale car interaction entre les voies ventrales et dorsales.

f) point faible du modèle
- pourquoi on a pas d'ataxie optique associé à nsu ? ce point est un peu éludé ...
- peut être que carte topos se trouve aussi dans regions ventrales, pq pas hippocampiques (place cells, etc). mais a ce stade de la recherche, pas d'évidence.
- ca amène les auteurs à dire que lésion dorsale seule ne suffit pas pour nsu ! il faut association d'hypoact dorsale ET de lésion ventrale. Ce dernier point est critique. 



**ref ** = Bays et al 2010 / Corbetta 98 / Behrmann & Geng 2002 / Chang & Snyder 2010 / Della Sala et al 2004 / Bartolomeo et al 1994 / Bisiach et al 2001


### Mesulam (1999)

 

Revue : Lien entre attention spatiale et négligence : contribution des lobes frontaux, pariétaux, et cingulaires dans la détection d’évènements saillants dans l’espace extra-personnel

**Résumé =**

NSU = perte de perception consciente et de R mentale de l’hémiespace gauche. Pourquoi consciente ? car éven non perçu = amorçage, donc notion de filtre attentionnel de haut niveau et non problème de sens périphérique.

Intro :

Attention = allocation des ressources de traitements – limitées – vers des events d’intérêts. Aucun nn n’est purement attentionnel, il y a seulement modulation = sélectivité et intensité de la réponse. Modulations sont domain spécifique (aires visuelles, wernick…) et domain-indp via le ARAS = système réticulaire ascendant (B-up) + fronto-limbique (Top-D).

-   **ASPECT COMPORTEMENTAL**

1.  R aspect

Perte de pcp des objets à gauche, perte de pcp de la saillance. Tests de neuropsy : extinction, **LB**, attt covert.

-   Multimodale **extinction** et cross-modale
-   LM = partie gauche perçue plus petite = - saillante. Ici pb post-sensoriel, on voit toute la ligne mais pb de R (aspect pcp). Confirmé par Bisiach = **imagination France, plaza** NB = qd hallucination = HR ! REM = HR.
-   Covert shift : **tâche de Posner**: on a pas le temps de faire saccades, uniquement fixation centrale ! On provoque attt endogène (fovéa = flèche) et exogène (indiçage). RT ++ quand target à gauche. Si cue à droite = no désengagement quand il faut regarder à gauche (indice incongruent). **Analogue à extinction**.

1.  Motor aspect

Réticence dans NSU pour explorer HL (saccades, mvts bras…). Exploratory déficit (eg dans CT)  + leftward mvt (hypokinésie directionnelle).

-   Test = **cancellation task** + **eye tracking** = oubli et RT ++, strategy scanning aberrante, et amplitude saccadiques faibles vers la gauche. Fixations ++ vers la droite, et phénomène de pb de MDT trans-accadique.
-   Problème scanning strategy = moins bonne détection target qd disposition aléa. = pas de stratégie endogène !
-   Pop out marche mieux que **attt selective** = recherche sérielle. Peut être que attt globale moins touchée que focale.
-   Fixation à droite = hypersaillance, qui diminue qd on peut les effacer durant CT.
-   **Exploration motrice tactile : indépendemment du membre used**! hypokinésie directionnelle

1.  Motivation aspect

-   Certains objets deviennent hypersaillants suite à attente (T-d selection, on active la R en amont). On peut réduire NSU si on renforce positivement l’exploration de l’HG. Les patients affamés ont réduction NSU pour item comestible dans test sheet !

 

1.  Problème central, non périph

-   **Phénomène amorçage sémantique** Eg maison en feu. Traitement implicite. Soutient pb post-sensoriel

1.  NSU et mvt des yeux = focal et global work space

-   Quand déplacement, interaction entre coordonnées rétino et égocentrées.
-   Qd saccade, rétine coord = espace focal, et surrounding devient allocentré. Or on a NSU sur les 2 plans ! qd pcp d’un mot, la moitié gauche du mot devient allocentré et ignorée (si forme égocentrée) ou page gauche ignorée (si forme allocentrée).

1.  **NSU monde/allo- ou objet-centrée**.

-   NB = quand sujet couché, no NSU dans la quadrant haut-gauche, maintenant en haut à droite. Suggère que NSU se fait relativement à l’axe gravitaire. **NSU monde**
-   Ou alors on a R fixe de l’ev = . Par exemple, **NSU objet** centré même quand objet incliné en oblique !

1.  Effet de segmentation-conceptualisation = traitement global/holiste diminue NSU

-   Le champ S peut être affecté par segmentation pcp = gestalt ou
-   **Gestalt** = no extinction quand kanizsa carrés. Ou paquerettes NSU allo ou égo en fonction de tiges uniques ou séparées !
-   **Concep** = les mots résistent mieux à NSU que les non mots. Quand moitié droite visage dégradée, on perçoit mieux moitié gauche car on a besoin de percevoir le visage comme un tout !
-   **L’aspect holistique du S déclenche une allocation attt endogène + forte, de façon automatique.** Donc baisse de NSU dans conditions de tt particulière.

1.  Espace lointain et espace proche

-   Problème distribution de l’attt également dans y axis = + grande difficulté pcp target dans hémiquadrant du bas. Mais moins prononcée (à ne pas confondre avec mispcp of verticality).
-   Problème varie avec z axis

 

-   **DOMINANCE SPATIALE DANS L’HEMISPHERE DROIT**
-   Asymétrie = NSU que quand lésion virtuelle de l’HD
-   Fréquence NSU > HD stroke
-   Hypothèse de Mésulam = HD alloue des ress attt également dans espace ipsi. En effet, pas activation HG quand even à gauche, inverse vrai. Idem avec mémoire de travail + imagerie. Davantage activation HD quand cibles à mémoriser ipsi, alors que HG est insensible à la charge cognitive.
-   2 prédictions =
-   HD > HG quand traitement attt bilatéral
-   Activations pattern différent selon ipsi et contra.

Jusqu’ici, preuves modestes, voir Corbetta et Shulman 2012. Ou 2014.

-   **ANATOMIE FONCTIONNELLE DE LA NSU**

 

1.  **Parietal** component

-   **Mss integration**
-   Réseau fronto-parietal, voir Rizzolatti. Vison for action.
-   Hyp que IPS est le noyau parietal du réseau attt. Avec spé plutôt visuo-attt, tournée vers l’action, en lien avec motivation. Eg = **LIP connecté avec FEF, réseau de coordination de l’exploration oculaire vers objets saillants**. Réseau top down en interaction avec voie Bup.

1.  **Temporo-parieto-occipital** component

-   Région qui traite le mouvement(aires MT chez le singe). Code le **flux optique + self motion/heading**.
-   Selon mesulam, peut être que cette région utile pour certains types d’attt en lien avec détection des mvts.

1.  **Frontal** component

-   **FEF** importante pour **cover shift, attt topdown**. Une fois que mvt des yeux contrôlés en imagerie.
-   Nœud frontal du réseau attt.
-   Utile pour exploration, composante motrice dans cancellation task.
-   NB = **afférence du cortex cingulaire vers FEF = PPC** à peut être que Cortex cingulaire informe ces deux nœuds sur la saillance motivationnelle du S.
-   Rôle **FEF dans MDT sensorimotrce (transaccadique**).
-   **SEF** = supp eye field = nn avec composante davantage motrice, coderaient **position orbitale liées à saccades durant exploration** Peut être qu’un S est donc aussi codé sur le plan moteur. Hyp = lésion SEF participerait à NSU objet centré !
-   SEF et FEF code extraspace, et système moteur peripersonnal space.
-   Difference entre LIP et FEF ? différentes étapes du traitement visuo spatial = le LIP envoie info purement visuelle, alors que FEF envoie info sensorimotrice ! vecteur saccadique. Connexion avec le colliculus, ce qui permet un contrôle fin sur le plan oculomoteur. FEF transforme info visuelle du LIP en coordonnées motrices !

1.  **Limbic** component

-   Rares cas de NSU après seulement stroke dans gyrus cingulaire.
-   Cing anté = motivation / Cing post = attt shift
-   Connexion limbique chez cing ventral, **visuo-spatial dans cing postero-dorsal** (activité quand **saccades**), **somatomoteur dans anté-dorsal** (activité quand **exploration motrice distale**). Activité liée selon les cas à **planification , exécution, et post-exécution (post saccade)** !
-   **Le gyrus cingulaire a donc un rôle de monitoring attt ! récupère feedback et ajuste ress attt.**

1.  **Sub cortical** component

-   Noyaux thalamiques actifs dans tâche attt
-   Eg = **pulvinar et attt selective**. Peut etre rôle d’aug du signal-noise ratio.
-   **NSU ici liées à diachisis** = hypometabolism cortical causé à distance par lésion thalamique.
-   Colliculus sup = activité tonique relachée pour saccades, pour fovéalisation. Afférences rétine/V1/LIP/FEF/peut etre cing. Cas de NSU chez le chat quand lésion CS.
-   Lésion **ARAS** = NSU chez chat et singe. Rôle dans attt exogène.

 

-   **NSU FRONTALE VS PARIETALE ?**

** **

-   Par = plutôt pcp = X LB/CT
-   Fr = plutôt motrice = X hypokinésie
-   Voir De Schoetten ici (distinction un peu grossière) et Lunven
-   **Cela dit, dichotomie stricte impossible, car dans réseau visuo-sensori-moteur tout est mélangé ! A cause de diaschisis !**
-   **A noter que Mesulam anticipe ici papier de de Schoetten et de son hyp «dysconnexion breakdown »** **à cad que la diaschisis est accompagnée d’un déficit qui sera définie de façon prédominante par le locus de la lésion, d’où une relative spécialisation du déficit dans une composante PAR ou FR !**

** **

-   **NEURAL NETWORK DE LA DISTRIBUTION DE L’ATTT SPATIALE**
-   Selon Mesulam, network avec 3 nœuds = fr/par/cing. Si l’un de ces nœuds est touché, on a déficit multimodal.
-   En revanche, lésion qui entraîne juste déconX mais sans léser les centres entraineraient plutôt NSU unimodale.
-   Utlité du réseau attt sp = repérer S saillant (pcp, t-par) sur le plan cptal (limbic), coder ce S dans coordonnée allo/égocentrée (Par) puis motrice (Fr). Par n’est pas seulement un répertoire de coord spatiale, c’est aussi un pont vers aires motrices qui permettront de selectionner le mvt adéquat et d’explorer la scène spatiale (déplacement de l’attt via saccades) de façon optimale.
-   **L’attt spatiale est un phénomène émergent ! une qualité de ce réseau, et non le résultat d’une séquence de traitements motivationnels, pcp, et moteurs.**

** **

-   **Overlap avec autres réseaux = Mdt, eye mvt, tt temporel**
-   **Comme attt et regard souvent corrélé, overlap logique entre réseau mvt oculaires et attt spatiale (avec covert attt inclus !)**
-   **Overlap aussi avec Mdt (verbale et spatiale).** Buffer = Par, Verbal = LPI, Spatial = OC (à vérifier)
-   **Idem avec tâche où l’attt est déplacée en fonction du temps.**
-   **Point commun ? utilisation de ress attt. = déplacement du focus attt dans le domaine temporel, représentationnel, et spatial.**

** **

 

### Lunven & Bartolomeo (2017) (updating of Bartolomeo, 2007)

 

**Résumé =** La prédominance des troubles visuo-spatiaux (dont la NSU) après lésion dans l'HD souligne la latéralisation de cette fonction. Dans ce sens, les faisceaux de substance blanche (SLF par ex), qui sous-tendent le fonctionnement des réseaux attentionnels ventraux et dorsaux et leur communication réciproque, semblent organisés de façon asymétrique.

La NSU est souvent étudiée comme le résultat de lésions surtout focales, auxquelles il serait possible d'associer un type particulier de NSU (eg, on retrouve des lésions du SMG, PoC, et STG dans la NSU personnelle). Cependant la NSU est extrêmement hétérogène, il est donc difficile de faire un corrélat strict entre un type de NSU et une lésion spécifique.

Les récents travaux sur les faisceaux de substance blanche relativisent l'importance des lésions corticales, qui prédisent peut être moins la survenue d'une NSU chronique qu'une atteinte axonale (Dorrichi, 2003). A ce titre la NSU peut être vue comme un syndrome de déconnexion intra-hémisphérique fronto-pariétal (lésion SLF), voir même fronto-occipital (lésion IFOF). SLF 1,2,3 ont rôles différents, slf2 vu comme un pivot qui connecte VAN et DAN, via PAR(van)-FR(dan) network, pour avoir communication entre slf 1 et 3. D’ailleurs on a gradient d’asymétrie, le VAN étant droit++, slf 2 droit +, slf 1 bilateral.

Cela dit la NSU peut également résulter d'une déconnexion inter-hémisphérique. Des patients avec callosotomie peuvent devenir hémi-négligents, surtout si le splenium est touché. D'ailleurs il est possible de recréer une déconnexion "pure" (sans lésion des aires corticales dans l'HD) en induisant une lésion du splénium et une hémianopsie qui privent l'HD de signaux sensoriels.

La balance inter-hémisphérique est importante pour la récupération. Un hyper-métabolisme de l'HG (au repos) est un mauvais signe de récupération. D'ailleurs les stimulations rTMS sur l'HG réduisent la NSU. Il est possible que cet hyper-métabolisme de l'HG soit une tentative de récupération. Dans ce cas une lésion du corps calleux aggraverait la NSU (Lunven, 2015). Une piste de rééducation intéressante serait donc de stimuler la communication inter-hémisphérique.

### De Schotten (2012)

 

**Hypothèse** : une lésion de la matière blanche prédit mieux la NSU chronique que des lésion de la matière grise

**Méthode **:

-   Pop expé 1 : 38 chronic neglect vs 20 acute neglect
-   hodological stat (wm) vs topological stat (max of voxel lesion mapping = VLSM
-   les patients faisaient tâche de bisection et de barrage de cible
-   On contrôlait pour la taille de la lésion
-   Reconstruction fictive des faisceaux de matière blanche (les 3 types : projectifs, associatifs, commisuraux)

 

-   Pop expé 2 : Case study chez 2 patients aigu ou chronique. Même analyse

**Resultats :**

-   Gray matter :
-   **Neglect best predicted by lesion FR-PAR-T** (two attentional system) + wm
-   Bisection predicted by lesion **PPC**-FR
-   Cancellation : **T-PARa-FR**
-   White matter :
-   **Neglect predicted by SLF II and AF** (fr-par portion) lesion
-   Deviation : SLF II and AF again
-   Case study :
-   **N+ showed SLF II**, and cortico-p-c lesions compared to N- patient. They exhibited two of us lesion grey matter (with more damage in N- patient paradoxically) and destruction of SLFIII, IFOF and UF, with SLF I safe

**Discussion:**

-   Important to distinguish **disconnected syndrome** (cortico modules remain safe, cf deafferentation in hemianopia with CCls lesion splenium) and a **disconnected breakdown**
-   If disconnected breakdown, it have an impact not only on long range wm fibers, but also on these short range : in function of the original predominance of the lesion, the nature of neglect won't be the same (eg : in bisection or cancellation tasks)

### Dorrichi & Tomaiuolo (2003)

**Hypothèse** = contrairement à Karnath qui met l’emphase sur une lésion STG que TPJ/IPL grey matter. Ca fait débat car c plutôt ipl en charge d'intégration mss et de cpt visuo-moteurs-attt. On pense que discrepancy due à lésion irm mesurée à j+9, phase acute. Or les symptomes de nsu se résolvent très vite. Ici on explore effet lésion white matter chez chronique (j+120) pour pallier à ce pb. néanmoins on retroquera que phén de plasticité possible à j+120.

**Méthode** = voxel base mapping chez NSU + ou -. Moins précis que DeSchoetten cela dit.  
- pop = 11 sub nsu / 10 m+ / 10 rbd
- lb + canc
- j + 80-180 (chronique) ; m = j+121. 


**Résultat** = corrélation avec lésion SLF ++ , surtout chez patient NSU +.  
- n+ = smg + slf + callosal radiation
- subn = slf
- rbd = corticospinal tract
- critique de bartolomeo : body neglect cachée ? 
- pas de lésion stg.

**Discussion** = Si déconnexion FR-PAR, va dans le sens d’un manque de communication SM. de +, damage grey matter tpj/stg fit bien avec implication de ces aires dans reorientation de l'attt et dans mss integration dans ips/smg rostral (voir ref 18).  
ici on extrapole, un peu comme C et S 2011 ou Rizzolatti, que deconnexion fr-par va empêcher com de coord spatiales des objets à aires frontales, et en retour empêcher contrôle top down de l'attt vers aires spatiales. de +, on a overlap avec circuit s-m, mdt spatiale ... beaucoup de fonctions désintégrées par deconnexion SLF. 


### PreMotor Neglect Theories

### Rizzolatti, Fogassi, & Gallese (1997)

Article de revue. Idées principales =

**Hypothèse** = la perception de l’espace n’est pas indépendante du système moteur. On encoderait la position d’un objet selon les afférences possibles, les visées motrices. Les travaux chez le singe supportent cette théorie.

-   **AIP** dans intra-PAR = activation quand on présente un objet3D + grasping. AIP connecté à PMv. Si on lèse ces 2 aires (**l’une ou l’autre ? quelle ref ?),** plus de grasping adapté.
-   **VIP** = activation quand vision ou toucher, neurones bimodaux. S’activent surtout dans l’espace très proche, près du visage. Certains RFS à prédominance tactile ne sont pas fovéa-centrés, c’est-à-dire que l’activation de ces neurones est modulé par mouvement du corps même si le regard est fixe. Circuit impliqué dans reaching.
-   Grasping + reaching = hypothèse que l’activation des neurones codent pour une action potentielle. Grasping = je code l’objet en mouvement ; reaching = je code l’espace en mouvement.
-   **LIP** = activation quand vision, avant et pendant saccades. Mais aussi nn tactiles, plus autour du corsp (moins la tête).

**Discussion** = chez le tout petit, l’espace est uniquement personnel et péri-personnel. L’espace serait initialement moteur, et c’est ensuite que se développe des RFS « téléceptifs », avec développement traitement allocentré.

### Grazziano et al. (1999)

Hypothèse = Chez le singe des neurones visuo-tactiles, bimodaux, participent à coder la position de notre corps dans l’espace, avec participation de neurones PMv. Ces neurones ont des RFS ancrés sur le bras, pas le fovéa.

**Méthode** :

-   enregistrer spike chez 36 nn dans PMv. On place le bras du singe sur un support fixe et on bouge la position du bras = **ipsi ou contra**
-   On **cache/non** dans une condition le bras. On créer **décorrélation avec faux bras/non. NB = protocole très élégant, ou les modalités sont interchangées parfaitement !**
-   On envoie une balle vers le bras.

**Résultats** =

-   Réponse la plus forte quand tactile + vision + balle vers bras.
-   RFS réponse modulée par la position du bras = non rétinotopique.
-   Quand on cache le bras, baisse de la réponse = importance nn visuels.
-   Bras taxidermiste déclenche réponse RFS visuels qui est plus forte que dans la condition cachée ! la vision influence la proprioception.

### Wolpert (1998) 

**Hypothèse :** Pour avoir une estimation de l'état de notre corps et du monde, de façon dynamique et efficiente (l'environnement corporel et extracorporel est source de bruit) notre SN intègre des informations sensorielles et motrices qui nous renseignent sur la configuration de notre corps et les interactions avec le monde (eg : le contact de ma main avec un arbre, l'angle de mes articulations). Ces estimations seraient misent à jour constamment. Le SPL pourrait stocker ces estimations corporelles et extracorporelles. 

**Méthode** : La patiente PJ a été soumise à une série de tests. Cette patiente est atteinte d'une lésion en évolution depuis 2 ans au moment de l'expérience, au niveau du lobe PAR gauche, et plus exactement le LPS. Celle-ci ne percoit plus ses membres dans la modalité tactile dès lors qu'elle ne les perçoit plus dans la modalité visuelle. A noter que cette patiente n'était pas atteinte de NSU, d'agnosie tactile, ni d'apraxie (atteinte motrice).

Cette disparition de la sensation tactile a été évaluée dans des tâches de pointages lent, de grip d'un objet de poids variable, de jugement proprioceptifs de la position du bras et d'un objet placé dans la paume de la main.

**Résultats** : dans les 4 évaluations, la sensation somatosensorielle disparaissait systématiquement au bout de 10 à 20 secondes.

**Interprétation** : Les estimations SM seraient intactes mais pas stockées à cause de la lésion du SPL. Les estimations SM sont donc en constante désactivation, et ce indépendamment d'une baisse de vigilance dont PJ n'était pas victime. Le LPS pourrait donc maintenir une représentation interne stable du corps dans l'espace (différente du schéma corporel).

 


 



### Representation Theories

### Rode et al (2010)
**contexte =** expé passée ont montré rés contradictoire sur effet de diminution de cues exogènes sur nsu R visuelle. des fois ça n'a pas d'effet (rode, 07). ici on réplique avec des contrôles sup', + irm hodos.
**méthode =** 
- 3 patients = 1 avec nsu R et pcp RD  / 1 avec nsu pcp PP seulement.
- RD = lésion WM slf, splenium, ifof
- PP = slf
- grey matter = lésion par-t ou par-occ
- tâche de rd = imaginer carte de la france dans plusieurs conditions = yeux ouverts/fermés, réponse verbale, manuelle, rotation 180°, + tâche contrôle de mémoire de travail verbale.
**résultat =**
- RD a bien asymmétrie rappel des villes, même qd rotation 180°, ou réponse manuelle. avoir les yeux ouverts avec les contours de la carte de la france n'a pas d'effet. nb = les villes doivent être "spatialisées" pour être négligées.
- PP n'a pas de mal à rappeler ville mentalement.
**discussion =** d'un point de vue théorique, va dans le sens d'un déficit attt exogene+endogene pour nsu pcp, et attt endogène mais pas exog pour nsu R. On ne sait pas si pb de distorsion de la R. 

### De Renzi et al. (1970)
**Contexte =** on se demande si déficit d'exploration de l'espace repose sur mécanisme pcp, moteur, ou R. Par exemple, dans modalité visuelle, perte d'exploration espace gauche peut être causé par pb attt, oculo-moteur, ou bien parce que le percept ne donne pas lieu à une R spatiale (relation spatiale intra et inter-objets). Ici on demande à sujets cérébrolésés de faire tâche visuelle et tactile d'exploration, comme ça on élimine biais attentionnel et oculo-moteur.
**méthode =**
- groupe contrôle = sujets sain ou alors lésés mais pas au cerveau
- rbd + lbd
- groupe de 30 à peu près. car on subdivise lbd et rbd en fonction de si hémianopie ou non.
- visual search = trouver nombre (pancarte) parmi distracteur.
- tactile search = maze avec rebords, trouver bille dans un coin.
- vd = time (pb = des fois on a perf plancher, time limit = 90s.)
**resultats** = 
- visuel = grosso modo, pb exploration + fort quand côté contra, avec effet délétère de hémianopie.
- tactile = idem, avec aussi effet délétère de hlh = intéressant d'un point de vue Kosslyn 2006, imagerie mentale ! qd on regarde nb de failure (xhi2), on a effet++ chez rbd pour côté contra qd hemianopie ! 
**discussion =** Selon de renzi, rés dans tâche tactile montre que nsu peut impliquer une "R mutilée" de l'espace. En effet, les patients percevaient bien l'espace du labyrinthe, mais n'étaient pas capable d'en reconstruire une R mentale.

### Guariglia & Piccardi (2010)

Ref liées à lire : **De Renzi E (1982) Disorders of space exploration and cognition Wiley, Chichester**

Revue. Idées importantes =

-   Le déficit de navigation n’est pas juste un épiphénomène de la NSU. De façon générale, NSU représentationnelle les plus touchées car incapable de construire une carte mentale spatiale de leur environnement. Touchés dans tâche « piscine Morris » (**Gariguilia 2005, Nico 2008**). A noter dissociation pour pb navigation dans espace personnel et extra personnel !
-   NSU perceptuelles n’ont pas ce problème malgré le déficit S. On retrouve chez ces patients une double dissociation : certains utilisent bien indices topos pour localiser objet, mais ne peuvent pas de servir d’indices visuels globaux (la pièce) et vice-versa. Interessant car problème de navigation a lieu dans des ev avec beaucoup d’indices visuels.
-   Différentes hyp = problème de transformation coord allo en égo ? surcharge cognitive, pb attentionall ++ quand il y a beaucoup d’indices ?

### Guariglia et al. (2005)

 

**Hypothèse/contexte =** Théorie 3 étapes dans la navigation : recherche directe quand nouveau env grâce à **idiothetic signals** (motor efference, somato, ppc, vestibular, optic flow…), puis repèrage indices pour avoir encodage de la position, ré-orientation grâce à R en mémoire quand indices idiothétiques ne sont plus dispo. Hypothèse : différents types de NSU peuvent affecter navigation différemment.

**Méthode** = on compare 5 groupes (contrôle, LB, RB, NSU-P+,NSU-R+) dans tâche de navigation, pièce vide sans indices. Ils font que les 1 et 3 étapes (searching directe, réorientation, searching durecte delayed) et on enregistre le temps mis et leur déplacement (analyse qualitative).

**Résultats** = les patients R+ mettent temps +++ quand ils doivent se réorienter,,c-a-d quand on les a changé de position avant de refaire la recherche (aller physiquement vers la cible). De plus, même déficit quand delayed + pas de réorientation = **suggère pas de cognitive map car indices idiothetic sont pas stockés en MLT**. Analyse confirmée par pathway aberrant.  
**Reflexion personnelle : le problème de navigation spatiale fit bien avec importance du système vestibulaire dans navigation spatiale (ahead signal). A l'avenir il serait intéressant d'étudier lien entre nsu représentationnelle et représentation de la verticalité**. 

### Nico et al. (2008)

 

**Hypothèse/contexte =** est ce que cette fois la presence d’indice, **landmarks**, va faciliter la recherche en condition ré-orientation (en + des signaux idiothétiques ?) ? Dans expé de Guariglia, les patients pouvaient que utiliser le module géométrique, c’est-à-dire codage euclidien de la cible. NB = quand on parle de pb de navigation = désorientation égocentrique.

Méthode = idem que Guariglia 2005, mais on rajoutait deux indices visuels dans la pièce. En plus de ça les sujets devaient ensuite faire une description de la pièce = sa forme et les indices.

**Résultats =**
- P+ = nsu perceptive
-   en searching, TR ++ chez P+ et N+ > autres groupes.
-   Immediate reaching (pas de réorientation) = idem+ rotation error, inaccuracy quand ils arrivent vers target chez les deux groupes NSU.
-   Delayed = R+ seulement + pathway
-   Quand on demande d’écrire landmark, les NSU n’arrivent à bien les situer entre eux.

**Interprétation =** Pour les NSU R+, on retrouve encore une fois le problème carte cognitive, pas de R, pas d’update. Chez NSU P+, on interprète leur difficultés comme étant la conséquence du déficit visuo-spatial égocentrique = utlisation automatique de repères mal localisés dans l’espace. Expliquerait les comportements cliniques de désorientation dans un environnement écologiquement riche en repère !

 
### Prism Adaptation

### Dijkerman et al. (2004)
**contexte =** maravita et mcintosh (03 et 02) ont montré effet PA sur tâches spatiales impliquant la somesthésie. Ici on regarde effet PA chez une patiente dans pressure sensitivity (hemianesthesia) et proprioception (finger position sense). 
**méthode=**
- KP, mca, m+3 qd test. nsu visuelle à +5w, dans line/star/letter/bit/rey. 
- lésion par-t
- pressure = monofilament sur main g ou d.
- finger position sense = on prend un des 5 doigts, main gauche ou droite, et on le tire vers le haut ou vers la bas.
- vd = ratio bonne réponse espace g/d.
- pa = rpa 10°, 2 cibles +/- 100
- 7 sessions = 3 baseline (vérifier stabilité baseline) espacée d'une semaine
- puis pa/+30 min/+1w/+3w
- puis à nouveau pa/+30 min (vérifier stabilité effet PA)
**résultats=**
- baseline ok = pooled
- effet * PA sur les 2 tâches, avec effet + fort sur monof. peut être car baseline + basse.
- effet à long terme pour monofilament qd on pooled les sessions post 1ere PA.
- effet 2ème PA idem.
- on contrôle aussi effet PA sur nsu = on remarque que nsu est stable, elle a régressé, mais reste résiduelle. donc effet sur somato pcp pas due à régression nsu ici. dommage de ne pas avoir mieux contrôler cor stat.
**discussion =** les auteurs ne spéculent pas vraiment ici. la tâche de pressure sensitivity est purement pcp, n'implique pas de R spatiale selon eux. Néanmoins Vallar ne serait pas d'accord, car PA a peut-être améliorée R du corps, et donc pcp signal contra devient à nouveau possible. à noter que finger position demande R spatiale. Voir Kerkhoff team 2011 sur effet GVS sur arm position. Va aussi dans le sens d'un effet PA sur déficit pcp "supramodal" dans la nsu. Jacobs argumente que ce déficit concerne tant l'attt que la R de l'espace. 

### Facchin et al. (2019)
**contexte =** nsu associée à hemianesthesia /somatosensory impairment (1°). hors, on a aussi pseudo-hemianesthesia (nsu tactile). effet pa sur nsu tactile peu investi, sauf par frassinetti, avec rés encourageant sur le fluff test. on a aussi travail de mcintosh (exploration haptique), dijkerman (pressure, finger sense position), maravita (tactile extinction), ou encor mattioli 2015 (nsu tactile). Ici on regarde + en détail effet PA sur pseudo hemianesthesie.
**méthode =**
- 36 rbd, dont 21 nsu. j 8-1200 (174 en moy)
- nsu basée sur 1/4 tests = star/lb/lecture/comb. 
- somato eval = uss/dss. tapping sur palme dorsale main contra (bisiach 1986). + stim electrocutanée pour enregistrer motor et pcp threshold (cs detection). 
- pa = 3 cibles (-21/0/+21). t = 90. 16.7°. sham + rpa sur 2 sessions différentes. nb = 12 patients ont refait rpa une deuxième fois pour regarder icc, ok ici. 
- on regarde aussi lésion.
- analyse = group (sham/rpa) * session (+ * arm (left/right) qd electro S)
**resultats =**
- ssa = tout *** = pes>.80. (contrôle)
- anova sur le toucher (uss) = ns. normal selon auteurs car seulement 7 patients avaient un déficit. 
- avec dss = interaction * = amélioration extinction (pes = .77), cad bcq plus d'identification correcte qd prismes.
- electro S = qd main gauche, amélioration du seuil de perception de la S qd prisme. ns pour seuil moteur.
- comb = interaction * (.44). idem pour str et lb (pas pour reading). (pes>.39). 
- lesion = pop = 19. vlsm sur somatosensory pcp => VD = pcp seuil. **SMG + PoC**. qd on regarde les corrélats liés à amélioration du seuil pcp, ns. du coup les auteurs font split half, puis contraste. on retrouve aires PoC+insula+ifg+caudé. 
**discussion =** rés en faveur d'effet + rpa sur nsu personnelle. en effet, rpa améliore extinction tactile, perception tactile (qd stim electrique avec intensité progressive). donc pa agit sur composante perceptive non visuelle. étude en faveur de vision mss de la nsu. critique = quid premotor nsu ? dans comb ? ici on a justement effet * sur pcp tactile, qui n'implique pas composante motrice, donc ça va.   
le rés sur les lésions liées à hemianesthesia (un mélange de vrai et de pseudo) conforte rés obtenus ailleurs avec hemian vraie (eg = meyer 16, preusser, 15), qui engageait operculum par. la seconde analyse était ns, ce qui veut dire que les patients avaient un certain degré de vraie hemianesthesie. les auteurs proposent overlap entre vraie et pseudo hemia.  
--> point de vue théorique = conforte déficit supra-modal de la R de l'espace. cf Jacobs 2012. critique cela dit = quid effet attt ? peut etre que prisme augmente attt portée à hemicorps gauche, et donc meilleure detection. Comme d'habitude, impossible de déméler clairement effet attt(pcp) de effet R ! Peut-être pcq il y a beaucoup d'interaction aussi. 

### Striemer & Danckert (2010a)

**contexte =** on critique proposition que prisme agit sur voie ventro-dorsale dans nsu, car lésée. pa devrait agir only sur voie dorsale, épargnée sur le plan structurel. On le prouve avec paradigme lm chez nsu. voir revue aussi.
**méthode=**
- pop = 8 normals + 3 nsu (lb, copy, target). lésion par-oc wm, classique. pre pa, biais leftward dans lm.
- lm = dire verbalement si cross + proche ou + loin (contrebalancé). 16 essais dont 10 milieu (voir 2016).
- pa = NSU = rpa 10°. 10 minutes. nb = pointages toutes les 2/3 s. / CONTROLE = lpa.
- ssa pre-post-fin.
- on regarde aussi lb.
**resultats =**
- ssa post-fin * pour tout le monde.
- lb = effet lpa * chez contrôle. shift rightw / shift leftw nsu. tous les patients outside normal range (*Crawford t test*)
- lm = ns chez controle, mais on remarque que shift pcp vers la droite + fort (aug left response) quand lm avant lb. / chez patients, biais leftw non altéré, rép gauche min 85%.
**discussion =** papier qui va dans le sens du modèle de d et s 10. chez nsu, perte de connexion entre voie dorsale et ventrale, et donc pas d'effet positif des prismes. **critique = la méthode lm ici est grossière, très peu d'essais, pré-bisection des lignes très proches du centre ! donc peut-être qu'on aurait pu observer un shift sur lignes pré-bisectées + en périphérie ... de +, 3 cas seulement ... est-ce que design + fin a été conçu dans la litté ?? 

### Striemer et al. (2016)
**contexte=** modèle de striemer et danckert 2010 prédit que effet attt ++ dans nsu ad pa. ici avec sujet sain on réplique paradigme de colent et on regarde si taille des prismes influe + sur aftereffect moteur vs perceptif. prédiction que si component attt + fort, shift LB + sensible à taille des prismes, car implique voie dorsale SM, alors que lm implique + voie ventrale.
**méthode =**
- pop = 47 droitiers
- 22 prismes 8.5° / 25 prismes 17° (tjrs LPA)
- lpa avec ecran tactile/ chin rest
- baseline = 2 blocs de lm + lb = comme ça on regarde si mesure est stable. ici oui, donc collapse.
- après prismes, toujours lm AVANT lb, car effet pcp s'évapore sinon (voir striemer 2010 avec nsu).
- on regarde SSA immédiat + à la fin des blocs, pour vérifier que effet PA tjrs présent.
- lb = 10 trials, paper-pen
- lm = 16 trials, pas de psychophy ici... on a 10 lignes présentées au milieu, et on fait croire que non ... + 6 lignes + périph' (1,3,5mm). dire si ligne + PROCHE du côté gauche ou droit. 
- prisme = 7-10 minutes, 200 pointages, 2 cibles +/-10cm.
- ssa = 5 trials pre-post-fin
- stat = session * prismes
**results=**
- difference entre ssa 8 vs 17° tjrs *, sauf en pretest. = effet durable des prismes. donc prisme + fort a effet SM + fort.
- lb = interaction group * session = pes = .15.  shift rightward LB 17°* mais pas si 8.5° (ns). 
- nb = ici on pourrait critiquer que groupe 8.5° avait perf + vers le centre/droit en baseline (donc biaisé à droite quand on prend en compte la pseudo nsu). donc ici on enlève outliers et on voit que rés idem.
- lm = main time * = pes = .12. pas d'interaction. réduction des réponses "plus près vers la droite" (pseudo nsu, on exaggère longueur segment gauche). 
- enfin, on regarde corrélation entre ssa shift et delta lb/lm. cor positive* avec lb-ssa. ns lm, cor négative proche de 0. 
- nb = les auteurs avaient aussi testé avc rpa, avec rés tjrs ns. 
**discussion = ** on observe shift rightward * pour lb si 17°, et shift pcp rightward global entre pre-post qql soit prisme (pas interaction). donc modifier magnitude de la recalibration SM impacte lb. donc chez nsu, prisme devrait avoir AE + fort dans tâche avec composante motrice. 

### Colent et al. (2000)
**Contexte** = en 2000, papier récent de rossetti. on ne sait pas encore si prisme agit sur attention spatiale, représentation spatiale, ou simple effet sm. ici on regarde chez sujet sain si prisme influe lm task, connue pour évaluer R de l'espace, et en + LB motrice classique.
**méthode =**
- pop = 14 droitiers
- 7 lpa vs 7 rpa
- pa = 20 minutes ! + 15°. 
- session pre-post
- lb = 10 essais / lm = 78 essais en tout. no time limit. de +, il semble que matériel pas présenté sur ordinateur...bruit ici car en + pas de chin rest, quid contrôle de la posture ...:-1:
- calcul pse dans les 2 tâches, mais pas via méthode psychophysique. je pense que ça impacte la fiabilité de la mesure...
**resultats=**
- lb = ns
- lm = on a shift * entre pre-post vers la droite.125.10 --> 123.98 => +1.12 ! 
- ns quand rpa. 
- taille d'effet à  calculer ici ! 
**discussion** = on peut exclure effet attentionnel, car effet ns dans plusieurs cond. pour la 1ère fois, on montre que effet prisme est inversé/nsu. on spécule que lpa interfère avec fonctionnement du lobe par droit. compatible avec influence sur R multimodale égocentrée de l'espace.




### Striemer & Danckert (2010b)
Papier d'opinion: ***Through a prism darkly: re-evaluating prisms and neglect***  
a) Prism adaptation and visual neglect
- PA = le système moteur a recalibré les cadres de référence des yeux et de la main pour les réaligner ensemble.
- effet présupposé sur **higher order visual cognition**, critiqué ici.
- ici on propose que pcp, voie ventrale, pas améliorée par PA, unlike attt spatiale, voie dorsale.

b) Are prisms really a cure for neglect ?
- Nys, Turton ... contre évidence pour long lasting aftereffect dans nsu. montre bien que PA ne marche pas tjrs.
- limite étude PA =
- 1) small groups, case studies, alors que nsu très variable. alors qu'on peut avoir effet spectaculaire au niveau ind, on peut avoir effet null au niveau du groupe.
- 2) tâche papier-crayon avec la main de pointage. Sarri 2008 a montré cor entre ssa shift et tâche visuo-spatiale (cet arg est selon moi + faible, car dans le temps ssa revient à la normale tandis que after effect persiste. cela dit, c'est peut être dû à effet oculaire persistant). 

c) When seeing is not believing: The influence of prisms on attention and perception in neglect
- effet robuste PA sur covert att et extinction. effet sur attt globale vs locale (bultitude). mais voir Morris 2004 (ns dans visual serach ecolo).
- dijkerman 2003, ferber 2003 => **après PA on + exploration overt vers la gauche, mais pas d'effet sur perception !**. 
- compatible avec rés de strimer 2010  : effet pa sur line bis manuelle, mais pas sur lm task (nb = *milner 93* pour ref ici).
- effet aussi contradictoire sur grey scale task et chimeric face task.

d) Potential inconsistencies
- visual search = affaire de pcp selon les auteurs. si rés * , peut être à cause d'effet "pop-out" et pas de limite de temps.
- chimeric object vs face = tâche objet + facile. moitié animal vs émotion, + subtile. effet PA dans une tâche trop simple ? 
- enfin, on K effet PA sur imagerie mentale. mais imageries peut se faire via voie dorsale. (sujet complexe). autre critique = carte de la france très K en MLT. donc scan mental image MLT. de +, hyp de Meneor voir *Laeng, B. and Teodorescu, D.S. (2002)*.

e) PA and the dorsal and ventral streams: A new look forward
- proposition ici = PA agit sur DAN = SPL/IPS(aip) impliqué dans visuomotor behavior et attt spatiale.
- compatible avec le fait que ces régions sont structurellement intactes dans la nsu ! voir Corbetta 2005.
- proposition : réalignement spatial via rCB est communiqué à left SPL/IPS, qui va envoyer ce signal à son homologue droit. du coup, capacité à ré-orienter attt vers la gauche. 
- modèle compatible avec dommage structurel ventral, qui empêche re réhabiliter la pcp ! 
- ![](https://i.imgur.com/GL3ijhR.png)
- **A lire =** *Husain, M. and Nachev, P. (2007) Space and the parietal cortex.TrendsCogn. Sci.11, 30–36 ; Goodale & Milner, 2006* 
- -->**IPL/STG important pour relier information visuelle de la voie ventrale à motor outputs de la voie dorsale.**
 
### Saj et al. (2013)

**Contexte =** Corbetta 2005 montre que amélioration nsu survient quand equilibre inter-H au niveau des réseaux DAN. ici on regarde si les prismes modifie act fonctionnelle chez nsu au niveau de ces aires.

**méthode =**
- 7 rbd, avec lésion focale 1 seul lobe et réduction alertness (:-1: sur quel critère ? la nsu souvent associée avec perte alertness et grosse lésion ! **ici on introduit un biais d'éch déjà important**)
- nsu = bell + copy + lb
- on excluait pb alertness car 3 sessions fmri dans la journée = au total = 22 minutes. 22 minutes de concentration c'est bcq pour un patient, surtout à J+10 ! donc on imagine bien qu'ici on n'avait pas nsu très sévère.
- J + 10/32 
- 26C sans PA, pour avoir baseline qualitative dans tâches.
- 3 tâches diff où on présente 3 formes alignées sur plan H = pcp/attt/mémoire (tâche contrpole). pcp = LB = dire si forme du milieu au milieu ou non des 2 autres. attt = visual search task = dire forme de item isolé. memoire : dire si triplet apparaissent même endroit qu'avant.
- VD = accuracy (cptal)
- chaque tâche : 30 essais * 3 scanning sessions. 
- **3 runs = baseline1/baseline2/post-effect**. on a 2 baseline pour contrôler effet d'apg. et un post-effet. on a PA entre session 2 et 3.
- PA = 20° / 2cibles +/-10°/80 essais. 5min. 
- 3x3x3, fwhm = 8mm

**Résultats=**
- *cptal =* 
- perf patients < controle. chez patients, perf right field > left. 
- pas de diff de perf entre session 1et2. mais session 1+2 != de session 3. NB = ici c'est un effet principal, il n'y a pas d'interaction. ca suggère que effet PA agit de façon non spécifique sur les tâches. même la t^che deM profite des prismes, mais quand même moins que les autres tâches pour ce qui est des S dans champ visuel gauche ! 
- *fMRI =*
-  **Chez contrôle**, toutes les tâches activaient de façon bil, surtout aires rSFG et rPPC quand attt + pcp versus memory qui active lParahipp + pCing.
-  **Chez patient**
-  LB = post-pre = act bil OCC+PPC+MFG
-  Search= idem avec act TPJ aussi.
-  Memory = ns.
-   pas de chgt aussi dans CB.
-   important, l'act de ces aites dans LB et search cor ave accuracy dans les tâches sur le plan cptal. cette cor était * pour SPL/MFG/OC **gauche**, et margin pour droit.

**Discussion =** Effet des prismes sur act de régions de la voie dorsale, impliquées dans attt endogène. ce rés ne corrobore pas rés de Luauté concernant act STS/STG chez sujet sain. ca va dans le sens de hyp de Corbetta, cad importance DAN dans NSU. De +, amélioration nsu ici cor avec recrutement bil et pas seulement réactivation de l'HD. modèle kinsbourne, corbetta, mesulam ? ce rés ne peut pas vraiment trancher.
 
### Luauté et al., 2009
**contexte=** on sait depuis *weiner 1983* que cervelet important. on pense aussi que cortex pariétal impliqué. ici on veut étudier corrélation entre PA sur le plan cptal et neuro-fonctionnel. Peut être que cortex pariétal = **component stratégie** whereas cervelet = **component true**. 

**Méthode=**
- pop = 14C sains
- 4 sessions en fMRI : neutral/PA left 10°X2/neutral avec 12 essais pointages par sessions. Mais du coup :-1: pour moi on n'est pas sur "true adaptation" component ... 24 pointages c'est très peu comparé aux 250 pointages préconisés par McIntosh 2019... mais c peut être suffisant sur très CT ...
- nb = lunette ici ne sont pas **fresnel** mais **point-to-point** = ne déforme pas ev surrounding. , ici patch sur un oeil, et déviation monoculaire du champ visuel. contrebal de l'oeil caché. 
- 2 targets +/- 10°.
- apparatus de pontages = tablette tactile.
- cluster >10 - fwhm = 8mm. - 2x2x2.
- :-1: LPA uniquement. pas de groupe contrôle RPA.

**Résultats =**  
i) cptal  
- run 3 error = run 1 => montre qu'il y a eu adaptation.
- error max sur run 2, -1.7°. 
- run 4 = shift 2.5° de l'after effect en moy.  

ii)fMRI  
- ici contrastes run 1+4 vs 2+3. on contrôle aires spé à port des prismes : M1l. cervelet bil. SMA bil. S1l. + POS/IPS, involved in visuo-motor control.
- ici contrastes run 2+3 vs run 1. activation STS/STG, plus forte run 3> 2 en bil. Cet effet n'est pas visuo-motor.
- ici contraste run 2 vs 3 pour voir effet phase précoce des prismes, où il y a bcq d'erreurs, sur activité fonct. rCB + rSPL + rIPS + lIPS + lIPL. A noter qu'on a encore act du cervelet, actif quand activité SM + detection erreur. De plus, on a baisse d'act des aires PAR en run 3 > 2, contrairement a act STS/stg qui augmente ! 
- on retrouve cor entre baisse d'act du lAIP et diminution erreur. plus je dévie, plus AIP activé. 
- on calcule aussi 2 indexs : error size vs error correction (error size à essai n vs n-1). on voit que act POSl modulée par *error correction* > size. alors que AIPl modulé par *error size* ! 
- trial by trial analyse sur run 2/3, on regarde time course aip pos, rCB. au fur et à mesure, act aip diminue, et pos/rCB augmente. mais on atteint plateau.
- enfin, on regarde de-adaptation sur run 4. contraste run 4 vs 1. on retrouve sans surprise act lAIP (car erreur dûe à aftereffect).

**Discussion =** grossomodo, PPC utile pour detection et correction erreur visuo-motrice. cohérent avec ataxie optique quand lésion POS voie dorsale. POS contribue à "stratégique" adaptation. Act du cervelet droit augmente aussi durant prismes, de façon *plus progressive*, ce qui est compatible avec idée du "true adaptation". **par true adaptation, on désigne la mise à jour du modèle interne qui ré-établit une association entre les coordonnées visuo-motrices et sensori-motrices.**. NB = le true adaptation component ne désigne pas l'effet postest sur cognition spatiale. on reste dans le domaine SM. Enfin, la nouveauté de cet article est la contribution du STG dans PA, bil. STG impliqué dans mss. peut être, spéculation, que *STG associe les coordonnées spatiales proprioceptives et visuelles dans un référentiel spatiale commun*. Peut être que STG impliqué dans arm in body et eye on head-body transformations.



### Serino et al. (2006)

**Contexte théorique =** Ici on explore si amélioration des prismes est corrélée avec exploration saccadique vers l'hémiespace gauche.

**Méthode =**
- pop = 24 rbd : 16 PA, 8 C. minimum M+3.matching age, sévérité, duration.
- groupe controle réhab classique.
- 4 sessions de test : avant/ + 1w/+1M/+3M.
- BIT
- saccadic eye mvt enregistré chez 11nsu+6C, dans tâche de lecture de mot isolé présenté 1 par 1 à l'écran pendant 4 s. On regarde erreur de lecture en fonction de la session, l'emplacement de l'"atterissage" de la 1ère saccade, et la proportion de temps passée à gauche vs droite. 
- PA : idem que Frassinetti sauf que 1 seule session par jour => 10 sessions sur 2 semaines. même procédure générale et apparatus.

**Résultats =**
- Amélioration BIT + reading accuracy avec session et PA.
- tous les patients ne se sont pas adaptés aux prismes (pas de error adaptation). de façon globale on a quand même after effect du groupe. Cet effet se stabilise sur les 2 semaines de pa.
- **pas de cor entre error reduction et after effect amplitude**
- cor entre error reduction durant la 1ère semaine de PA et BIT amélioration avant vs après PA. 
- PA a shifté 1st saccade + vers la gauche > C. eg : on est passé de -0.7° avant PA à -1.87° après PA. On remarque aussi que error reduction first week est aussi corrélé à 1st saccade + vers la gauche en post test. mais pas de cor avec after effect. C'est cohérent avec cor entre baisse de nsu dans BIT et saccade vers la gauche.
- + de temps passé à gauche en terme d'exploration, mais pas de cor avec nsu improvement.
- méthode un peu ancienne d'analyse IRMa (:-1:) suggère que patients avec lésion occipitale s'adaptaient moins que patients sans ces lésions à PA. 

**Discussion =** On retrouve encore une fois effet à long terme (+3 M) des prismes sur nsu. De +, cette amélioration corrélait avec exploration saccadaique vers la gauche. Ici on extrapole sur lien entre attt spatiale et gaze, avec idée que exploration spatiale contralésionnelle est renforcée positivement ce qui explique effet à cognitif LT, contrairement à effet purement SM. 
 
### Frassinetti et al. (2002)

**Contexte théorique =** Rossetti (1998) a montré post effet des prismes dans nsu pour quelques heures. Ici on explore effet à long terme (+5 semaines = 720h). On regarde si effet + sur composante cptale de la BIT (+ecolo). On regarde aussi effet sur nsu perso, peri, far space.

**Méthode =**
- 13 rbd avec nsu chronique (minimum M+3), pour éviter effet de récupération spontanée. matching pour j+ et sévérité nsu. mais pas pour lésion. 
- 7 nsu + 6 c. les patients contrôle n'ont pas de pa, mais prise en charge classique sur le plan moteur et cognitif.
- test nsu : bit cog + cptale (question vie tous les jours) + fluff/search perispace d'objet/room description far space + bell/reading + motricité
- prisme procédure : +/-21/0° : 3 cibles. pointages effectué avant(60 trials)/pendant(90trials)/après(30 trials).
- **PA 2 fois/jours sur 2 semaines, en tout 20 PA sessions sur les 2 semaines.**
- On teste VD sur 5 sessions : avant/immediat/+2j/+1w/+5w après les 2 w de PA).

**Résultats =**
- tous les patients se sont adaptés, sauf le patient RD, qui avait taille lésion ++. 
- Effet * de la PA chez groupe PA vs controle dans le temps dans test BIT, reading, bells, perispace search, room description, mais pas fluff test, malgré rés encourageant (manque de puissance ici ?).
- adaptation effect : à quel point il y a erreur de pointages corrigés pendant les prismes.
- after-effect : erreur de pointage après avoir enlevé les prismes.
- On constate quel'after-effect diminue au fur et à mesure des sessions de tests (+2/1w/+5w), contrairement aux tests de nsu. Cela suggère que réponse oculomotrice biaisée vers la gauche résiste au temps, contrairement à l'effet purement sensori-moteur.
- Patient RD : pas d'adaptation effect, car pointages sytématiquement à droite. effet à j+2 sur test nsu mitigés. 

**Discussion =** Effet à long terme des prismes sur la nsu dans tâches extrapersonnelles. effet sur nsu perso pas clair (mais étudié dans études futures). tous les patients ne s'adaptent pas aux prismes. effet SM manuel n'explique pas meilleure perf car after effect pointage vers la gauche diminue dans le temps.  
Mécanisme pas bien connu, effet de réactivation non spécifique de l'HD ? effet attt via shift oculomoteur qui réactive le DAN ?
 
### Rode et al. (2001)

**Contexte =** Effet PA sur tâche papier-crayon. mais qui impliquait réponse sensorimotrice aussi impliquée dans PA. ici on investit effet PA sur tâche imagerie mentale spatiale.

**Méthode =**
- pop = 2c / 2nsu
- screening de hemi-parésie-aesthésie-anopie, nb, bn, R.
- test à m+1.
- prisme **droit** avec 50 pointages, 10°.
- tâches : ssa, daisy R, france. Pour tâches R, on regardait avant, après, +24h.
- NB = 1/2 patient avait ssa déplacée patho vers la gauche. va dans sens observation farnè, chokron...

**Résultat =**
- SSA = effet des prismes sur patient efficace, shift sys vers la gauche. pas testé chez patient.
- daisy = dessinée en entière juste après pa, et presque entière (moitié de la partie gauche) à +24h
- france = augmentation spectaculaire des villes citées à gauche. chi 2 sgn. effet à +24h disparait. pas de composante sensori-motrice ici, ce qui explique peut être moindre robustesse < daisy.  
- effet ns chez contrôle pour la tâche carte de france.

**Discussion =** Effet des PA droit sur R mentale de l'espace. Tâche qui nécessite de générer l'image mentale stockée en MLT et de l'explorer. NB = quid des perfs des sujets sains quand PA vers la gauche ? papier là dessus ? à voir.





### Pseudo nsu 

### Bareham 2014

**Hypothèse** = La baisse de vigilance/éveil produirait un biais spatial similaire à la NSU chez des sujets sains dans la modalité auditive.

**Méthode** = Un groupe de sujets sains, placé dans un contexte d'endormissement, devaient discriminer la direction (gauche/droite) de stimuli auditifs présentés via un casque auditif. Les réponses des sujets étaient associées à un état d'éveil ou d'endormissement, donné par une mesure en EEG.

**Résultats** = Avec l'endormissement, les sujets faisaient plus d'erreurs pour les sons venant de gauche, tandis que la précision restait stable ou s'améliorait pour les sons venant de droite.

**Conclusion** =  La baisse de vigilance module la perception des sons dans l'espace, avec une perception des sons déviée vers la droite de façon semblable à la négligence auditive observée chez certains patients NSU.  Ces résultats peuvent s'expliquer par une compétition entre les 2 hémisphères. Etant donné que le circuits dédié à l'attention exogène est latéralisé dans l'hémisphère droit chez les droitiers, une baisse de vigilance induite par l'endormissement diminuerait l'activité de l'hémisphère droit au profit de l'hémisphère gauche, qui pourrait alors traiter les sons de l'hémi-espace droit avec plus d'acuité.

** **

** **

** **

** **

 

 

### Dorrian 2015

 

**Hypothèse** = La baisse de vigilance/éveil produirait un biais spatial similaire à la NSU chez des sujets sains sur le plan visuo spatial, dans une tâche de repère spatial (landmark task).

**Méthode =** Plus de 800 sujets (dont 250 exclus) ont rempli à distance (via une plateforme internet) un questionnaire qui catégorisait leur chronotype biologique (chronotype du matin vs du soir). On observait également l'horaire de passation du test dans la journée, qui pouvait être favorable ou défavorable en terme de ressources attentionnelles, sur la base du chronotype du sujet. Les sujets réalisaient ensuite une tâche de repère spatial.

**Résultat =** En contrôlant la variable "âge", l'analyse a révélé que les sujets présentant un chronotype matinal étaient davantage biaisés vers la droite dans la tâche de repère quand ils réalisaient la tâche à un horaire défavorable sur le plan attentionnel.

**Conclusion et limite =** Le rythme biologique module les ressources attentionnelles, qui en retour modulent notre perception de l'espace dans une tâche de repère.

Néanmoins il y a ici de nombreuses variables non contrôlées concernant les conditions de passation du test (environnement très variable selon les sujets). Cela pourrait expliquer pourquoi malgré le très grand nombre de sujets ces auteurs ne trouvent pas d'effet significatif dans la condition "chronotype tardif".

 

### Zago 2017

 

Hypothèse : La BL permet de tester la latéralité des tt visuo-spatiaux.

Méthode : 51 sujets droitiers sains ont été soumis à un protocole IRMf. Ils étaient soumis à une tâche de BL perceptive (chacune s'affichant 1500ms) et une tâche contrôle visuo-motrice (saccade oculaire vers une cible, s'affichant aussi 1500ms) afin de contrôler les activations oculomotrices et le shift attentionnel. Les participants réalisaient aussi une tâche de barrages de cible papier-crayon. Enfin, on mesurait aussi leurs capacités visuo-spatiales à l'aide de plusieurs tests (rotation mentale, bloc de Corsi, maze3D, matrices de Raven).

VD :

1.  Score d'erreur BL pcp et direction du biais (droite/gauche)
2.  Score de barrage de cible
3.  Score de capacités visuo-spatiales
4.  Index de latéralisation hémisphérique calculé pour chaque PP à partir de la tâche de BL et et la tâche contrôle. On effectuait une corrélation entre l'index de latéralité obtenu et les erreurs dans la BL, le barrage de cible, et le score de capacité visuo-spatiale.

VI : tâche utilisée dans l'IRMf.

Résultats :

1.  Et b) On retrouve une pseudo-négligence chez les sujets droitiers dans les tâches de BL et de barrage
2.  **L'activation latéralisée due à la tâche de BL était corrélée au biais observé dans la BL. En d'autres termes, plus l'hémisphère droit était activé, plus on retrouvait un biais vers la gauche dans la BL. Cette activation latéralisée n'était pas corrélée aux erreurs dans la tâche de barrage de cible, ni aux capacités visuo-spatiales. Il y a bien une dissociation neuro-fonctionnelle entre la BL et le barrage de cible.**
3.  Une fois les activations oculomotrices contrôlées, on retrouvait dans la BL des activations au niveau de l'IPS/IOC/MOG/V1/TOJ/IFG/SFG/insula. Donc des régions plutôt ventrales postérieures dans l'ensemble, dont certaines impliquées dans le cadre de référence allocentré, les traitements visuels, et l'attention focale (insula).
4.  Enfin, à noter que la corrélation LB/activation HD était assez petite malgré 51 PP. Les auteurs suggèrent que l'asymétrie VS serait assez subtile à détecter.

 

Critique = les auteurs concluent que la BL évalue mieux que la tâche de barrage la latéralisation des tt VS. Cependant l'index de latéralité était calculé à partir de la tâche de BL vs contrôle (tâche oculomotrice), et non pas la BL vs barrage de cible. Les auteurs partent du principe que les aires activées lors de la tâche contrôle seraient les mêmes que dans la tâche barrage.

 

 

### Li 2017

 

**Hypothèse** : Chez les patients NSU on constate 2 types de symptomes : un déficit de l'attention spatiale et des déficits attententionnels non spatiaux. Par exemple les USN mettent plus de temps à détecter une cible même si elle est présentée dans l'espcae ispilésionnel. De même, ces patients montrent un déficit dans des tâches de clignement attentionnel, ce que les auteurs appellent le "déficit temporel". L'hypothèse des auteurs serait que le déficit temporel serait modulé par le déficit spatial.

Les performances dans la tâche de clignement attentionnel devrait être modulée par la position de la référence égocentrée chez des patients RBD avec NSU vis-à-vis des stimuli.

**Méthode** : 4 groupes (N+, N-, RBD sans NSU, n=15 ; groupe sain n=30) devait rapporter verbalement si ils avaient perçu une cible T1 puis une cible T2, dont l'intervalle de présentation variait en fonction du SOA. On présentait les stimuli à +/- 40 degrés de la référence égocentrée (et non pas en référence à la tête).

Résultats : La cible T1 était perçue avec la même acuité par tous les groupes. En revanche, le groupe N+ identifiait moins souvent la cible T2 quand elle était présentée dans l'espace gauche. On retrouvait une moins bonne acuité dans le groupe N+ dans l'espace droit comparé aux autres groupes hormis le groupe N-. Des analyses individuelles des temps de latence a montré que les patients NSU mettaient plus de temps à identifier la cible T2.

Interprétation : Karnath défend l'idée que la référence égocentrée serait une représentation supramodale qui nous sous tendrait la localisation des objets par rapport à notre corps (ancre spatiale). La déviation de la RE chez les patients modulerait le déficit de traitement temporel (attention sélective en quelque sorte), plus important dans l'espace contralésionnel. Selon es auteurs ce résultat plaide pour une vision unitaire la NSU, dont le déficit spatial serait le cœur.

Néanmoins cette conclusion est assez spéculative, ils n'ont pas étendu leur analyses à d'autres types de déficit non spatiaux (MDT VS, attention soutenue).

### McCourt and Jewell 1998

 

**Problématique** : quels facteurs modulent la pseudo-négligence ? La Pn et la vraie NSU partagent-elles des mécanismes cognitifs et des substrats anatomiques similaires ?

 La pn est un phénomène assez robuste (méta-analyse de 70 études à l’appui), avec une taille d’effet entre 0.40 et 1.30. Celle-ci reflète une asymétrie dans le déploiement de l’attention spatiale, peut être due à une asymétrie inter-hémisphérique, avec un rôle prépondérant de l’H droit.

**Stimuli utilisés :**

Les lignes étaient constituées d’un contrast de « Michelson » (100% contraste), ie une alternance de bandes noires et blanches, coupées à un endroit de la ligne par une inversion du contraste dans le 2^ème^ segment. On les présentait sur un fond gris. Elles mesuraient 17.75 cm, à 45 cm du sujet, avec une hauteur de 0.39 degrés et une largeur de 22.3 degrés. La segmentation s’effectuait dans une fenêtre allant jusqu’à 0.88 degrés à droite ou à gauche par rapport au milieu de la ligne. La présentation était tachitoscopique (« flash ») : 150 ms de présentation seulement pour éviter une exploration saccadique. La rémanence rétinienne (mémoire iconique) suffit à répondre. Les sujets appuyaient sur un bouton avec soit leur main gauche/droite de façon contre-balancée

**Analyse** : pse moyen et écart type.

**Méthode et résultats** : les expérimentateurs ont réalisés plusieurs expériences pour tester différents facteurs modulateurs de la PN :

-   **Position sur l’axe horizontal**: une présentation très à gauche augmente la Pnsu, l’inverse à droite, de façon linéaire. Interprétation : augmentation de l’implication de l’H droit. *Quand on fait pareil chez NSU, ça ne fonctionne pas*, peut être simplement pour des raisons quantitatives : *une simple présentation visuelle ne réactive pas assez l’HD, contrairement à des stimulations ppc ou vestib par ex.*
-   **Position sur l’axe vertical** et **largeur verticale de la ligne**: On a diminution de la pn, comme pour la nsu, quand on se rapproche d’un pattern plus unitaire (comme un cercle ou un carré). La pnsu est plus grande quand on a la ligne en haut à gauche, et non pas en bas à gauche comme dans la NSU. Interprétation : le upper field traiterait des infos de grande résolution, avec la voie parvocellulaire. On a plus la voie magno pour le down field. Or le lobe pariétal et endommagé chez NSU, donc on a logiquement des problèmes pour champ visuel down, avec un HD moins impliqué dans cet espace là. Or quand je suis en champ visuel upper, j’ai moins de problème, car à la base j’ai un biais à gauche comme le révèle cette expé ! donc quand upper + NSU : je suis MOINS biaisé à gauche ; quand down + NSU : je suis BIAISé à droite !
-   Longueur de ligne : plus elle grande, plus il y a un biais. Pas d’interprétation via modèle ici qui puisse expliquer déviation NSU à droite et pNSU à gauche en même temps.
-   Stimulus contraste et masque : On teste si effet d’un masque empêche un possible phénomène de scanning attentionnel mental similaire à celui des saccades occulaires. Avec un masque de haut contraste, on devrait interrompre ce processus en dégradant la mémoire iconique du stimulus. Avec ou sans masque on a les mêmes perfs. Pour le contraste, on manipule le contraste de faible à fort de gauche à droite ou droite à gauche chez des lignes segmentées. On observe bien un effet du contrastes sur le jugement de segmentation : le côté avec le contraste le moins fort tend à être perçu plus long. Interprétation : si contraste faible, on a un plus grand recrutement de la voie dorsale OC-PAR (avec FB PAR sur les aires visuelles), un plus grand recrutement de l’attention, et donc une perception du segment plus grande. Ce contrôle TD pourrait être peu performant chez les NSU (PEvoqué plus lent quand stimlulus visuel à gauche de 25-40 ms, problème dans tâche blink attt).

 

### Chokron & Imbert 1995

 

Contexte théorique : La NSU est vue à ce moment-là comme la conséquence d’un shift de la référence égocentrée, une sorte d’ancre spatiale (représentation multi-modale) ancrée dans le tronc. La NSU ne serait donc pas rétinotopique, mais égocentrique. De plus on sait que ré-orienter le tronc vers la gauche diminue la négligence

Hypothèse : Ici on teste un sujet NSU pour voir si il est sensible à déviation du tronc, et des sujets sains pour voir si cette même déviation influence la SA : autrement dit, est ce que un shift de la RE résulte dans un shift du SA ?

**Méthode** :

-   la tête reste droite mais le tronc : +/- 15 degrés
-   4 positions (15,30 +/-)
-   Essais avec les 2 mains

**Résultats** :

-   la SA shift en même temps que la ER chez sujets sains
-   idem avec l’eccentricité
-   Chez sujet NSU même tendance. ex : Quand main droite + 15° : grosse déviation à droite, moins grande quand main gauche.
-   Le sujet NSU déviait spontanément vers la droite.

**Discussion** :

-   L’endroit d’où on commence le pointage joue un rôle, plus que la main.
-   Diminution de la NSU quand pointage gauche attribuable peut être à ré-activation de l’hémisphère droit ?       
-   Mais peut être aussi à indiçage spatio-moteur !
-   Mais c’est peut être aussi un effet de scanning ! quand scanning left to right = pseudo-NSU !
-   Le scanning (via position départ) + main used seraient 2 facteurs additifs !

**Conclusion** : résultat en faveur de ER = ancre spatiale dans tâche égocentrée

 

### Chokron & Bartolomeo 1997

 

**Contexte** : Théorie de Karnath, ici on essaie de démontrer que la déviation de l’ER n’est pas une condition obligatoire de la NSU, pas la cause, mais plutôt la conséquence.

**Méthode** : on a testé des sujets avec lésion PAR ++> avec ou sans NSU dans des tâches pour détecter NSU qu’on va corréler avec des perfs de SA (manip avec 4 positions).

**Résultats** :

-   On retrouve effets très classique (gradation selon position avec main droite) chez sujet sain, légère déviation vers la droite.
-   On retrouve tous les cas de figure chez P+/- et NSU+/- en termes de déviation patho du droit devant. On a même des sujets NSU qui sont déviés à gauche.

**Conclusion** : Résultats dans le sens de déviation ER = conséquence de la NSU ou d’une lésion PAR, et non à la racine de la NSU.

 

### VanVleet and DeGutis 2013

 

La NSU peut être divisée en 2 symptômes :

-   Le déficit spatial, qui correspond à un déficit des cadres de références allocentrés et égocentrés. Pour expliquer ce déficit on a théories anat : de Mesulam (arg : HD sensible à hausse matériel visuel dans les 2 H dans une tâche de MCT visuelle) et de Kinsbourne. Concernant les modèles on retrouve a) une hyperattention vers la droite, avec des stimulus hyper-saillants ce qui explique les refixations et le manque de désengagement b) déficit binding cct complexes dans l'hm gauche c) problème de désengagement (Posner), de foa trop petit (Robertson).
-   Le déficit non latéralisé spatiallement : Déficit quasi universel ! on le retrouve dans l'attention soutenue (**Malhotra, 2005; Robertson, 95,98**), sélective (**Husain, 1997** dans tâche blink attt) (on voit bien qu'on est pas dans la dichotomie endogène/exogène), la mdt spatiale(**Malhotra, 2005**). De plus, ce déficit prédit mieux NSU que le déficit spatial latéralisé (**Peers, 2006**). Sur le plan théorique on retrouve le fait que l'HD contient le VAN (travaux balance interH, Berham, pseudoneglect…), qui va lui-même modulé le DAN (peut être via IFJ, voir He 2007, C ET S).
-   Quels traitements ? a) Pour le déficit spatial latéralisé on a PA et visual scanning par exemple. Mais la difficulté avec traitements Tdwn c'est qu'on a patients souvent anosognosique. B) pour balance interH on a TMS (effet sur extinction par ex, voir Koch). C) Pour le déficit nonSp on a les médicaments norad, ou des traitements sur ordi qui font travailler les patients de façon ludique sur l'attention tonique et phasique, avec répercussion positive sur déficit Sp comme par ex recherche de cible avec cct conjointe (programme TAPAT). D) a l'avenir il faudra explorer les jeux d'action (travail sur attt exog), les combinaisons de traitement

 

 

### Chokron 2007

**Résumé** = Chokron (2007) passe en revue plusieurs théories, et leur adéquation avec les symptômes de la NSU et les effets positifs des techniques de réhabilitation (PA, OKS, TENS,CV…). Les auteurs font l'hypothèse d'une déconnexion sensorimotrice dans l'hémi-espace gauche comme étant à l'origine de la NSU. Par exemple le spatial remapping, qui repose sur l'intégration d'information visuelle et motrice, s'avère déficitaire dans l'hémi-espace gauche chez les patients NSU (Pisella et Mattingley, 2004). Les méthodes de réhabilitation vestibulaires ou proprioceptives pourraient alors temporairement réactiver les aires lésées responsables de l'intégration multisensorielle (PPC) et permettre la recalibration sensorimotrice dans l'hémi-espace gauche. Des lésions au niveau des faisceaux de matière blanche pourraient être responsable de cette déconnexion entre l'action et la perception.

### Robertson 1998

 

Hypothèse : L'attention peut être subdivisée en au moins 2 types : l'attention **spatiale sélective (fr-par)**, qui améliore pcp d'un objet précis dans l'espace, et l'alertness' **nonspatiale** **(état de vigilance générale)**. L'altertness non spatiale peut à son tour être subdivisée : l'attention **tonique** (attention soutenue, endogène) reposerait sur des circuits FR(Acc-DMPF)-PPC dans l'HD, endommagés chez les patients USN. En revanche l'attention **phasique**, qui permettait de s'orienter automatiquement vers un stimulus saillant, serait préservée car elle reposerait sur un circuit ascendant thalamo-mésencéphalique. Les auteurs proposent que stimuler l'attention phasique avec un son bref -avant une tâche de discrimination spatiale- pourrait stimuler l'attention spatiale et ainsi diminuer le biais ipsilésionel.

**Méthode** : On montrait à 8 sujets RBD (avec négligence et/ou extinction visuelle) une barre sur un écran, latéralisé à droite ou à gauche, suivie après un temps de latence variable (SOA) par l'apparition d'une deuxième barre à l'endroit opposé de la première. La tâche consistait à dire quelle était l'emplacement de la 1^ère^ barre apparue. Un seuil psychophysique était calculé (méthode PEST) pour calculer à partir de quel SOA les sujets jugeaient que les barres de droite et de gauche apparaissaient en même temps dans pouvoir discriminer laquelle précédait l'autre. Dans 25%, les essais étaient précédés d'un son de 300ms, et dans 75% d'un blanc. Dans une seconde variante de l'expérience on plaçait l'enceinte sonore à droite des sujets pour vérifier si la localisation spatiale du son jouait un rôle. Enfin, un groupe contrôle de patients LDB, avec un même pattern de lésion (AVC dans l'artère cérébrale moyenne) réalisait aussi l'expérience initiale.

**Résultat** : Dans la condition aucun son, les sujets USN montraient un SOA de +497ms, ce qui signifie qu'en dessous de ce temps de latence les sujets percevaient la barre de droite apparaissant en 1^ère^ alors que c'était celle de gauche. En d'autres termes, il fallait attendre minimum une demi seconde pour que les sujets jugent que la barre de gauche précédait la barre de droite. Dans la condition avec son ce pattern s'inversait : avant 7ms, les sujets jugeaient la barre de gauche comme étant la 1^ère^ affichée alors qu'il s'agissait de la barre de droite (N=8). On retrouvait les mêmes résultats chez 2 patients re-testés  plus tard avec un déplacement du son vers la droite. Enfin, les patients LDB ne montraient de performances proches de celles de sujets sains dans les 2 conditions (moyenne SOA = + 21ms).

**Interprétation** : l'attention phasique a accéléré la vitesse de traitement de l'espace visuel, en annulant totalement le biais initial ipsilésionel. La méthode PEST utilisée ici permet de certifier que la préparation d'une réponse motrice n'était  pas impliquée dans cet effet. De plus, cet effet semble purement attentionnel, et non pas sensoriel, puisque on a utilisé la modalité auditive et non pas la modalité visuelle pour stimuler l'attention phasique. Cette facilité de traitement visuelle ne peut pas être attribuée à un effet sensoriel auditif.



### Wolpert, Ghahramani, & Jordan (1995)
***An internal model for sensorimotor integration***
- Modèle interne : système qui imite le comportement d'un processus naturel
- 2 types de mi = 
- **forward** (prédiction des conséquences sensorielles de l'action, commande motrices)
- **inverse** (calcul des commandes motrices nécessaires pour générer tel état sensoriel)
- forward model très utile pour mouvement balistique, rapide (no feedback), motor learning (si mise à jour), correction (*ré-afférence*) de la commande motrice en cours de route grâce à motor outflow signal (efference copy), imagerie motrice...
- comment estime rposition et vélocité du bras dans l'espace ? 3 options = signal proprioceptif, moteur, les deux.
- force field paradigm permet justement de tester implication signal moteur dans estimation position de l'effecteur dans pointages, grâce à after effect.

![](https://i.imgur.com/yB49VCi.png)
- Filtre de kalman = algo récursif qui se sert du feedback pour corriger l'erreur. kalman attribue alors *weights* +/- fort à composante forward ou sensory correction (delta prediction - actual feedback). 
- prédiction du modèle = si je perds afférence, j'augmente variance du contrôle moteur.
- patiente PJ illustre cette prédiction = lésion SPL gauche, perte de sensation tactile de la main, bras, proprioception... performance bonne pendant 10/20 s dans tâche de position bras, grip ... puis dégradation rapide. *proposition : perte d'afférence entraîne accumulation de bruit dans le système SM, car pas de stockage sensory feedback. dégradation de la perception (pas de la sensation, qui est intacte !)*.
- SPL utile pour modèle interne SM. NB = ici ça n'est pas encodage du percept qui est déficiente, car mvt balistique, rapide, ok chez PJ. C'est le stockage du feedback ppc qui pêche, et empêche updating adéquat du modèle interne.

 
### Schintu 2017

**Hypothèse** = Un effet post test de la RPA sur le traitement visuo-spatial serait observable uniquement sur le long terme après la PA.

**Méthode** = Des sujets sains étaient répartis dans un groupe LPA ou RPA, puis à nouveau répartis en 2 sous-groupes en fonction de la latéralisation de leur biais initial dans une tâche de repère (landmark task). Afin d'évaluer sur le plan sensorimoteur l'effet de la PA, on mesurait en post test leurs performances dans des tâches de pointage aveugle et de pointage droit-devant (pointer vers un axe imaginaire délimitant les côtés gauche et droit du corps). On évaluait également leurs performances post tests sur le plan visuo-spatial, en demandant aux sujets de juger visuellement si un point présenté sur un écran était à droite ou à gauche de la ligne médiane du corps (visual shift task), puis de juger dans une 2^ème^ tâche quel côté d'une ligne coupée en deux était le plus long (landmark task).  Les mesures post test étaient effectuées graduellement dans le temps à 6 reprises, avec une échelle allant de 2min jusqu’à 8heures.

**Résultat** = On observait un effet post test jusqu'à plus 8 heures après la PA dans le domaine sensorimoteur, quelque soit le groupe. On n'obtenait en revanche aucun effet post test sur le plan visuo-spatial dans le groupe RPA, ainsi que dans le sous-groupe LPA  qui présentait initialement une pseudo-négligence (contrairement au sous-groupe LPA sans pseudo négligence).

**Conclusion** = Seul le sous-groupe LPA avec un biais initial vers la droite dans la tâche de repère présentait des changements perceptifs post tests dans des tâches de jugement visuel et de repère. L'absence d'effets post tests dans l'autre sous-groupe LPA démontre l'influence des biais visuo-spatiaux préexistants sur les processus mis en jeu dans l'adaptation prismatique.

** **

### O'shea 2017

 

**Hypothèse **: L’effet des prismes, quand une seule session, dure à peu près 1 journée. Autre limite, ça marche pas avec tous les patients. Ici hypothèse que induire de  la plasticité au niveau de M1 gauche va consolider l’apg des prismes, et donc la durée de l’AE

**Méthode** :

MESURES : On utilise la tdcs pour stimuler pendant 20min des sujets sains et 3 sujets patho chronique, sur 3 zones : M1, PPC, CB + sham. Chez sujet sain on faisait aussi une spectroscopie à résonance magnétique pour quantifier la quantité de GABA et GLUT dans M1, comparé à zone OCC. Une baisse de GLUT = meilleure consolidation puisque le GABA est inhibiteur. Chez USN, on regardait AE chez le 1^er^ patient dans washout et rétention, à 4 PA sessions, dont une pas sham. Chez les autres, on regardait AE dans tâches USN uniquement.

TACHES : adaptation PA classique, où on regarde l’after effect pendant période de washout et de rétention dans une tâche de pointage DD sans ou avec retour visuel.

Pour les USN, on faisait une dizaine de tâche papier-crayon classique (limites : pas de tâche dénuée d’une composante visuo-motrice…) : barrage ++, lecture, et bissection de lignes.

On regardait aussi tâche de priming moteur

**Résultats** :

- Chez sujets sains, effet tdcs vs sham plus fort sur AE dans whashout et rétention, mais que dans tâche DD purement proprioceptive. Corrélation de cette plus grande durée avec diminution du GABA. Ca veut dire que plasticité neuronale associée à meilleure consolidation.

- Chez USN, sujet 1 a même résultat que sujets sains, mais AE observés même après sham, même si effet tdcs plus fort*. Chez les 3 USN on a AE dans tâche USN. Amélioration dans tâche priming moteur : moins d’hypokinésie directionnelle.

**Discussion **: montre que stimuler l’HG, contrairement aux théories Kinsbourne, peut être bénéfique. Ici à mon avis ça montre que PA crée plus un biais qu’elle n’en enlève. Les AE chez USN pouvaient durer jusqu’à 50 jours. Egalement peut etre que tdcs crée plus d’erreur d’adaptation, et donc une adaptation qui nécessite une plus grande MAJ du modèle interne, et donc plus forte consolidation. En gros, on a stimulé un circuit sensorimoteur intact en créant un biais.

 

### Bultitude 2013

** **

**Hypothèse** = Le spatial remapping (SR) désigne la capacité à intégrer des informations oculomotrices et visuelles dans le temps afin de paramétrer les saccades visuelles de façon optimales, et ainsi percevoir notre environnement de façon fluide et dynamique. Le SR serait altéré chez des sujets sains après avoir été exposé à la LPA.

**Méthode** = Des sujets sains étaient répartis dans 2 groupes, RPA et LPA. Avant la PA, tous les sujets passaient par une phase de fausse PA (lunettes sans distorsion visuelle) suivie d'une mesure du SR dans un paradigme de double saccade (paradigme double step saccade : DSS). Dans ce paradigme les sujets doivent effectuer deux saccades successives, à chaque fois vers une cible qui disparaît au moment où le sujet initie la saccade. On reproduisait ensuite la même procédure avec la RPA et LPA, puis on comparait les trajectoires moyennes des saccades post tests (saccade 1 et saccade 2) aux mesures pré-tests.

**Résulta**t = On retrouvait un décalage du point d'arrivée de la saccade 2 en post test dans les deux groupes LPA et RPA. Cependant pour le groupe RPA ce décalage covariait avec le décalage de la saccade 1, qui ne reflète pas le SR mais un effet de décalage oculomoteur induit par les prismes. Seules les saccades 2 réalisées dans  l'hémi-espace gauche après LPA étaient indépendantes du décalage oculomoteur. 

**Conclusion** = Seule la LPA est en mesure d'altérer le SR, et ce dans l'hémi-espace gauche, ce qui peut être interprété par la latéralisation des traitements visuo-spatiaux dans l'hémisphère droit. La LPA perturberait l'hémisphère droit, et par là même le spatial remapping.

** **

### Rossetti 1998

 

**Hypothèsèses** = les patients NSU s'adapteraient aux lunettes prismatiques (expé 1). Cette adaptation sensorimotrice pourrait influencer des tâches visuo-spatiales utilisées pour diagnostiquer la NSU (expé 2).

**Méthode** = On comparait des sujets sains à des sujets NSU dans une tâche de pointage droit devant avant et après RPA (expé 1). Dans la 2^ème^ expérience on comparait deux groupes de patients NSU avant et après PA dans plusieurs batteries de tests (barrage decible,  bissection de ligne motrice, copie de dessin, dessin de mémoire, lecture). Le groupe contrôle était soumis à une PA factice (prismes neutres). Le groupe NSU expérimental était soumis à la RPA.

**Résultat** = Après adaptation prismatique, les patients NSU et les sujets sains pointaient davantage vers la gauche (expé 1). Par la suite, les signes de négligence diminuaient  chez le groupe NSU avec RPA comparé au groupe NSU contrôle, jusqu'à plus de deux heures après la PA.

**Conclusion** = La PA induit une adaptation sensorimotrice qui améliore les signes de négligences chez les patients. Un nouvel apprentissage sensorimoteur pourrait donc réorganiser la cognition spatiale et affecter en retour des représentations spatiales de plus haut niveau, impliquées par exemple dans la bissection de ligne et le barrage de cible.

 

 

### Jacquin-Courtois 2013

L'adaptation prismatique est aujourd'hui la technique de réhabilitation bottom up la plus efficace pour diminuer les symptômes de négligence. La PA entraîne une adaptation sur le plan visuo-moteur et proprioceptif, qui module la cognition spatiale, et en retour les processus cognitifs de haut niveau. 

La PA entraîne un after effect (AE) sur la référence égocentrée, mesurée par la SA task (pouvant être visuelle, proprioceptive et motrice/manuelle, proprioceptive (mouvement passif du bras), visuo-proprioceptive (OL pointing)).

Les AE entraînés par PA ne se limitent pas à la composante sensorimotrice, mais peuvent s'étendre (expansion) sur des processus cognitifs de haut niveau. La PA n'améliore pas seulement les performances des patients RBD sur le plan moteur, mais également sur des tâches de jugement visuel, sur l'exploration oculomotrice, l'imagerie mentale, ou encore la navigation (fauteuil roulant). De même ces effets ne se limitent pas non plus à la composante visuelle, avec des effets dans les modalités haptiques (discriminer 2 formes différentes à gauche et à droite), et auditives (écoute dichotique gauche), tactile (baisse de l'extinction tactile quand stimulation bilatérale). Enfin, les effets de la PA ne sont pas seulement spécifiques au déficit VS observé chez les patients USN, et améliorent la composante Vconstructive, la dysgraphie, les troubles de la posture, et aussi des troubles du schéma corporel (CRPS). La PA a donc des effets sur le plan supramodal, au-delà de la composante SM. Enfin, notons que la PA a des effets positifs sur des troubles spatiaux non latéralisés, telles que la détection de cibles, le désengagement, et le traitement local de stimulus.

Les mécanismes impliqués ne sont pas encore totalement élucidés, car cela nécessiterait d'avoir une compréhension des liens entre des processus VM et des processus supramodaux.

Nous avons 2 hypothèses pour expliquer **l'hyper-adaptabilité** chez les patients USN : soit ils utiliseraient un réseau de façon plus intense qui entraînerait une récupération induise ou spontanée, soit ce serait du fait d'être inconscient vis à vis de l'adaptation SM. En effet, les sujets sains utiliseraient une composante stratégique, grâce au PAR, c’est-à-dire une réponse motrice leur permettant de corriger rapidement l'erreur détectée. Les patients RBD n'utiliseraient pas cette composante, mais plutôt une composante de réalignement (true adaptation), observable sur le long terme après une longue exposition aux prismes.

La PA serait efficace car elle serait active, ce qui stabiliserait à LT ces effets, contrairement aux autres techniques BUP qui sont passives et ne durent que le temps de la stimulation.

Sur le plan anatomique, il y a une nette implication des es aires IPS, PAR, du cervelet. Après PA, le réseau DAN est plus activé dans les 2 H, accompagnée de meilleure  perf dans des tâches comme la bissection de ligne et l'exploration. Une hypothèse serait que la PA activerait le lobe OC gauche, puis le cervelet droit, qui à son tour inhiberait le réseau DAN gauche. C'est cohérent avec le fait qu'une inhibition du cervelet (tdcs) entraîne de meilleures perf cognitives.

Enfin, à noter que la PA répétée entraîne des AE sur le plus LT. De futures recherches sont nécessaires pour optimiser l'effet des PA (combinaison avec une autre technique BUP, TMS, médocs norad pour améliorer les symptômes non latéralisés).

 

 

### Motor adaptation paradigm

### Michel 2018

**Hypothèse** = La PA génère des effets post test sur la plan visuo-spatial après adaptation sensorimotrice. On devrait donc pouvoir reproduire cet effet post test dans une tâche de bissection de ligne dans un paradigme de champ de force, qui lui aussi comprend une phase d'apprentissage sensorimoteur. Etant donné qu'on observe un effet post test sur la bissection de ligne chez les sujets sains seulement après la LPA, on s'attend à ce que seul un champ de force vers la gauche reproduise le même résultat.

**Méthode** = Des sujets sains devaient pointer avec un bras robot vers plusieurs cibles fixes. Le robot appliquait des forces constantes vers la droite (groupe 1) ou la gauche (groupe 2). La bissection de ligne perceptive était mesurée avant et après la tâche. On mesurait l'effet post test à 2 moments (phase 1 et phase 2) afin de quantifier la désadaptation dans le temps et son influence sur la BL. 

**Résultat** = On observait aucun effet post test sur la BL après exposition aux champs de force, quelque soit le groupe. L'absence de corrélation entre la désadaptation et la BL, dans les phases 1 et 2 post test, montre que l'absence d'effet post test n'est pas due à la désadaptation motrice.

**Conclusion** = L'adaptation sensorimotrice provoquée par les champs de force n'affectait pas la bissection de ligne perceptive, contrairement à la PA. La différence entre les champs de force et la PA pourrait résider dans deux processus d'adaptation différente. Le champ de force ne donnerait lieu qu'à une adaptation rapide, consciente, qui corrige efficacement l'erreur à court terme. La PA comporterait en plus une phase de réalignement inter-sensorielle, plus lente et implicite, générant des effets plus généraux sur la cognition spatiale.

** **

### Ostry 2010

** **

**Hypothèse** = Les apprentissages moteurs par champs de forces moduleraient la proprioception, et donc la perception de l'espace personnel.

**Méthode expé 1** = Deux groupes de sujets sains suivaient une procédure d'adaptation avec champs de force (courbées vers la droite ou la gauche) dans une tâche de pointage. Après cette phase les sujets ré-effectuait le même pointage cette fois soumis à des forces linéaires vers la droite ou la gauche. En fin de mouvement les sujets devaient dire vers quel côté leur bras avait été déplacé. Cette mesure binaire était répétée plusieurs fois afin de calculer la frontière perceptive entre la gauche et la droite (PES). Une mesure du PES avant et après l'adaptation motrice était calculée. Un groupe contrôle suivait la même procédure mais sans adaptation motrice (pointage "passif" courbé par la perturbation).

**Méthode expé 2** = Les sujets devaient exécuter des mouvements latéraux avec des champs de force dirigés vers ou à l'opposé de leur corps sur le plan sagittal. Leur main droite était alors placée à plusieurs endroits le long de la ligne sagittale médiane (sur le plan horizontal) et les sujets devaient dire si elle se trouvait plus proche ou plus loin de leur corps par comparaison avec leur main gauche (positionnée de façon fixe). Une mesure du PES avant et après l'adaptation motrice était calculée.

**Résultats expés 1 et 2** = Après adaptation à un champ de force latéral, on observait une modification du ressenti du déplacement du bras (PES expé1) ou de la position du bras (PES expé 2), dans la direction opposée au champ de force. Cet effet n'affectait pas la précision du PES (JND). La cinématique du mouvement n'était pas responsable de cet effet puisqu'on n'observait aucune altération proprioceptive chez le groupe contrôle passif (expé 1).

**Conclusion** = L'adaptation motrice s'accompagnait à la fois de changements moteurs et sensoriels. **Critique de Reuter 2018 : cet effet du champ de force la position perçue de la main était cependant très minime (2 mm).**

** **

### Brown 2007

** **

**Hypothèse** = Un apprentissage moteur via champ de force pourrait influencer la perception d'un stimulus visuel en mouvement, dans l'espace péri-personnel. 

**Méthode expé 1** = La tâche consistait à intercepter, via un mouvement d'atteinte, une cible visuelle se déplaçant de gauche à droite. Un bras robot appliquait des forces constantes vers la droite, la gauche, ou aucune force, chez 3 groupes respectivement. Tous les sujets (sauf le groupe "forces nulles") avaient au préalable suivis une phase d'adaptation aux champs de forces dans une tâche de pointage. Ces mêmes forces étaient appliquées dans le mouvement d'interception.

**Méthode expé 2** = Après la même phase d'adaptation,  3 nouveaux groupes  exécutaient la même tâche mais interceptaient la cible en appuyant sur un bouton avec la main gauche pendant que le robot appliquait des forces latéralisées sur la main droite. Cette 2^ème^ expé servait à vérifier si les connaissances motrices acquises sur l'environnement physique étaient suffisantes pour influencer la perception visuelle, indépendamment du mode de réponse.

**Méthode expés 3 et 4** = Afin de tester si la seule présence d'information proprioceptive et visuelle était suffisante pour influencer la perception visuelle (hypothèse de l'intégration multisensorielle), 3 nouveaux groupes étaient soumis à la 2^ème^ expé mais **sans phase d'adaptation préalable**. La direction du champ de force alternait toutes les 5 (expé 3) ou 10 essais (expé 4) pour empêcher l'adaptation vers une direction spécifique.

**Résultat** = Le champ de force vers la droite facilitait l'interception de la cible (moins d'erreur et TR plus court), avec des caractéristiques cinématiques du mouvement similaires entre les groupes (expé 1). L'expé 2 montrait aussi un TR à l'avantage du groupe" force droite", mais pas d'effet de la latéralité sur la précision (nombre d'erreurs). Les expés 3 et 4 confirmaient bien que l'apprentissage moteur était nécessaire pour observer ces effets (aucune différence entre les groupes).

**Conclusion et limite** = Le système moteur pourrait se comporter comme un système sensoriel en communiquant aux aires perceptives (PPC, MT) des informations physiques (la gravité ici) sur l'environnement. Cela dit, des connaissances explicites sur la direction du champ de force pourraient aussi expliquer en partie ces effets. De plus, aucune autre mesure n'a été effectuée pour évaluer l'effet de cet apprentissage moteur sur le plan visuo-spatial (bissection de ligne par exemple), il est donc difficile de généraliser cet effet à la cognition spatiale.

### Vahdat, Darainy, Milner, & Ostry 2011

 

**Hypothèse/contexte :** un apprentissage moteur peut entraîner des changements sur le plan perceptif, congruent avec le mouvement appris. Eg : apg prononciation change la perception du son. Sur le plan neuronal, difficile de dire avec IRMf si les changements d’activations observées sont dus à une meilleure maîtrise de la tâche (différence pré/posttest) ou des changements perceptifs indépendants de la façon de faire la tâche.

**Méthode** : pour résoudre ce problème d’interprétation, utilisation de la connectivité fonctionnelle. Cette technique permet d’identifier les changements de connectivité liés de façon séparée soit à l’apprentissage moteur, soit à l’apprentissage perceptif qui suit l’apprentissage moteur.

Ici on choisit des zones d’intérêts appartenant au réseau de l’apprentissage sensorimoteur : s1/s2/PPC, pmd/pmv/sma/m1/cb/bg. Détails : les « seeds » étaient identifier avec une tâche de mouvement cyclique du bras. Pour différencier les changements de connectivité dus à l’apg moteur ou pcp, les auteurs ont isolés la part de variance entre pre/posttest attribuable à l’apg moteur et pcp, en isolant leurs variances résiduelles respectives. On pouvait alors analyser la corrélation entre cette variance résiduelle et la variance de FC.

La tâche d’apg force field est identique à Ostry 2010. On faisait d’abord familiarisation sans champ de force suivi d’1h d’imagerie au repos 1h plus tard. Le  2^ème^ jour on fait vraiment FF + FC idem. Comme ça on pouvait observer les changements de connectivité entre avant et après FF.

**Résultats** :

-   Sur le plan comportemental, résultats classiques : changements du PES pcp.
-   La variation de FC corrélée à l’apg FF, à la fois pour les effets moteurs et pcp.
-   Eg : chagt de connectivité entre s2 et pmv/sm associé à apg pcp + chat connectivité entre cb  et spl/prefrontal. Ici on a une corrélation générale positive entre pre et post test. **On a la plus forte corrélation entre s2 et pmv et s2 et sma.**
-   Chgt de connectivité dû à apg moteur entre Cb, m1, et sma. Ici on a corrélation négative. Normal, car cb à inhibition sur les aires contra (parallèle avec théorie PA).
-   Ces variations de connectivité étaient liées particulièrement aux facteurs pi ou mi, apg pcp ou moteur, de façon congruente à chat de connectivité observés.
-   2 contrôles : résultats pas dûs à changements de connectivité du DMNF ou TPN. Cb contralateral pas important non plus.

 

**Discussion** : resting state après 1h, après ff, témoigne de changements sur le plan sensoimoteur. Les changements de connectivité pouvaient être associés à une composante motrice, pcp, ou les 2. On a l’implication d’un réseau fronto-parieto-cerebelleux. Les changements de connectivité sont corrélés à la magnitude de l’apg moteur et pcp.

**Ouverture** : travaux de Romo montrent que s2 impliqué dans apg somatosensoriel, transformation d’input sensoriel en commandes motrices, prise de décision perceptive. Implication de s2 et PPC, pas trop de s1 : processus de haut niveau. On pourrait avoir aires s2/ppc qui stockent en mémoire info somato, qui tout en recevant info motrices de pmv/sma, pourraient comparer nouvel influx somato à mémoire. Travaux TMS pourrait permettre plus d’avancées.

** **

### Ostry & Gribble 2016

 

**Résumé =** L'apprentissage moteur (AM) induit une plasticité sur le plan sensoriel. Cette relation est bidirectionnelle car un apprentissage perceptif peut également faciliter un AM. Cette relation peut s'observer sur le plan neuro-fonctionnel, avec une interconnexion forte entre Les aires  motrices et somesthésiques. Par exemple des champs récepteurs bimodaux (tactiles et visuels) sont présents dans les aires motrices. La connectivité au repos entre les aires sensorimotrices (SMA, PM, SII) est renforcée après AM (voir Vahdat, 2014). A noter que la densité dendritique de plusieurs aires corticales (MT, SMG, PPC, mais aussi le cervelet) augmente après un AM sur le long terme (jonglage, tapping, musique…).

Sur le plan comportemental des travaux montrent qu'un AM avec champs de force peut changer la représentation des frontières perceptives entre la droite et la gauche sur le plan proprioceptif (Ostry, 2010), mais aussi la façon de traiter des stimuli visuels dynamiques (Brown, 2007). Cette plasticité sensorielle variait toujours dans le même sens que l'AM.

Un entraînement perceptif peut faciliter l'AM, en augmentant l'excitabilité du cervelet et des aires motrices. On observe un changement de connectivité sur le plan sensorimoteur. Par exemple un mouvement passif du poignent pendant un mois augmente l'activité de M1 lors de ce même mouvement. Une hypothèse serait que les informations sensorielles guident l'AM lors des 1ères phases de l'apprentissage où le résultat sensoriel que l'on souhaite atteindre n'est pas encore connu du système moteur.

### Reuter 2018

 

Hypothèse : Une "pure" adaptation sensorimotrice via champs de force, sans la composante visuelle, peut modifier l'allocation de l'attention dans l'espace. Cette hypothèse a déjà été testé par Martin-Arevalo (2018) dans la PA.

Méthode : 39 sujets droitiers sains (2 groupes n=19) ont réalisé la tâche d'orientation attentionnelle de Posner avant et après adaptation à un champ de force (vers la droite ou la gauche selon le groupe). On mesurait dans la tâche de Posner le temps de réaction et les composantes P1 et N1 en EEG pour les phases indices et cibles. Les signaux P1 et N1 correspondent à un traitement visuel précoce, lui-même soumis à la réallocation de l'attention. **La P1 témoigne du désengagement de l'atention, alors que la N1 correspond à une réorientation de l'attention.** On contrôlait aussi l'activité EEG des mouvements oculaires pour exclure tout effet oculomoteur sur l'allocation de l'attention en posttest.

Résultat :

-   L'adaptation motrice n'a pas influencé le temps de détection des cibles. Les auteurs argumentent sur le fait que cette mesure soit trop impure (composante attentionnelles, cognitive et motrice) pour détecter des changements subtils de réallocation de l'attention.
-   En posttest, le signal P1 dans la phase "cible" était de plus faible amplitude et arrivait plus tard comparé au pré-test. De plus, cet effet dépendait de la latéralité de l'adaptation motrice : la P1 était plus faible quand la cible était présentée dans l'hémi-espace opposé à la direction du mouvement de pointage compensatoire.
-   On retrouvait ce même effet dans la N1, mais seulement dans la condition où le champ de force allait vers la gauche.

Interprétation : **Une simple adaptation sensorimotrice, sans composante visuo-motrice (PA), peut modifier l'allocation des ressources attentionnelles dans une tâche de détection de cible**. **Après champ de force les sujets orientaient plus efficacement leur attention vers l'hémi-espace congruent avec la direction du mouvement de compensation**.

Selon les auteurs cette réallocation ne peut pas être simplement l'effet de la perturbation en elle-même, car celle-ci était très minime (le champ de force s'est installé progressivement sur plus de 200 essais pour ne pas être perçu consciemment). De plus, l'erreur dans ce type de paradigme (champs de force et PA) est souvent dans le sens opposée à la réallocation de l'attention, donc l'erreur dans un espace donné n'indice pas la réallocation de l'attention dans ce même espace.

La réallocation de l'attention spatiale pourrait être due ici à un nouvel apprentissage sensorimoteur dans l'espace, qui aurait orienté l'attention vers ce même espace. Des arguments neurofonctionnels vont dans ce sens (eg : implication du cervelet et des lobes PAR, SPL, IPS, DAN.

Pour expliquer l'asymmétrie de l'effet de l'adaptation motrice sur la N1, vue seulement dans le groupe LFF, les auteurs renvoient à l'asymétrie des réseaux VAN, latéralisés à droite. Les sujets n'auraient pas besoin de réallouer leur attention quand il y a perturbation vers la droite, car ils activeraient leur hémisphère droit (VAN), déjà capable de réorienter l'attention dans l'hémiespace gauche.

Critique : pas de résultat sur le plan comportemental qui viendrait confirmer les effets trouvés en EEG. De plus l'absence de résultat dans la condition N1 "cible" est difficilement interprétable.

### Diedrichsen, Hashambhoy, Rane, Shadmehr, 2005

 

**Hypothèse** : Plusieurs sources d’erreurs possibles : déplacement de la target (target error), ou perturbation du mouvement (visuelle : kinématique, ppc/FF : dynamique) (execution erreur). On faut hyp que ces deux types d’erreurs, pseudo aléatoires, vont activer des aires cérébrales différentes.

**Méthode** : dispositif IRM + robot. Le sujet doit faire mvt de pointage vers une cible pour provoquer son explosion. Il voiut la cible sur un miroir en haut de l’IRM. Via ce miroir, target error ou kin error. Via robot : dynamic error. Cette erreur n’était pas vraiment aléa puisque 3 chois d’erreurs possibles à chaque essai (limites). On mesurait l’adaptation aussi pour voir si MAJ modèle interne, avec une mesure angulaire (écart) et temps de pointage. On contraôlait aussi act IRM des yeux avec une tâche contrôle de fixation.

**Résultat **:

- quand on compare target error et kin error (pas FF), on a un peu d’adaptation pour kin error. On a act différente avec target error qui active BG et PPC/IPS/SPL, et CB. Error kin activait aires plutôt antérieures du PAR : SII, PoC. Pas d’act cervelet ou GB pour kin.

- quand on compare kin et dyn error, on prédit MAJ de deux modèles internes différents. On a bien meilleure adaptation pour FF. On retrouvait en IRMf les mêmes régions activées. M1 + SII bil. Onavait aug act CB pour les deux errors.

**Discussion** : possible ici que act M1/SII-Poc pas vraiment dues au signal d’erreurs en tant que tel mais à MAJ modèle interne, car tjrs un peu d’adaptation malgré nature pseudo-random. Pas d’adaptation pour target error.

Target error associée à MAJ de la localisation de la cible pour ajuster saccades, donc act PPC logique. Peut être que ça engage des boucles inhibitrices BG (en lien avec FEF) qui s’activent pour choisir le programme moteur le plus adapté (interaction en boucle cognitive et motrice). Donc quand on a target error, on a perturbation du but de la saccade, pas de la saccade.

Exe error : cette étude montre qu’on a zone comme PoC/SPLa qui reçoit signal d’erreur, utilisé pour MAJ motrice.  

Dans les deux cas on a act CB : utile pour apg moteur + feedback sensoriel.

Bernardi,

Hypothèse/contexte = voir un motor learning peut faciliter ce même apg moteur ou interférer avec un apg moteur opposé.

Méthode : Des sujets sains regardaient une vidéo de FFL vers la gauche ou la droite + 1 groupe contrôle regardait FFL dans une direction mais « scrambled », cads séquence pointage aléatoire. Comme ça on contrôlait effet de vision latérale.

Résultats = Quand on enregistre sensed limb après vidéo, on a effet du FFL pour les deux directions. De plus, c’est la même magnitude que quand ils font FFL juste après. De plus, effet pas dû à observation de l’erreur online, car pas d’effet chez le groupe contrôle.

### Dupierrix 2008

**Hypothèse** = Des habitudes sensorimotrices sur le court terme induiraient des biais post tests dans la bissection de ligne perceptive et motrice, et donc la perception de l'espace extra-personnel et personnel. 

**Méthode** = 2 groupes devaient réaliser plusieurs pointages systématiquement dans l'hémi-espace droit ou gauche (en fonction du groupe). La BL motrice et perceptive était mesurée avant et après la tâche.

**Résultat** = Les BL motrice et perceptive étaient biaisées par rapport à la baseline vers la droite ou la gauche en accord avec la direction du pointage.     

**Conclusion et limite** = On retrouvait un effet significatif dans la BL perceptive uniquement dans des conditions très particulières (en fonction de la longueur et de l'emplacement de la ligne). Néanmoins cette étude montre que la perception spatiale dépendrait bien de nos habitudes sensori-motrices (à court et à long terme), elles même déterminées par notre culture.

** **

### Dupierrix 2009

**Hypothèse** = Des habitudes sensorimotrices sur le court terme induiraient des biais post test sur  la proprioception, et donc sur la perception de l'espace personnel.

**Méthode** = 2 groupes devaient pointer pendant 5 minutes systématiquement vers l'hémi-espace droit ou gauche (en fonction du groupe). On mesurait la performance dans une tâche de droit-devant perceptif avant et après la tâche, mais aussi à long terme avec un post test 19 à 24h après la tâche. 

**Résultat** = On observait un effet post test dans la tâche de droit-devant perceptif, vers la droite ou la gauche, en accord avec la direction du pointage durant la tâche. Seul le biais vers la droite (après pointage à droite) augmentait entre le 1^er^ et le 2^ème^ post test.

**Conclusion Dupierrix 2008 et 2009** = Des pointages latéralisés répétés dans le temps peuvent  influencer la perception de l'espace personnel (droit devant perceptif), péri-personnel (bissection de ligne motrice), aussi bien que extra-personnel (bissection de ligne perceptive). On observe dans ces 2 études un biais post test plus important et plus facilement détectable lorsque le pointage est dirigé vers la droite. Cela démontre l'influence du système moteur sur la cognition spatiale.

### Herlihey Black Ferber 2013

 

**Hypothèse** = Une session de pointage latéralisée, en activant le système moteur, pourrait interagir avec des fonctions perceptives elles-mêmes latéralisées. Ici on teste l’influence du LP sur la SA (réplication Dupierrix, congruent mais pas significatifs) et sur le traitement attentionnel global/focalisé (déjà influencé par la PA).

**Méthode** = design pre-test-post avec tâche de Navon = congruence/no et donner lettre globale ou focale. On mesure RTs.

**Résultats/Conclusion** = analyse séparées sur LP droite/gauche.

-   LP gauche = augmentation de l’interférence globale (augmentation des RTs quand on doit donner réponse focale + item global incongruent) = congruent avec fonction HD.
-   LD droite = diminution de l’interférence globale (RTs plus rapide sur items focaux + incongruent global) = congruent avec fonction HG.

 

### Ronchi 2011

 

**Hypothèse** = La simple observation d'erreur latéralisées dans un mouvement de pointage peut induire un effet post test dans des tâches spatiales qui évaluent la référence égocentrée. De plus cet effet serait dépendant du type d'erreur observée, constante ou dégressive.

**Méthode expe 2 (l'expe 1 était exploratoire) =** Des sujets sains observaient à la 1^ère^ personne le bras de l'expérimentateur réalisant un mouvement de pointages vers des cibles fixes. Le bras du sujet était placé juste en dessous du plan de travail expérimental où étaient réalisés les pointages. Dans un 1^er^ groupe l'expérimentateur réalisait constamment une erreur de pointage à droite de la cible, avec en moyenne le même écart (2 cm). Dans le second groupe l'expérimentateur faisait une erreur initiale plus large, puis se corrigeait au fil des pointages pour revenir progressivement à une erreur de 2 cm, comme dans le 1^er^ groupe. En pré et post-test, les sujets devaient localiser des stimuli par rapport à la position de leur corps dans l'espace (codage spatial égocentré). On retrouvait ici diverses variantes de la tâche de droit-devant perceptive : une variante classique dans la modalité proprioceptive, une variante visuelle, et une variante visuo-proprioceptive. Dans la variante visuelle les sujets devaient se prononcer dès qu'un point lumineux en mouvement atteignait leur ligne médiane corporelle. Dans la variante visuo-proprioceptive les sujets devaient pointer à l'aveugle vers un point lumineux, fixe, placé sur la ligne médiane corporelle. Un questionnaire rempli à la fin de l'expérience évaluait si les sujets avaient eu l'illusion de déplacer leur propre bras pendant la phase d'observation.

**Résultat =** On observait un biais post test vers la gauche dans la tâche de droit devant proprioceptif quand l'erreur était constante, ainsi qu'un biais post-test vers la droite dans la tâche visuo-proprioceptive quand l'erreur était dégressive. Les sujets du groupe "erreur constante" ressentaient davantage une illusion d'agentivité que dans le groupe 'erreur dégressive".

**Conclusion et limite =** L'observation répétée d'un mouvement erroné induit une plasticité du codage égocentré de l'espace. De plus cet effet n'est pas dû à des facteurs cognitifs top down (car on ne retrouvait pas de différence de résultats entre des sujets naïfs et non naïfs (connaissance ou non du paradigme utilisé).

 

 

 

### Castiello 2004

**Hypothèse** = est ce qu’une recalibration visuo-motrice vers la gauche peut améliorer la perception des objets venant de gauche ?

**Méthode** = 6 NSU vs 6 Contrôles

-   Baseline dans tâche de report (objet g/m/d ?) et de grasping (idem)
-   Training avec essai congruent et incongruent = un apparatus crée mismatch ou non entre la main réelle et virtuelle. Important pour essais vers la gauche = eg je pointe au milieu mais j j’ai un retour à gauche à nouvelle calibration car je saisis objet MAIS ne voyant main à gauche !
-   Posttest tâche de report

**Resultats** =

-   Amélioration du grasping du vrai objet vers la gauche (certains essais) pendant phase 2) était corrélé à nombre d’essais incongruent + gauche.
-   Augmentation (effet plafond) ++ des perfs dans groupe NSU dans tâche de report.

**Discussion** = Une recalibration VM (différent des prismes = décorrélation) dans espace gauche améliore la perception dans cet espace. PA = biais SM, **ici = décalage entre perception espace personnel et extrapersonnel.** Interprétation = nouvelle calibration visuo-proprioceptive engage des réseaux PAR non lésés ?

### Tsirlin 2010

**Hypothèse** = Une déconnection sensorimotrice dans un hémi-espace induirait un biais post test dans la bissection de ligne perceptive dans ce même hémi-espace. 

**Méthode** = 2 groupes de sujets sains devaient reproduire un tracé virtuel, apparaissant progressivement sur une tablette, à l'aide d'un bras robot. Quelque soit le groupe, le tracé virtuel allait soit de gauche à droite, soit de droite à gauche. Pour le groupe expérimental, le bras robot appliquait des forces aléatoires dès lors que le tracé dépassait la ligne médiane du corps du participant, de façon à ce que le feedback haptique soit perturbé dans l'hémi espace droit ou gauche. Le groupe contrôle effectuait la tâche sans perturbation. La BL était évaluée avant et après la tâche.

**Résultat** = On retrouvait un effet post test dans la BL avec un biais plus prononcé vers la gauche chez le groupe expérimental quand les sujets étaient perturbés dans l'hémi-espace droit (et donc quand la perturbation ciblait l'hémisphère gauche).

**Conclusion et limite** = On ne retrouve pas de biais vers la droite quand l'hémisphère droit est ciblé. Les auteurs expliquent cette asymétrie par le fait que les habitudes sensorimotrices d'écriture/lecture sont latéralisées à droite (écriture de gauche à droite) et auraient donc davantage été contrariées par les perturbations qui ciblaient l'hémisphère gauche. Néanmoins, d'autres explications sont possibles. De plus les groupes expérimentaux regroupaient à peine 3 ou 4 participants, ce résultat est donc peu fiable.

### Thürer, Stockinger, Putze, Schultz, & Stein 2017

 

**Hypothèse/Contexte** = effet test-retest dans tâche motrice amélioré par conditions random. Hyp que cette interférence consolide plus la mémoire motrice. Hyp frontrale = davantage de ressources cognitives pendant planification du mouvement, de feedforward, composante explicite du mouvement. Hyp pariétale = composante implicite, davantage besoin d’intégration multisensorielle, de feedback online, dans le control moteur.

**Méthode** =

-   Mesures
-   Pop = 24 (sous power)
-   On teste cette comparaison de modèle avec tâche cptale de type champ de force + EEG, et on regarde si corrélation.
-   Essai null/FF/FF channel. Channel permettait de voir counterforce du sujet après le FF.
-   On analysait PD max en début/fin de session (composante feedforward) dans FF.
-   On analysait perf dans FF channel pour mesurer adaptation du fedforward.
-   EEG = gamma et alpha band corrélées ensemble. Alpha = signifie inhibition. On regardait ces oscillation dans phase feedforward et exécution.
-   Protocole
-   Day 1 = main (group random ou non) + washout (null)
-   Day 2 = washout + main + restest washout + main transfer (tâche pointage légèrement différente).
-   Stat =

-  group * time dans main (début/fin session)

- group * time dans retest main + transfer

 

**Résultats** =

-   Sur le plan cptal, bien que adaptation dans les 2 groupes, pas d’amélioration du feedforward chez groupe random.
-   Effet test-retest plus fort chez random, avec un washout plus long. Et c’est pas dû à du feedforward. Ca va contre hyp frontale.
-   Sur le plan EEG, résultats contradictoires = **baisse alpha** **dans PAR/T > chez group random quand fin main** (donc moins d’inhibition, PAR a plus d’importance) MAIS **augmentation de l’alpha dans main retest**!
-   Corrélation avec alpha band power * (main – restest periode) PD (FF) /FFcomp (FF channel). Corr (-) avec PD, augmentation de cette corré dans le group random. Pas de corré avec FFcomp.

**Discussion** = résultats dans le sens de l’hyp PAR, effet random expliqué par plus grande intégration sensorielle, voir dorsale implicite. A noter que aug des perds dans group random pouvait être dû à feedback ou à force.

### Wei, Wert, Körding 2010

 

Hypothèse/contexte = Comment le sys nerveux s’adapte quand l’environnement change de façon aléatoire ? quelle stratégie ? selon théorie bayésienne, si incertitude constante = stratégie non spécifique.

Méthode =

-   On soumet de façon pseudo-aléa des sujets à 5 champs de force différents + 1 perturbation visuelle.
-   Chaque champ de force suivi d’un null force (expé 1) ou channel (expé 2). Avec channel on essaie d’évaluer effet de l’adaptation random sur adaptation à un champ pas random.
-   Stats = on regardait corrélation entre profil de vitesse pointage et le champ de force précédent.

**Résultats/discussion** = on a counterforce dans le sens opposé au champ de force, mais profil de pointage pas spécifique à un FF particulier expérimenté juste avant. Hyp = notre système perceptif n’arrive pas à estimer les paramètres sensoriels des perturbations random. Pas de MAJ modèle interne mêmes si amélioration des performances ! peut être due à force du bras … (**Osu 2003**)

 

 

### Burdet, Osu, Franklin, Milner, Kawato 2001

 

Hypothèse = on utilise une stratégie de « impedance geometry», une stratégie efficace et adaptée dans un ev très instable. Cette stratégie est sélective ! pas de co-contraction des muscles en permanence !

Attt = ici juste enregistrer force de la main n’est pas suffisant ! c’est différent de impedenace en fait ! Comme la rigidité des muscles augmentent avec la force, force = mauvais indicateur d’un contrôle de type impendance. . 

**Impedance def = résistance à un mouvement opposé !**

**Méthode** =

-   FF classique = null/FF random (8 choix)/washout
-   On enregistrait stiffness au niveau de la main, coude, épaules + curve pattern (AUC)

**Résultats** =

-   Les sujets s’adaptent très bien au FF random perpendiculaire, avec FF restest > à group control !
-   **On observe bien stiffness qui augmentent dans la direction opposée au FF random**
-   **Pas de corrélation entre stiffness hand et force hand.**
-   **Ce changement d’impedance était feedforward, car mesuré dans essais washout ! (implicite pour les sujets).**

** **

### Kirsch & Kunde 2015

 

**Hypothèse/contexte** = la perception se fait à l’échelle de l’action (**Profitt 2013**). Si on diminue l’amplitude d’un geste de pointage, on diminue la distance perçue entre le corps et l’objet (tâche égocentrée).

**Méthode** = pointage avec stop/no (à plusieurs distances), cibles à plusieurs distances, puis juger à chaque fois si longueur horizontale ajustée manuellement correspondait à la distance entre le corps et la cible à pointer.

**Résultats/discussion** = On avait effet du stop/no stop sur perception distance. Quand on module planif du geste (planif d’un pointage moins ample, plus lent, confirmé par l’analyse du profil de vitesse), on influe sur perception espace péri-personnel.

### Coello, Bartolo, Amiri, Devanne, Houdayer, Derambure 2008

 

Hypothèse/contexte = influence du système moteur surtout dans espace péripersonnel, assuré par circuit PAR-FR en chage de la perception du corps, traitement égocentré… à l’inverse, processus visuo-spatial dans espace extra-personnel. Hypothèse que TMS sur PM (zone grasping) va perturber la façon dont on perçoit les distances entre un objet manipulable et le corps.

Méthode = TMS dans 3 tâche = distinction gauche/droite (tâche contrôle, 0 effet sur accuracy et RT) et dire si objet attrapable ou non (en manipulant la distance), mouvement d’atteinte (contrôle pour vérifier que TMS n’affectait QUE la planif du mouvement, pas son exécution. 0 csq sur kinématique, RT)

**Résultats** = TMS avait un effet facilitateur sur RTs quand on stimulait  les zones PM et OCC, mais moins quand Motor + cible ambiguës. Pas d’effet sur frontière avec surestimation de la longueur du bras.

**Interprétation** = Inhibition sur aires motrices inhibent représentations qui participent à estimation distance objet dans espace péripersonnel. Pas que traitement visuel en jeu.

### Iachini Ruggiero Ruotolo Vinciguerra 2014

 

**Hypothèse/Contexte** = Comme Coello, espace péripersonnel ) = action space. Hypothèse que contraindre le système moteur va contraindre jugement spatial des objets. Comparaison de modèle = si les affordances sont importantes, alors la variable « type d’objet » aura un effet majeur. Sinon, c’est que la seule limite espace péri-extra personnel est suffisante pour que le système moteur participe à la localisation des objets.

Méthode = Double tâche = dire si objet à gauche/droite de la SA + **manipulable ou non** + variable **objet péri/extra** + **bras bloqué/no**. On mesure à chaque fois accuracy et RTs. On fait 2 expés avec d’abord réponse que main gauche avec main droite immobilisée dans le dos, puis après on contrebalance + objet sans poignet.

**Résultats** =

-   Expé 1 = RT plus rapide quand péri + free hand. Acc péri>, free> quand péri.
-   Expé 2 = RT > objet pas manip, free> quand péri. Acc > quand péri, Acc> non manip, free>

**Discussion** = le système moteur facilite la détection, en temps et en précision, surtout quand l’objet est présenté dans l’espace péri-personnel. Montre que le système moteur participe à un certain degré à la perception de l’espace. Peut-être que même si objet dans l’espace proche, on active des commandes motrices abstraites !

### Bourgeois & Coello 2012

 

**Contexte/hypothèse** = perception de l’espace péripersonnel est codé sur le plan moteur. Si on influence la portée de l’action, on influence la portée de l’espace péripersonnel (sa frontière avec l’espace extra-personnel). A l’appui, réseau PAR-FR impliqué dans perception objet proche + planification et exécution de mouvements. Overlap ! Alors que PAR-T plus impliqué dans perception objet loin. Congruent avec travaux tt égocentrés/allocentrés … **Bonne synthèse**

**Méthode =** 40 sujets, 4*10

-   Main cachée par un miroir, tâche de pointage dirigée vers des cibles. Juste après pointage, tâche de perception = est-ce que nouvelle cible est atteignable ? On faisait plusieurs blocs =
-   Exép 1 = Manipulation = on crée un décalage de +/- 15mm à chaque nouveau pointage. On contrôlait la variable kinématique = le sujet faisait exactement le même pointage tout le temps, mais avec retour visuel différent.
-   Expé 2 = On fait pareil mais avec un retour visuel random de +/- 7.5mm suivi aussi de jugement d’atteinte.

**Résultats** =

-   Expé 1 = les sujets dans condition shift-toward avait jugement d’atteinte plus courts. Idem pour condition shift-away. Mais pas résultats congruents dans shift away pour les 1ers essais. Hypothèse que variabilité non prédite va « paralyser » le système moteur.
-   Cette hyp est soutenue par expé 2 = quand on fait que du random, on a baisse frappante de la magnitude d’atteinte.

**Interprétation** = Quand pas de prédiction précise d’un mouvement moteur (cf simulation motrice jeannerod...) à cause de variabilité VM, dans ce cas strétgie « conservatrice » du système moteur, qui perçoit espace péripersonnel moins large.

 

